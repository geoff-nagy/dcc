#!/bin/bash

valgrind --gen-suppressions=yes --undef-value-errors=yes --expensive-definedness-checks=yes --suppressions=valgrind-suppressions.txt ./dcc-debug gui=true world=../config/worlds/simulated_autolab_sim_vicon.gum trial=../config/trials/pairwise_flocking_lab.gum

#$1 $2 $3 $4 $5 $6 $7 $8
