#include "util/model.h"
#include "util/wavefront.h"
#include "util/log.h"

#include "GL/glew.h"

#include "glm/glm.hpp"
using namespace glm;

#include <stdint.h>

Model::Model()
	: vao(0)
{ }

Model::Model(WavefrontModel *model)
{
	buildVBOs(model);
	numTriangles = model -> numTriangles;
}

Model::~Model()
{
	if(vao)
	{
		glDeleteBuffers(3, vbos);
		glDeleteVertexArrays(1, &vao);
	}
}

Model *Model::fromWavefront(WavefrontModel *model)
{
	Model *result = NULL;

	if(model != NULL)
	{
		result = new Model(model);
	}

	return result;
}

void Model::render()
{
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, numTriangles * 3);
}

void Model::buildVBOs(WavefrontModel *model)
{
	// allocate memory to contain all geometry data
	uint32_t numVertices = model -> numTriangles * 3;
	vec3 *vertices = new vec3[numVertices];
	vec3 *normals = new vec3[numVertices];
	vec2 *texCoords = new vec2[numVertices];

	WavefrontGroup *group;
	uint32_t index;
	uint32_t i, j, k;

	// loop through all groups
	index = 0;
	for(i = 0; i < model -> numGroups; ++ i)
	{
		// loop through all triangles in current group
		group = model -> groups[i];
		for(j = 0; j < group -> numTriangles; ++ j)
		{
			// loop through all vertices in current triangle
			for(k = 0; k < 3; ++ k)
			{
				vertices[index] = group -> vertices[group -> triangles[j].vertices[k]];
				normals[index] = group -> normals[group -> triangles[j].normals[k]];
				texCoords[index] = group -> texCoords[group -> triangles[j].texCoords[k]];
				index ++;
			}
		}
	}

	// create our OpenGL vertex array
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(3, vbos);

    // store vertex data
    glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * numVertices, vertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);

    // store normals data
    glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * numVertices, normals, GL_STATIC_DRAW);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);

    // store tex coords data
    glBindBuffer(GL_ARRAY_BUFFER, vbos[2]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * numVertices, texCoords, GL_STATIC_DRAW);
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);

	// free up the memory we used
	delete[] texCoords;
	delete[] normals;
	delete[] vertices;
}
