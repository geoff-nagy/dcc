#include "util/shader.h"
#include "util/loadtext.h"
#include "util/log.h"

#include "GL/glew.h"

#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
using namespace glm;

#include <fstream>
#include <cstdlib>
#include <iostream>
#include <string>
using namespace std;

// - - - public functions - - - //

Shader *Shader::fromFile(const std::string &vertFilename, const std::string &fragFilename)
{
	string vertContents = loadText(vertFilename);
	string fragContents = loadText(fragFilename);
	return fromMemory(vertFilename + " | " + fragFilename, (char*)vertContents.c_str(), vertContents.size(), (char*)fragContents.c_str(), fragContents.size());
}

Shader *Shader::fromMemory(const std::string &name, char *vertData, uint32_t vertDataLen, char *fragData, uint32_t fragDataLen)
{
	return loadShader(name, vertData, vertDataLen, fragData, fragDataLen);
}

Shader::~Shader()
{
	glDetachShader(program, vertexShader);
	glDetachShader(program, fragmentShader);
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    glDeleteProgram(program);
}

void Shader::bind()
{
	glUseProgram(program);
}

void Shader::unbind()
{
	glUseProgram(0);
}

void Shader::link()
{
	GLint result;

	// don't bother re-linking if we've done it already (which we may very well have...)
	if(!linked)
	{
		// attempt to link the program
		glLinkProgram(program);
		glGetProgramiv(program, GL_LINK_STATUS, &result);

		// see if we were successful
		if(!result)
		{
			LOG_INFO("Shader::Shader() compiled but could not be linked");
			printShaderLogInfo(vertexShader);
			printShaderLogInfo(fragmentShader);
			LOG_ERROR("");
		}

		// store result
		linked = result;
	}
}

void Shader::bindAttrib(const char *var, unsigned int index)
{
    glBindAttribLocation(program, index, var);
}

void Shader::uniform1f(const char *var, float val)
{
    glUniform1f(getUniLoc(program, var), val);
}

void Shader::uniform1i(const char *var, int val)
{
    glUniform1i(getUniLoc(program, var), val);
}

void Shader::uniform1fv(const char *var, int count, float *vals)
{
    glUniform1fv(getUniLoc(program, var), count, vals);
}

void Shader::uniform2f(const char *var, float v1, float v2)
{
    glUniform2f(getUniLoc(program, var), v1, v2);
}

void Shader::uniform2fv(const char *var, int count, float *vals)
{
    glUniform2fv(getUniLoc(program, var), count, vals);
}

void Shader::uniformVec2(const char *var, const vec2 &v)
{
    uniform2f(var, v.x, v.y);
}

void Shader::uniform3iv(const char *var, int count, int *vals)
{
    glUniform3iv(getUniLoc(program, var), count, vals);
}

void Shader::uniform3fv(const char *var, int count, float *vals)
{
    glUniform3fv(getUniLoc(program, var), count, vals);
}

void Shader::uniform3f(const char *var, const float v1, const float v2, const float v3)
{
    glUniform3f(getUniLoc(program, var), v1, v2, v3);
}

void Shader::uniformVec3(const char *var, const vec3 &v)
{
    uniform3f(var, v.x, v.y, v.z);
}

void Shader::uniformMat3(const char *var, const mat3 &mat)
{
    glUniformMatrix3fv(getUniLoc(program, var), 1, false, (GLfloat*)value_ptr(mat));
}

void Shader::uniform4iv(const char *var, int count, int *vals)
{
    glUniform4iv(getUniLoc(program, var), count, vals);
}

void Shader::uniform4fv(const char *var, int count, float *vals)
{
    glUniform4fv(getUniLoc(program, var), count, vals);
}

void Shader::uniform4f(const char *var, float v1, float v2, float v3, float v4)
{
    glUniform4f(getUniLoc(program, var), v1, v2, v3, v4);
}

void Shader::uniformVec4(const char *var, vec4 v)
{
    uniform4f(var, v.x, v.y, v.z, v.w);
}

void Shader::uniformMat4(const char *var, const mat4 &mat)
{
    glUniformMatrix4fv(getUniLoc(program, var), 1, false, (GLfloat*)value_ptr(mat));
}

// - - - private functions - - - //

Shader *Shader::loadShader(const string &name, char *vertData, uint32_t vertLen, char *fragData, uint32_t fragLen)
{
	Shader *result = new Shader(vertData, vertLen, fragData, fragLen);
	result -> name = name;

	return result;
}

Shader::Shader(char *vertData, int vertLen, char *fragData, int fragLen)
{
    GLint vertexCompiled;
    GLint fragCompiled;

	// create our separate vertex and fragment shader program
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

    // specify source code for the vertex and shader programs
	glShaderSource(vertexShader, 1, &vertData, &vertLen);
	glShaderSource(fragmentShader, 1, &fragData, &fragLen);

	// compile the vertex shader and check for errors
    glCompileShader(vertexShader);
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &vertexCompiled);
    if(!vertexCompiled)
    {
		LOG_INFO("Shader::Shader() could not compile vertex shader");
		printShaderLogInfo(vertexShader);
		LOG_ERROR("");
    }

    // compile the fragment shader and check for errors
    glCompileShader(fragmentShader);
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &fragCompiled);
    if(!fragCompiled)
    {
		LOG_INFO("Shader::Shader() could not compile fragment shader");
		printShaderLogInfo(fragmentShader);
		LOG_ERROR("");
    }

    // if we compiled successfully, bind them together into our final shader program
    // that we activate when we want to render with it
    if(vertexCompiled && fragCompiled)
    {
        program = glCreateProgram();
        glAttachShader(program, vertexShader);
        glAttachShader(program, fragmentShader);
    }

    // note that we've not linked the program yet
    linked = false;
}

GLint Shader::getUniLoc(GLuint program, const char *name)
{
    GLint loc;
    loc = glGetUniformLocation(program, name);

	if(loc == -1)
	{
		LOG_INFO("Shader::getUniLoc(): uniform \"" << name << "\" has not been defined");
	}

    return loc;
}

void Shader::printShaderLogInfo(GLuint obj)
{
    int infologLength = 0;
    int charsWritten  = 0;
    char *infoLog;

	glGetShaderiv(obj, GL_INFO_LOG_LENGTH,&infologLength);

	if (infologLength > 1)
	{
		infoLog = (char *)malloc(infologLength);
		glGetShaderInfoLog(obj, infologLength, &charsWritten, infoLog);
		LOG_INFO(infoLog);
		free(infoLog);
	}

}
