#pragma once

#include "GL/glew.h"

#include "glm/glm.hpp"

#include <string>
#include <map>

class Shader;
class TextGeom;
struct dtx_font;

class LineGraph
{
private:
	static const int NUM_TASKS;					// how many tasks we have
	static const int NUM_SAMPLES;				// how many time instances we track per task (i.e., how long in time, measured in update steps)
	static const float MAX_TIME;				// max cap on how long we think a task should take

	static const float GRAPH_WIDTH;
	static const float GRAPH_HEIGHT;

	static const float MARGIN_HORIZONTAL;
	static const float MARGIN_VERTICAL;

	static LineGraph *instance;

	LineGraph();

	struct dtx_font *font;

	glm::mat4 projection;

	Shader *backgroundShader;
	GLuint backgroundVAO;
	GLuint backgroundVBOs[1];

	Shader *lineShader;
	GLuint lineVAO;
	GLuint lineVBOs[2];

	int numTasksTracked;
	std::map<std::string, GLfloat*> taskValues;
	std::map<std::string, int> numSamples;
	std::map<std::string, TextGeom*> textGeoms;

	void setupFont();
	void setupBackground();
	void setupLines();

public:
	static LineGraph *getInstance();

	~LineGraph();

	void plot(const std::string &task, float value);

	void render();
};
