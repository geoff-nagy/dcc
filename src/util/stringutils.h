#pragma once

#include <string>

// thanks, StackOverflow!
// https://stackoverflow.com/a/874160
bool strEndsWith(const std::string &str, const std::string &end);

// get time expressed in, e.g., "1h 13m 12s"
std::string strFormatTime(int seconds);

// get time in seconds expressed in, e.g., "12:45:00:90"
std::string strFormatSeconds(float floatingPointSeconds);
