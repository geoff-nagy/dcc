#include "util/rnd.h"

#include <stdint.h>
#include <random>
using namespace std;

static mt19937_64 mt(0);

void rndSeed(uint32_t seed)
{
	mt.seed(seed);
}

uint32_t rnd()
{
	return mt();
}

uint32_t rndLinear(uint32_t minVal, uint32_t maxVal)
{
	return minVal + (rnd() % (maxVal - minVal + 1));
}

bool rndBool()
{
	return rnd() % 2 == 0;
}
