#pragma once

class PIDController
{
public:
	// this is bad encapsulation, but it makes for easier param setting/getting
	float pGain;				// gain on proportional term
	float iGain;				// gain on integral term
	float dGain;				// gain on differential term
	float iLimit;				// limit for integral term
	float oldError;				// previous error, for I and D terms
	float lastResult;			// last computed controller output
	float integral;				// integral term

	PIDController();
	PIDController(float pGain, float iGain, float dGain, float iLimit);

	// pass in error and get a result!
	float update(float error, float deltaSeconds);

	// ...or, use the previous result!
	float getLastResult();

	// reset the controller
	void reset();
};
