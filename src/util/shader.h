#pragma once

#include "GL/glew.h"

#include "glm/glm.hpp"

#include <string>

class Shader
{
public:
	static Shader *fromFile(const std::string &vertFilename, const std::string &fragFilename);
	static Shader *fromMemory(const std::string &name, char *vertData, uint32_t vertDataLen, char *fragData, uint32_t fragDataLen);

	~Shader();

	void bind();
	void unbind();
	void link();
	void bindAttrib(const char*, unsigned int);

	void uniform1i(const char*, int);
	void uniform1f(const char*, float);
	void uniform1fv(const char*, int, float*);
	void uniform2f(const char*, float, float);
	void uniform2fv(const char*, int, float*);
	void uniformVec2(const char*, const glm::vec2&);
	void uniform3iv(const char*, int, int*);
	void uniform3fv(const char*, int, float*);
	void uniform3f(const char*, const float, const float, const float);
	void uniformVec3(const char*, const glm::vec3&);
	void uniformMat3(const char*, const glm::mat3&);
	void uniform4iv(const char*, int, int*);
	void uniform4fv(const char*, int, float*);
	void uniform4f(const char*, float, float, float, float);
	void uniformVec4(const char*, glm::vec4);
	void uniformMat4(const char*, const glm::mat4&);

	GLuint fragmentShader;
	GLuint vertexShader;
	GLuint program;

private:
	Shader(char *vertData, int vertLen, char *fragData, int fragLen);

	static Shader *loadShader(const std::string &name, char *vertData, uint32_t vertLen, char *fragData, uint32_t fragLen);

	GLint getUniLoc(GLuint, const char*);
	void printShaderLogInfo(GLuint);

	std::string name;
	bool linked;
};
