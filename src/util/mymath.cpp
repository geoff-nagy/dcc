#include "util/mymath.h"

#include "glm/glm.hpp"
#include "glm/gtx/rotate_vector.hpp"
using namespace glm;

#include <vector>
using namespace std;

// given a point on a sphere, and a target position on the sphere, compute the next point on the sphere
// that you should travel to, to get inc radians closer to the target point
vec3 computeNextPointOnSphere(const vec3 &actual, const vec3 &target, float inc)
{
	vec3 rotVec = normalize(cross(actual, target));
	vec3 next = normalize(rotate(actual, inc, rotVec));
	return next;
}

// linearly accelerate a value to another value by the given step size
float accel(float actual, float target, float step)
{
	float result = target;			// default is the given target in case we're close

	// make sure step is always positive so addition/substraction makes sense
	step = fabs(step);

	// accelerate by at most step in order to make actual as close to target as we can
	if(actual < target && target - actual > step)
		result = actual + step;
	if(actual > target && actual - target > step)
		result = actual - step;

	return result;
}

// linearly accelerate a vector, element-wise, to another vector by the given step size
glm::vec3 accel(const glm::vec3 &actual, const glm::vec3 &target, const glm::vec3 &step)
{
	return vec3(accel(actual.x, target.x, step.x),
				accel(actual.y, target.y, step.y),
				accel(actual.z, target.z, step.z));
}

// adapted from:
// https://rosettacode.org/wiki/Find_the_intersection_of_a_line_with_a_plane#C.2B.2B
dvec3 linePlaneIntersect(const dvec3 &rayVector, const dvec3 &rayPoint, const dvec3 &planeNormal, const dvec3 &planePoint)
{
	dvec3 diff = rayPoint - planePoint;
	double prod1 = dot(diff, planeNormal);
	double prod2 = dot(rayVector, planeNormal);
	double prod3 = prod1 / prod2;
	return rayPoint - rayVector * prod3;
}

// interpolate between two angles
double curveAngle(double actual, double target, double tension)
{
	double diff;
	double compare;

    actual = wrapDegrees(actual);
    target = wrapDegrees(target);
    compare = wrapDegrees(actual - target);

    if(compare < 180.0)
        diff = -(compare);
    else
        diff = 360.0 - compare;

    return actual + (diff * tension);
}

// wrap degrees from 0 to 360
double wrapDegrees(double val)
{
    double result = val;

    if(val >= 360.0)
        result = val - 360.0;
    else if(val < 0.0)
        result = 360.0 + val;

    return result;
}

// adapted from: https://www.programiz.com/cpp-programming/examples/standard-deviation
void calculateMeanAndStdDev(vector<float> &data, float *avg, float *stddev)
{
    float sum;
	float mean;
    float standardDeviation;
    vector<float>::iterator i;

    // compute sum of data and its mean
    sum = 0.0f;
    for(i = data.begin(); i != data.end(); ++ i)
    {
        sum += *i;
    }
    mean = sum / (float)data.size();

    // compute std dev
    standardDeviation = 0.0f;
    for(i = data.begin(); i != data.end(); ++ i)
    {
        standardDeviation += pow(*i - mean, 2);
	}
	standardDeviation = sqrt(standardDeviation / (float)data.size());

	// return results
	*avg = mean;
	*stddev = standardDeviation;
}

// note: this is a hack to prevent linking to the GLIBC 2.27 version of libm;
// note that we're still invoking the C lib version of pow, not the GLM one
float powf(float a, float b)
{
	return std::pow((double)a, (double)b);
}

// clamp a floating-point value between two others
float clamp(float incoming, float minValue, float maxValue)
{
	if(incoming < minValue) incoming = minValue;
	if(incoming > maxValue) incoming = maxValue;
	return incoming;
}

// normalize an angle given in radians to the range -PI to PI
float normalizeRadians(float radians)
{
	while(radians < -M_PI) radians += 2.0 * M_PI;
	while(radians > M_PI) radians -= 2.0 * M_PI;
	return radians;
}

bool rayIntersectsSphere(const dvec3 &point1, const dvec3 &point2, const dvec3 &sphere, double radius, dvec3 &intersect)
{
    double a,b,c;
    double bb4ac;
    dvec3 dp;
    double mu[2];
    dvec3 intersects[2];
    dvec3 intersectOffset;
    double dist[2];
    double closestDist;
    double maxDist;

    bool result = false;

    dp.x = point2.x - point1.x;
    dp.y = point2.y - point1.y;
    dp.z = point2.z - point1.z;
    a = dp.x * dp.x + dp.y * dp.y + dp.z * dp.z;
    b = 2 * (dp.x * (point1.x - sphere.x) + dp.y * (point1.y - sphere.y) + dp.z * (point1.z - sphere.z));
    c = sphere.x * sphere.x + sphere.y * sphere.y + sphere.z * sphere.z;
    c += point1.x * point1.x + point1.y * point1.y + point1.z * point1.z;
    c -= 2 * (sphere.x * point1.x + sphere.y * point1.y + sphere.z * point1.z);
    c -= radius * radius;
    bb4ac = b * b - 4 * a * c;

    if(bb4ac >= 0.0)
    {
        mu[0] = (-b + sqrt(bb4ac)) / (2.0 * a);
        mu[1] = (-b - sqrt(bb4ac)) / (2.0 * a);

        intersects[0] = point1 + ((point2 - point1) * mu[0]);
        intersects[1] = point1 + ((point2 - point1) * mu[1]);

        dist[0] = length(point1 - intersects[0]);
        dist[1] = length(point1 - intersects[1]);
        closestDist = 0.0;

        if(dist[0] < dist[1])
        {
            closestDist = dist[0];
            intersect = intersects[0];
        }
        else
        {
            closestDist = dist[1];
            intersect = intersects[1];
        }

        intersectOffset = point1 - intersect;
        if(dot((point1 - point2), intersectOffset) > 0.0)
        {
			maxDist = length(point1 - point2);
			if(closestDist < maxDist)
			{
				result = true;
			}
		}
    }

   return result;
}

bool rayIntersectsCircle(const dvec2 &point1, const dvec2 &point2, const dvec2 &sphere, double radius, dvec2 &intersect)
{
    double a,b,c;
    double bb4ac;
    dvec2 dp;
    double mu[2];
    dvec2 intersects[2];
    dvec2 intersectOffset;
    double dist[2];
    double closestDist;
    double maxDist;

    bool result = false;

    dp.x = point2.x - point1.x;
    dp.y = point2.y - point1.y;
    a = dp.x * dp.x + dp.y * dp.y;
    b = 2 * (dp.x * (point1.x - sphere.x) + dp.y * (point1.y - sphere.y));
    c = sphere.x * sphere.x + sphere.y * sphere.y;
    c += point1.x * point1.x + point1.y * point1.y;
    c -= 2 * (sphere.x * point1.x + sphere.y * point1.y);
    c -= radius * radius;
    bb4ac = b * b - 4 * a * c;

    if(bb4ac >= 0.0)
    {
        mu[0] = (-b + sqrt(bb4ac)) / (2.0 * a);
        mu[1] = (-b - sqrt(bb4ac)) / (2.0 * a);

        intersects[0] = point1 + ((point2 - point1) * mu[0]);
        intersects[1] = point1 + ((point2 - point1) * mu[1]);

        dist[0] = length(point1 - intersects[0]);
        dist[1] = length(point1 - intersects[1]);
        closestDist = 0.0;

        if(dist[0] < dist[1])
        {
            closestDist = dist[0];
            intersect = intersects[0];
        }
        else
        {
            closestDist = dist[1];
            intersect = intersects[1];
        }

        intersectOffset = point1 - intersect;
        if(dot((point1 - point2), intersectOffset) > 0.0)
        {
			maxDist = length(point1 - point2);
			if(closestDist < maxDist)
			{
				result = true;
			}
		}
    }

   return result;
}
