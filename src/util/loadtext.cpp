#include "util/loadtext.h"
#include "util/log.h"

#include <stdio.h>
#include <string>
using namespace std;

string loadText(const string &filename)
{
	FILE *file;
	size_t len;
	char *content = NULL;
	string result;

	file = fopen(filename.c_str(), "rt");
	if(file != NULL)
	{
		fseek(file, 0, SEEK_END);
		len = ftell(file);
		rewind(file);

		content = (char*)malloc(sizeof(char) * (len + 1));
		len = fread(content, sizeof(char), len, file);
		content[len] = '\0';

		fclose(file);
		result = string(content);
		free(content);
	}
	else
	{
		LOG_ERROR("loadText() failed to open \"" << filename << "\"");
	}

	return result;
}

// thanks, StackOverflow!
// https://stackoverflow.com/a/2069241
char *sgets(char *str, int num, char **input)
{
    char *next = *input;
    int numread = 0;
    bool isNewLine;

    while(numread + 1 < num && *next)
    {
        isNewLine = (*next == '\n');
        *str++ = *next++;
        numread ++;
        // newline terminates the line but is included
        if(isNewLine)
            break;
    }

    if (numread == 0)
        return NULL;  // "eof"

    // must have hit the null terminator or end of line
    *str = '\0';  // null terminate this string

    // set up input for next call
    *input = next;
    return str;
}
