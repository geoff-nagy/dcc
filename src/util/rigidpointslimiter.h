#pragma once

#include "glm/glm.hpp"

#include <vector>

class RigidPointsLimiter
{
public:
	RigidPointsLimiter();
	RigidPointsLimiter(float tolerance);
	~RigidPointsLimiter();

	void input(std::vector<glm::vec3> &points);
	void get(std::vector<glm::vec3> &output);

private:
	float tolerance;

	std::vector<glm::vec3> lastPoints;
	std::vector<glm::vec3> filteredPoints;
};
