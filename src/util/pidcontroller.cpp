#include "util/pidcontroller.h"

PIDController::PIDController()
	: pGain(0.0),
	  iGain(0.0),
	  dGain(0.0),
	  iLimit(0.0),
	  oldError(0.0),
	  lastResult(0.0),
	  integral(0.0)
{
	// does anybody have anything to say...?
}

PIDController::PIDController(float n_pGain, float n_iGain, float n_dGain, float n_iLimit)
	: pGain(n_pGain),
	  iGain(n_iGain),
	  dGain(n_dGain),
	  iLimit(n_iLimit)
{
	reset();
}

float PIDController::update(float error, float deltaSeconds)
{
	// integral term with optional constraints
	integral += (error * deltaSeconds);
	if(iLimit != 0.0f)
	{
		if(integral > iLimit) integral = iLimit;
		if(integral < -iLimit) integral = -iLimit;
	}

	// apply:    p term            i term               d term
	lastResult = (error * pGain) + (integral * iGain) + (((error - oldError) / deltaSeconds) * dGain);

	// save old error for later
	oldError = error;

	// we're done!
	return lastResult;
}

float PIDController::getLastResult()
{
	return lastResult;
}

void PIDController::reset()
{
	oldError = 0.0;
	lastResult = 0.0;
	integral = 0.0;
}
