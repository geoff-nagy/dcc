#include "util/wavefront.h"
#include "util/loadtext.h"
#include "util/log.h"

#include "glm/glm.hpp"
using namespace glm;

#include <string.h>
#include <string>
#include <sstream>
#include <iomanip>
using namespace std;

// - - - defines - - - //

#define BUFFER_SIZE 512

// - - - WavefrontGroup implementation - - - //

WavefrontGroup::WavefrontGroup()
{
	name = "[unnamed group]";
	vertices = NULL;
	numVertices = 0;
	normals = NULL;
	numNormals = 0;
	texCoords = NULL;
	numTexCoords = 0;
	triangles = NULL;
	numTriangles = 0;
}

WavefrontGroup::~WavefrontGroup()
{
	delete[] vertices;
	delete[] normals;
	delete[] texCoords;
	delete[] triangles;
}

// - - - WavefrontModel implementation - - - //

WavefrontModel::WavefrontModel()
{
	name = "[unnamed model]";
	groups = NULL;
	numGroups = 0;
	numTriangles = 0;
}

WavefrontModel::~WavefrontModel()
{
	uint32_t i;

	if(groups)
	{
		for(i = 0; i < numGroups; ++ i)
		{
			delete groups[i];
		}
		free(groups);
	}
}

// - - - public functions - - - //

WavefrontModel *WavefrontModel::fromFile(const std::string &filename)
{
	string contents = loadText(filename);
	return fromMemory(filename, (char*)contents.c_str(), contents.size());
}

WavefrontModel *WavefrontModel::fromMemory(const std::string &name, char *data, uint32_t len)
{
	return loadOBJ(name, data, len);
}

// - - - private functions - - - //

WavefrontModel *WavefrontModel::loadOBJ(const string &name, char *data, uint32_t len)
{
	WavefrontModel *result = new WavefrontModel();
	result -> name = name;

	if(!result -> geometryAllocate(data, len))
	{
		delete result;
		return NULL;
	}

	if(!result -> geometryBuild(data, len))
	{
		delete result;
		return NULL;
	}

	return result;
}

string WavefrontModel::toString(bool verbose)
{
	uint32_t i, j, k;
	stringstream ss;

	ss << "Wavefront OBJ summary: " << endl;
	ss << "    name      : " << name << endl;
	ss << "    triangles : " << numTriangles << endl;

	// print group contents
	ss << "    groups    : " << numGroups << endl;
	for(i = 0; i < numGroups; ++ i)
	{
		ss << "        name       : " << groups[i] -> name << endl;
		ss << "        vertices   : " << groups[i] -> numVertices << endl;
		ss << "        tex coords : " << groups[i] -> numTexCoords << endl;
		ss << "        normals    : " << groups[i] -> numNormals << endl;
		ss << "        triangles  : " << groups[i] -> numTriangles << endl;

		// print triangle data
		if(verbose)
		{
			ss << "              tri        v      t      n            v      t      n            v      t      n" << endl;
			ss << "              --------------------------------------------------------------------------------" << endl;
			for(j = 0; j < groups[i] -> numTriangles; ++ j)
			{
				ss << "            " << fixed << setw(5) << j << "  | ";
				for(k = 0; k < 3; ++ k)
				{
					ss << fixed << setw(5) << groups[i] -> triangles[j].vertices[k] << "  ";
					ss << fixed << setw(5) << groups[i] -> triangles[j].texCoords[k] << "  ";
					ss << fixed << setw(5) << groups[i] -> triangles[j].normals[k];
					if(k != 2) ss << "        ";
				}
				ss << endl;
			}
		}
	}

	return ss.str();
}

// - - - private functions - - - //

bool WavefrontModel::geometryAllocate(char *data, uint32_t len)
{
	WavefrontGroup *currentGroup = NULL;
	uint32_t vIndex, tIndex, nIndex;
	uint32_t read;
	uint32_t line;
	char buffer[BUFFER_SIZE];
	char *bufferPtr;
	uint32_t i;

	// read contents of data to figure out how much space we need for the various model components
	line = 0;
	while(sgets(buffer, BUFFER_SIZE, &data) != NULL)
	{
		line ++;
		bufferPtr = buffer;

		if     (buffer[0] == '#') { }	// comment ignored; do nothing
		else if(buffer[0] == 'm') { }	// material ignored; do nothing
		else if(buffer[0] == 'u') { }	// undocumented but valid token; do nothing
		else if(buffer[0] == 's') { }	// undocumented but valid token; do nothing
		else if(buffer[0] == 'o' || buffer[0] == 'g')
		{
			// new Wavefront object or group; add to the model as a group
			currentGroup = new WavefrontGroup();
			currentGroup -> name = "";
			bufferPtr += 2; while(*bufferPtr != '\n') currentGroup -> name += (*bufferPtr++);
			groups = (WavefrontGroup**)realloc(groups, sizeof(WavefrontGroup*) * (numGroups + 1));
			groups[numGroups++] = currentGroup;
		}
		else if(buffer[0] == 'v')
		{
			// this is vertex data; is it a vertex, normal, or a texture coordinate?
			if     (buffer[1] == ' ') currentGroup -> numVertices ++;
			else if(buffer[1] == 'n') currentGroup -> numNormals ++;
			else if(buffer[1] == 't') currentGroup -> numTexCoords ++;
			else
			{
				LOG_ERROR(errorHeader(name, line) << "token '" << buffer[1] << "' after vertex token 'v' was not recognized");
				return false;
			}
		}
		else if(buffer[0] == 'f')
		{
			// make sure that texture indices were specified
			if(currentGroup -> numTexCoords == 0)
			{
				LOG_ERROR(errorHeader(name, line) << "no texture coordinates were defined");
				return false;
			}

			// this is face data; for convenience we only accept triangles since that's how we'll render them
			bufferPtr ++;
			if(strstr(bufferPtr, "//"))
			{
				LOG_ERROR(errorHeader(name, line) << "all faces must specify texture coordinate indices");
				return false;
			}
			else if(sscanf(bufferPtr, "%d/%d/%d%n", &vIndex, &tIndex, &nIndex, &read) == 3)
			{
				/* first tri corner already read above in if statement */          bufferPtr += read;
				sscanf(bufferPtr, "%d/%d/%d%n", &vIndex, &tIndex, &nIndex, &read); bufferPtr += read;
				sscanf(bufferPtr, "%d/%d/%d%n", &vIndex, &tIndex, &nIndex, &read); bufferPtr += read;
				currentGroup -> numTriangles ++;

				// make sure we don't read an additional vertex
				if(sscanf(bufferPtr, "%d/%d/%d%n", &vIndex, &tIndex, &nIndex, &read) > 0)
				{
					LOG_ERROR(errorHeader(name, line) << "all faces must be triangles");
					return false;
				}
			}
			else
			{
				LOG_ERROR(errorHeader(name, line) << "faces must be specified using format (v/t/n)");
				return false;
			}
		}
		else
		{
			LOG_ERROR(errorHeader(name, line) << "did not recognize token '" << buffer[0] << "'");
			return false;
		}
	}

	// now allocate space for the components
	for(i = 0; i < numGroups; ++ i)
	{
		currentGroup = groups[i];
		currentGroup -> vertices = new vec3[currentGroup -> numVertices];
		currentGroup -> normals = new vec3[currentGroup -> numNormals];
		currentGroup -> texCoords = new vec2[currentGroup -> numTexCoords];
		currentGroup -> triangles = new WavefrontTriangle[currentGroup -> numTriangles];

		numTriangles += currentGroup -> numTriangles;
	}

	return true;
}

bool WavefrontModel::geometryBuild(char *data, uint32_t len)
{
	WavefrontGroup *currentGroup = NULL;
	uint32_t groupIndex = 0;
	uint32_t groupVertexIndex = 0;
	uint32_t groupTexCoordIndex = 0;
	uint32_t groupNormalIndex = 0;
	uint32_t groupTriangleIndex = 0;

	vec3 value;
	uint32_t read;
	uint32_t line;
	char buffer[BUFFER_SIZE];
	char *bufferPtr;
	uint32_t i;

	line = 0;
	while(sgets(buffer, BUFFER_SIZE, &data) != NULL)
	{
		line ++;
		bufferPtr = buffer;

		if     (buffer[0] == '#') { }	// comment ignored; do nothing
		else if(buffer[0] == 'm') { }	// material ignored; do nothing
		else if(buffer[0] == 'u') { }	// undocumented but valid token; do nothing
		else if(buffer[0] == 's') { }	// undocumented but valid token; do nothing
		else if(buffer[0] == 'o' || buffer[0] == 'g')
		{
			// advance to the next group
			currentGroup = groups[groupIndex++];
			groupVertexIndex = 0;
			groupTexCoordIndex = 0;
			groupNormalIndex = 0;
			groupTriangleIndex = 0;
		}
		else if(buffer[0] == 'v')
		{
			// this is vertex data; is it a vertex, normal, or a texture coordinate?
			if(buffer[1] == ' ')
			{
				// read the vertex data
				bufferPtr += 2;
				if(sscanf(bufferPtr, "%f %f %f", &value.x, &value.y, &value.z) == 3)
				{
					currentGroup -> vertices[groupVertexIndex++] = value;
				}
				else
				{
					LOG_ERROR(errorHeader(name, line) << "all vertices must specify a 3-component vector");
					return false;
				}
			}
			else if(buffer[1] == 'n')
			{
				// store the normal
				bufferPtr += 2;
				if(sscanf(bufferPtr, "%f %f %f", &value.x, &value.y, &value.z) == 3)
				{
					currentGroup -> normals[groupNormalIndex++] = value;
				}
				else
				{
					LOG_ERROR(errorHeader(name, line) << "all normals must specify a 3-component vector");
					return false;
				}
			}
			else if(buffer[1] == 't')
			{
				// store the tex coord
				bufferPtr += 2;
				if(sscanf(bufferPtr, "%f %f", &value.x, &value.y) == 2)
				{
					currentGroup -> texCoords[groupTexCoordIndex++] = value;
				}
				else
				{
					LOG_ERROR(errorHeader(name, line) << "all texture coordinates must specify a 2-component vector");
					return false;
				}
			}
		}
		else if(buffer[0] == 'f')
		{
			bufferPtr ++;
			for(i = 0; i < 3; ++ i)
			{
				// read the vertex, tex coord, and normal indices
				sscanf(bufferPtr, "%d/%d/%d%n", &(currentGroup -> triangles[groupTriangleIndex].vertices)[i], &(currentGroup -> triangles[groupTriangleIndex].texCoords)[i], &(currentGroup -> triangles[groupTriangleIndex].normals)[i], &read);
				bufferPtr += read;

				// adjust the indices by -1 since the Wavefront format uses a 1-based indexing system
				currentGroup -> triangles[groupTriangleIndex].vertices[i] --;
				currentGroup -> triangles[groupTriangleIndex].texCoords[i] --;
				currentGroup -> triangles[groupTriangleIndex].normals[i] --;
			}

			// advance to next triangle
			groupTriangleIndex ++;
		}
		else
		{
			LOG_ERROR(errorHeader(name, line) << "did not recognize token '" << buffer[0] << "'");
			return false;
		}
	}

	return true;
}

string WavefrontModel::errorHeader(const string &filename, uint32_t line)
{
	stringstream ss;
	ss << "[" << filename << ", line " << line << "]: ";
	return ss.str();
}
