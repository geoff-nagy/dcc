#include "util/progressbar.h"

#include <cstdio>
#include <iostream>
#include <string>
using namespace std;

void printProgressBar(int percent, const string &message)
{
	int i;
	int halfPercent = percent / 2;

	cout << "\r                   [";
	for(i = 0; i <= halfPercent; i ++)
		cout << "=";

	if(halfPercent < 50)
		cout << ">";

	for(; i < 50; i ++)
		cout << " ";
	cout << "]: " << percent << "%   " << message << "\r";			// the last \r here moves the cursor to the beginning of the line
	fflush(stdout);						// print immediately
}
