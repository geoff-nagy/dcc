#pragma once

#include "glm/glm.hpp"

#include <string>

// - - - types - - - //

class WavefrontTriangle
{
public:
	uint32_t vertices[3];		// these represent indexes into the owning group's vertices, normals, and texture coordinates
	uint32_t normals[3];
	uint32_t texCoords[3];
};

class WavefrontGroup
{
public:
	WavefrontGroup();
	~WavefrontGroup();

	std::string name;

	glm::vec3 *vertices;
	uint32_t numVertices;

	glm::vec3 *normals;
	uint32_t numNormals;

	glm::vec2 *texCoords;
	uint32_t numTexCoords;

	WavefrontTriangle *triangles;
	uint32_t numTriangles;
};

class WavefrontModel
{
public:
	~WavefrontModel();

	static WavefrontModel *fromFile(const std::string &filename);
	static WavefrontModel *fromMemory(const std::string &name, char *data, uint32_t len);

	std::string toString(bool verbose = false);

	std::string name;

	WavefrontGroup **groups;
	uint32_t numGroups;
	uint32_t numTriangles;					// total within all groups

private:
	WavefrontModel();

	static WavefrontModel *loadOBJ(const std::string &filename, char *data, uint32_t len);

	bool geometryAllocate(char *data, uint32_t len);
	bool geometryBuild(char *data, uint32_t len);

	std::string errorHeader(const std::string &filename, uint32_t line);
};
