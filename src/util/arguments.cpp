#include "util/arguments.h"
#include "util/log.h"

#include <iomanip>
#include <string>
#include <vector>
#include <map>
using namespace std;

Arguments::Arguments() { }

Arguments::~Arguments() { }

void Arguments::registerFlag(const string &name)
{
	addArg(name, TYPE_FLAG, false);
}

void Arguments::registerBool(const string &name, bool required)
{
	addArg(name, TYPE_BOOL, required);
}

void Arguments::registerInt(const string &name, bool required)
{
	addArg(name, TYPE_INT, required);
}

void Arguments::registerString(const string &name, bool required)
{
	addArg(name, TYPE_STRING, required);
}

void Arguments::process(int numArgs, char *argv[])
{
	vector<Argument>::iterator j, k;
	bool found;
	int i;

	// clear old list of args
	provided.clear();
	flags.clear();
	bools.clear();
	ints.clear();
	strings.clear();

	// now read the list
	for(i = 1; i < numArgs; ++ i)
	{
		parse(argv[i]);
	}

	// make sure all required args are provided
	j = args.begin();
	while(j != args.end())
	{
		if(j -> required)
		{
			k = provided.begin();
			found = false;
			while(!found && k != provided.end())
			{
				found = j -> name.compare((k++) -> name) == 0;
			}

			// throw an error for the first missing one we find
			if(!found)
			{
				LOG_ERROR("required " << getTypeStr(j -> type) <<  " \"" << j -> name << "\" was not supplied");
			}
		}

		// advance
		++ j;
	}
}

string Arguments::toStr()
{
	vector<Argument>::iterator i;
	stringstream ss;
	uint32_t longestLen = 0;

	for(i = provided.begin(); i != provided.end(); ++ i)
	{
		if(i -> name.size() > longestLen) longestLen = i -> name.size();
	}

	for(i = provided.begin(); i != provided.end(); ++ i)
	{
		ss << setw(8) << left << getTypeStr(i -> type) << " " << setw(longestLen) << i -> name;
		if(i -> type == TYPE_BOOL) ss << " : " << (bools[i -> name] ? "true" : "false");
		else if(i -> type == TYPE_INT) ss << " : " << ints[i -> name];
		else if(i -> type == TYPE_STRING) ss << " : " << strings[i -> name];
		if(i != provided.end() - 1) ss << endl;
	}

	return ss.str();
}

string Arguments::getUsage()
{
	return "todo";
}

bool Arguments::getFlag(const string &name)
{
	bool result = false;
	if(flags.find(name) != flags.end()) result = true;
	return result;
}

bool Arguments::getBool(const string &name)
{
	bool result = false;
	if(bools.find(name) != bools.end()) result = bools[name];
	return result;
}

int Arguments::getInt(const string &name)
{
	int result = 0;
	if(ints.find(name) != ints.end()) result = ints[name];
	return result;
}

string Arguments::getString(const string &name)
{
	string result = "";
	if(strings.find(name) != strings.end()) result = strings[name];
	return result;
}

void Arguments::addArg(const string name, Type type, bool required)
{
	Argument a;
	vector<Argument>::iterator i;
	bool found;

	// see if this arg is already defined
	found = false;
	i = args.begin();
	while(!found && i < args.end())
	{
		found = (i++) -> name.compare(name) == 0;
	}

	// if already defined, throw an error
	if(found)
	{
		LOG_ERROR("switch \"" << name << "\" has already been registered");
	}

	// register the argument
	a.name = name;
	a.type = type;
	a.required = required;
	args.push_back(a);
}

void Arguments::parse(const string &arg)
{
	int count;
	Argument a;
	Type type = TYPE_FLAG;
	int equalsIndex;
	bool alreadyDefined;
	vector<Argument>::iterator j;
	string name = "UNTITLED";
	string strValue = "NULL";
	int intValue = 0;
	int i;

	// count the number of ='s
	count = 0;
	i = 0;
	while(i < (int)arg.size() && count < 2)
	{
		if(arg[i] == '=')
		{
			count ++;
			equalsIndex = i;
		}
		++ i;
	}

	// figure out how to parse based on that
	if(count == 0)
	{
		type = TYPE_FLAG;
		name = arg;
		intValue = 1;
	}
	else if(count == 1)
	{
		if(equalsIndex == 0 || equalsIndex == (int)arg.size() - 1)
		{
			syntaxError(arg);
		}
		else
		{
			// split the string into a name and a value
			name = arg.substr(0, equalsIndex);
			strValue = arg.substr(equalsIndex + 1, arg.size() - equalsIndex + 1);

			// determine what type of data we're dealing with here
			if(isNumeric(strValue))
			{
				type = TYPE_INT;
				intValue = atoi(strValue.c_str());
			}
			else if(isBool(strValue))
			{
				type = TYPE_BOOL;
				intValue = strValue.compare("true") == 0;
			}
			else
			{
				type = TYPE_STRING;
			}
		}
	}
	else
	{
		syntaxError(arg);
	}

	// make sure this argument type is allowed at all
	if(false)//!isArgAllowed(name, type))
	{
		LOG_ERROR("the " << getTypeStr(type) << " switch named \"" << name << "\" is not recognized");
	}
	else
	{
		// make sure the switch name isn't already defined
		alreadyDefined = false;
		j = provided.begin();
		while(!alreadyDefined && j != provided.end())
		{
			alreadyDefined = (j++) -> name.compare(name) == 0;
		}

		// if the switch is already defined, say so
		if(alreadyDefined)
		{
			LOG_ERROR("the switch \"" << name << "\" was defined multiple times");
		}

		// define the switch if it isn't already
		a.name = name;
		a.type = type;
		provided.push_back(a);
		if     (type == TYPE_FLAG) flags[name] = true;
		else if(type == TYPE_BOOL) bools[name] = intValue;
		else if(type == TYPE_INT) ints[name] = intValue;
		else if(type == TYPE_STRING) strings[name] = strValue;
	}
}

bool Arguments::isArgAllowed(const string &arg, Type type)
{
	bool result = false;
	vector<Argument>::iterator i = args.begin();

	while(!result && i != args.end())
	{
		result = i -> name.compare(arg) == 0 && i -> type == type;
		++ i;
	}

	return result;
}

string Arguments::getTypeStr(const Type &type)
{
	string result = "UNKNOWN";
	if     (type == TYPE_FLAG) result = "flag";
	else if(type == TYPE_BOOL) result = "bool";
	else if(type == TYPE_INT) result = "int";
	else if(type == TYPE_STRING) result = "string";
	return result;
}

void Arguments::syntaxError(const string &arg)
{
	LOG_ERROR("switch \"" << arg << "\" contains a syntax error");
}

bool Arguments::isNumeric(const string &str)
{
	bool numeric = true;
	string::const_iterator i;

	numeric = true;
	i = str.begin();
	if(*i == '-') ++ i;
	while(numeric && i != str.end())
	{
		numeric = *i >= '0' && *i <= '9';
		++ i;
	}

	return numeric;
}

bool Arguments::isBool(const string &str)
{
	string lower = toLower(str);
	return lower.compare("true") == 0 || lower.compare("false") == 0;
}

string Arguments::toLower(const string &str)
{
	string result = "";
	string::const_iterator i;
	char ch;

	for(i = str.begin(); i != str.end(); ++i)
	{
		ch = *i;
		if(ch >= 'A' && ch <= 'Z') ch += 'a' - 'A';
		result += ch;
	}

	return result;
}
