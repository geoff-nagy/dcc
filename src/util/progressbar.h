#pragma once

#include <string>

void printProgressBar(int percent, const std::string &message = "");
