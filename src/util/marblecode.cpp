#include "marblecode.h"

#include <assert.h>
#include <cmath>
#include <string>
#include <iostream>
using namespace std;

MarbleCode::MarbleCode(int bits)
	: bits(bits), ones(ceil((float)bits / 2.0f))
{
	assert(bits % 2 == 1);
	assignValue(0);
	maxValue = (((bits - ones) * ones) + 1) / 2;
}

MarbleCode::MarbleCode(int bits, int ones)
	: bits(bits), ones(ones)
{
	assert(bits % 2 == 1);
	assignValue(0);
	maxValue = (((bits - ones) * ones) + 1) / 2;
}

MarbleCode::MarbleCode(int bits, int ones, int value)
	: bits(bits), ones(ones)
{
	assert(bits % 2 == 1);
	assignValue(value);
	maxValue = (((bits - ones) * ones) + 1) / 2;
}

MarbleCode::~MarbleCode() { }

// binary shorthand operators //

MarbleCode& MarbleCode::operator += (const MarbleCode &v)
{
	operator+=(v.toDec());
	return *this;
}

MarbleCode& MarbleCode::operator -= (const MarbleCode &v)
{
	operator-=(v.toDec());
	return *this;
}

MarbleCode& MarbleCode::operator += (int v)
{
	if     (v > 0) while((v--) != 0) increment();
	else if(v < 0) while((v++) != 0) decrement();

	return *this;
}

MarbleCode& MarbleCode::operator -= (int v)
{
	if     (v > 0) while((v--) != 0) decrement();
	else if(v < 0) while((v++) != 0) increment();

	return *this;
}

// logical operators

bool MarbleCode::operator == (const MarbleCode &v) const
{
	return value.compare(v.value) == 0;
}

bool MarbleCode::operator != (const MarbleCode &v) const
{
	return !operator==(v);
}

bool MarbleCode::operator >= (const MarbleCode &v) const
{
	return toDec() >= v.toDec();
}

bool MarbleCode::operator <= (const MarbleCode &v) const
{
	return toDec() <= v.toDec();
}

bool MarbleCode::operator > (const MarbleCode &v) const
{
	return toDec() > v.toDec();
}

bool MarbleCode::operator < (const MarbleCode &v) const
{
	return toDec() < v.toDec();
}

// unary operators

MarbleCode MarbleCode::operator - () const
{
	MarbleCode result = *this;
	int i;

	for(i = 0; i < bits; ++ i)
	{
		result.value[i] = value[bits - i - 1];
	}

	return result;
}

const MarbleCode& MarbleCode::operator = (const MarbleCode &v)
{
	assignValue(v.toDec());
	return *this;
}

const MarbleCode& MarbleCode::operator = (int v)
{
	assignValue(v);
	return *this;
}

const MarbleCode& MarbleCode::operator ++ ()
{
	increment();
	return *this;
}

const MarbleCode MarbleCode::operator ++ (int)
{
	MarbleCode result = *this;
	increment();
	return result;
}

const MarbleCode& MarbleCode::operator -- ()
{
	decrement();
	return *this;
}

const MarbleCode MarbleCode::operator -- (int)
{
	MarbleCode result = *this;
	decrement();
	return result;
}

int MarbleCode::toDec() const
{
	int result = 0;
	int i;

	for(i = 0; i < bits; ++ i)
	{
		if(value[i] == '1') result += (i - (bits / 2));
	}

	return result;
}

string MarbleCode::toStr() const
{
	return value;
}

int MarbleCode::getBits()
{
	return bits;
}

int MarbleCode::getOnes()
{
	return ones;
}

int MarbleCode::getMax()
{
	return maxValue;
}

// - - - private methods - - - //

void MarbleCode::zero()
{
	int i;

	value.assign(bits, '0');

	// assign 1 to first half of ones
	for(i = 0; i < (ones / 2); ++ i) value[i] = '1';

	// assign 1 to last half of ones
	for(i = bits - 1; i >= bits - (ones / 2); -- i) value[i] = '1';

	// assign 1 to the middle bit if number of ones is odd
	if(ones % 2 == 1) value[bits / 2] = '1';
}

void MarbleCode::assignValue(int value)
{
	zero();
	operator+=(value);
}

void MarbleCode::increment()
{
	int i = bits - 1;

	// find the first zero, starting on the right
	while(i >= 0 && value[i] == '1') --i;

	// find the next one
	while(i >= 0 && value[i] == '0') --i;

	// if we found the one to move, shift it right
	if(i >= 0)
	{
		value[i] = '0';
		value[i + 1] = '1';
	}
}

void MarbleCode::decrement()
{
	int i = 0;

	// first the first zero, starting on the left
	while(i < bits && value[i] == '1') ++i;

	// find the next one
	while(i < bits && value[i] == '0') ++i;

	// if we found the one to move, shift it left
	if(i < bits)
	{
		value[i] = '0';
		value[i - 1] = '1';
	}
}
