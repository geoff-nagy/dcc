#pragma once

#include <string>
#include <vector>
#include <map>

class Arguments
{
public:
	typedef enum TYPE
	{
		TYPE_FLAG = 0,
		TYPE_BOOL,
		TYPE_INT,
		TYPE_STRING
	} Type;

	typedef struct ARGUMENT
	{
		std::string name;
		Type type;
		bool required;
	} Argument;

	Arguments();
	~Arguments();

	void registerFlag(const std::string &name);
	void registerBool(const std::string &name, bool required);
	void registerInt(const std::string &name, bool required);
	void registerString(const std::string &name, bool required);

	void process(int args, char *argv[]);
	std::string toStr();
	std::string getUsage();

	bool getFlag(const std::string &name);
	bool getBool(const std::string &name);
	int getInt(const std::string &name);
	std::string getString(const std::string &name);

private:
	void addArg(const std::string name, Type type, bool required);
	void parse(const std::string &arg);
	bool isArgAllowed(const std::string &arg, Type type);
	std::string getTypeStr(const Type &type);
	void syntaxError(const std::string &arg);
	bool isNumeric(const std::string &str);
	bool isBool(const std::string &str);
	std::string toLower(const std::string &str);

	// list of arguments we accept
	std::vector<Argument> args;

	// actual values provided by the user
	std::vector<Argument> provided;
	std::map<std::string, bool> flags;
	std::map<std::string, bool> bools;
	std::map<std::string, int> ints;
	std::map<std::string, std::string> strings;
};
