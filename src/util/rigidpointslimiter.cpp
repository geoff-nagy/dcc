#include "util/rigidpointslimiter.h"
#include "util/log.h"

#include "glm/glm.hpp"
using namespace glm;

#include <vector>
using namespace std;

RigidPointsLimiter::RigidPointsLimiter()
	: tolerance(0.0f)
{ }

RigidPointsLimiter::RigidPointsLimiter(float tolerance)
	: tolerance(tolerance)
{ }

RigidPointsLimiter::~RigidPointsLimiter() { }

void RigidPointsLimiter::input(vector<vec3> &points)
{
	vec3 point;
	float minDiff;
	float maxTolerance;
	int i;

	// just use the first set of points we get if this is our first time
	if(lastPoints.size() == 0)
	{
		lastPoints.assign(points.begin(), points.end());
		filteredPoints.assign(points.begin(), points.end());
	}
	else if(points.size() == lastPoints.size())
	{
		// see the minimum amount all points have moved across time
		minDiff = FLT_MAX;
		for(i = 0; i < (int)points.size(); ++ i)
		{
			minDiff = glm::min(minDiff, length(points[i] - lastPoints[i]));
		}

		//LOG_INFO("min diff is " << minDiff);

		// apply tolerance percentage to account for noise
		maxTolerance = minDiff + tolerance;

		// now limit motion of all other points to that amount
		filteredPoints.clear();
        for(i = 0; i < (int)points.size(); ++ i)
        {
			point = points[i];
			if(length(point - lastPoints[i]) > maxTolerance)
			{
				point = normalize(point - lastPoints[i]) * maxTolerance;
			}
			filteredPoints.push_back(point);
        }
	}
	else
	{
		LOG_INFO("RigidPointsLimiter was primed for " << lastPoints.size() << " points but received " << points.size());
	}
}

void RigidPointsLimiter::get(vector<vec3> &output)
{
	output.clear();
	output.assign(filteredPoints.begin(), filteredPoints.end());
}
