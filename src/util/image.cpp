#include "util/image.h"
#include "util/log.h"

#include "lodepng/lodepng.h"

/*
#include "opencv2/core/core.hpp"
#include "opencv2/opencv.hpp"
using namespace cv;*/

#include <stdint.h>
#include <cstdlib>
#include <cstring>
#include <iostream>
using namespace std;

Image::Image(unsigned int size, Channels channels)
{
	width = size;
	height = size;
	numPixels = width * height;

	this -> channels = channels;
	numChannels = (int)channels;

	pixels = new uint16_t[width * height * numChannels];
	memset(pixels, 0, width * height * numChannels);
}

Image::Image(unsigned int width, unsigned int height, Channels channels)
{
	this -> width = width;
	this -> height = height;
	numPixels = width * height;

	this -> channels = channels;
	numChannels = (int)channels;

	pixels = new uint16_t[width * height * numChannels];
	memset(pixels, 0, width * height * numChannels);
}

Image::Image(unsigned int width, unsigned int height, Channels channels, uint16_t *data)
{
	this -> width = width;
	this -> height = height;
	numPixels = width * height;

	this -> channels = channels;
	numChannels = (int)channels;

	pixels = new uint16_t[width * height * numChannels];
	memcpy(pixels, data, width * height * numChannels * sizeof(uint16_t));
}

Image::~Image()
{
	delete[] pixels;
}


unsigned int Image::getWidth() { return width; }
unsigned int Image::getHeight() { return height; }
unsigned int Image::getNumPixels() { return numPixels; }

uint16_t *Image::getData() { return pixels; }

void Image::getPixel(uint16_t *pixel, uint16_t *r, uint16_t *g, uint16_t *b)
{
	*r = *(pixel + 0);
	*g = *(pixel + 1);
	*b = *(pixel + 2);
}

void Image::getPixel(uint16_t *pixel, uint16_t *r, uint16_t *g, uint16_t *b, uint16_t *a)
{
	*r = *(pixel + 0);
	*g = *(pixel + 1);
	*b = *(pixel + 2);
	*a = *(pixel + 3);
}

Image::Channels Image::getChannels() { return channels; }
int Image::getNumChannels() { return numChannels; }
