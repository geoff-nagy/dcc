#include "util/stringutils.h"

#include <string>
#include <sstream>
#include <iomanip>
using namespace std;

bool strEndsWith(const string &str, const string &end)
{
    bool result = false;

    if (str.length() >= end.length())
    {
        result = (0 == str.compare (str.length() - end.length(), end.length(), end));
    }

    return result;
}

string strFormatTime(int seconds)
{
	int secs = seconds % 60;
	int mins = seconds / 60;
	int hrs = mins / 60;
	int days = hrs / 24;
	stringstream ss;

	// now apply modulus to get properly-wrapped amounts
	mins %= 60;
	hrs %= 24;
	days %= 365;

	// days, hours, and minutes are optional
	if(days) ss << days << "d ";
	if(hrs)  ss << hrs << "h ";
	if(mins) ss << mins << "m ";

	// always print seconds
	ss << secs << "s";

	return ss.str();
}

string strFormatSeconds(float fpSeconds)
{
	const int SECONDS_PER_HOUR = 3600;
	const int SECONDS_PER_MINUTE = 60;

	stringstream s;
	int hours, minutes, seconds, hundredths;

	seconds = fpSeconds;
	hundredths = ((float)fpSeconds - (float)seconds) * 100.0f;

	hours = seconds / SECONDS_PER_HOUR;
	seconds %= SECONDS_PER_HOUR;

	minutes = seconds / SECONDS_PER_MINUTE;
	seconds %= SECONDS_PER_MINUTE;

	if(hours > 0)
	{
		s << setw(2) << setfill('0') << hours << ":" << setw(2) << setfill('0') << minutes << ":" << setw(2) << setfill('0') << seconds << ":" << setw(2) << setfill('0') << hundredths;
	}
	else if(minutes > 0)
	{
		s << setw(2) << setfill('0') << minutes << ":" << setw(2) << setfill('0') << seconds << ":" << setw(2) << setfill('0') << hundredths;
	}
	else
	{
		s << setw(2) << setfill('0') << seconds << ":" << setw(2) << setfill('0') << hundredths;
	}

	return s.str();
}
