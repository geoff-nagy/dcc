#include "util/log.h"

#include <cstdlib>
#include <ctime>
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

const string Log::LOG_FILENAME = "log.txt";

ofstream Log::file;

Log::Log() { }
Log::~Log() { }

void Log::open()
{
	// if the log file isn't open, open it
	if(!file.is_open())
	{
		file.open(LOG_FILENAME.c_str());
		if(!file.is_open())
		{
			cerr << "Log::open() could not create log file " << LOG_FILENAME << "! No file logging will take place!" << endl;
		}
	}
}

void Log::close()
{
	// close the log file if it's open
	if(file.is_open())
	{
		file.close();
	}
	else
	{
		cerr << "Log::close() could not close the log file because it is currently not open!" << endl;
	}
}

void Log::output(string message)
{
	const int TIME_BUFFER_SIZE = 256;

	time_t rawtime;
	struct tm * timeinfo;
	char timeStr[TIME_BUFFER_SIZE];

	// get the current time and make it user-readable
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	strftime(timeStr, TIME_BUFFER_SIZE, "[%d-%m %I:%M:%S]: ", timeinfo);

	// echo the message to stdout
	cout << timeStr << " " << message << endl;
	//flush(stdout);				// flush the console output immediately

	// immediately print the message to the file
	if(file.is_open())
	{
		file << timeStr << message << endl;
		file.flush();			// flush the log output immediately
	}
}

void Log::info(string message)
{
	output(message);
}

void Log::error(string message)
{
	output(message + " [ABORTING]");
	close();
	exit(1);
}
