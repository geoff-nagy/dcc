#include "util/linegraph.h"
#include "util/shader.h"

#include "libdrawtext/drawtext.h"
#include "libdrawtext/drawtext_impl.h"

#include "GL/glew.h"

#include "GLFW/glfw3.h"

#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"
using namespace glm;

#include <iostream>
#include <sstream>
using namespace std;

const int LineGraph::NUM_TASKS = 10;
const int LineGraph::NUM_SAMPLES = 800;
const float LineGraph::MAX_TIME = 1.0 / 15.0;//30.0;

const float LineGraph::GRAPH_WIDTH = 1000.0;
const float LineGraph::GRAPH_HEIGHT = 200.0;

const float LineGraph::MARGIN_HORIZONTAL = 5.0;
const float LineGraph::MARGIN_VERTICAL = 5.0;

LineGraph *LineGraph::instance = NULL;

LineGraph::LineGraph()
{
	numTasksTracked = 0;
	projection = ortho(0.0, 1366.0, 0.0, 768.0);

	setupFont();
	setupBackground();
	setupLines();
}

LineGraph::~LineGraph()
{
	// todo
}

LineGraph *LineGraph::getInstance()
{
	if(instance == NULL)
	{
		instance = new LineGraph();
	}

	return instance;
}

void LineGraph::setupFont()
{
	font = dtx_open_font("ttf/white_rabbit.ttf", 12);
}

void LineGraph::setupBackground()
{
	const float TEXT_SPACE = 100.0;

	const vec2 VERTICES[] = {vec2(0.0, 0.0),
							 vec2(0.0, GRAPH_HEIGHT + (MARGIN_VERTICAL * 2.0)),
							 vec2(GRAPH_WIDTH + (MARGIN_HORIZONTAL * 2.0) + TEXT_SPACE, 0.0),
							 vec2(GRAPH_WIDTH + (MARGIN_HORIZONTAL * 2.0) + TEXT_SPACE, GRAPH_HEIGHT + (MARGIN_VERTICAL * 2.0))};

	// generate our vertex array object and buffers
	glGenVertexArrays(1, &backgroundVAO);
	glBindVertexArray(backgroundVAO);
	glGenBuffers(1, backgroundVBOs);

	// throw the vertex data onto the GPU
	glBindBuffer(GL_ARRAY_BUFFER, backgroundVBOs[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * 4, VERTICES, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);

	// set up quad rendering shader
	backgroundShader = new Shader("shaders/quad.vert", "shaders/quad.frag");
	backgroundShader -> bindAttrib("a_Vertex", 0);
	backgroundShader -> link();
	backgroundShader -> bind();
	backgroundShader -> uniformMatrix4fv("u_Projection", 1, value_ptr(projection));
	backgroundShader -> uniformVec4("u_Color", vec4(0.0, 0.0, 0.0, 0.8));
	backgroundShader -> unbind();
}

void LineGraph::setupLines()
{
	GLfloat *xCoords = new GLfloat[NUM_SAMPLES];
	int i;

	for(i = 0; i < NUM_SAMPLES; i ++)
	{
		xCoords[i] = MARGIN_HORIZONTAL + ((float)i / NUM_SAMPLES) * GRAPH_WIDTH;
	}

	// generate our vertex array object and buffers
	glGenVertexArrays(1, &lineVAO);
	glBindVertexArray(lineVAO);
	glGenBuffers(2, lineVBOs);

	// throw the vertex data onto the GPU
	glBindBuffer(GL_ARRAY_BUFFER, lineVBOs[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * NUM_SAMPLES, xCoords, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);

	// throw the vertex data onto the GPU
	glBindBuffer(GL_ARRAY_BUFFER, lineVBOs[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * NUM_SAMPLES, 0, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);

	// set up line rendering shader
	lineShader = new Shader("shaders/graphlines.vert", "shaders/graphlines.frag");
	lineShader -> bindAttrib("a_VertexX", 0);
	lineShader -> bindAttrib("a_VertexY", 1);
	lineShader -> link();
	lineShader -> bind();
	lineShader -> uniformMatrix4fv("u_Projection", 1, value_ptr(projection));
	lineShader -> unbind();

	delete[] xCoords;
}

void LineGraph::plot(const string &task, float value)
{
	stringstream ss;

	// clamp to range [0, 1]
	value = glm::clamp(value, 0.0f, 1.0f);

	// build task string with value attached
	ss << task << ": " << value;

	// allocate space for the task timing if this is a task we haven't seen before
	if(taskValues.find(task) == taskValues.end())
	{
		taskValues[task] = new GLfloat[NUM_SAMPLES];
		numSamples[task] = 0;
		textGeoms[task] = new TextGeom(ss.str(), font);
	}
	else
	{
		textGeoms[task] -> setText(ss.str());
	}

	// is the graph full already?
	if(numSamples[task] < NUM_SAMPLES)
	{
		// easy case: we're still filling up the graph
		taskValues[task][numSamples[task]++] = value * GRAPH_HEIGHT;
	}
	else
	{
		// harder case: graph is full, so ditch oldest values by moving everything over one element to the left
		memmove(&taskValues[task][0], &taskValues[task][1], sizeof(GLfloat) * NUM_SAMPLES - 1);
		taskValues[task][NUM_SAMPLES - 1] = value * GRAPH_HEIGHT;
	}
}

void LineGraph::render()
{
	const float TEXT_PADDING = 12.0;
	const vec4 COLORS[] = {vec4(0.0, 0.0, 1.0, 1.0),			// task 0: blue
						   vec4(0.0, 1.0, 0.0, 1.0),			// task 1: green
						   vec4(0.0, 1.0, 1.0, 1.0),			// task 2: cyan
						   vec4(1.0, 0.0, 0.0, 1.0),			// task 3: red
						   vec4(1.0, 0.0, 1.0, 1.0),			// task 4: purple
						   vec4(1.0, 1.0, 0.0, 1.0),			// task 5: yellow
						   vec4(1.0, 1.0, 1.0, 1.0),			// task 6: white
						   vec4(0.0, 0.0, 0.3, 1.0),			// task 7: dark blue
						   vec4(0.0, 0.3, 0.0, 1.0),			// task 8: dark green
						   vec4(0.0, 0.3, 0.3, 1.0)};			// task 9: dark cyan

	TextRenderer *text = TextRenderer::getInstance();

	map<string, GLfloat*>::iterator i;
	mat4 textModelMat;
	int j;

	// set rendering params
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// render the background
	backgroundShader -> bind();
	glBindVertexArray(backgroundVAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	// iterate through our tasks and print the times for them
	i = taskValues.begin();
	j = 0;
	while(i != taskValues.end())
	{
		lineShader -> bind();
		lineShader -> uniformVec4("u_Color", COLORS[j]);

		glBindVertexArray(lineVAO);
		glBindBuffer(GL_ARRAY_BUFFER, lineVBOs[1]);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLfloat) * numSamples[i -> first], &i -> second[0]);
		glDrawArrays(GL_LINE_STRIP, 0, numSamples[i -> first]);

		textModelMat = mat4(1.0);
		textModelMat = translate(textModelMat, vec3(TEXT_PADDING + GRAPH_WIDTH + (MARGIN_HORIZONTAL * 2.0f),
													MARGIN_VERTICAL + ((j + 1) * TEXT_PADDING),
													1.0f));

		text -> bindShader();
		text -> setColor(COLORS[j]);
		text -> setProjection(projection);
		text -> setModel(textModelMat);
		text -> render(textGeoms[i -> first]);

		i ++;
		j ++;
	}
}
