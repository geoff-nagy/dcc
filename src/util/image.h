#pragma once

#include <stdint.h>
#include <string>

class Image
{
public:
	typedef enum CHANNELS
	{
		CHANNELS_GREY = 1,
		CHANNELS_RGB = 3,
		CHANNELS_RGBA = 4
	} Channels;

	Image(unsigned int size, Channels channels);
	Image(unsigned int width, unsigned int height, Channels channels);
	Image(unsigned int width, unsigned int height, Channels channels, uint16_t *pixels);
	~Image();

	unsigned int getWidth();
	unsigned int getHeight();
	unsigned int getNumPixels();

	uint16_t *getData();

	void getPixel(uint16_t *pixel, uint16_t *r, uint16_t *g, uint16_t *b);
	void getPixel(uint16_t *pixel, uint16_t *r, uint16_t *g, uint16_t *b, uint16_t *a);

	Channels getChannels();
	int getNumChannels();

private:
	unsigned int width;
	unsigned int height;
	unsigned int numPixels;
	uint16_t *pixels;

	Channels channels;
	int numChannels;
};
