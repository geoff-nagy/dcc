#pragma once

#include <string>

class MarbleCode
{
public:
	MarbleCode(int bits);
	MarbleCode(int bits, int ones);
	MarbleCode(int bits, int ones, int value);
	~MarbleCode();

	// binary operators
	MarbleCode operator + (const MarbleCode &v) const;
	MarbleCode operator - (const MarbleCode &v) const;
	MarbleCode operator * (const MarbleCode &v) const;
	MarbleCode operator / (const MarbleCode &v) const;

	// binary shorthand operators
	MarbleCode &operator += (const MarbleCode &v);
	MarbleCode &operator -= (const MarbleCode &v);
	MarbleCode &operator *= (const MarbleCode &v);
	MarbleCode &operator /= (const MarbleCode &v);

	// binary shorthand operators
	MarbleCode &operator += (int v);
	MarbleCode &operator -= (int v);
	MarbleCode &operator *= (int v);
	MarbleCode &operator /= (int v);

	// logical operators
	bool operator == (const MarbleCode &v) const;
	bool operator != (const MarbleCode &v) const;
	bool operator >= (const MarbleCode &v) const;
	bool operator <= (const MarbleCode &v) const;
	bool operator > (const MarbleCode &v) const;
	bool operator < (const MarbleCode &v) const;

	// unary operators
	MarbleCode operator - () const;							// negation
	const MarbleCode& operator = (const MarbleCode &v);		// assignment
	const MarbleCode& operator = (int v);					// assignment
	const MarbleCode& operator ++ ();						// prefix increment
	const MarbleCode  operator ++ (int);					// postfix increment
	const MarbleCode& operator -- ();						// prefix decrement
	const MarbleCode  operator -- (int);					// postfix decrement

	// get integer value
	int toDec() const;

	// get internal representation
	std::string toStr() const;

	// get internal details
	int getBits();
	int getOnes();

	// get max value we can represent
	int getMax();

private:
	void zero();
	void assignValue(int value);
	void increment();
	void decrement();

	int bits;						// number of bits in internal representation
	int ones;						// number of bits that will always be set to 1
	int maxValue;					// maximim positive (and negative) integer value we can represent
	std::string value;				// internal string encoding of value
};
