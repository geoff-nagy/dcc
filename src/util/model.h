#pragma once

#include "GL/glew.h"

#include <stdint.h>

class WavefrontModel;

class Model
{
public:
	static Model *fromWavefront(WavefrontModel *model);

	~Model();

	void render();

	GLuint vao;
	GLuint vbos[3];
	uint32_t numTriangles;

private:
	Model();
	Model(WavefrontModel *model);

	void buildVBOs(WavefrontModel *model);
};
