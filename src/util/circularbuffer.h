#pragma once

#include "glm/glm.hpp"

// this is a circular buffer class which only computes a filtered output
// using the averages of values actually contained in the buffer, meaning
// that the filtered output is still somewhat accurate as it initially
// gets more full

class CircularBuffer
{
public:
	CircularBuffer();
	CircularBuffer(uint8_t bufferSize);

	void addEntry(const glm::vec3 &value);
	glm::vec3 getFilteredOutput();

private:
	glm::vec3 *buffer;
	uint8_t bufferSize;
	uint8_t bufferIndex;
	uint8_t numEntries;

	glm::vec3 filteredOutput;
};
