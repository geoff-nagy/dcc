#include "circularbuffer.h"

#include "glm/glm.hpp"
using namespace glm;

CircularBuffer::CircularBuffer()
{
	buffer = NULL;
	bufferSize = 0;

	bufferIndex = 0;
	numEntries = 0;
}

CircularBuffer::CircularBuffer(uint8_t bufferSize)
{
	this -> bufferSize = bufferSize;
	buffer = (vec3*)malloc(sizeof(vec3) * bufferSize);

	bufferIndex = 0;
	numEntries = 0;
}

void CircularBuffer::addEntry(const vec3 &value)
{
	vec3 bufferSum;
	uint16_t i;

	// make sure we actually have a buffer!
	if(bufferSize)
	{
		// track how many entries are currently in the buffer
		numEntries ++;
		if(numEntries >= bufferSize)
		{
			numEntries = bufferSize;
		}

		// insert the value into the buffer
		buffer[bufferIndex++] = value;
		if(bufferIndex >= bufferSize)
			bufferIndex = 0;

		// sum the buffer values we have for an average
		for(i = 0; i < numEntries; ++ i)
		{
			bufferSum += buffer[i];
		}

		// compute the new mean
		filteredOutput = bufferSum / (float)numEntries;
	}
}

vec3 CircularBuffer::getFilteredOutput()
{
	return filteredOutput;
}
