#pragma once

#include "glm/glm.hpp"

#include <vector>

// given a point on a sphere, and a target position on the sphere, compute the next point on the sphere
// that you should travel to, to get inc radians closer to the target point
glm::vec3 computeNextPointOnSphere(const glm::vec3 &actual, const glm::vec3 &target, float inc);

// linearly accelerate a value to another value by the given step size (which *must* be positive---
// accel() will add or substract accordingly)
float accel(float actual, float target, float step);

// linearly accelerate a vector, element-wise, to another vector by the given step size
glm::vec3 accel(const glm::vec3 &actual, const glm::vec3 &target, const glm::vec3 &step);

// adapted from:
// https://rosettacode.org/wiki/Find_the_intersection_of_a_line_with_a_plane#C.2B.2B
glm::dvec3 linePlaneIntersect(const glm::dvec3 &rayVector, const glm::dvec3 &rayPoint, const glm::dvec3 &planeNormal, const glm::dvec3 &planePoint);

// interpolate between two angles
double curveAngle(double actual, double target, double tension);

// wrap degrees from 0 to 360
double wrapDegrees(double val);

// adapted from: https://www.programiz.com/cpp-programming/examples/standard-deviation
void calculateMeanAndStdDev(std::vector<float> &data, float *avg, float *stddev);

// note: this is a hack to prevent linking to the GLIBC 2.27 version of libm
float powf(float, float);

// clamp a floating-point value between two others
float clamp(float incoming, float minValue, float maxValue);

// normalize an angle given in radians to the range -PI to PI
float normalizeRadians(float radians);

// test to see if a ray going between two points intersects a given sphere, and where, if so
bool rayIntersectsSphere(const glm::dvec3 &point1, const glm::dvec3 &point2, const glm::dvec3 &sphere, double radius, glm::dvec3 &intersect);

// same as above, only in 2D
bool rayIntersectsCircle(const glm::dvec2 &point1, const glm::dvec2 &point2, const glm::dvec2 &sphere, double radius, glm::dvec2 &intersect);
