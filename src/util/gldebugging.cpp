#include "util/gldebugging.h"
#include "util/log.h"

#include <GL/glew.h>

#include <assert.h>
#ifdef USING_WINDOWS
	#include <windows.h>
#endif

#include <string.h>
#include <sstream>
#include <iostream>
using namespace std;

// thanks to:
// https://blog.nobel-joergensen.com/2013/02/17/debugging-opengl-part-2-using-gldebugmessagecallback/

#ifdef USING_WINDOWS
static void APIENTRY openglCallbackFunction(GLenum source,
#else
static void openglCallbackFunction(GLenum source,
#endif
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	const void* userParam)
{
	stringstream ss;
	string trimmed = string(message).substr(0, strlen(message) - 1);

	// we don't care about informational messages; only warnings or errors
	if (type != GL_DEBUG_TYPE_OTHER)
	{
		ss << "[GL ";
		switch (type)
		{
		case GL_DEBUG_TYPE_ERROR:
			ss << "ERROR";
			break;
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
			ss << "DEPRECATION NOTE";
			break;
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
			ss << "UNDEFINED BEHAVIOR";
			break;
		case GL_DEBUG_TYPE_PORTABILITY:
			ss << "PORTABILITY NOTE";
			break;
		case GL_DEBUG_TYPE_PERFORMANCE:
			ss << "PERFORMANCE NOTE";
			break;
		case GL_DEBUG_TYPE_OTHER:
			ss << "DEBUG NOTE";
			break;
		}
		ss << ", CODE " << id << "]: ";
		ss << trimmed << " ";

		switch (severity)
		{
		case GL_DEBUG_SEVERITY_LOW:
			ss << "(low severity)";
			break;
		case GL_DEBUG_SEVERITY_MEDIUM:
			ss << "(med severity)";
			break;
		case GL_DEBUG_SEVERITY_HIGH:
			ss << "(severe)";
			break;
		}
		//LOG_INFO(ss.str());
	}
}

void initGLDebugger()
{
	GLuint unusedIds = 0;

	// make sure message callback debugging is supported
	if(glDebugMessageCallback)
	{
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(openglCallbackFunction, NULL);
        glDebugMessageControl(GL_DONT_CARE,
							  GL_DONT_CARE,
							  GL_DONT_CARE,
							  0,
							  &unusedIds,
							  true);
    }
    else
    {
        cout << "-- GL callback debugging is not available" << endl;
	}
}
