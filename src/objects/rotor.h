#pragma once

#include "glm/glm.hpp"

class Rotor
{
public:
	Rotor(const glm::vec3 &pos, float maxImpulse);
	~Rotor();

	glm::vec3 pos;
	float maxImpulse;
	float speed;
};
