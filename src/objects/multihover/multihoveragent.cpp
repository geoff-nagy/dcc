#include "objects/multihover/multihoveragent.h"

#include "objects/droneflightcontroller.h"
#include "objects/droneinterface.h"

#include "sensing/dronedetection.h"

#include "util/log.h"

#include "glm/glm.hpp"
using namespace glm;

MultiHoverAgent::MultiHoverAgent(DroneFlightController *flightController, const DroneDetection &detection, float targetHeight)
	: flightController(flightController), state(STATE_LANDED), targetHeight(targetHeight), landingBlindTimer(0.0f)
{
	// prime IDs and our first detection
	setDroneDetection(detection);
	id = detection.id;

	// set default flight command
	flightCommand.id = 0;
	flightCommand.thrust = 0.0f;
	flightCommand.roll = 0.0f;
	flightCommand.pitch = 0.0f;
	flightCommand.yaw = 0.0f;
}

MultiHoverAgent::~MultiHoverAgent()
{
	delete flightController;
}

void MultiHoverAgent::setDroneDetection(const DroneDetection &detection)
{
	this -> detection = detection;
	detectionAge = 0.0f;
}

FlightCommand MultiHoverAgent::getFlightCommand()
{
	return flightCommand;
}

bool MultiHoverAgent::isLanded()
{
	return state == STATE_LANDED;
}

int MultiHoverAgent::getID()
{
	return id;
}

vec3 MultiHoverAgent::getTargetPosition()
{
	return targetPosition;
}

void MultiHoverAgent::takeOff()
{
	if(state == STATE_LANDED)
	{
		state = STATE_TAKE_OFF;
		autoTimer = 0.0f;
	}
}

void MultiHoverAgent::landSafely()
{
	if(state != STATE_LANDING_BLIND)
	{
		state = STATE_LANDING_SAFE;
	}
}

void MultiHoverAgent::scram()
{
	LOG_INFO("    [MHVA]: MultiHoverAgent " << id << " is SCRAMing");
	state = STATE_LANDED;
}

void MultiHoverAgent::update(float dt)
{
	const float MAX_DETECTION_AGE_SEC = 3.0f;
	const float LANDING_BLIND_TIME = 3.0f;

	// age our detection; is it up to date?
	detectionAge += dt;
	if(detectionAge >= MAX_DETECTION_AGE_SEC || detection.age >= MAX_DETECTION_AGE_SEC)
	{
		// land blindly if we've not already tried to do so
		if(state != STATE_LANDING_BLIND && state != STATE_LANDED)
		{
			// start timer for blind landing
			state = STATE_LANDING_BLIND;
			landingBlindTimer = LANDING_BLIND_TIME;

			// notify the user
			LOG_INFO("    [MHVA]: MultiHoverAgent " << id << " is going to land blind for safety ***");
		}
	}

	// update based on state
	if     (state == STATE_LANDED) updateStateLanded(dt);
	else if(state == STATE_TAKE_OFF) updateStateTakeOff(dt);
	else if(state == STATE_GAINING_HEIGHT) updateStateGainingHeight(dt);
	else if(state == STATE_FLYING) updateStateFlying(dt);
	else if(state == STATE_LANDING_SAFE) updateStateLandingSafe(dt);
	else if(state == STATE_LANDING_BLIND) updateStateLandingBlind(dt);
}

void MultiHoverAgent::updateStateLanded(float dt)
{
	// nullify flight command
	flightCommand.id = id;
	flightCommand.thrust = 0.0f;
	flightCommand.roll = 0.0f;
	flightCommand.pitch = 0.0f;
	flightCommand.yaw = 0.0f;
}

void MultiHoverAgent::updateStateTakeOff(float dt)
{
	const float MAX_TAKEOFF_SPEED = 1.5f;

	autoTimer += dt;

	//if(autoTimer >= 10.0f)
	{
		// target a position directly above
		targetPosition = vec3(detection.pos.x, targetHeight, detection.pos.z);
		flightController -> setTargetPosition(targetPosition);
		flightController -> setMaxTargetYSpeed(MAX_TAKEOFF_SPEED);				// take off slower and in control

		// transition to gaining height stage
		state = STATE_GAINING_HEIGHT;

		// notify user
		LOG_INFO("    [MHVA]: MultiHoverAgent " << id << " is taking off");
	}
}

void MultiHoverAgent::updateStateGainingHeight(float dt)
{
	const float OFF_GROUND_HEIGHT = targetHeight - 0.1f;

	// if we're high enough off the ground, go to flying state
	if(detection.pos.y >= OFF_GROUND_HEIGHT)
	{
		// go to normal flying state
		state = STATE_FLYING;
		autoTimer = 0.0f;

		// notify user
		LOG_INFO("    [MHVA]: MultiHoverAgent " << id << " has reached minimum flight height");
	}

	// control flight
    updateFlightControl(dt);
}

void MultiHoverAgent::updateStateFlying(float dt)
{
	// nothing special happens here
	updateFlightControl(dt);
/*
	autoTimer += dt;
	if(autoTimer >= 5.0f)
	{
		landSafely();
	}*/
}

void MultiHoverAgent::updateStateLandingSafe(float dt)
{
	const float LANDING_HEIGHT = 0.05f;
	const float LANDING_HEIGHT_TOLERANCE = 0.03f;
	const float LANDING_SPEED = 0.1f;

	// move to ground
	flightController -> setTargetPosition(vec3(targetPosition.x, LANDING_HEIGHT, targetPosition.z));
	flightController -> setMaxTargetYSpeed(LANDING_SPEED);			// land slow

	// if we're low enough, cut throttle
	if(detection.pos.y <= LANDING_HEIGHT + LANDING_HEIGHT_TOLERANCE)
	{
		// land and notify user
		state = STATE_LANDED;
		LOG_INFO("    [MHVA]: MultiHoverAgent " << id << " has landed safely");
	}

	// control flight
	updateFlightControl(dt);
}

void MultiHoverAgent::updateStateLandingBlind(float dt)
{
	const float BLIND_LANDING_THROTTLE = 0.4f;

	// do not control flight; just set flight command to something approximating a gentle landing for a fixed time
	landingBlindTimer -= dt;
	if(landingBlindTimer <= 0.0f)
	{
		// this will cut throttle
		state = STATE_LANDED;
		LOG_INFO("    [MHVA]: MultiHoverAgent " << id << " has completed emergency landing");
	}
	else
	{
		// manually set to a gentle-ish landing
		flightCommand.id = id;
		flightCommand.thrust = BLIND_LANDING_THROTTLE;
		flightCommand.roll = 0.0f;
		flightCommand.pitch = 0.0f;
		flightCommand.yaw = 0.0f;
	}
}

void MultiHoverAgent::updateFlightControl(float dt)
{
	const float MAX_TARGET_XZ_SPEED = 0.4f;
	const float MAX_TARGET_Y_SPEED = 0.4f;
	const float MAX_YAW_SPEED = 1.0f;
	const float TARGET_HEADING = 0.0f;

    // set flight parameters
	flightController -> setMaxTargetXZSpeed(MAX_TARGET_XZ_SPEED);
	flightController -> setMaxTargetYSpeed(MAX_TARGET_Y_SPEED);
	flightController -> setMaxTargetYawSpeed(MAX_YAW_SPEED);
	flightController -> setTargetHeading(TARGET_HEADING);

	// update flight controller and receive instructions
	flightController -> update(detection, dt);
	flightCommand = flightController -> getLastFlightCommand();
	flightCommand.id = id;
}
