#pragma once

#include "sensing/dronedetection.h"

#include "objects/droneinterface.h"

#include "util/circularbuffer.h"

class DroneFlightController;

class MultiHoverAgent
{
public:
	typedef enum STATE
	{
		STATE_LANDED = 0,
		STATE_TAKE_OFF,
		STATE_GAINING_HEIGHT,
		STATE_FLYING,
		STATE_LANDING_SAFE,
		STATE_LANDING_BLIND
	} State;

	MultiHoverAgent(DroneFlightController *flightController, const DroneDetection &detection, float targetHeight);
	~MultiHoverAgent();

	// used by higher-level logic to update the detection of the drone
	void setDroneDetection(const DroneDetection &detection);

	// used by higher-level logic to command the drone itself (simulated or real)
	FlightCommand getFlightCommand();
	bool isLanded();
	int getID();
	glm::vec3 getTargetPosition();

	// various self-explanatory commands
	void takeOff();
	void landSafely();
	void scram();

	// called to update the agent behaviours
	void update(float dt);

private:
	// state update methods
	void updateStateLanded(float dt);
	void updateStateTakeOff(float dt);
	void updateStateGainingHeight(float dt);
	void updateStateFlying(float dt);
	void updateStateLandingSafe(float dt);
	void updateStateLandingBlind(float dt);

	// flight control methods
	void updateFlightControl(float dt);

	// drone control
	int id;
	DroneFlightController *flightController;
	State state;
	DroneDetection detection;
	FlightCommand flightCommand;
	float detectionAge;
	float targetHeight;
	float landingBlindTimer;
	glm::vec3 targetPosition;

	float autoTimer;
};
