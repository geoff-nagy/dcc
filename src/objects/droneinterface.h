#pragma once

#include <map>
#include <vector>

#include "sensing/dronedetection.h"

// represents information that is sent to a drone
typedef struct FLIGHT_COMMAND
{
	uint32_t id;
	float thrust;
	float roll;
	float pitch;
	float yaw;
} FlightCommand;

// this is an abstract class that issues flight commands to drones;
// in order to control simulated or real drones, you'll need to inherit this class
class DroneInterface
{
public:
	DroneInterface();
	virtual ~DroneInterface() = 0;

	// set a flight command for a specific ID
	void addCommand(FlightCommand flightCommand);
	void addCommand(uint32_t id, float thrust, float roll, float pitch, float yaw);

	// delete a flight command for a specific ID
	void clearCommand(uint32_t id);

	// delete all flight commands
	void clearCommands();

	// send the flight commands; this must be implemented by super classes
	virtual void sendCommands(float time) = 0;

	// send a ping command
	virtual void ping(uint32_t id) = 0;

	// retrieve flight command list
	std::map<uint32_t, FlightCommand>::iterator getBegin();
	std::map<uint32_t, FlightCommand>::iterator getEnd();
	uint32_t getNumCommands();

private:
	std::map<uint32_t, FlightCommand> commands;
};
