#include "objects/obstacle.h"

#include "glm/glm.hpp"
using namespace glm;

Obstacle::Obstacle(const vec3 &pos, float radius, float avoidRadius)
	: pos(pos), radius(radius), avoidRadius(avoidRadius), obscuring(false)
{
	// does anyone have anything to say...?
}

Obstacle::~Obstacle() { }
