#pragma once

#include "experiments/pairwiseflocking/pairwiseflockingneighbourentry.h"

#include "objects/obstacle.h"

#include "sensing/dronedetection.h"

#include "util/pidcontroller.h"

#include "glm/glm.hpp"

#include <set>
#include <vector>

class Obstacle;

class PairwiseFlockingAgent
{
public:
	typedef enum AGENT_STATE
	{
		STATE_LANDED = 0,
		STATE_TAKING_OFF,
		STATE_WAITING_TO_FLY,
		STATE_FLYING,
		STATE_LANDING,
		STATE_EMERGENCY_STOP
	} AgentState;

	typedef enum FLOCKING_METRIC
	{
		METRIC_EUCLIDEAN_DISTANCE = 0,
		METRIC_TOPOLOGICAL_DISTANCE
	} FlockingMetric;

	static const double CATCH_UP_DISTANCE;

	static void resetIDs();

	PairwiseFlockingAgent(uint32_t id, float viewRange, bool closePairingEnabled);

	uint32_t getID();

	void setPartnerID(int partnerID);
	int getPartnerID();
	bool getCatchingUpToPartner();
	bool getCanSeePartner();

	void setInteractionsEnabled(bool interactionsEnabled);
	bool getInteractionsEnabled();

	void setHeight(float height);
	void setPos(const glm::vec3 &pos);
	void setVelocity(const glm::vec3 &velocity);
	glm::vec3 getVelocity();

	void setBounds(const glm::vec2 &boundsArea, float boundsEdge);
	bool isCloseToBounds();

	void renderDetections();
	void renderData();

	void clearObstacles();
	void storeObstacle(const Obstacle &obstacle);

	void clearNeighbours();
	void storeNeighbour(const DroneDetection &neighbour);
	void sortAndLimitNeighbours(int numTrackedIfSingle, int numTrackedIfPaired);

	void update(float dt);

	std::vector<DroneDetection>::iterator getNeighboursBegin();
	std::vector<DroneDetection>::iterator getNeighboursEnd();

	//void setMaxXZVelocity(float maxXZVelocity);
	//void setMAXYVelocity(float maxYVelocity();

	//void setTargetHeight(float targetHeight, float maxYVelocity);

	void setTargetHeight(float targetHeight, float targetYVelocity);
	//void setTargetVelocity(const glm::vec3 &targetVelocity);

	//float getHeightPIDResult();
	float getVelocityXPIDResult();
	float getVelocityYPIDResult();
	float getVelocityZPIDResult();

private:
	static const float TURN_RATE;						// how fast the agent can rotate in place, in radians; this combined with our FOV limits how fast the agent can move
	static const float TURN_CYCLE_TIME;					// total time (in sim steps) to complete one full rotation

	static const float SAFE_ZONE_BEGIN;					// pixel distance at which neighbours exert no force
	static const float SAFE_ZONE_END;					// pixel distance at which neighbours begin exterting an attractive force
	static const float RANGE_END;						// pixel distance at which neighbours are no longer considered

	static const float MAX_VELOCITY;					// determined by TURN_RATE and FOV_RADIANS

	uint32_t id;										// our global ID
	float height;										// height from ground (altitude)
	glm::vec3 pos;										// in world space
	glm::vec3 velocity;									// in local space
	float dir;											// in world space, in radians

	AgentState state;									// what this agent is doing (sitting, flying, landing, etc.)

	float viewRange;									// maximum range at which we can see other neighbours, in meters
	bool closePairing;									// do we snuggle up close to our mate?

	bool boundsEnabled;									// off by default
	glm::vec2 boundsArea;								// area we don't want to fly out of
	float boundsEdge;									// distance from bounds at which we start to experience a repulsive force
	bool closeToBounds;

	int numAttractions;									// attraction force control
	glm::vec2 attractVec;

	int numRepulsions;									// repulsion force control
	glm::vec2 repulseVec;

	int numAlignments;									// alignment force control
	glm::vec2 alignVec;

	int numAvoidances;									// steering force away from obstacles
	glm::vec2 avoidVec;

	int numWallForces;
	glm::vec2 wallVec;

	float obstacleForce;

	float targetHeight;
	float targetYVelocity;
	PIDController heightPID;

	float baseThrust;
	float minThrust;

	float maxXZVelocity;
	glm::vec2 targetXZVelocity;
	PIDController velocityXPID;
	PIDController velocityYPID;
	PIDController velocityZPID;

	bool interactionsEnabled;							// do we flock and avoid obstacles?
	int partnerID;										// or -1 if no partner
	bool canSeePartner;									// is our partner visible/in range?
	float partnerDist;									// distance to partner, if we have one
	bool catchingUpToPartner;							// are we catching up to our partner because we're too separated?
	std::vector<DroneDetection> neighbours;				// other interaction partners we may want to consider
	std::vector<Obstacle> obstacles;					// obstacles we've detected

	// - - - methods - - - //

	void setupPIDControllers();

	void resetInteractions();
	void interactWith(const DroneDetection &agent, int index);
	void interactWith(const Obstacle &obj);
	void keepInBounds();
};
