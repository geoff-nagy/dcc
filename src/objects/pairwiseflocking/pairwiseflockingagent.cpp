#include "objects/pairwiseflocking/pairwiseflockingagent.h"
#include "objects/obstacle.h"

#include "util/mymath.h"
#include "util/pidcontroller.h"
#include "util/log.h"

#include "glm/glm.hpp"
#include "glm/gtc/random.hpp"
#include "glm/gtx/norm.hpp"
using namespace glm;

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

// - - - prototypes - - - //

// this is used to sort neighbours by distance that we're tracking
static bool sortNeighboursByDistance(const DroneDetection &a, const DroneDetection &b);

// - - - constants - - - //

const double PairwiseFlockingAgent::CATCH_UP_DISTANCE = 0.5f;//80.0f;//0.5f;

const float PairwiseFlockingAgent::MAX_VELOCITY = 0.4f;//(((M_PI * 2.0f) - FOV_RADIANS) / TURN_RATE);// * 0.5f;

// - - - implementation - - - //

PairwiseFlockingAgent::PairwiseFlockingAgent(uint32_t id, float viewRange, bool closePairing)
	: id(id), viewRange(viewRange), closePairing(closePairing)
{
	partnerID = -1;
	canSeePartner = false;
	interactionsEnabled = false;
	catchingUpToPartner = false;

	setupPIDControllers();

	height = 0.0f;
	targetHeight = 0.0f;
	targetYVelocity = 0.0f;

	maxXZVelocity = 0.3f;

	boundsEnabled = false;
	boundsEdge = 0.0f;
	closeToBounds = false;

	state = STATE_LANDED;
}

void PairwiseFlockingAgent::setupPIDControllers()
{
	// we only need a PID controller for Y position (altitude)
	heightPID = PIDController(3.0f, 0.3f, 0.0f, 2.0f);

	// set PID gains for velocity
	velocityXPID = PIDController(0.5f, 0.0f, 0.06f, 0.0f);
	velocityYPID = PIDController(0.6f, 0.0f, 0.04f, 0.0f);
	velocityZPID = PIDController(0.5f, 0.0f, 0.06f, 0.0f);
}

uint32_t PairwiseFlockingAgent::getID()
{
	return id;
}

void PairwiseFlockingAgent::setPartnerID(int id)
{
	partnerID = id;
}

int PairwiseFlockingAgent::getPartnerID()
{
	return partnerID;
}

bool PairwiseFlockingAgent::getCatchingUpToPartner()
{
	return catchingUpToPartner;
}

bool PairwiseFlockingAgent::getCanSeePartner()
{
	return canSeePartner;
}

void PairwiseFlockingAgent::setInteractionsEnabled(bool interactionsEnabled)
{
	this -> interactionsEnabled = interactionsEnabled;
}

bool PairwiseFlockingAgent::getInteractionsEnabled()
{
	return interactionsEnabled;
}

void PairwiseFlockingAgent::setHeight(float height)
{
	this -> height = height;
}

void PairwiseFlockingAgent::setPos(const vec3 &pos)
{
	this -> pos = pos;
}

void PairwiseFlockingAgent::setVelocity(const glm::vec3 &velocity)
{
	this -> velocity = velocity;
}

vec3 PairwiseFlockingAgent::getVelocity()
{
	return velocity;
}

void PairwiseFlockingAgent::setBounds(const vec2 &boundsArea, float boundsEdge)
{
	boundsEnabled = true;
	this -> boundsArea = boundsArea;
	this -> boundsEdge = boundsEdge;
}

bool PairwiseFlockingAgent::isCloseToBounds()
{
	return closeToBounds;
}

void PairwiseFlockingAgent::clearObstacles()
{
	obstacles.clear();
}

void PairwiseFlockingAgent::storeObstacle(const Obstacle &obj)
{
	obstacles.push_back(obj);
}

void PairwiseFlockingAgent::clearNeighbours()
{
	neighbours.clear();
	canSeePartner = false;
}

void PairwiseFlockingAgent::storeNeighbour(const DroneDetection &other)
{
	neighbours.push_back(other);
	if(partnerID == (int)other.id)
	{
		canSeePartner = true;
		partnerDist = other.dist;
	}
}

void PairwiseFlockingAgent::sortAndLimitNeighbours(int numTrackedIfSingle, int numTrackedIfPaired)
{
	const int NUM_NEIGHBOURS = (partnerID >= 0 && canSeePartner ? numTrackedIfPaired : numTrackedIfSingle);

	DroneDetection partner;
	vector<DroneDetection>::iterator i;
	int j;

	catchingUpToPartner = false;

	// remove the partner entry if we have one
	if(partnerID >= 0 && canSeePartner)
	{
		// store for re-insertion at the front of the list later on
		partner = neighbours[0];
		if((int)partner.id != partnerID)
		{
			// make sure this is actually our partner!
			LOG_INFO("this is not correct; " << id << "'s partnerID is " << partnerID << " but read partner.id is " << partner.id);
			LOG_INFO("neighbours.size(): " << neighbours.size());
		}
		neighbours.erase(neighbours.begin(), neighbours.begin() + 1);

		// if we're going to lose sight of our partner, ignore everyone else
		if(partnerDist > CATCH_UP_DISTANCE)
		{
			// [disabled for now]
			//neighbours.clear();
			//catchingUpToPartner = true;
		}
	}

	// sort the neighbours by their distance from us; note that we have to use sortNeighboursByDistance() since the DroneDetection
	// less-than operator is already defined and that we sort in NON-DESCENDING ORDER so when we get rid of the last N
	// neigbours in the code block below, we're left with the closest ones
	sort(neighbours.begin(), neighbours.end(), sortNeighboursByDistance);

	// erase all but the closest neighbours
	for(j = 0, i = neighbours.begin(); j < NUM_NEIGHBOURS && i != neighbours.end(); ++ j, ++ i);		// skip over NUM_NEIGHBOURS entries; these are closest to us
	neighbours.erase(i, neighbours.end());																// erase remaining entries

	// re-add our partner at the beginning of the list
	if(partnerID >= 0 && canSeePartner)
	{
		neighbours.insert(neighbours.begin(), partner);
	}
}

void PairwiseFlockingAgent::update(float dt)
{
	/*
	const float ATTRACT_GAIN = 0.1f;//0.2f;//1.0f;//0.2f;
	const float REPEL_GAIN = 10.0f;//10.0f;//5.0f;//10.0f;//30.0f;//30.0f;
	const float ALIGN_GAIN = 10.0f;//5.0f;//5.0f;//2.5f;//5.0f;//10.0f;
	const float AVOID_GAIN = 0.2f;//5.0f;//5.0f;//5.0f;
	const float WALL_FORCE_GAIN = 5.0f;
	*/

	// social forces gains
	const float ATTRACT_GAIN = 0.1f;
	const float REPEL_GAIN = 0.2f;
	const float ALIGN_GAIN = 0.1f;
	const float AVOID_GAIN = 0.2f;
	const float WALL_FORCE_GAIN = 5.0f;

	vector<DroneDetection>::iterator i;
	vector<Obstacle>::iterator j;
	float heightOutput;
	float mag;
	int k;

	// zero out all interaction forces for this new frame
	resetInteractions();

	// interact with neighbours and obstacles if enabled
	if(interactionsEnabled)
	{
		// now interact with all neighbours, including the partner
		for(i = neighbours.begin(), k = 1; i != neighbours.end(); ++ i, ++ k)
		{
			interactWith(*i, k);
		}

		// now interact with all obstacles
		for(j = obstacles.begin(); j != obstacles.end(); ++ j)
		{
			interactWith(*j);
		}

		// also keep us in bounds
		keepInBounds();

		// accumulate interaction forces
		if(numAttractions) targetXZVelocity = (targetXZVelocity + (attractVec / (float)numAttractions) * ATTRACT_GAIN) * obstacleForce;
		if(numRepulsions)  targetXZVelocity = (targetXZVelocity + (repulseVec / (float)numRepulsions) * REPEL_GAIN) * obstacleForce;
		if(numAlignments)  targetXZVelocity = (targetXZVelocity + (alignVec / (float)numAlignments) * ALIGN_GAIN) * obstacleForce;
		if(numAvoidances)  targetXZVelocity = (targetXZVelocity + (avoidVec / (float)numAvoidances) * AVOID_GAIN);// * (1.0f - obstacleForce);
		if(numWallForces)  targetXZVelocity = (targetXZVelocity + (wallVec / (float)numWallForces) * WALL_FORCE_GAIN);

		// add some random motion if we're not moving
		mag = length(targetXZVelocity);
		if(mag == 0.0f)
		{
			// randomize motion
			targetXZVelocity = circularRand(maxXZVelocity);
		}
		else
		{
			// move at a fixed speed
			targetXZVelocity = normalize(targetXZVelocity) * maxXZVelocity;
		}
	}
	else
	{
		// set target velocity to nothing
		targetXZVelocity = vec2(0.0f);
	}

	// update velocity along XZ plane
	velocityXPID.update(targetXZVelocity.x - velocity.x, dt);
	velocityZPID.update(targetXZVelocity.y - velocity.z, dt);

	// determine difference in target versus actual height
	heightOutput = heightPID.update(targetHeight - height, dt);

	// limit output factor for upwards throttle
	if     (heightOutput > targetYVelocity) heightOutput = targetYVelocity;
	else if(heightOutput < -targetYVelocity) heightOutput = -targetYVelocity;

	// determine final height output based on measured y velocity
	velocityYPID.update(heightOutput - velocity.y, dt);
}

void PairwiseFlockingAgent::resetInteractions()
{
	numAttractions = 0;
	attractVec = vec3(0.0f);
	numRepulsions = 0;
	repulseVec = vec3(0.0f);
	numAlignments = 0;
	alignVec = vec3(0.0f);
	numAvoidances = 0;
	avoidVec = vec3(0.0f);
	numWallForces = 0;
	wallVec = vec3(0.0f);
	obstacleForce = 1.0f;
}

void PairwiseFlockingAgent::interactWith(const DroneDetection &other, int index)
{
	// [note]: I suspect that there is some optimal CLOSE_REPEL value that's partially a function of view range
	// that helps keep the flock together when pairing is present
	const float BASE_REPEL = 1.0f;//0.6f;//1.2f;//80.0f;
	const float CLOSE_REPEL = 0.5f;// 0.3f;//0.3f;//20.0f;
	const float REPEL_DIST = ((int)other.id == partnerID && canSeePartner && closePairing ? CLOSE_REPEL : BASE_REPEL);
	const float ALIGN_DIST = viewRange;

	const float PARTNER_ATTRACT_WEIGHT = 0.2f;				// only active if we see our mate and close pairing is enabled; see below

	vec2 otherXZPos = vec2(other.pos.x, other.pos.z);

	// if close enough, attract to neighbour (or if this is a partner that we can see)
	//if(other.dist < ATTRACT_DIST)// || (canSeePartner && (int)other.id == partnerID) || partnerID == -1)
	{
		// if we've got a partner that we're close to losing, ignore all other attraction forces
		if(!catchingUpToPartner || ((int)other.id == partnerID && canSeePartner))
		{
			if(partnerID == -1 ||																	// we have no partner OR
			  (canSeePartner && partnerDist < CATCH_UP_DISTANCE && (int)other.id != partnerID) ||	// we do, but this is not them OR
			  (canSeePartner && (int)other.id == partnerID))										// we do, we can see them, and this is them
			{
				// mates experience a stronger attractional pull, but regular agents do not
				numAttractions ++;
				attractVec += normalize(otherXZPos) * (1.0f / (float)index) * ((int)other.id == partnerID && canSeePartner && closePairing ? PARTNER_ATTRACT_WEIGHT : 1.0f);
			}
		}
	}

	// if too close, repel away from neighbour
	if(other.dist < REPEL_DIST)
	{
		// repulsion force scales down linearly with distance
		numRepulsions ++;
		repulseVec += normalize(-otherXZPos) * (1.0f - (other.dist / REPEL_DIST));
	}

	// if close enough, align velocity with neighbour; note that our neighbour's velocity is in our frame of reference,
	// so we have to transform it into our global frame of reference (for which we have our own velocity, and this is
	// reasonable since drones can measure this on their own without a global sensor)
	if(other.dist < ALIGN_DIST)
	{
		//numAlignments = 1;
		numAlignments ++;
		alignVec += (vec2(other.velocity.x, other.velocity.z) + vec2(velocity.x, velocity.z)) * (1.0f / (float)index);
	}
}

void PairwiseFlockingAgent::interactWith(const Obstacle &obj)
{
	vec2 diff = vec2(obj.pos.x, obj.pos.z);
	vec2 velocityXZ = vec2(velocity.x, velocity.z);
	float dist = length(diff);

	float forwardComponent = dot(diff, velocityXZ);
	vec2 forwardOffset = velocityXZ * forwardComponent * 2.0f;
	vec2 offForwardOffset = diff - forwardOffset;

	bool inCylinder = length(offForwardOffset) < obj.avoidRadius;
	bool nearby = forwardComponent < dist;
	bool inFront = forwardComponent > 0.0f;
	vec2 steerAway;

	if(inCylinder && nearby && inFront)
	{
		// [todo]: see if it is necessary to scale down existing social forces (it's not)
		//obstacleForce = 0.01f;//glm::max(obstacleForce, dist / obj.avoidRadius);

		steerAway = offForwardOffset * -1.0f;
		numAvoidances = 1;
		//numAvoidances ++;
		avoidVec += steerAway;
	}
}

void PairwiseFlockingAgent::keepInBounds()
{
	const float MIN_X = (-boundsArea.x / 2.0f) + boundsEdge;
	const float MAX_X = (boundsArea.x / 2.0f) - boundsEdge;
	const float MIN_Z = (-boundsArea.y / 2.0f) + boundsEdge;
	const float MAX_Z = (boundsArea.y / 2.0f) - boundsEdge;

	// this adds a repulsion force away from the outer edges of the world when the agent gets too close
	closeToBounds = false;
	if(boundsEnabled)
	{
		if(pos.x < MIN_X)
		{
			++ numWallForces;
			wallVec += vec2(1.0f, 0.0) * ((fabs(pos.x) - fabs(MIN_X)) / boundsEdge);
			closeToBounds = true;
		}

		if(pos.x > MAX_X)
		{
			++ numWallForces;
			wallVec += vec2(-1.0f, 0.0) * ((pos.x - MAX_X) / boundsEdge);
			closeToBounds = true;
		}

		if(pos.z < MIN_Z)
		{
			++ numWallForces;
			wallVec += vec2(0.0f, 1.0) * ((fabs(pos.z) - fabs(MIN_Z)) / boundsEdge);
			closeToBounds = true;
		}

		if(pos.z > MAX_Z)
		{
			++ numWallForces;
			wallVec += vec2(0.0f, -1.0) * ((pos.z - MAX_Z) / boundsEdge);
			closeToBounds = true;
		}
	}
}

vector<DroneDetection>::iterator PairwiseFlockingAgent::getNeighboursBegin()
{
	return neighbours.begin();
}

vector<DroneDetection>::iterator PairwiseFlockingAgent::getNeighboursEnd()
{
	return neighbours.end();
}

void PairwiseFlockingAgent::setTargetHeight(float targetHeight, float targetYVelocity)
{
	this -> targetHeight = targetHeight;
	this -> targetYVelocity = targetYVelocity;
}

float PairwiseFlockingAgent::getVelocityXPIDResult()
{
	return velocityXPID.getLastResult();
}

float PairwiseFlockingAgent::getVelocityYPIDResult()
{
	return velocityYPID.getLastResult();
}

float PairwiseFlockingAgent::getVelocityZPIDResult()
{
	return velocityZPID.getLastResult();
}

// - - - other functions - - - //

// agents are sorted in non-descending distance
bool sortNeighboursByDistance(const DroneDetection &a, const DroneDetection &b)
{
	return a.dist < b.dist;
}
