#include "objects/object.h"

#include "glm/glm.hpp"
using namespace glm;

Object::Object()
{
	side = vec3(1.0, 0.0, 0.0);
	up = vec3(0.0, 1.0, 0.0);
	forward = vec3(0.0, 0.0, 1.0);
}

Object::~Object()
{
	// anything?
}

void Object::setPos(const vec3 &pos)
{
	oldPos = this -> pos;
	this -> pos = pos;
}

vec3 Object::getPos()
{
	return pos;
}

vec3 Object::getOldPos()
{
	return oldPos;
}

void Object::setForward(const vec3 &forward)
{
	this -> forward = forward;
}

vec3 Object::getForward()
{
	return forward;
}

void Object::setSide(const vec3 &side)
{
	this -> side = side;
}

vec3 Object::getSide()
{
	return side;
}

void Object::setUp(const vec3 &up)
{
	this -> up = up;
}

vec3 Object::getUp()
{
	return up;
}

float Object::getPitch()
{
	return asin(forward.y);
}

float Object::getYaw()
{
	return atan2(forward.z, forward.x);
}

float Object::getRoll()
{
	return atan2(side.y, up.y);
}

mat4 Object::buildModelview()
{
	mat4 result(1.0);
	result[0] = vec4(side, 0.0);
	result[1] = vec4(up, 0.0);
	result[2] = vec4(forward, 0.0);
	result[3] = vec4(pos, 1.0);
	return result;
}

void Object::pitch6DOF(float radians)
{
	vec3 forward = getForward();
	vec3 side = getSide();
	vec3 up = getUp();

	float s = sinf(radians);
	float c = cosf(radians);

	vec3 n_forward = (forward * c) - (up * s);
	vec3 n_up = (up * c) + (forward * s);
	forward = n_forward;
	up = n_up;

	orthonormalize6DOF(forward, side, up);
}

void Object::roll6DOF(float radians)
{
	vec3 forward = getForward();
	vec3 side = getSide();
	vec3 up = getUp();

	float s = sinf(radians);
	float c = cosf(radians);

	vec3 n_side = (side * c) + (up * s);
	vec3 n_up = (up * c) - (side * s);
	side = n_side;
	up = n_up;

	orthonormalize6DOF(forward, side, up);
}

void Object::yaw6DOF(float radians)
{
	vec3 forward = getForward();
	vec3 side = getSide();
	vec3 up = getUp();

	float s = sinf(radians);
	float c = cosf(radians);

	vec3 n_forward = (forward * c) + (side * s);
	vec3 n_side = (side * c) - (forward * s);
	forward = n_forward;
	side = n_side;

	orthonormalize6DOF(forward, side, up);
}

void Object::orthonormalize6DOF(const vec3 &forward, const vec3 &side, const vec3 &up)
{
	setForward(normalize(forward));
	setSide(normalize(cross(up, forward)));
	setUp(normalize(cross(forward, side)));
}
