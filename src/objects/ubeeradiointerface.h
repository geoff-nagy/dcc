#pragma once

#include "objects/droneinterface.h"

#include "libserial/SerialPort.h"

#include <stdint.h>
#include <string>

class UBeeRadioInterface : public DroneInterface
{
public:
	static const uint32_t BROADCAST_ID;						// addresses all ubees

	static const uint8_t MSG_FLASH_LED;						// instruct drone to flash its LED
	static const uint8_t MSG_SET_ID;						// instruct drone to change its ID
	static const uint8_t MSG_SET_PARAMS;					// instruct drone to change a set of parameters
	static const uint8_t MSG_REQUEST_DATA;					// instruct drone to return some data
	static const uint8_t MSG_SINGLE_FLIGHT_COMMAND;			// send flight control command (pitch, roll, yaw)
	static const uint8_t MSG_COMPOUND_FLIGHT_COMMAND;		// send multiple different flight control commands to multiple ID'd drones
	static const uint8_t MSG_ACK;							// generic acknowledgment
	static const uint8_t MSG_NAK;							// generic non-acknowledgment
	static const uint8_t MSG_RECALIBRATE_IMU;				// instruct drone to recalibrate its IMU
	static const uint8_t MSG_START_IMU_TEST;				// instruct drone to start continuous transmission of IMU values
	static const uint8_t MSG_END_IMU_TEST;					// instruct drone to stop continuous transmission of IMU values
	static const uint8_t MSG_IMU_STATE;						// this is a special message from the drone containing IMU state
	static const uint8_t MSG_DATA_PACKET;					// some data packet sent by the drone

    UBeeRadioInterface(const std::string &device);
    ~UBeeRadioInterface();

	// send the commands that have been queued by the super class
	virtual void sendCommands(float time);

	// radio/uBee-specific commands such as an LED ping
	virtual void ping(uint32_t id);

	void getLastSent(std::vector<uint8_t> &bytes);

private:
	void init();
	void shutdown();

	std::string device;
	LibSerial::SerialPort *serialPort;

	std::vector<uint8_t> lastSent;
};
