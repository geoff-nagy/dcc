#pragma once

#include "glm/glm.hpp"

class Object
{
public:
	Object();
	virtual ~Object() = 0;

	void setPos(const glm::vec3 &pos);
	glm::vec3 getPos();
	glm::vec3 getOldPos();

	void setForward(const glm::vec3 &forward);
	glm::vec3 getForward();

	void setSide(const glm::vec3 &side);
	glm::vec3 getSide();

	void setUp(const glm::vec3 &up);
	glm::vec3 getUp();

	float getPitch();
	float getYaw();
	float getRoll();

	glm::mat4 buildModelview();

	void pitch6DOF(float radians);
	void roll6DOF(float radians);
	void yaw6DOF(float radians);

	virtual void update(float dt) = 0;

private:
	void orthonormalize6DOF(const glm::vec3 &forward, const glm::vec3 &side, const glm::vec3 &up);

	glm::vec3 pos;
	glm::vec3 oldPos;

	glm::vec3 forward;
	glm::vec3 side;
	glm::vec3 up;
};
