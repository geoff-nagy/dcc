#include "objects/physicsobject.h"

#include "util/log.h"

#include "glm/glm.hpp"
using namespace glm;

PhysicsObject::PhysicsObject(float mass)
	: mass(mass)
{
	inverseMass = 1.0f / mass;
}

PhysicsObject::~PhysicsObject()
{

}

void PhysicsObject::applyImpulse(const vec3 &impulse, const vec3 &contact)
{
	velocity += inverseMass * impulse;
	angularVelocity += cross(impulse, contact);
}

void PhysicsObject::setImpulse(const vec3 &impulse)
{
	this -> impulse = impulse;
}

vec3 PhysicsObject::getImpulse()
{
	return impulse;
}

void PhysicsObject::update(float dt)
{
	const float GRAVITY_ACCEL = -9.81f;

	vec3 pos = getPos();

	// build forces and turn it into velocity
	impulse += vec3(0.0f, GRAVITY_ACCEL, 0.0f);				// gravity
	velocity += /*mat3(buildModelview()) * */impulse * dt;	// impulse
	velocity *= 0.999f;										// air drag

	// move the object
	pos += velocity * dt;

	// collision with floor
	if(pos.y < 0.0f)
	{
		pos.y = 0.0f;
		velocity = vec3(0.0f);
	}

    // update position
	setPos(pos);
}
