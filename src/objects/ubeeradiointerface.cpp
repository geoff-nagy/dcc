#include "objects/ubeeradiointerface.h"

#include "util/log.h"

#include "libserial/SerialPort.h"
#include "libserial/SerialPortConstants.h"
using namespace LibSerial;

#include "glm/glm.hpp"
#include "glm/gtc/random.hpp"
using namespace glm;

#include <stdint.h>
#include <string>
using namespace std;

// generic message format:

// format       : [header, 2 bytes]  |  [ ... data ... ]
// byte index   :    0         1     |     2      [...]                 (len - 2)
// byte meaning : [ cmd ]   [ len ]  |  [ id ]    [ ...command-specific data... ]

// "cmd" is one of the command bytes below
// "len" is the remaining length of the message in bytes, after (and not including) this "len" byte
// "id" is either a unicast ID [0-254] referring to a single drone or the broadcast ID 255

// this means that every command will have a "len" byte of at least 1, containing at least the ID byte

const uint32_t UBeeRadioInterface::BROADCAST_ID = 255;					// addresses all ubees

const uint8_t UBeeRadioInterface::MSG_FLASH_LED = 1;					// instruct drone to flash its LED
const uint8_t UBeeRadioInterface::MSG_SET_ID = 2;						// instruct drone to change its ID
const uint8_t UBeeRadioInterface::MSG_SET_PARAMS = 3;					// instruct drone to change a set of parameters
const uint8_t UBeeRadioInterface::MSG_REQUEST_DATA = 4;					// instruct drone to return some data
const uint8_t UBeeRadioInterface::MSG_SINGLE_FLIGHT_COMMAND = 5;		// send flight control command (pitch, roll, yaw)
const uint8_t UBeeRadioInterface::MSG_COMPOUND_FLIGHT_COMMAND = 6;		// send multiple different flight control commands to multiple ID'd drones
const uint8_t UBeeRadioInterface::MSG_ACK = 7;							// generic acknowledgment
const uint8_t UBeeRadioInterface::MSG_NAK = 8;							// generic non-acknowledgment
const uint8_t UBeeRadioInterface::MSG_RECALIBRATE_IMU = 9;				// instruct drone to recalibrate its IMU
const uint8_t UBeeRadioInterface::MSG_START_IMU_TEST = 10;				// instruct drone to start continuous transmission of IMU values
const uint8_t UBeeRadioInterface::MSG_END_IMU_TEST = 11;				// instruct drone to stop continuous transmission of IMU values
const uint8_t UBeeRadioInterface::MSG_IMU_STATE = 12;					// this is a special message from the drone containing IMU state
const uint8_t UBeeRadioInterface::MSG_DATA_PACKET = 13;					// some data packet sent by the drone

UBeeRadioInterface::UBeeRadioInterface(const string &device)
	: device(device), serialPort(NULL)
{
	init();
}

UBeeRadioInterface::~UBeeRadioInterface()
{
	shutdown();
}

void UBeeRadioInterface::sendCommands(float time)
{
	const int MAX_COMMANDS_IN_COMPOUND = 5;

    map<uint32_t, FlightCommand>::iterator i;
    //int j;
    int numCommands;
	int yaw;
	int thrust;
	int roll;
	int pitch;
	DataBuffer buffer;			// alias for vector<uint8_t>, defined in SerialPortConstants.h

	// extra crap for stress-testing
	//addCommand(3, 1.0f, linearRand(-1.0f, 1.0f), linearRand(-1.0f, 1.0f), linearRand(-1.0f, 1.0f));
	//addCommand(7, 1.0f, linearRand(-1.0f, 1.0f), linearRand(-1.0f, 1.0f), linearRand(-1.0f, 1.0f));
	//addCommand(8, 1.0f, linearRand(-1.0f, 1.0f), linearRand(-1.0f, 1.0f), linearRand(-1.0f, 1.0f));
	//addCommand(3, 1.0f, -1.0f, 1.0f, linearRand(-1.0f, 1.0f));
	//addCommand(7, 1.0f, -1.0f, 1.0f, linearRand(-1.0f, 1.0f));
	//addCommand(8, 1.0f, -1.0f, 1.0f, linearRand(-1.0f, 1.0f));

	// make sure we can actually send this number of commands
	// [todo]: send multiple compound flight commands in the case where we have too many drones to address for one compound flight command
	numCommands = getNumCommands();
	if(numCommands > MAX_COMMANDS_IN_COMPOUND)
	{
		// truncate commands to fit into one compound flight command
		// [todo]: send multiple compound flight commands in the case where we have too many drones to address for one compound flight command;
		//         this isn't really a problem right now since we only will ever fly a few uBees at once, but in the future this may change
		LOG_INFO("UBeeRadioInterface::sendCommands() is truncating compound flight commands from " << numCommands << " to " << MAX_COMMANDS_IN_COMPOUND);
		numCommands = MAX_COMMANDS_IN_COMPOUND;
	}

	// make sure we actually have commands to send
	buffer.clear();
	if(numCommands > 0)
	{
		// iterate through (possibly truncated) list of commands
		buffer.push_back(MSG_COMPOUND_FLIGHT_COMMAND);			// msg byte
		buffer.push_back(5 * numCommands);						// len byte (length is number of bytes remaining after this len byte)
		for(i = getBegin(); i != getEnd(); ++ i)
		{
			// bring thrust into byte range
			thrust = (int)(round(i -> second.thrust * 255.0f));

			// RPY values are guaranteed to be in the range [-1.0, 1.0], so scale them into byte range
			roll = 127 + (int)(round(i -> second.roll * 127.0f));
			pitch = 127 + (int)(round(i -> second.pitch * 127.0f));
			yaw = 127 + (int)(round(i -> second.yaw * 127.0f));

			// clamp values to be double-sure that they make sense
			yaw = clamp(yaw, 0, 255);
			thrust = clamp(thrust, 0, 255);
			roll = clamp(roll, 0, 255);
			pitch = clamp(pitch, 0, 255);

			// uncomment to see what's going on

			LOG_INFO("--------------------------------");
			LOG_INFO("[UBRI] RADIO OUTPUT:");
			LOG_INFO("ID     : " << i -> first);
			LOG_INFO("yaw    : " << yaw);
			LOG_INFO("thrust : " << thrust);
			LOG_INFO("roll   : " << roll);
			LOG_INFO("pitch  : " << pitch);


			// build the command
			buffer.push_back((uint8_t)i -> first);	// this is the drone ID
			buffer.push_back((uint8_t)yaw);
			buffer.push_back((uint8_t)thrust);
			buffer.push_back((uint8_t)roll);
			buffer.push_back((uint8_t)pitch);
		}

		// send the command over serial
		serialPort -> Write(buffer);
		serialPort -> FlushOutputBuffer();

		// save commands for debugging later on
		lastSent.clear();
		lastSent.assign(buffer.begin(), buffer.end());

	}
}

void UBeeRadioInterface::ping(uint32_t id)
{
	LOG_INFO("UBeeRadioInterface::ping() is sending a ping to " << id);

	DataBuffer buffer = {MSG_FLASH_LED, 1, (unsigned char)id};
	serialPort -> Write(buffer);
	serialPort -> FlushOutputBuffer();
}

void UBeeRadioInterface::getLastSent(std::vector<uint8_t> &bytes)
{
	if(lastSent.size())
	{
		bytes.clear();
		bytes.assign(lastSent.begin(), lastSent.end());
	}
}

void UBeeRadioInterface::init()
{
	serialPort = new SerialPort(device, BaudRate::BAUD_38400);//BAUD_57600);//BAUD_115200);//BAUD_9600);
	if(serialPort -> IsOpen())
	{
		LOG_INFO("UBeeRadioInterface::init(): serial port \"" << device << "\" opened successfully");
	}
	else
	{
		LOG_ERROR("UBeeRadioInterface::init(): could not open serial port \"" << device << "\"");
	}
}

void UBeeRadioInterface::shutdown()
{
	if(serialPort)
	{
		LOG_INFO("UBeeRadioInterface::shutdown(): shutting down serial device \"" << device << "\"...");
		serialPort -> Close();
		delete serialPort;
	}
	serialPort = NULL;
}
