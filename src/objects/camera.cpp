#include "objects/camera.h"

#include "util/log.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
using namespace glm;

#include <fstream>
#include <string>
using namespace std;

Camera::Camera() { }

Camera::~Camera() { }

Camera *Camera::orthoCam(double left, double right, double bottom, double top)
{
	const double ORTHO_NEAR_RANGE = -1.0;
	const double ORTHO_FAR_RANGE = 1.0;

	Camera *camera = new Camera();

	camera -> projection = ortho(left, right, bottom, top, ORTHO_NEAR_RANGE, ORTHO_FAR_RANGE);
	camera -> setView(dmat4(1.0));

	return camera;
}

Camera *Camera::perspectiveCam(double fov, double aspect, double near, double far)
{
	Camera *camera = new Camera();

	camera -> projection = perspective(radians(fov), aspect, near, far);
	camera -> setView(dmat4(1.0));

	return camera;
}

Camera *Camera::fromViewProjectionMatrixFile(const string &filename)
{
	Camera *result = NULL;
	double projection[16];
	double view[16];

	ifstream file;
	int i;

	// attempt to open the file
	file.open(filename.c_str(), ifstream::in);
	if(file.is_open())
	{
		result = new Camera();

		// read projection matrix values
		for(i = 0; i < 16; ++ i)
		{
			file >> projection[i];
		}

		// read view matrix values
		for(i = 0; i < 16; ++ i)
		{
			file >> view[i];
		}

		// read camera position
		file >> result -> pos.x;
		file >> result -> pos.y;
		file >> result -> pos.z;

		// read camera forward vector
		file >> result -> forward.x;
		file >> result -> forward.y;
		file >> result -> forward.z;

		// assign matrices
		result -> projection = make_mat4(projection);
		result -> setView(make_mat4(view));
	}

	return result;
}

void Camera::getProjection(dmat4 *projection)
{
	*projection = this -> projection;
}

void Camera::getView(dmat4 *view)
{
	*view = this -> view;
}

void Camera::getViewProjection(dmat4 *viewProjection)
{
	*viewProjection = this -> viewProjection;
}

dvec3 Camera::getPos()
{
	return pos;
}

dvec3 Camera::getForward()
{
	return forward;
}

void Camera::setView(const dmat4 &view)
{
	this -> view = view;
	viewProjection = projection * view;
}

void Camera::lookAt(const dvec3 &eye, const dvec3 &center, const dvec3 &up)
{
	setView(glm::lookAt(eye, center, up));
}
