#include "objects/droneflightcontroller.h"
#include "objects/droneinterface.h"

#include "util/pidcontroller.h"
#include "util/log.h"

#include "glm/glm.hpp"
using namespace glm;

#include <vector>
using namespace std;

DroneFlightController::DroneFlightController(PIDController posXPID, PIDController posYPID, PIDController posZPID,
											 PIDController velXPID, PIDController velYPID, PIDController velZPID,
											 PIDController headingPID, float minThrust, float baseThrust)
	: posXPID(posXPID), posYPID(posYPID), posZPID(posZPID), velXPID(velXPID), velYPID(velYPID), velZPID(velZPID), headingPID(headingPID), oldDir(0.0f),
	  minThrust(minThrust), baseThrust(baseThrust), targetYVel(0.0f), maxTargetXZSpeed(0.0f), maxTargetYSpeed(0.0f), maxTargetYawSpeed(0.0f)
{
	// reset flight command
	flightCommand.id = 0;
	flightCommand.thrust = 0.0f;
	flightCommand.roll = 0.0f;
	flightCommand.pitch = 0.0f;
	flightCommand.yaw = 0.0f;

	// target a position by default
	flightMode = FLIGHT_MODE_POSITION;

	//controlTimer = 0.0f;
	//numDetections = 0;

	velBuffer = CircularBuffer(3);
}

DroneFlightController::~DroneFlightController()
{
	// anything?
}

void DroneFlightController::setMinThrust(float minThrust)
{
	this -> minThrust = minThrust;
}

void DroneFlightController::setBaseThrust(float baseThrust)
{
	this -> baseThrust = baseThrust;
}

float DroneFlightController::getMinThrust()
{
	return minThrust;
}

float DroneFlightController::getBaseThrust()
{
	return baseThrust;
}

void DroneFlightController::setTargetPosition(const glm::vec3 &targetPos)
{
	this -> targetPos = targetPos;
	flightMode = FLIGHT_MODE_POSITION;
}

void DroneFlightController::setTargetXZVelocity(const glm::vec2 &targetXZVel)
{
	this -> targetXZVel = targetXZVel;
	flightMode = FLIGHT_MODE_VELOCITY;
}

void DroneFlightController::setTargetHeading(float targetHeading)
{
	this -> targetHeading = targetHeading;
}

void DroneFlightController::setMaxTargetXZSpeed(float maxTargetXZSpeed)
{
	this -> maxTargetXZSpeed = maxTargetXZSpeed;
}

void DroneFlightController::setMaxTargetYSpeed(float maxTargetYSpeed)
{
	this -> maxTargetYSpeed = maxTargetYSpeed;
}

void DroneFlightController::setMaxTargetYawSpeed(float maxTargetYawSpeed)
{
	this -> maxTargetYawSpeed = maxTargetYawSpeed;
}

void DroneFlightController::update(DroneDetection detection, float dt)
{
/*
	const float ALPHA = 0.8f;

	vec3 newVel;

	// re-compute our velocity with respect to the time delta used by us, not whatever tracker we used
	detection.velocity = (detection.pos - oldPos) / dt;
	oldPos = detection.pos;
	//detection.velocity = (detection.pos - detection.oldPos) / dt;

	newVel = detection.velocity;
	newVel.x = (ALPHA * oldVel.x) + ((1.0f - ALPHA) * detection.velocity.x);
	newVel.z = (ALPHA * oldVel.z) + ((1.0f - ALPHA) * detection.velocity.z);
	oldVel = detection.velocity;

	detection.velocity = newVel;*/

	// re-compute velocity based not on the tracking results, but since our last flight controller update
	detection.velocity = (detection.pos - oldPos) / dt;
	oldPos = detection.pos;

	// use a short circular averaging buffer to filter our velocity slightly
	velBuffer.addEntry(detection.velocity);
	detection.velocity.x = velBuffer.getFilteredOutput().x;
	detection.velocity.z = velBuffer.getFilteredOutput().z;

	// now update the flight controller
	if     (flightMode == FLIGHT_MODE_POSITION) updatePositionFlightMode(detection, dt);
	else if(flightMode == FLIGHT_MODE_VELOCITY) updateVelocityFlightMode(detection, dt);

	// there is a potential issue with this code below, due to the 0.02 update rate we've set here when we already have
	// a 30Hz (0.02 sec) update rate calling this code; we could potentially end up invoking the flight controller below
	// at half the rate we're sending commands, with strange values of controlTimer; this may have been causing our issue
	// we were having; we would effectively be using a delta time below twice what was necessary (causing larger than actual)
	// velocity values/control outputs to be sent, while our communication rate was twice this update rate; this might explain
	// why we had that weird oscillation for some of the drones some of the time

	// note that having corrected this may require re-tuning of some PID parameters (possibly); we'll have to see

/*
	// accumulate detections
	detectionAvg = detectionAvg + detection;
	++ numDetections;

	// is it time for a control update?
	controlTimer += dt;
	if(controlTimer >= 0.02f)
	{
		// compute average of detections we've accumulated since the last control update
		detectionAvg = detectionAvg / (float)numDetections;

		// now update the flight controller
		if     (flightMode == FLIGHT_MODE_POSITION) updatePositionFlightMode(detectionAvg, controlTimer);
		else if(flightMode == FLIGHT_MODE_VELOCITY) updateVelocityFlightMode(detectionAvg, controlTimer);

		// un-comment to see what's happening

		LOG_INFO("--------------------------------");
		LOG_INFO("DRONE FLIGHT CONTROLLER FOR AGENT " << detection.id << ": ");
		LOG_INFO("pos   : " << detectionAvg.pos.x << ", " << detectionAvg.pos.y << ", " << detectionAvg.pos.z);
		LOG_INFO("vel   : " << detectionAvg.velocity.x << ", " << detectionAvg.velocity.y << ", " << detectionAvg.velocity.z);
		LOG_INFO("speed : " << length(detectionAvg.velocity) << " m/s");


		// reset control timer
		controlTimer = 0.0f;

		// reset detection average
		numDetections = 0;
		detectionAvg.pos = vec3(0.0f);
		detectionAvg.oldPos = vec3(0.0f);
		detectionAvg.velocity = vec3(0.0f);
		detectionAvg.dir = 0.0f;
	}*/
}

void DroneFlightController::updatePositionFlightMode(DroneDetection detection, float dt)
{
	const float MIN_HEIGHT_FOR_XZ_CONTROL = 0.1f;
	const float YAW_COMP_FILTER_ALPHA = 0.9f;//95f;

	const float MAX_ROLL_OUTPUT = 1.0f;
	const float MAX_PITCH_OUTPUT = 1.0f;

	float targetSpeed;
	float rollOutput;
	float pitchOutput;
	float yawOutput;
	float filteredDir;

	LOG_INFO("[ " << detection.id << "] drone is updating position");

	// - - - position and velocity control - - - //

	// get update from positional PID controllers
	if(detection.pos.y >= MIN_HEIGHT_FOR_XZ_CONTROL)
	{
		posXPID.update(targetPos.x - detection.pos.x, dt);
		posZPID.update(targetPos.z - detection.pos.z, dt);
	}
	else
	{
		posXPID.reset();
		posZPID.reset();
	}

	// always update throttle demand
	posYPID.update(targetPos.y - detection.pos.y, dt);

	// turn those into a velocity vector
	targetXZVel = vec2(posXPID.getLastResult(), posZPID.getLastResult());
	targetYVel = posYPID.getLastResult();

	// limit our XZ speed
	targetSpeed = length(targetXZVel);
	if(targetSpeed > maxTargetXZSpeed)
	{
		targetXZVel = (targetXZVel / targetSpeed) * maxTargetXZSpeed;
	}

	// limit our Y speed
	if     (targetYVel > maxTargetYSpeed) targetYVel = maxTargetYSpeed;
	else if(targetYVel < -maxTargetYSpeed) targetYVel = -maxTargetYSpeed;

	// input desired speeds into our velocity PID controllers; note that targetXZVel is a vec2
	if(detection.pos.y >= MIN_HEIGHT_FOR_XZ_CONTROL)
	{
		velXPID.update(targetXZVel.x - detection.velocity.x, dt);
		velZPID.update(targetXZVel.y - detection.velocity.z, dt);
	}
	else
	{
		velXPID.reset();
		velZPID.reset();
	}

	// always update throttle demand
	velYPID.update(targetYVel - detection.velocity.y, dt);

	// - - - yaw control - - - //

	// filter the yaw, which can be jittery
	filteredDir = (oldDir * YAW_COMP_FILTER_ALPHA) + (detection.dir * (1.0f - YAW_COMP_FILTER_ALPHA));
	oldDir = filteredDir;

	// build target heading
	headingPID.update(targetHeading - filteredDir/*detection.dir*/, dt);

	// limit yaw speed
	yawOutput = headingPID.getLastResult();
	yawOutput = clamp(yawOutput, -maxTargetYawSpeed, maxTargetYawSpeed);

	// get roll and pitch demands, taking into account local transformation of drone
	rollOutput = (velXPID.getLastResult() * cos(filteredDir)) + (velZPID.getLastResult() * sin(filteredDir));
	pitchOutput = (velZPID.getLastResult() * cos(filteredDir)) + (velXPID.getLastResult() * sin(filteredDir));

	// clamp to actual ranges allowed
	rollOutput = clamp(rollOutput, -MAX_ROLL_OUTPUT, MAX_ROLL_OUTPUT);
	pitchOutput = clamp(pitchOutput, -MAX_PITCH_OUTPUT, MAX_PITCH_OUTPUT);
	pitchAndRollOutput = vec2(rollOutput, pitchOutput);

	// build flight command we'll send via our drone interface
	flightCommand.id = detection.id;
	flightCommand.thrust = velYPID.getLastResult() + baseThrust;
	flightCommand.roll = rollOutput;
	flightCommand.pitch = pitchOutput;
	flightCommand.yaw = yawOutput;

	// make sure minimum thrust is applied
	if(flightCommand.thrust < minThrust)
	{
		flightCommand.thrust = minThrust;
	}
}

void DroneFlightController::updateVelocityFlightMode(DroneDetection detection, float dt)
{
	const float MIN_HEIGHT_FOR_XZ_CONTROL = 0.1f;
	const float YAW_COMP_FILTER_ALPHA = 0.9f;//0.999f;

	const float MAX_ROLL_OUTPUT = 0.3f;//1.0f;
	const float MAX_PITCH_OUTPUT = 0.3f;//1.0f;

	float targetSpeed;
	float rollOutput;
	float pitchOutput;
	float yawOutput;
	float filteredDir;

	LOG_INFO("[ " << detection.id << "] drone is updating velocity");

	// - - - position and velocity control - - - //

	// always update throttle demand
	posYPID.update(targetPos.y - detection.pos.y, dt);
	targetYVel = posYPID.getLastResult();

	// limit our XZ speed
	targetSpeed = length(targetXZVel);
	if(targetSpeed > maxTargetXZSpeed)
	{
		targetXZVel = (targetXZVel / targetSpeed) * maxTargetXZSpeed;
	}

	// limit our Y speed
	if     (targetYVel > maxTargetYSpeed) targetYVel = maxTargetYSpeed;
	else if(targetYVel < -maxTargetYSpeed) targetYVel = -maxTargetYSpeed;

	// [possible problem area?]
	// input desired speeds into our velocity PID controllers; note that targetXZVel is a vec2
	if(detection.pos.y >= MIN_HEIGHT_FOR_XZ_CONTROL)
	{
		velXPID.update(targetXZVel.x - detection.velocity.x, dt);
		velZPID.update(targetXZVel.y - detection.velocity.z, dt);
	}
	else
	{
		velXPID.reset();
		velZPID.reset();
	}

	// always update throttle demand
	velYPID.update(targetYVel - detection.velocity.y, dt);

	// - - - yaw control - - - //

	// filter the yaw, which can be jittery
	filteredDir = (oldDir * YAW_COMP_FILTER_ALPHA) + (detection.dir * (1.0f - YAW_COMP_FILTER_ALPHA));
	oldDir = filteredDir;

	// build target heading
	headingPID.update(targetHeading - filteredDir/*detection.dir*/, dt);

	// limit yaw speed
	yawOutput = headingPID.getLastResult();
	yawOutput = clamp(yawOutput, -maxTargetYawSpeed, maxTargetYawSpeed);

	// get roll and pitch demands, taking into account local transformation of drone
	rollOutput = (velXPID.getLastResult() * cos(filteredDir)) + (velZPID.getLastResult() * sin(filteredDir));
	pitchOutput = (velZPID.getLastResult() * cos(filteredDir)) + (velXPID.getLastResult() * sin(filteredDir));

	// clamp to actual ranges allowed
	rollOutput = clamp(rollOutput, -MAX_ROLL_OUTPUT, MAX_ROLL_OUTPUT);
	pitchOutput = clamp(pitchOutput, -MAX_PITCH_OUTPUT, MAX_PITCH_OUTPUT);
	pitchAndRollOutput = vec2(rollOutput, pitchOutput);

	// build flight command we'll send via our drone interface
	flightCommand.id = detection.id;
	flightCommand.thrust = velYPID.getLastResult() + baseThrust;
	flightCommand.roll = rollOutput;
	flightCommand.pitch = pitchOutput;
	flightCommand.yaw = yawOutput;

	// make sure minimum thrust is applied
	if(flightCommand.thrust < minThrust)
	{
		flightCommand.thrust = minThrust;
	}
}

FlightCommand DroneFlightController::getLastFlightCommand()
{
	return flightCommand;
}

vec2 DroneFlightController::getTargetXZSpeed()
{
	return targetXZVel;
}

float DroneFlightController::getTargetYSpeed()
{
	return targetYVel;
}

vec2 DroneFlightController::getPitchAndRollOutput()
{
	return pitchAndRollOutput;
}
