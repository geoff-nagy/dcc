#pragma once

#include "util/pidcontroller.h"

#include "glm/glm.hpp"

class FlightControl
{
public:
	uint32_t id;

	glm::vec3 targetVelocity;
	glm::vec3 targetPosition;

	PIDController velocityPIDX;
	PIDController velocityPIDY;
	PIDController velocityPIDZ;

	PIDController positionPIDX;
	PIDController positionPIDY;
	PIDController positionPIDZ;
};
