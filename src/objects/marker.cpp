#include "objects/marker.h"

#include "glm/glm.hpp"
using namespace glm;

Marker::Marker()
	: id(0), pos(0.0f), seen(false), color(1.0f), dirty(false)
{ }

Marker::Marker(const glm::vec3 &pos)
	: id(0), pos(pos), seen(false), color(1.0f), dirty(false)
{ }

Marker::Marker(const Marker &other)
{
	this -> id = other.id;
	this -> pos = other.pos;
	this -> seen = other.seen;
	this -> color = other.color;
	this -> dirty = other.dirty;

}

Marker& Marker::operator= (const Marker& other)
{
	this -> id = other.id;
	this -> pos = other.pos;
	this -> seen = other.seen;
	this -> color = other.color;
	this -> dirty = other.dirty;
	return *this;
}
