#include "objects/simulateddrone.h"

#include "util/mymath.h"
#include "util/log.h"

#include "glm/glm.hpp"
using namespace glm;

#include <iostream>
using namespace std;

SimulatedDrone::SimulatedDrone(float grams, uint32_t id, float maxRotorImpulse, float maxYawSpeed)
	: PhysicsObject(grams), id(id), maxRotorImpulse(maxRotorImpulse), maxYawSpeed(maxYawSpeed)
{
	thrust = 0.0f;
	roll = 0.0f;
	pitch = 0.0f;
	yaw = 0.0f;

	actualThrust = 0.0f;
	actualRoll = 0.0f;
	actualPitch = 0.0f;
	actualYaw = 0.0f;
}

SimulatedDrone::~SimulatedDrone()
{
	// anything?
}

void SimulatedDrone::addRotor(const Rotor &rotor)
{
	rotors.push_back(rotor);
}

void SimulatedDrone::setRotorSpeed(int index, float speed)
{
	rotors[index].speed = speed;
}

void SimulatedDrone::addViconMarker(const vec3 &pos)
{
	viconMarkers.push_back(pos);
}

void SimulatedDrone::clearViconMarkers()
{
	viconMarkers.clear();
}

uint32_t SimulatedDrone::getNumViconMarkers()
{
	return viconMarkers.size();
}

vec3 SimulatedDrone::getViconMarker(uint32_t index)
{
	vec3 result;

	if(index < viconMarkers.size())
	{
		// return the vicon marker position in world space
		result = getPos() + (getSide() * viconMarkers[index].x) + (getUp() * viconMarkers[index].y) + (getForward() * viconMarkers[index].z);
	}
	else
	{
		LOG_ERROR("SimulatedDrone id " << id << " has no Vicon marker index " << index);
	}

	return result;
}

uint32_t SimulatedDrone::getID()
{
	return id;
}

void SimulatedDrone::setThrust(float thrust)
{
	thrust = clamp(thrust, 0.0f, 1.0f);
	this -> thrust = thrust;
}

void SimulatedDrone::setRoll(float roll)
{
	roll = clamp(roll, -1.0f, 1.0f);
	this -> roll = roll;
}

void SimulatedDrone::setPitch(float pitch)
{
	pitch = clamp(pitch, -1.0f, 1.0f);
	this -> pitch = pitch;
}

void SimulatedDrone::setYaw(float yaw)
{
	yaw = clamp(yaw, -1.0f, 1.0f);
	this -> yaw = yaw;
}

void SimulatedDrone::update(float dt)
{
	const float MAX_ANGLE = radians(15.0f);
	const float DEMAND_SPEED = 2.5f * dt;
	const float YAW_DEMAND_SPEED = 8.0f * dt;
	const float MAX_YAW_SPEED = radians(90.0f);

	vec3 impulse;

	// move gradually to desired demands to reflect the fact that motors have to accelerate
	actualThrust = accel(actualThrust, thrust, DEMAND_SPEED);
	actualRoll = accel(actualRoll, roll, DEMAND_SPEED);
	actualPitch = accel(actualPitch, pitch, DEMAND_SPEED);
	actualYaw = accel(actualYaw, yaw, YAW_DEMAND_SPEED);

	// compute desired roll and pitch angles based on max target roll and pitch angles of the uBee
	float rollAngle = -sinf(actualRoll * MAX_ANGLE);
	float pitchAngle = sinf(actualPitch * MAX_ANGLE);

	// apply impulse
	impulse += getSide() * rollAngle * maxRotorImpulse;
	impulse += getForward() * pitchAngle * maxRotorImpulse;
	impulse += getUp() * actualThrust * (1.0f - glm::max(rollAngle, pitchAngle)) * maxRotorImpulse;
	setImpulse(impulse);

	// apply yaw
	yaw6DOF(-actualYaw * MAX_YAW_SPEED * dt);

	// update our physics object
	PhysicsObject::update(dt);
}
