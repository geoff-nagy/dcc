#include "objects/simulatedubee.h"

#include "sensing/markerplacement.h"

#include "util/marblecode.h"
#include "util/log.h"

#include "glm/glm.hpp"
#include "glm/gtc/random.hpp"
using namespace glm;

#include <string>
using namespace std;

const float SimulatedUBee::BASE_THRUST = 0.75f;
const float SimulatedUBee::MIN_THRUST = 0.4f;

SimulatedUBee::SimulatedUBee(uint32_t id)
	: SimulatedDrone(38.0f, id, 12.5f, 1.0f)
{
	// add four rotors to the drone based on real specifications
	addRotor(Rotor(vec3(0.035f, 0.005f, -0.035f), 12.5f));
	addRotor(Rotor(vec3(0.035f, 0.005f, 0.035f), 12.5f));
	addRotor(Rotor(vec3(-0.035f, 0.005f, 0.035f), 12.5f));
	addRotor(Rotor(vec3(-0.035f, 0.005f, -0.035f), 12.5f));
	throttle = 0.0f;
}

SimulatedUBee::~SimulatedUBee()
{

}

void SimulatedUBee::update(float dt)
{
	const float MAX_YAW_DRIFT = 0.005f;

	// add random yaw drift if we're in the air, so we're forced to compensate for it
	if(getPos().y > 0.01f)
	{
		Object::yaw6DOF(linearRand(-MAX_YAW_DRIFT, MAX_YAW_DRIFT));
	}

	// update our drone physics
	SimulatedDrone::update(dt);
}
