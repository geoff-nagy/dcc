#include "objects/point.h"

#include "glm/glm.hpp"
using namespace glm;

Point::Point()
{
	color = vec4(1.0f);
	size = 1.0f;
}

Point::Point(const vec3 &pos, const vec4 &color, float size)
{
	this -> pos = pos;
	this -> color = color;
	this -> size = size;
}

Point::~Point() { }

bool Point::operator== (const Point &other)
{
	return pos == other.pos && color == other.color && size == other.size;
}
