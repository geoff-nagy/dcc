#include "objects/rotor.h"

#include "glm/glm.hpp"
using namespace glm;

Rotor::Rotor(const vec3 &pos, float maxImpulse)
	: pos(pos), maxImpulse(maxImpulse), speed(0.0f)
{

}

Rotor::~Rotor() { }
