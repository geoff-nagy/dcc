#include "objects/droneinterface.h"

#include "util/log.h"

#include "glm/glm.hpp"
using namespace glm;

#include <map>
using namespace std;

DroneInterface::DroneInterface() { }

DroneInterface::~DroneInterface() { }

void DroneInterface::addCommand(FlightCommand flightCommand)
{
	addCommand(flightCommand.id, flightCommand.thrust, flightCommand.roll, flightCommand.pitch, flightCommand.yaw);
}

void DroneInterface::addCommand(uint32_t id, float thrust, float roll, float pitch, float yaw)
{
	thrust = clamp(thrust, 0.0f, 1.0f);
	roll = clamp(roll, -1.0f, 1.0f);
	pitch = clamp(pitch, -1.0f, 1.0f);
	yaw = clamp(yaw, -1.0f, 1.0f);

	commands[id].id = id;
	commands[id].thrust = thrust;
	commands[id].roll = -roll;
	commands[id].pitch = pitch;
	commands[id].yaw = yaw;
}

void DroneInterface::clearCommand(uint32_t id)
{
	map<uint32_t, FlightCommand>::iterator i = commands.find(id);

	// make sure the entry actually exists
	if(i != commands.end())
	{
		commands.erase(i);
	}
}

void DroneInterface::clearCommands()
{
	commands.clear();
}

map<uint32_t, FlightCommand>::iterator DroneInterface::getBegin()
{
	return commands.begin();
}

map<uint32_t, FlightCommand>::iterator DroneInterface::getEnd()
{
	return commands.end();
}

uint32_t DroneInterface::getNumCommands()
{
	return commands.size();
}
