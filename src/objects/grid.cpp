#include "objects/grid.h"

#include "rendering/linerenderer.h"

#include "util/shader.h"

#include "GL/glew.h"

#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
using namespace glm;

Grid::Grid(int horizontalLineCount, int verticalLineCount, float lineSpacing)
{
	this -> horizontalLineCount = horizontalLineCount;
	this -> verticalLineCount = verticalLineCount;
	this -> lineSpacing = lineSpacing;

	size = vec2(horizontalLineCount - 1, verticalLineCount - 1);
	halfSize = size / 2.0f;

	//setupVBOs(horizontalLineCount, verticalLineCount, lineSpacing);
	//setupShader();
}

Grid::~Grid()
{
	//delete shader;

	//glDeleteBuffers(1, vbos);
	//glDeleteVertexArrays(1, &vao);
}

void Grid::setupVBOs(int horizontalLineCount, int verticalLineCount, float lineSpacing)
{
	GLfloat vertices[(horizontalLineCount + verticalLineCount) * 12];
	GLfloat *vertex = vertices;
	int i;

	// add a horizontal row of lines
	for(i = 0; i < horizontalLineCount; i ++)
	{
		*vertex++ = -((horizontalLineCount - 1) * lineSpacing) / 2.0;
		*vertex++ = 0.0;
		*vertex++ = -(((horizontalLineCount - 1) * lineSpacing) / 2.0) + i * lineSpacing;

		*vertex++ = ((horizontalLineCount - 1) * lineSpacing) / 2.0;
		*vertex++ = 0.0;
		*vertex++ = -(((horizontalLineCount - 1) * lineSpacing) / 2.0) + i * lineSpacing;
	}

	// add a vertical row of lines
	for(i = 0; i < verticalLineCount; i ++)
	{
		*vertex++ = -(((verticalLineCount - 1) * lineSpacing) / 2.0) + i * lineSpacing;
		*vertex++ = 0.0;
		*vertex++ = -((verticalLineCount - 1) * lineSpacing) / 2.0;

		*vertex++ = -(((verticalLineCount - 1) * lineSpacing) / 2.0) + i * lineSpacing;
		*vertex++ = 0.0;
		*vertex++ = ((verticalLineCount - 1) * lineSpacing) / 2.0;
	}

	// set up our VAO and VBOs
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, vbos);

	// the only vertex attributes we have to define are line end point positions, and these never change
	glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * (horizontalLineCount + verticalLineCount) * 12, vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);
}

void Grid::setupShader()
{
	//shader = new Shader("../shaders/grid.vert", "../shaders/grid.frag");
	shader = Shader::fromFile("../shaders/grid.vert", "../shaders/grid.frag");
	shader -> bindAttrib("a_Vertex", 0);
	shader -> link();
	shader -> bind();
	shader -> uniformVec4("u_Color", vec4(0.5f, 0.5f, 0.5f, 0.5f));
	shader -> unbind();
}

void Grid::render(const mat4 &viewProjection)
{
	const vec4 MAJOR_LINE_COLOR_EDGE(1.0f, 0.8f, 0.3f, 0.75f);
	const vec4 MAJOR_LINE_COLOR_INNER(1.0f, 1.0f, 1.0f, 0.25f);
	const vec4 MINOR_LINE_COLOR(1.0f, 1.0f, 1.0f, 0.1f);

	const float MINOR_LINE_MULTIPLIER = 10.0f;

	const vec3 CORNER(0.0f);//-halfSize.x, 0.0f, halfSize.y);
	const float AXIS_LINE_LENGTH = 1.0f;
	const vec4 X_AXIS_COLOR(1.0f, 0.0f, 0.0f, 1.0f);
	const vec4 Y_AXIS_COLOR(0.0f, 0.0f, 1.0f, 1.0f);
	const vec4 Z_AXIS_COLOR(0.0f, 1.0f, 0.0f, 1.0f);

	LineRenderer *lines = LineRenderer::getInstance();
	int i;

	// render major lines across x axis
	for(i = 0; i < horizontalLineCount; ++ i)
	{
		lines -> add(vec3(-halfSize.x + (i * lineSpacing), 0.0f, -halfSize.y), vec3(-halfSize.x + (i * lineSpacing), 0.0f, halfSize.y), (i == 0 || i == horizontalLineCount - 1 ? MAJOR_LINE_COLOR_EDGE : MAJOR_LINE_COLOR_INNER));
	}

	// render major lines across y axis
	for(i = 0; i < verticalLineCount; ++ i)
	{
		lines -> add(vec3(-halfSize.x, 0.0f, -halfSize.y + (i * lineSpacing)), vec3(halfSize.x, 0.0f, -halfSize.y + (i * lineSpacing)), (i == 0 || i == verticalLineCount - 1 ? MAJOR_LINE_COLOR_EDGE : MAJOR_LINE_COLOR_INNER));
	}

	// render minor lines across x axis
	for(i = 0; i < (int)((horizontalLineCount - 1) * MINOR_LINE_MULTIPLIER); ++ i)
	{
		lines -> add(vec3(-halfSize.x + (i * lineSpacing / MINOR_LINE_MULTIPLIER), 0.0f, -halfSize.y), vec3(-halfSize.x + (i * lineSpacing / MINOR_LINE_MULTIPLIER), 0.0f, halfSize.y), MINOR_LINE_COLOR);
	}

	// render minor lines across y axis
	for(i = 0; i < (int)((verticalLineCount - 1) * MINOR_LINE_MULTIPLIER); ++ i)
	{
		lines -> add(vec3(-halfSize.x, 0.0f, -halfSize.y + (i * lineSpacing / MINOR_LINE_MULTIPLIER)), vec3(halfSize.x, 0.0f, -halfSize.y + (i * lineSpacing / MINOR_LINE_MULTIPLIER)), MINOR_LINE_COLOR);
	}

	// now render everything
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);

	lines -> send();
	lines -> render(viewProjection);
	lines -> clear();

	// axis lines
	lines -> add(CORNER, CORNER + vec3(AXIS_LINE_LENGTH, 0.0f, 0.0f), X_AXIS_COLOR);
	lines -> add(CORNER, CORNER + vec3(0.0f, AXIS_LINE_LENGTH, 0.0f), Y_AXIS_COLOR);
	lines -> add(CORNER, CORNER + vec3(0.0f, 0.0f, AXIS_LINE_LENGTH), Z_AXIS_COLOR);

	// now render axis lines
	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	lines -> send();
	lines -> render(viewProjection);
	lines -> clear();
	glDepthMask(GL_TRUE);
}
