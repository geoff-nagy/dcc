#pragma once

#include "objects/simulateddrone.h"

#include "glm/glm.hpp"

#include <stdint.h>
#include <string>

class SimulatedUBee : public SimulatedDrone
{
public:
	static const float BASE_THRUST;
	static const float MIN_THRUST;

	SimulatedUBee(uint32_t id);
	~SimulatedUBee();

	virtual void update(float dt);

private:
	float throttle;

	// anything else?
};
