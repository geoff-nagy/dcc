#pragma once

#include "objects/object.h"

#include "glm/glm.hpp"

class PhysicsObject : public Object
{
public:
	PhysicsObject(float mass);
	virtual ~PhysicsObject() = 0;

	void applyImpulse(const glm::vec3 &impulse, const glm::vec3 &contact);

	void setImpulse(const glm::vec3 &impulse);
	glm::vec3 getImpulse();

	virtual void update(float dt);

private:
	float mass;
	float inverseMass;

	glm::vec3 angularVelocity;

	glm::vec3 velocity;
	glm::vec3 impulse;
};
