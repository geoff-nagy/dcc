#pragma once

#include "glm/glm.hpp"

class Marker
{
public:
	Marker();
	Marker(const glm::vec3 &pos);
	Marker(const Marker &other);

	Marker& operator=( const Marker& other );

	uint32_t id;
	glm::vec3 pos;
	bool seen;
	glm::vec3 color;

	bool dirty;
};
