#pragma once

#include "objects/droneinterface.h"
#include "objects/flightcontrol.h"

#include "glm/glm.hpp"

#include <vector>
#include <queue>
#include <utility>
#include <map>

class SimulatedDroneSwarm;

class SimulationUBeeInterface : public DroneInterface
{
public:
	SimulationUBeeInterface(SimulatedDroneSwarm *droneSwarm, float delaySeconds);
	~SimulationUBeeInterface();

	// instead of sending the commands immediately, we put them in a time-delayed queue to simulate the delay in radio communications
	virtual void sendCommands(float time);

	// ping a drone or drones
	virtual void ping(uint32_t id);

private:
	void addNewCommands(float time);
	void transmitDelayedCommands(float time);

	SimulatedDroneSwarm *droneSwarm;
	float delaySeconds;

	std::queue<std::pair<float, std::map<uint32_t, FlightCommand> > > flightCommandQueue;
};
