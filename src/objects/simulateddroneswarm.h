#pragma once

#include "glm/glm.hpp"

#include <stdint.h>
#include <vector>
#include <map>

class SimulatedDrone;

class SimulatedDroneSwarm
{
public:
	SimulatedDroneSwarm();
	~SimulatedDroneSwarm();

	// add a new drone to the simulated swarm
	virtual void addDrone(SimulatedDrone *drone, const glm::vec3 &pos);

	// remove all drones from the simulated swarm
	virtual void clearDrones();

	// handle simulated communications with a specific drone
	virtual void sendFlightCommand(uint32_t id, float thrust, float roll, float pitch, float yaw);

	// update physics and control logic of the drones
	virtual void update(float dt);

	// these methods are needed to allow global sensors to detect these drones
	std::vector<SimulatedDrone*>::iterator getDronesBegin();
	std::vector<SimulatedDrone*>::iterator getDronesEnd();

private:
	std::vector<SimulatedDrone*> drones;
	std::map<uint32_t, SimulatedDrone*> dronesByID;
};
