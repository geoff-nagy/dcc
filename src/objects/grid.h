#pragma once

#include "GL/glew.h"

#include "glm/glm.hpp"

class Shader;

class Grid
{
public:
	Grid(int horizontalLineCount, int verticalLineCount, float lineSpacing);
	~Grid();

	void render(const glm::mat4 &viewProjection);

private:
	int horizontalLineCount;
	int verticalLineCount;
	int lineSpacing;
	glm::vec2 size;
	glm::vec2 halfSize;

	GLuint vao;
	GLuint vbos[1];

	Shader *shader;

	void setupVBOs(int horizontalLineCount, int verticalLineCount, float lineSpacing);
	void setupShader();
};
