#pragma once

#include "experiments/pairwiseflocking/pairwiseflockingneighbourentry.h"

#include "objects/obstacle.h"
#include "objects/droneinterface.h"

#include "sensing/dronedetection.h"

#include "util/pidcontroller.h"

#include "glm/glm.hpp"

#include <set>
#include <vector>

class Obstacle;
class DroneFlightController;

class PairwiseFlockingLabAgent
{
public:
	typedef enum STATE
	{
		STATE_LANDED = 0,
		STATE_TAKE_OFF,
		STATE_GAINING_HEIGHT,
		STATE_POSITIONING,
		STATE_FLYING,
		STATE_LANDING_SAFE,
		STATE_LANDING_BLIND
	} State;

	typedef enum FLOCKING_METRIC
	{
		METRIC_EUCLIDEAN_DISTANCE = 0,
		METRIC_TOPOLOGICAL_DISTANCE
	} FlockingMetric;

	static const double CATCH_UP_DISTANCE;

	PairwiseFlockingLabAgent(DroneFlightController *flightController, const DroneDetection &detection, float targetHeight, float viewRange, bool closePairingEnabled);
	~PairwiseFlockingLabAgent();

	// used by higher-level logic to update the detection of the drone
	void setDroneDetection(const DroneDetection &detection);
	DroneDetection getDroneDetection();
	float getTargetHeight();

	// used by higher-level logic to command the drone itself (simulated or real)
	FlightCommand getFlightCommand();
	bool isInPosition();
	bool isLanded();
	int getID();

	// various self-explanatory commands
	void takeOff();
	void landSafely();
	void scram();

	// main update routine; should be called regularly
	void update(float dt);

	// partner status
	void setPartnerID(int partnerID);
	int getPartnerID();
	bool getCatchingUpToPartner();
	bool getCanSeePartner();

	// enable or disable flocking
	void setInteractionsEnabled(bool interactionsEnabled);
	bool getInteractionsEnabled();

	// wall bounds handling
	void setBounds(const glm::vec2 &boundsArea, float boundsEdge);
	bool isCloseToBounds();

	// obstacle handling
	void clearObstacles();
	void storeObstacle(const Obstacle &obstacle);

	// neighbour handling
	void clearNeighbours();
	void storeNeighbour(const DroneDetection &neighbour);
	void sortAndLimitNeighbours(int numTrackedIfSingle, int numTrackedIfPaired);
	std::vector<DroneDetection>::iterator getNeighboursBegin();
	std::vector<DroneDetection>::iterator getNeighboursEnd();

	// debugging
	glm::vec3 getPos();
	glm::vec3 getVelocity();
	glm::vec2 getTargetXZVelocity();
	glm::vec2 getSteerAway();
	glm::vec3 getStartingPosition();
	glm::vec2 getPitchAndRollOutput();
	void setDetectionFail(bool detectionFail);
	bool getDetectionFail();

private:
	static const float TURN_RATE;						// how fast the agent can rotate in place, in radians; this combined with our FOV limits how fast the agent can move
	static const float TURN_CYCLE_TIME;					// total time (in sim steps) to complete one full rotation

	static const float SAFE_ZONE_BEGIN;					// pixel distance at which neighbours exert no force
	static const float SAFE_ZONE_END;					// pixel distance at which neighbours begin exterting an attractive force
	static const float RANGE_END;						// pixel distance at which neighbours are no longer considered

	static const float MAX_VELOCITY;					// determined by TURN_RATE and FOV_RADIANS

	// - - - interaction forces - - - //

	bool boundsEnabled;									// off by default
	glm::vec2 boundsArea;								// area we don't want to fly out of
	float boundsEdge;									// distance from bounds at which we start to experience a repulsive force
	bool closeToBounds;

	int numAttractions;									// attraction force control
	glm::vec2 attractVec;

	int numRepulsions;									// repulsion force control
	glm::vec2 repulseVec;

	int numAlignments;									// alignment force control
	glm::vec2 alignVec;

	int numAvoidances;									// steering force away from obstacles
	glm::vec2 avoidVec;

	int numWallForces;
	glm::vec2 wallVec;

	float obstacleForce;

	// - - - drone flight control - - - //

	int id;
	DroneFlightController *flightController;
	State state;
	DroneDetection detection;
	FlightCommand flightCommand;
	float detectionAge;
	float targetHeight;
	float landingBlindTimer;
	float oldMinThrust;

	glm::vec2 targetXZVelocity;							// social forces result
	float directionTimer;								// used to encourage drones to go to one end of the lab

	glm::vec3 startingPosition;							// used so that each drone begins flocking from a specified, known starting position
	bool inPosition;

	bool detectionFail;

	// - - - flocking settings - - - //

	float viewRange;									// maximum range at which we can see other neighbours, in meters
	bool closePairing;									// do we snuggle up close to our mate?

	bool interactionsEnabled;							// do we flock and avoid obstacles?
	int partnerID;										// or -1 if no partner
	bool canSeePartner;									// is our partner visible/in range?
	float partnerDist;									// distance to partner, if we have one
	bool catchingUpToPartner;							// are we catching up to our partner because we're too separated?
	std::vector<DroneDetection> neighbours;				// other interaction partners we may want to consider
	std::vector<Obstacle> obstacles;					// obstacles we've detected
	glm::vec2 steerAway;

	// - - - methods - - - //

	// state update methods
	void updateStateLanded(float dt);
	void updateStateTakeOff(float dt);
	void updateStateGainingHeight(float dt);
	void updateStatePositioning(float dt);
	void updateStateFlying(float dt);
	void updateStateLandingSafe(float dt);
	void updateStateLandingBlind(float dt);

	// flight control methods
	void updateFlightControl(float dt);
	void updateInteractions(float dt);
	void handleBlindLanding(float dt);
	void resetInteractions();
	void interactWith(const DroneDetection &agent, int index);
	void interactWith(const Obstacle &obj);
	void keepInBounds();
};

