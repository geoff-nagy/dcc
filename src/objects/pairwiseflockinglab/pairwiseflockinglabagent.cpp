#include "objects/pairwiseflockinglab/pairwiseflockinglabagent.h"

#include "objects/obstacle.h"
#include "objects/droneflightcontroller.h"

#include "util/mymath.h"
#include "util/pidcontroller.h"
#include "util/log.h"

#include "glm/glm.hpp"
#include "glm/gtc/random.hpp"
#include "glm/gtx/norm.hpp"
using namespace glm;

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

// - - - prototypes - - - //

// this is used to sort neighbours by distance that we're tracking
static bool sortNeighboursByDistance(const DroneDetection &a, const DroneDetection &b);

// - - - constants - - - //

const double PairwiseFlockingLabAgent::CATCH_UP_DISTANCE = 1.0f;//80.0f;//0.5f;

// [todo]: this is unused; remove
const float PairwiseFlockingLabAgent::MAX_VELOCITY = 0.4f;//(((M_PI * 2.0f) - FOV_RADIANS) / TURN_RATE);// * 0.5f;

// - - - implementation - - - //

PairwiseFlockingLabAgent::PairwiseFlockingLabAgent(DroneFlightController *flightController, const DroneDetection &detection, float targetHeight, float viewRange, bool closePairing)
	: flightController(flightController), state(STATE_LANDED), targetHeight(targetHeight), landingBlindTimer(0.0f), viewRange(viewRange), closePairing(closePairing)
{
	// set some defaults
	partnerID = -1;
	canSeePartner = false;
	interactionsEnabled = false;
	catchingUpToPartner = false;
	boundsEnabled = false;
	boundsEdge = 0.0f;
	closeToBounds = false;
	directionTimer = 0.0f;
	inPosition = false;
	detectionFail = false;

	// prime IDs and our first detection
	setDroneDetection(detection);
	id = detection.id;

	// set default flight command
	flightCommand.id = id;
	flightCommand.thrust = 0.0f;
	flightCommand.roll = 0.0f;
	flightCommand.pitch = 0.0f;
	flightCommand.yaw = 0.0f;
}

PairwiseFlockingLabAgent::~PairwiseFlockingLabAgent()
{
	delete flightController;
}

void PairwiseFlockingLabAgent::setDroneDetection(const DroneDetection &detection)
{
	this -> detection = detection;
	detectionAge = 0.0f;
}

DroneDetection PairwiseFlockingLabAgent::getDroneDetection()
{
	return detection;
}

float PairwiseFlockingLabAgent::getTargetHeight()
{
	return targetHeight;
}

FlightCommand PairwiseFlockingLabAgent::getFlightCommand()
{
	return flightCommand;
}

bool PairwiseFlockingLabAgent::isInPosition()
{
	return inPosition;
}

bool PairwiseFlockingLabAgent::isLanded()
{
	return state == STATE_LANDED;
}

int PairwiseFlockingLabAgent::getID()
{
	return id;
}

void PairwiseFlockingLabAgent::takeOff()
{
	if(state == STATE_LANDED)
	{
		state = STATE_TAKE_OFF;
	}
}

void PairwiseFlockingLabAgent::landSafely()
{
	if(state != STATE_LANDING_BLIND)
	{
		state = STATE_LANDING_SAFE;
	}
}

void PairwiseFlockingLabAgent::scram()
{
	LOG_INFO("    [PFLA]: PairwiseFlockingLabAgent " << id << " is SCRAMing");
	state = STATE_LANDED;
}

void PairwiseFlockingLabAgent::update(float dt)
{
	// land blind if tracking is lost
	handleBlindLanding(dt);

	// update based on state
	if     (state == STATE_LANDED) updateStateLanded(dt);
	else if(state == STATE_TAKE_OFF) updateStateTakeOff(dt);
	else if(state == STATE_GAINING_HEIGHT) updateStateGainingHeight(dt);
	else if(state == STATE_POSITIONING) updateStatePositioning(dt);
	else if(state == STATE_FLYING) updateStateFlying(dt);
	else if(state == STATE_LANDING_SAFE) updateStateLandingSafe(dt);
	else if(state == STATE_LANDING_BLIND) updateStateLandingBlind(dt);
}

void PairwiseFlockingLabAgent::handleBlindLanding(float dt)
{
/*
	const float MAX_DETECTION_AGE_SEC = 3.0f;
	const int MAX_DETECTION_AGE_FRAMES = 512;

	const float LANDING_BLIND_TIME = 3.0f;

	// age our detection; is it up to date?
	detectionAge += dt;
	if(detectionAge >= MAX_DETECTION_AGE_SEC || detection.age >= MAX_DETECTION_AGE_FRAMES)
	{
		// land blindly if we've not already tried to do so
		if(state != STATE_LANDING_BLIND && state != STATE_LANDED)
		{
			// start timer for blind landing
			state = STATE_LANDING_BLIND;
			landingBlindTimer = LANDING_BLIND_TIME;

			// notify the user
			LOG_INFO("    [PFLA]: PairwiseFlockingLabAgent " << id << " is going to land blind for safety ***");
		}
	}*/
}

void PairwiseFlockingLabAgent::updateStateLanded(float dt)
{
	// nullify flight command
	flightCommand.id = id;
	flightCommand.thrust = 0.0f;
	flightCommand.roll = 0.0f;
	flightCommand.pitch = 0.0f;
	flightCommand.yaw = 0.0f;
}

void PairwiseFlockingLabAgent::updateStateTakeOff(float dt)
{
	const float MAX_TAKEOFF_SPEED = 0.75f;//1.5f;

	startingPosition = vec3(detection.pos.x, targetHeight, detection.pos.z);
	flightController -> setTargetPosition(startingPosition);
	flightController -> setMaxTargetYSpeed(MAX_TAKEOFF_SPEED);				// take off slower and in control

	// transition to gaining height stage
	state = STATE_GAINING_HEIGHT;

	// notify user
	LOG_INFO("    [PFLA]: PairwiseFlockingLabAgent " << id << " is taking off");
}

void PairwiseFlockingLabAgent::updateStateGainingHeight(float dt)
{
	const float OFF_GROUND_HEIGHT = targetHeight - 0.1f;

	// if we're high enough off the ground, go to flying state
	if(detection.pos.y >= OFF_GROUND_HEIGHT)
	{
		// go to starting position state
		state = STATE_POSITIONING;

		// notify user
		LOG_INFO("    [PFLA]: PairwiseFlockingLabAgent " << id << " has reached minimum flight height; moving into position");
	}

	// control flight
    updateFlightControl(dt);
}

void PairwiseFlockingLabAgent::updateStatePositioning(float dt)
{
	const float MAX_STARTING_POSITION_TOLERANCE = 0.25f;

	vec2 xzDetection(detection.pos.x, detection.pos.z);
	vec2 xzTarget(startingPosition.x, startingPosition.z);
	float dist = length(xzDetection - xzTarget);

	// if we're close enough to our starting position and the master controller has said we can start flocking, do it
	if(dist <= MAX_STARTING_POSITION_TOLERANCE)
	{
		// notify user
		if(!inPosition)
		{
			LOG_INFO("    [PFLA]: PairwiseFlockingLabAgent " << id << " has reached starting position");
		}

		// indicate that we're in position
		inPosition = true;

		// if the master controller says we can start flocking, do it
		if(interactionsEnabled)
		{
			// go to normal flying state
			state = STATE_FLYING;

			// notify user
			LOG_INFO("    [PFLA]: PairwiseFlockingLabAgent " << id << " is beginning flocking");
		}
	}
	else
	{
		inPosition = false;
	}

	// control flight
	updateFlightControl(dt);
}

void PairwiseFlockingLabAgent::updateStateFlying(float dt)
{
	const float MAX_FORCE_DIRECTION_TIME = 2.0f;

	// drones move in one direction for a brief amount of time (to the other end of the lab)
	directionTimer += dt;
	if(directionTimer < MAX_FORCE_DIRECTION_TIME)
	{
		targetXZVelocity = vec2(0.2f, 0.0f);
	}

	// handle pairwise flocking logic
	updateInteractions(dt);

	// nothing special happens here
	updateFlightControl(dt);
}

void PairwiseFlockingLabAgent::updateStateLandingSafe(float dt)
{
	const float LANDING_HEIGHT = 0.14f;
	const float LANDING_HEIGHT_TOLERANCE = 0.03f;
	const float LANDING_SPEED = 0.1f;

	// move to ground
	flightController -> setTargetPosition(vec3(detection.pos.x, LANDING_HEIGHT, detection.pos.z));
	flightController -> setMaxTargetYSpeed(LANDING_SPEED);			// land slow

	// if we're low enough, cut throttle
	if(detection.pos.y <= LANDING_HEIGHT + LANDING_HEIGHT_TOLERANCE)
	{
		// land and notify user
		state = STATE_LANDED;
		LOG_INFO("    [PFLA]: PairwiseFlockingLabAgent " << id << " has landed safely");
	}

	// control flight
	updateFlightControl(dt);
}

void PairwiseFlockingLabAgent::updateStateLandingBlind(float dt)
{
	const float BLIND_LANDING_THROTTLE = 0.4f;

	// do not control flight; just set flight command to something approximating a gentle landing for a fixed time
	landingBlindTimer -= dt;
	if(landingBlindTimer <= 0.0f)
	{
		// this will cut throttle
		state = STATE_LANDED;
		LOG_INFO("    [PFLA]: PairwiseFlockingLabAgent " << id << " has completed emergency landing");
	}
	else
	{
		// manually set to a gentle-ish landing
		flightCommand.id = id;
		flightCommand.thrust = BLIND_LANDING_THROTTLE;
		flightCommand.roll = 0.0f;
		flightCommand.pitch = 0.0f;
		flightCommand.yaw = 0.0f;
	}
}

void PairwiseFlockingLabAgent::updateFlightControl(float dt)
{
	const float MAX_TARGET_XZ_SPEED = 0.2f;
	const float MAX_TARGET_Y_SPEED = 0.3f;
	const float MAX_YAW_SPEED = 1.0f;
	const float TARGET_HEADING = 0.0f;

	const float SANITY_CHECK_MAX_VELOCITY = 0.2f;

	float speed;

    // set flight parameters
	flightController -> setMaxTargetXZSpeed(MAX_TARGET_XZ_SPEED);
	flightController -> setMaxTargetYSpeed(MAX_TARGET_Y_SPEED);
	flightController -> setMaxTargetYawSpeed(MAX_YAW_SPEED);
	flightController -> setTargetHeading(TARGET_HEADING);

	// due to tracking errors, make sure our detection velocity doesn't erroneously return something hideously fast;
	// this can occur if tracking is momentarily lost and then re-acquired; although factoring in delta time in between
	// subsequent successful detections in the TrackingMatcher usually compensates for this, there are some rare
	// occasions where it does not; I think this might occur when detections coming in from the network are somewhat
	// delayed and then spewed out in rapid succession, but I can't be sure; in any case, a simple sanity check can
	// work around this; this prevents extreme corrections to extreme perceived velocities that aren't actually there
    speed = length(detection.velocity);
    if(speed > SANITY_CHECK_MAX_VELOCITY)
    {
		// normalize and then re-scale to max sane velocity
		detection.velocity = (detection.velocity / speed) * SANITY_CHECK_MAX_VELOCITY;
    }

	// update flight controller and receive instructions
	flightController -> update(detection, dt);
	flightCommand = flightController -> getLastFlightCommand();
	flightCommand.id = id;
}

void PairwiseFlockingLabAgent::setPartnerID(int id)
{
	partnerID = id;
}

int PairwiseFlockingLabAgent::getPartnerID()
{
	return partnerID;
}

bool PairwiseFlockingLabAgent::getCatchingUpToPartner()
{
	return catchingUpToPartner;
}

bool PairwiseFlockingLabAgent::getCanSeePartner()
{
	return canSeePartner;
}

void PairwiseFlockingLabAgent::setInteractionsEnabled(bool interactionsEnabled)
{
	this -> interactionsEnabled = interactionsEnabled;
}

bool PairwiseFlockingLabAgent::getInteractionsEnabled()
{
	return interactionsEnabled;
}

void PairwiseFlockingLabAgent::setBounds(const vec2 &boundsArea, float boundsEdge)
{
	boundsEnabled = true;
	this -> boundsArea = boundsArea;
	this -> boundsEdge = boundsEdge;
}

bool PairwiseFlockingLabAgent::isCloseToBounds()
{
	return closeToBounds;
}

void PairwiseFlockingLabAgent::clearObstacles()
{
	obstacles.clear();
}

void PairwiseFlockingLabAgent::storeObstacle(const Obstacle &obj)
{
	obstacles.push_back(obj);
}

void PairwiseFlockingLabAgent::clearNeighbours()
{
	neighbours.clear();
	canSeePartner = false;
}

void PairwiseFlockingLabAgent::storeNeighbour(const DroneDetection &other)
{
	neighbours.push_back(other);
	if(partnerID == (int)other.id)
	{
		canSeePartner = true;
		partnerDist = other.dist;
	}
}

void PairwiseFlockingLabAgent::sortAndLimitNeighbours(int numTrackedIfSingle, int numTrackedIfPaired)
{
	const int NUM_NEIGHBOURS = (partnerID >= 0 && canSeePartner ? numTrackedIfPaired : numTrackedIfSingle);

	DroneDetection partner;
	vector<DroneDetection>::iterator i;
	int j;

	catchingUpToPartner = false;

	// remove the partner entry if we have one
	if(partnerID >= 0 && canSeePartner)
	{
		// store for re-insertion at the front of the list later on
		partner = neighbours[0];
		if((int)partner.id != partnerID)
		{
			// make sure this is actually our partner!
			LOG_INFO("this is not correct; " << id << "'s partnerID is " << partnerID << " but read partner.id is " << partner.id);
			LOG_INFO("neighbours.size(): " << neighbours.size());
		}
		neighbours.erase(neighbours.begin(), neighbours.begin() + 1);

		// if we're going to lose sight of our partner, ignore everyone else
		if(partnerDist > CATCH_UP_DISTANCE)
		{
			// [note]: comment this out to disable the catch-up behaviour
			//neighbours.clear();				// remember, we re-add our neighbour later
			//catchingUpToPartner = true;		// indicate we're catching up
		}
	}

	// sort the neighbours by their distance from us; note that we have to use sortNeighboursByDistance() since the DroneDetection
	// less-than operator is already defined and that we sort in NON-DESCENDING ORDER so when we get rid of the last N
	// neigbours in the code block below, we're left with the closest ones
	sort(neighbours.begin(), neighbours.end(), sortNeighboursByDistance);

	// erase all but the closest neighbours
	for(j = 0, i = neighbours.begin(); j < NUM_NEIGHBOURS && i != neighbours.end(); ++ j, ++ i);		// skip over NUM_NEIGHBOURS entries; these are closest to us
	neighbours.erase(i, neighbours.end());																// erase remaining entries

	// re-add our partner at the beginning of the list
	if(partnerID >= 0 && canSeePartner)
	{
		neighbours.insert(neighbours.begin(), partner);
	}
}

void PairwiseFlockingLabAgent::updateInteractions(float dt)
{
	// social forces gains
	const float ATTRACT_GAIN = 0.05f;
	const float REPEL_GAIN = 0.6f;//0.6f;//0.4f;//0.7f;//0.6f;//0.3f;//0.2f;
	const float ALIGN_GAIN = 0.2f;//0.05f;//0.1f;//0.4f;//0.1f;
	const float OBSTACLE_AVOID_GAIN = 0.12f;//0.15f;//0.075f;//0.05f;//0.2f;//2.5f;
	const float WALL_FORCE_GAIN = 0.5f;//7.0f;
	const float EAST_FORCE_GAIN = 0.04f;//0.01f;//0.01f;
	const float CONSTANT_SPEED = 0.2f;

	vector<DroneDetection>::iterator i;
	vector<Obstacle>::iterator j;
	vec3 targetPos;
	//float mag;
	int k;

	// zero out all interaction forces for this new frame
	resetInteractions();

	// interact with neighbours and obstacles if enabled
	if(interactionsEnabled)
	{
		// now interact with all neighbours, including the partner
		for(i = neighbours.begin(), k = 1; i != neighbours.end(); ++ i, ++ k)
		{
			interactWith(*i, k);
		}

		// now interact with all obstacles
		steerAway = vec2(0.0f);
		for(j = obstacles.begin(); j != obstacles.end(); ++ j)
		{
			interactWith(*j);
		}
		avoidVec = steerAway;

		// also keep us in bounds
		keepInBounds();

		// accumulate interaction forces
		//if(numAvoidances == 0)
		{
			if(numAttractions) targetXZVelocity = (targetXZVelocity + (attractVec / (float)numAttractions) * ATTRACT_GAIN) * obstacleForce;
			if(numRepulsions)  targetXZVelocity = (targetXZVelocity + (repulseVec / (float)numRepulsions) * REPEL_GAIN) * obstacleForce;
			if(numAlignments)  targetXZVelocity = (targetXZVelocity + (alignVec / (float)numAlignments) * ALIGN_GAIN) * obstacleForce;
			if(numAvoidances)  targetXZVelocity = (targetXZVelocity + (avoidVec / (float)numAvoidances) * OBSTACLE_AVOID_GAIN);// * (1.0f - obstacleForce);
			if(numWallForces)  targetXZVelocity = (targetXZVelocity + (wallVec / (float)numWallForces) * WALL_FORCE_GAIN);
		}
		//else
		{
			//targetXZVelocity = circularRand(1.0f);
		}

		// add eastwards force if we're not catching up to partner
		if(!catchingUpToPartner)
		{
			targetXZVelocity = targetXZVelocity + (vec2(1.0f, 0.0f) * EAST_FORCE_GAIN);
		}

		// move at a fixed speed
		targetXZVelocity = normalize(targetXZVelocity) * CONSTANT_SPEED;

		// add some random motion if we're not moving
		/*
		mag = length(targetXZVelocity);
		if(mag == 0.0f)
		{
			// just set on trajectory to east side of lab
			targetXZVelocity = vec2(1.0f, 0.0f) * EAST_FORCE_GAIN;
		}
		else
		{
			// move at a fixed speed
			targetXZVelocity = normalize(targetXZVelocity) * CONSTANT_SPEED;
		}*/
	}
	else
	{
		// set target velocity to nothing
		targetXZVelocity = vec2(0.0f);
	}

	// this is just used to enforce a target height
	targetPos = vec3(detection.pos.x, targetHeight, detection.pos.z);

	// position-control mode enforces a target height
	flightController -> setTargetPosition(targetPos);

	// now switch to velocity-control mode to move in desired direction while keeping height fixed
	flightController -> setTargetXZVelocity(targetXZVelocity);
}

// debugging

void PairwiseFlockingLabAgent::resetInteractions()
{
	numAttractions = 0;
	attractVec = vec3(0.0f);
	numRepulsions = 0;
	repulseVec = vec3(0.0f);
	numAlignments = 0;
	alignVec = vec3(0.0f);
	numAvoidances = 0;
	avoidVec = vec3(0.0f);
	numWallForces = 0;
	wallVec = vec3(0.0f);
	obstacleForce = 1.0f;
}

void PairwiseFlockingLabAgent::interactWith(const DroneDetection &other, int index)
{
	// [note]: I suspect that there is some optimal CLOSE_REPEL value that's partially a function of view range
	// that helps keep the flock together when pairing is present
	const float BASE_REPEL = 1.0f;//1.0f;//0.6f;//1.2f;//80.0f;
	const float CLOSE_REPEL = 0.75f;//0.5f;// 0.3f;//0.3f;//20.0f;			// 0.6 for 150cm, 0.75 for 200cm
	const float REPEL_DIST = ((int)other.id == partnerID && canSeePartner && closePairing ? CLOSE_REPEL : BASE_REPEL);
	const float ALIGN_DIST = viewRange;

	const float PARTNER_ATTRACT_WEIGHT = 0.6f;//0.4f;//0.2f;				// only active if we see our mate and close pairing is enabled; see below

	vec2 otherXZPos = vec2(other.pos.x, other.pos.z);

	// if close enough, attract to neighbour (or if this is a partner that we can see)
	//if(other.dist < ATTRACT_DIST)// || (canSeePartner && (int)other.id == partnerID) || partnerID == -1)
	{
		// if we've got a partner that we're close to losing, ignore all other attraction forces
		if(!catchingUpToPartner || ((int)other.id == partnerID && canSeePartner))
		{
			if(partnerID == -1 ||																	// we have no partner OR
			  (canSeePartner && partnerDist < CATCH_UP_DISTANCE && (int)other.id != partnerID) ||	// we do, but this is not them OR
			  (canSeePartner && (int)other.id == partnerID))										// we do, we can see them, and this is them
			{
				// mates experience a stronger attractional pull, but regular agents do not
				numAttractions ++;
				attractVec += normalize(otherXZPos) * (1.0f / (float)index) * ((int)other.id == partnerID && canSeePartner && closePairing ? PARTNER_ATTRACT_WEIGHT : 1.0f);
			}
		}
	}

	// if too close, repel away from neighbour
	if(other.dist < REPEL_DIST)
	{
		// repulsion force scales down linearly with distance
		numRepulsions ++;
		repulseVec += normalize(-otherXZPos) * (1.0f - (other.dist / REPEL_DIST));
	}

	// if close enough, align velocity with neighbour; note that our neighbour's velocity is in our frame of reference,
	// so we have to transform it into our global frame of reference (for which we have our own velocity, and this is
	// reasonable since drones can measure this on their own without a global sensor)
	if(other.dist < ALIGN_DIST)
	{
		//numAlignments = 1;
		numAlignments ++;
		alignVec += (vec2(other.velocity.x, other.velocity.z) + vec2(detection.velocity.x, detection.velocity.z)) * (1.0f / (float)index);
	}
}

void PairwiseFlockingLabAgent::interactWith(const Obstacle &obj)
{
	vec2 diff = vec2(obj.pos.x, obj.pos.z);
	float dist = length(diff);
	vec2 side;

	if(dist < obj.avoidRadius)
	{
		side = vec2(-detection.velocity.y, detection.velocity.x);
		if(dot(side, vec2(obj.pos.x, obj.pos.z)) > 0.0f)
		{
			steerAway = -side;
		}
		else
		{
			steerAway = side;
		}

		steerAway = normalize(-normalize(diff) + circularRand(0.5f));

		steerAway *= (1.0f - (dist / obj.avoidRadius));

		avoidVec += steerAway;
		numAvoidances = 1;
	}
}

void PairwiseFlockingLabAgent::keepInBounds()
{
	const float MIN_X = (-boundsArea.x / 2.0f) + boundsEdge;
	const float MAX_X = (boundsArea.x / 2.0f) - boundsEdge;
	const float MIN_Z = (-boundsArea.y / 2.0f) + boundsEdge;
	const float MAX_Z = (boundsArea.y / 2.0f) - boundsEdge;

	// this adds a repulsion force away from the outer edges of the world when the agent gets too close
	closeToBounds = false;
	if(boundsEnabled)
	{
		if(detection.pos.x < MIN_X)
		{
			++ numWallForces;
			wallVec += vec2(1.0f, 0.0) * ((fabs(detection.pos.x) - fabs(MIN_X)) / boundsEdge);
			//closeToBounds = true;
		}

		if(detection.pos.x > MAX_X)
		{
			++ numWallForces;
			wallVec += vec2(-1.0f, 0.0) * ((detection.pos.x - MAX_X) / boundsEdge);
			closeToBounds = true;			// we hit the east wall; use this as a visual indicator to stop the trial
		}

		if(detection.pos.z < MIN_Z)
		{
			++ numWallForces;
			wallVec += vec2(0.0f, 1.0) * ((fabs(detection.pos.z) - fabs(MIN_Z)) / boundsEdge);
			//closeToBounds = true;
		}

		if(detection.pos.z > MAX_Z)
		{
			++ numWallForces;
			wallVec += vec2(0.0f, -1.0) * ((detection.pos.z - MAX_Z) / boundsEdge);
			//closeToBounds = true;
		}
	}
}

vector<DroneDetection>::iterator PairwiseFlockingLabAgent::getNeighboursBegin()
{
	return neighbours.begin();
}

vector<DroneDetection>::iterator PairwiseFlockingLabAgent::getNeighboursEnd()
{
	return neighbours.end();
}

vec3 PairwiseFlockingLabAgent::getPos()
{
	return detection.pos;
}

vec3 PairwiseFlockingLabAgent::getVelocity()
{
	return detection.velocity;
}

vec2 PairwiseFlockingLabAgent::getTargetXZVelocity()
{
	return flightController -> getTargetXZSpeed();
}

vec2 PairwiseFlockingLabAgent::getPitchAndRollOutput()
{
	return flightController -> getPitchAndRollOutput();
}

void PairwiseFlockingLabAgent::setDetectionFail(bool detectionFail)
{
	this -> detectionFail = detectionFail;
}

bool PairwiseFlockingLabAgent::getDetectionFail()
{
	return detectionFail;
}

vec2 PairwiseFlockingLabAgent::getSteerAway()
{
	return steerAway;
}

vec3 PairwiseFlockingLabAgent::getStartingPosition()
{
	return startingPosition;
}

// - - - other functions - - - //

// agents are sorted in non-descending distance
bool sortNeighboursByDistance(const DroneDetection &a, const DroneDetection &b)
{
	return a.dist < b.dist;
}
