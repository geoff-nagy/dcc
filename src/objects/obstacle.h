#pragma once

#include "glm/glm.hpp"

class Obstacle
{
public:
	Obstacle(const glm::vec3 &pos, float radius, float avoidRadius);
	~Obstacle();

	glm::vec3 pos;
	float radius;
	float avoidRadius;
	bool obscuring;
};
