#include "objects/box/boxagent.h"

#include "objects/droneflightcontroller.h"
#include "objects/droneinterface.h"

#include "sensing/dronedetection.h"

#include "util/log.h"

#include "glm/glm.hpp"
using namespace glm;

#include <vector>
using namespace std;

BoxAgent::BoxAgent(DroneFlightController *flightController, const DroneDetection &detection, float targetHeight, vector<vec3> &targetVertices)
	: flightController(flightController), state(STATE_LANDED), targetHeight(targetHeight), landingBlindTimer(0.0f), targetVertices(targetVertices), vertexIndex(0)
{
	// prime IDs and our first detection
	setDroneDetection(detection);
	id = detection.id;

	// set default flight command
	flightCommand.id = 0;
	flightCommand.thrust = 0.0f;
	flightCommand.roll = 0.0f;
	flightCommand.pitch = 0.0f;
	flightCommand.yaw = 0.0f;
}

BoxAgent::~BoxAgent()
{
	delete flightController;
}

void BoxAgent::setDroneDetection(const DroneDetection &detection)
{
	this -> detection = detection;
	detectionAge = 0.0f;
}

FlightCommand BoxAgent::getFlightCommand()
{
	return flightCommand;
}

bool BoxAgent::isLanded()
{
	return state == STATE_LANDED;
}

int BoxAgent::getID()
{
	return id;
}

int BoxAgent::getTargetVertexIndex()
{
	return vertexIndex;
}

DroneDetection BoxAgent::getDetection()
{
	return detection;
}

DroneFlightController *BoxAgent::getFlightController()
{
	return flightController;
}

void BoxAgent::takeOff()
{
	if(state == STATE_LANDED)
	{
		state = STATE_TAKE_OFF;
	}
}

void BoxAgent::landSafely()
{
	if(state != STATE_LANDING_BLIND)
	{
		state = STATE_LANDING_SAFE;
	}
}

void BoxAgent::scram()
{
	LOG_INFO("    [BOXA]: BoxAgent " << id << " is SCRAMing");
	state = STATE_LANDED;
}

void BoxAgent::update(float dt)
{
	const float MAX_DETECTION_AGE_SEC = 3.0f;

	const float LANDING_BLIND_TIME = 3.0f;

	// age our detection; is it up to date?
	detectionAge += dt;
	if(detectionAge >= MAX_DETECTION_AGE_SEC || detection.age >= MAX_DETECTION_AGE_SEC)
	{
		// land blindly if we've not already tried to do so
		if(state != STATE_LANDING_BLIND && state != STATE_LANDED)
		{
			// start timer for blind landing
			state = STATE_LANDING_BLIND;
			landingBlindTimer = LANDING_BLIND_TIME;

			// notify the user
			LOG_INFO("    [BOXA]: BoxAgent " << id << " is going to land blind for safety ***");
		}
	}

	// update based on state
	if     (state == STATE_LANDED) updateStateLanded(dt);
	else if(state == STATE_TAKE_OFF) updateStateTakeOff(dt);
	else if(state == STATE_GAINING_HEIGHT) updateStateGainingHeight(dt);
	else if(state == STATE_FLYING) updateStateFlying(dt);
	else if(state == STATE_LANDING_SAFE) updateStateLandingSafe(dt);
	else if(state == STATE_LANDING_BLIND) updateStateLandingBlind(dt);
}

void BoxAgent::updateStateLanded(float dt)
{
	// nullify flight command
	flightCommand.id = id;
	flightCommand.thrust = 0.0f;
	flightCommand.roll = 0.0f;
	flightCommand.pitch = 0.0f;
	flightCommand.yaw = 0.0f;
}

void BoxAgent::updateStateTakeOff(float dt)
{
	//const float MAX_TARGET_Y_SPEED = 10.0f;					// set to something fast so we take off fast

	// set full throttle
	//oldMinThrust = flightController -> getMinThrust();
	//flightController -> setMinThrust(1.0f);
	//flightController -> setBaseThrust(flightController -> getBaseThrust());
	//flightController -> setMaxTargetYSpeed(MAX_TARGET_Y_SPEED);			// can be overriden using min thrust if necessary

	// target a position directly above
	flightController -> setTargetPosition(vec3(detection.pos.x, targetHeight, detection.pos.z));

	// transition to gaining height stage
	state = STATE_GAINING_HEIGHT;

	// notify user
	LOG_INFO("    [BOXA]: BoxAgent " << id << " is taking off");
}

void BoxAgent::updateStateGainingHeight(float dt)
{
	const float OFF_GROUND_HEIGHT = targetHeight - 0.1f;
	//const float MAX_TARGET_Y_SPEED = 0.4f;					// set to something fast so we take off fast

	// if we're high enough off the ground, go to flying state
	if(detection.pos.y >= OFF_GROUND_HEIGHT)
	{
		// go to normal flying state
		state = STATE_FLYING;

		// set thrust to approx. hover amount
		//flightController -> setMinThrust(oldMinThrust);
		//flightController -> setMaxTargetYSpeed(MAX_TARGET_Y_SPEED);

		// notify user
		LOG_INFO("    [BOXA]: BoxAgent " << id << " has reached minimum flight height");
	}

	// control flight
    updateFlightControl(dt);
}

void BoxAgent::updateStateFlying(float dt)
{
	// nothing special happens here
	updateFlightControl(dt);

	// update vertex selection
	updateVertexSelection(dt);
}

void BoxAgent::updateStateLandingSafe(float dt)
{
	const float LANDING_HEIGHT = 0.12f;
	const float LANDING_SPEED = 0.1f;

	// move to ground
	flightController -> setTargetPosition(vec3(detection.pos.x, LANDING_HEIGHT, detection.pos.z));
	flightController -> setMaxTargetYSpeed(LANDING_SPEED);			// land slow

	// if we're low enough, cut throttle
	if(detection.pos.y <= LANDING_HEIGHT)
	{
		// land and notify user
		state = STATE_LANDED;
		LOG_INFO("    [BOXA]: BoxAgent " << id << " has landed safely");
	}

	// control flight
	updateFlightControl(dt);
}

void BoxAgent::updateStateLandingBlind(float dt)
{
	const float BLIND_LANDING_THROTTLE = 0.4f;

	// do not control flight; just set flight command to something approximating a gentle landing for a fixed time
	landingBlindTimer -= dt;
	if(landingBlindTimer <= 0.0f)
	{
		// this will cut throttle
		state = STATE_LANDED;
		LOG_INFO("    [BOXA]: BoxAgent " << id << " has completed emergency landing");
	}
	else
	{
		// manually set to a gentle-ish landing
		flightCommand.id = id;
		flightCommand.thrust = BLIND_LANDING_THROTTLE;
		flightCommand.roll = 0.0f;
		flightCommand.pitch = 0.0f;
		flightCommand.yaw = 0.0f;
	}
}

void BoxAgent::updateFlightControl(float dt)
{
	const float MAX_TARGET_XZ_SPEED = 0.5f;
	const float MAX_TARGET_Y_SPEED = 0.4f;
	const float MAX_YAW_SPEED = 1.0f;
	const float TARGET_HEADING = 0.0f;

    // set flight parameters
	flightController -> setMaxTargetXZSpeed(MAX_TARGET_XZ_SPEED);
	flightController -> setMaxTargetYSpeed(MAX_TARGET_Y_SPEED);
	flightController -> setMaxTargetYawSpeed(MAX_YAW_SPEED);
	flightController -> setTargetHeading(TARGET_HEADING);

	// update flight controller and receive instructions
	flightController -> update(detection, dt);
	flightCommand = flightController -> getLastFlightCommand();
	flightCommand.id = id;
}

void BoxAgent::updateVertexSelection(float dt)
{
	const float CLOSE_ENOUGH_DIST = 0.1f;

	float dist;

	// if we're close enough to the current target, advance to next target
	flightController -> setTargetPosition(targetVertices[vertexIndex]);
	dist = length((targetVertices[vertexIndex]) - detection.pos);
	if(dist <= CLOSE_ENOUGH_DIST)
	{
		++ vertexIndex;
		if(vertexIndex >= (int)targetVertices.size())
		{
			vertexIndex = 0;
		}
		flightController -> setTargetPosition(targetVertices[vertexIndex]);
	}
}
