#pragma once

#include "sensing/dronedetection.h"

#include "objects/droneinterface.h"

#include "glm/glm.hpp"

#include <vector>

class DroneFlightController;

class BoxAgent
{
public:
	typedef enum STATE
	{
		STATE_LANDED = 0,
		STATE_TAKE_OFF,
		STATE_GAINING_HEIGHT,
		STATE_FLYING,
		STATE_LANDING_SAFE,
		STATE_LANDING_BLIND
	} State;

	BoxAgent(DroneFlightController *flightController, const DroneDetection &detection, float targetHeight, std::vector<glm::vec3> &targetVertices);
	~BoxAgent();

	// used by higher-level logic to update the detection of the drone
	void setDroneDetection(const DroneDetection &detection);

	// used by higher-level logic to command the drone itself (simulated or real)
	FlightCommand getFlightCommand();
	bool isLanded();
	int getID();
	int getTargetVertexIndex();
	DroneDetection getDetection();
	DroneFlightController *getFlightController();

	// various self-explanatory commands
	void takeOff();
	void landSafely();
	void scram();

	// called to update the agent behaviours
	void update(float dt);

private:
	// state update methods
	void updateStateLanded(float dt);
	void updateStateTakeOff(float dt);
	void updateStateGainingHeight(float dt);
	void updateStateFlying(float dt);
	void updateStateLandingSafe(float dt);
	void updateStateLandingBlind(float dt);

	// flight control methods
	void updateFlightControl(float dt);
	void updateVertexSelection(float dt);

	// drone control
	int id;
	DroneFlightController *flightController;
	State state;
	DroneDetection detection;
	FlightCommand flightCommand;
	float detectionAge;
	float targetHeight;
	float landingBlindTimer;
	float oldMinThrust;
	std::vector<glm::vec3> targetVertices;
	int vertexIndex;
};
