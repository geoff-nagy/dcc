#include "objects/simulateddroneswarm.h"
#include "objects/simulateddrone.h"

#include "util/log.h"

#include "glm/glm.hpp"
using namespace glm;

#include <stdint.h>
#include <vector>
#include <map>
using namespace std;

SimulatedDroneSwarm::SimulatedDroneSwarm()
{
	// anything?
}

SimulatedDroneSwarm::~SimulatedDroneSwarm()
{
	vector<SimulatedDrone*>::iterator i;

	// clean up after ourselves
	for(i = drones.begin(); i != drones.end(); ++ i)
	{
		delete *i;
	}
}

void SimulatedDroneSwarm::addDrone(SimulatedDrone *drone, const vec3 &pos)
{
	drones.push_back(drone);					// unordered list
	dronesByID[drone -> getID()] = drone;		// indexed by ID

	drone -> setPos(pos);
}

void SimulatedDroneSwarm::clearDrones()
{
	drones.clear();
}

void SimulatedDroneSwarm::sendFlightCommand(uint32_t id, float thrust, float roll, float pitch, float yaw)
{
	map<uint32_t, SimulatedDrone*>::iterator i;

	// make sure we actually know a drone by that name
	i = dronesByID.find(id);
	if(i != dronesByID.end())
	{
		i -> second -> setThrust(thrust);
		i -> second -> setRoll(roll);
		i -> second -> setPitch(pitch);
		i -> second -> setYaw(yaw);
	}
	else
	{
		// technically, this is okay; in the real world this command would just be ignored if no such drone existed, but if this happened
		// it would probably mean a mistake was made somewhere that we need to address
		LOG_INFO("WARNING: SimulatedDroneSwarm::sendFlightCommand() does not know a drone with ID " << id);
	}
}

vector<SimulatedDrone*>::iterator SimulatedDroneSwarm::getDronesBegin()
{
	return drones.begin();
}

vector<SimulatedDrone*>::iterator SimulatedDroneSwarm::getDronesEnd()
{
	return drones.end();
}

void SimulatedDroneSwarm::update(float dt)
{
	vector<SimulatedDrone*>::iterator i;

	for(i = drones.begin(); i != drones.end(); ++ i)
	{
		(*i) -> update(dt);
	}
}
