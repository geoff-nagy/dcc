#pragma once

#include "glm/glm.hpp"

class Point
{
public:
	Point();
	Point(const glm::vec3 &pos, const glm::vec4 &color, float size);
	virtual ~Point();

	virtual bool operator== (const Point &other);

	glm::vec3 pos;
	glm::vec4 color;
	float size;
};
