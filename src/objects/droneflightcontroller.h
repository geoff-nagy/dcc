#pragma once

#include "objects/droneinterface.h"

#include "util/pidcontroller.h"
#include "util/circularbuffer.h"

#include "glm/glm.hpp"

class DroneFlightController
{
public:
	// at the bare minimum, PID gains must be specified
	DroneFlightController(PIDController posXPID, PIDController posYPID, PIDController posZPID,
						  PIDController velXPID, PIDController velYPID, PIDController velZPID,
                          PIDController headingPID, float minThrust, float baseThrust);
	~DroneFlightController();

	// used to ensure that drone will not drop out of the sky; you should use these
	void setMinThrust(float minThrust);				// thrust will never go below this
	void setBaseThrust(float baseThrust);			// this thrust amount is always added

	// used for retrieving thrust levels
	float getMinThrust();
	float getBaseThrust();

	// flight control
	void setTargetPosition(const glm::vec3 &pos);
	void setTargetXZVelocity(const glm::vec2 &targetXZVel);
	void setTargetHeading(float heading);
	void setMaxTargetXZSpeed(float speed);
	void setMaxTargetYSpeed(float speed);
	void setMaxTargetYawSpeed(float speed);					// in radians

	// should be called regularly to update PID controllers; computes resulting FlightCommand
	void update(DroneDetection detection, float dt);

	// should be called after update()
	FlightCommand getLastFlightCommand();

	// some useful debugging functions
    glm::vec2 getTargetXZSpeed();
    float getTargetYSpeed();
    glm::vec2 getPitchAndRollOutput();

private:
	typedef enum FLGHT_MODE
	{
		FLIGHT_MODE_POSITION = 0,
		FLIGHT_MODE_VELOCITY
	} FlightMode;

	void updatePositionFlightMode(DroneDetection detection, float dt);
	void updateVelocityFlightMode(DroneDetection detection, float dt);

	// are we targeting a position or a velocity?
	FlightMode flightMode;

	// position PID controllers
	PIDController posXPID;
	PIDController posYPID;
	PIDController posZPID;

	// velocity PID controllers
	PIDController velXPID;
	PIDController velYPID;
	PIDController velZPID;

	// yaw PID controller
	PIDController headingPID;
	float oldDir;

	// thrust control
	float minThrust;
	float baseThrust;

	// flight control
	glm::vec3 targetPos;
	glm::vec2 targetXZVel;
	float targetHeading;
	float targetYVel;
	float maxTargetXZSpeed;
	float maxTargetYSpeed;
	float maxTargetYawSpeed;

	// output
	FlightCommand flightCommand;

	glm::vec3 oldPos;
	glm::vec3 oldVel;

	CircularBuffer velBuffer;

	//float controlTimer;
	//DroneDetection detectionAvg;
	//int numDetections;

	// debugging
	glm::vec2 pitchAndRollOutput;
};
