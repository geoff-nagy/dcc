#pragma once

#include "objects/physicsobject.h"
#include "objects/rotor.h"

#include <vector>

class SimulatedDrone : public PhysicsObject
{
public:
	SimulatedDrone(float grams, uint32_t id, float maxRotorImpulse, float maxYawSpeed);
	virtual ~SimulatedDrone() = 0;

	void addRotor(const Rotor &rotor);
	void setRotorSpeed(int index, float speed);

	void addViconMarker(const glm::vec3 &pos);		// specified in drone coordinate frame
	void clearViconMarkers();

	uint32_t getNumViconMarkers();
	glm::vec3 getViconMarker(uint32_t index);		// return world-space position of the indexed Vicon marker

	uint32_t getID();

	void setThrust(float thrust);
	void setRoll(float roll);
	void setPitch(float pitch);
	void setYaw(float yaw);

	virtual void update(float dt);

private:
	uint32_t id;									// unique ID of simulated drone

	std::vector<Rotor> rotors;						// list of attached rotors the drone has
	std::vector<glm::vec3> viconMarkers;			// vicon markers attached to drone, in drone's local coordinate space

	float maxRotorImpulse;							// the maximum physics impulse that a single rotor of this drone can impart
	float maxYawSpeed;								// how fast the drone can yaw, in radians per second

	float thrust;									// these represent flight-stick values ranging from -1.0 to 1.0
	float roll;
	float pitch;
	float yaw;

	float actualThrust;
	float actualRoll;
	float actualPitch;
	float actualYaw;
};
