#pragma once

#include "glm/glm.hpp"

#include "GL/glew.h"

#include <string>

class Camera
{
private:
	// camera projection and view properties
	glm::dmat4 projection;
	glm::dmat4 view;
	glm::dmat4 viewProjection;

	// extracted camera properties
	glm::dvec3 pos;
	glm::dvec3 forward;

	Camera();

public:
	static Camera *orthoCam(double left, double right, double bottom, double top);
	static Camera *perspectiveCam(double fov, double aspect, double near, double far);
	static Camera *fromViewProjectionMatrixFile(const std::string &filename);

	~Camera();

	void getProjection(glm::dmat4 *projection);
	void getView(glm::dmat4 *view);
	void getViewProjection(glm::dmat4 *viewProjection);
	glm::dvec3 getPos();
	glm::dvec3 getForward();

	void setView(const glm::dmat4 &view);
	void lookAt(const glm::dvec3 &eye, const glm::dvec3 &center, const glm::dvec3 &up);
};
