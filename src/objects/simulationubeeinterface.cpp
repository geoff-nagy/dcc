#include "objects/simulationubeeinterface.h"
#include "objects/simulateddrone.h"
#include "objects/simulateddroneswarm.h"
#include "objects/droneinterface.h"

#include "util/log.h"

#include "glm/glm.hpp"
using namespace glm;

#include <vector>
#include <utility>
#include <map>
using namespace std;

SimulationUBeeInterface::SimulationUBeeInterface(SimulatedDroneSwarm *droneSwarm, float delaySeconds)
	: droneSwarm(droneSwarm), delaySeconds(delaySeconds)
{
	// anything?
}

SimulationUBeeInterface::~SimulationUBeeInterface() { }

void SimulationUBeeInterface::sendCommands(float time)
{
	// step 1: queue up new commands we've received
	addNewCommands(time);

	// step 2: transmit older time-delayed commands we've queued
	transmitDelayedCommands(time);
}

void SimulationUBeeInterface::ping(uint32_t id)
{
	// not really needed in simulation mode
}

void SimulationUBeeInterface::addNewCommands(float time)
{
	pair<float, map<uint32_t, FlightCommand> > commandsByTime;
	map<uint32_t, FlightCommand> commands;
	map<uint32_t, FlightCommand>::iterator i;

	// insert the commands we've built up so far from the parent DroneInterface class into a queue for "transmission" later
	commandsByTime.first = time + delaySeconds;
	for(i = getBegin(); i != getEnd(); ++ i)
	{
		commandsByTime.second[i -> second.id] = i -> second;
		/*
		droneSwarm -> sendFlightCommand(i -> second.id,
										i -> second.thrust,
										i -> second.roll,
										i -> second.pitch,
										i -> second.yaw);*/
	}

	// do actual insertion
	flightCommandQueue.push(commandsByTime);

	// remove flight commands listed in the parent DroneInterface class; they are now in our own queue and we don't need to store them anymore
	clearCommands();
}

void SimulationUBeeInterface::transmitDelayedCommands(float time)
{
	map<uint32_t, FlightCommand> commandGroup;
	map<uint32_t, FlightCommand>::iterator i;

	// keep sending time-delayed commands that need to be transmitted now
	while(!flightCommandQueue.empty() && flightCommandQueue.front().first <= time)
	{
		// strip the next command group from the queue
		commandGroup = flightCommandQueue.front().second;
		flightCommandQueue.pop();

		// transmit the commands contained in the command group
		for(i = commandGroup.begin(); i != commandGroup.end(); ++ i)
		{
            droneSwarm -> sendFlightCommand(i -> second.id,
											i -> second.thrust,
											i -> second.roll,
											i -> second.pitch,
											i -> second.yaw);
		}
	}
}
