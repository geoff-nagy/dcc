#include "rendering/linerenderer.h"

#include "util/shader.h"
#include "util/log.h"

#include "GL/glew.h"

#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
using namespace glm;

#include <iostream>
#include <cstdlib>
using namespace std;

const int LineRenderer::MAX_POINTS = 32768;

LineRenderer *LineRenderer::instance = NULL;

LineRenderer::LineRenderer()
{
	points = new vec3[MAX_POINTS];
	colors = new vec4[MAX_POINTS];
	numPoints = 0;

	loadShaders();
	setupVBOs();
}

LineRenderer::~LineRenderer()
{
	delete[] points;
	delete[] colors;

	delete shader;

	glDeleteBuffers(2, vbos);
	glDeleteVertexArrays(1, &vao);

	instance = NULL;
}

LineRenderer *LineRenderer::getInstance()
{
	if(instance == NULL)
	{
		instance = new LineRenderer();
	}

	return instance;
}

void LineRenderer::loadShaders()
{
	shader = Shader::fromFile("../shaders/line.vert", "../shaders/line.frag");
	shader -> bindAttrib("a_Vertex", 0);
	shader -> bindAttrib("a_Color", 1);
	shader -> link();
	shader -> bind();
	// uniforms go here
	shader -> unbind();
}

void LineRenderer::setupVBOs()
{
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(2, vbos);

	glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * MAX_POINTS, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);

	glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec4) * MAX_POINTS, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
}

void LineRenderer::add(const vec3 &p1, const vec3 &p2, const vec4 &color)
{
	add(p1, p2, color, color);
}

void LineRenderer::add(const vec3 &p1, const vec3 &p2, const vec4 &color1, const vec4 &color2)
{
	if(numPoints < MAX_POINTS)
	{
		points[numPoints + 0] = p1;
		points[numPoints + 1] = p2;
		colors[numPoints + 0] = color1;
		colors[numPoints + 1] = color2;
		numPoints += 2;
	}
	else
	{
		cerr << "LineRenderer::add() cannot succeed because the line limit of " << MAX_POINTS << " is reached" << endl;
		exit(1);
	}
}

void LineRenderer::send()
{
	// send the line data to the GPU
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vec3) * numPoints, points);
	glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vec4) * numPoints, colors);
}

void LineRenderer::clear()
{
	numPoints = 0;
}

void LineRenderer::render(const mat4 &viewProjection)
{
	// prepare the shader
	shader -> bind();
	shader -> uniformMat4("u_ViewProjection", viewProjection);

	// render the lines
	glBindVertexArray(vao);
	glDrawArrays(GL_LINES, 0, numPoints);
}
