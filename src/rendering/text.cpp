#include "rendering/text.h"

#include "util/shader.h"
#include "util/log.h"

#include "libdrawtext/drawtext.h"

#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"
using namespace glm;

#include <string>
using namespace std;

const std::string Text::FONT_FILENAME = "../ttf/arial.ttf";
const int Text::FONT_SIZE = 12;

Text *Text::instance = NULL;

Text::Text()
{
	setupFont();
	setupShader();
}

Text::~Text()
{
	dtx_close_font(font);
	dtx_gl_kill();
}

Text *Text::getInstance()
{
	if(instance == NULL)
	{
		instance = new Text();
	}

	return instance;
}

void Text::setupFont()
{
	dtx_gl_init();
    font = dtx_open_font(FONT_FILENAME.c_str(), FONT_SIZE);

    if(font)
    {
		dtx_prepare(font, FONT_SIZE);
		dtx_use_font(font, FONT_SIZE);
		dtx_use_interpolation(0);
    }
    else
    {
		LOG_ERROR("Text::setupFont() could not load \"" << FONT_FILENAME << "\"");
    }
}

void Text::setupShader()
{
	shader = Shader::fromFile("../shaders/text.vert", "../shaders/text.frag");
	shader -> bindAttrib("a_Vertex", 0);
	shader -> bindAttrib("a_TexCoord", 1);
	shader -> link();
	shader -> bind();
	shader -> uniform1i("u_Texture", 0);
	shader -> unbind();
}

void Text::setViewProjection(const mat4 &viewProjection)
{
	shader -> bind();
	shader -> uniformMat4("u_VP", viewProjection);
}

void Text::render(const string &text, const vec2 &pos, const vec4 &color, TextAlignment textAlign)
{
	int width;
	mat4 modelMat = mat4(1.0f);

	// move to initial position
	modelMat = translate(modelMat, vec3(pos, 0.0f));

	// align appropriately, if desired
	if(textAlign == ALIGN_CENTER)
	{
		width = dtx_string_width(text.c_str());
		modelMat = translate(modelMat, vec3((int)(-width / 2.0f), 0.0f, 0.0f));
	}
	else if(textAlign == ALIGN_RIGHT)
	{
		width = dtx_string_width(text.c_str());
		modelMat = translate(modelMat, vec3(-width, 0.0f, 0.0f));
	}

	// mirror vertically since we treat upper left of window as origin
	modelMat = scale(modelMat, vec3(1.0f, -1.0f, 1.0f));

	// activate shader program
	shader -> bind();
	shader -> uniformMat4("u_Model", modelMat);
	shader -> uniformVec4("u_Color", color);

	// render the text
	dtx_string(text.c_str());
}
