#pragma once

#include "GL/glew.h"

#include "glm/glm.hpp"

class Shader;

class LineRenderer
{
public:
	static LineRenderer *getInstance();

	~LineRenderer();

	void add(const glm::vec3 &p1, const glm::vec3 &p2, const glm::vec4 &color);
	void add(const glm::vec3 &p1, const glm::vec3 &p2, const glm::vec4 &color1, const glm::vec4 &color2);
	void send();
	void clear();

	void render(const glm::mat4 &viewProjection);

private:
	static const int MAX_POINTS;

	static LineRenderer *instance;

	LineRenderer();

	void loadShaders();
	void setupVBOs();

	Shader *shader;
	GLuint vao;
	GLuint vbos[2];

	int numPoints;

	glm::vec3 *points;				// each pair of points for 1 line
	glm::vec4 *colors;				// each pair of colours for 1 line
};
