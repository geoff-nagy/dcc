#pragma once

#include "GL/glew.h"

#include "glm/glm.hpp"

class Shader;

class Quad
{
public:
	static Quad *getInstance();

	Quad();
	~Quad();

	void setViewProjection(const glm::mat4 &viewProjection);
	void render(const glm::vec2 &pos, const glm::vec2 &size, const glm::vec4 &color, GLuint texture = 0);
	void render(const glm::mat4 &model, const glm::vec4 &color, GLuint texture = 0);

private:
	static const int NUM_VERTICES;

	static Quad *instance;

	GLuint vao;
	GLuint vbos[2];
	Shader *textureShader;
	Shader *plainShader;

	void setupVBOs();
	void setupShader();
};
