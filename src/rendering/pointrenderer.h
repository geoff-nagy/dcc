#pragma once

#include "GL/glew.h"

#include "glm/glm.hpp"

class Shader;

class PointRenderer
{
private:
	static const int MAX_POINTS;

	static PointRenderer *instance;

	Shader *shader;

	GLuint vao;
	GLuint vbos[3];				// vertex positions, vertex colours, point sizes

	glm::vec3 *vertices;
	glm::vec3 *vertexPtr;

	glm::vec4 *colors;
	glm::vec4 *colorPtr;

	float *sizes;
	float *sizePtr;

	int numPoints;

	PointRenderer();

	void setupVBOs();
	void setupShader();

public:
	static PointRenderer *getInstance();

	~PointRenderer();

	void add(const glm::vec3 &point, const glm::vec4 &color, float size);
	void send();
	void clear();

	void render(const glm::mat4 &viewProjection);
};
