#include "rendering/quad.h"

#include "util/shader.h"

#include "GL/glew.h"

#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"
using namespace glm;

const int Quad::NUM_VERTICES = 4;

Quad *Quad::instance = NULL;

Quad::Quad()
{
	setupVBOs();
	setupShader();
}

Quad::~Quad()
{
	delete textureShader;
	delete plainShader;

	glDeleteBuffers(2, vbos);
	glDeleteVertexArrays(1, &vao);
}

void Quad::setupVBOs()
{
	const vec2 VERTICES[] = {vec2(0.0f, 0.0f),
							 vec2(0.0f, 1.0f),
							 vec2(1.0f, 0.0f),
							 vec2(1.0f, 1.0f)};
	const vec2 TEX_COORDS[] = {vec2(1.0f, 0.0f),
							   vec2(1.0f, 1.0f),
							   vec2(0.0f, 0.0f),
							   vec2(0.0f, 1.0f)};

	// generate our vertex array object and buffers
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(2, vbos);

	// throw the vertex data onto the GPU
	glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * NUM_VERTICES, VERTICES, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);

	// throw the tex coord data onto the GPU
	glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * NUM_VERTICES, TEX_COORDS, GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
}

void Quad::setupShader()
{
	textureShader = Shader::fromFile("../shaders/quad-textured.vert", "../shaders/quad-textured.frag");
	textureShader -> bindAttrib("a_Vertex", 0);
	textureShader -> bindAttrib("a_TexCoord", 1);
	textureShader -> link();
	textureShader -> bind();
	textureShader -> uniform1i("u_Texture", 0);
	textureShader -> unbind();

	plainShader = Shader::fromFile("../shaders/quad-plain.vert", "../shaders/quad-plain.frag");
	plainShader -> bindAttrib("a_Vertex", 0);
	plainShader -> link();
	plainShader -> bind();
	plainShader -> unbind();
}

Quad *Quad::getInstance()
{
	if(instance == NULL)
	{
		instance = new Quad();
	}

	return instance;
}

void Quad::setViewProjection(const mat4 &viewProjection)
{
	textureShader -> bind();
	textureShader -> uniformMat4("u_VP", viewProjection);

	plainShader -> bind();
	plainShader -> uniformMat4("u_VP", viewProjection);
}

void Quad::render(const vec2 &pos, const vec2 &size, const vec4 &color, GLuint texture)
{
	mat4 modelMat = mat4(1.0f);

	modelMat = translate(modelMat, vec3(pos, 0.0f));
	modelMat = scale(modelMat, vec3(size, 1.0f));
	render(modelMat, color, texture);
}

void Quad::render(const mat4 &modelMat, const vec4 &color, GLuint texture)
{
	if(texture == 0)
	{
		plainShader -> bind();
		plainShader -> uniformMat4("u_Model", modelMat);
		plainShader -> uniformVec4("u_Color", color);
	}
	else
	{
		textureShader -> bind();
		textureShader -> uniformMat4("u_Model", modelMat);
		textureShader -> uniformVec4("u_Color", color);
		glBindTexture(GL_TEXTURE_2D, texture);
	}

	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, NUM_VERTICES);
}
