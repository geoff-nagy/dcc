#include "rendering/pointrenderer.h"

#include "util/shader.h"

#include "GL/glew.h"

#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
using namespace glm;

#include <iostream>
using namespace std;

const int PointRenderer::MAX_POINTS = 500000;

PointRenderer *PointRenderer::instance = NULL;

PointRenderer::PointRenderer()
{
	numPoints = 0;

	vertices = new vec3[MAX_POINTS];
	vertexPtr = vertices;

	colors = new vec4[MAX_POINTS];
	colorPtr = colors;

	sizes = new float[MAX_POINTS];
	sizePtr = sizes;

	setupVBOs();
	setupShader();
}

PointRenderer::~PointRenderer()
{
	delete[] sizes;
	delete[] colors;
	delete[] vertices;

	delete shader;

	glDeleteBuffers(3, vbos);
	glDeleteVertexArrays(1, &vao);

	instance = NULL;
}

PointRenderer *PointRenderer::getInstance()
{
	if(instance == NULL)
	{
		instance = new PointRenderer();
	}
	return instance;
}

void PointRenderer::setupVBOs()
{
	// set up our VAO and VBOs
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(3, vbos);

	// vertex positions
	glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * MAX_POINTS, NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	// vertex colours
	glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec4) * MAX_POINTS, NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(1);

	// point sizes
	glBindBuffer(GL_ARRAY_BUFFER, vbos[2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * MAX_POINTS, NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(2);

	// allow shader-configurable point sizes
	glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
}

void PointRenderer::setupShader()
{
	shader = Shader::fromFile("../shaders/point.vert", "../shaders/point.frag");
	shader -> bindAttrib("a_Vertex", 0);
	shader -> bindAttrib("a_Color", 1);
	shader -> bindAttrib("a_Size", 2);
	shader -> link();
	shader -> bind();
	// uniforms go here
	shader -> unbind();
}

void PointRenderer::add(const vec3 &point, const vec4 &color, float size)
{
	if(numPoints < MAX_POINTS)
	{
		numPoints ++;
		(*vertexPtr++) = point;
		(*colorPtr++) = color;
		(*sizePtr++) = size;
	}
}

void PointRenderer::send()
{
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vec3) * numPoints, vertices);
	glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vec4) * numPoints, colors);
	glBindBuffer(GL_ARRAY_BUFFER, vbos[2]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * numPoints, sizes);
}

void PointRenderer::clear()
{
	numPoints = 0;
	vertexPtr = vertices;
	colorPtr = colors;
	sizePtr = sizes;
}

void PointRenderer::render(const mat4 &viewProjection)
{
	shader -> bind();
	shader -> uniformMat4("u_ViewProjection", viewProjection);

	glBindVertexArray(vao);
	glDrawArrays(GL_POINTS, 0, numPoints);
}
