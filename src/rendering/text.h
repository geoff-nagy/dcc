#pragma once

#include "glm/glm.hpp"

#include <string>

struct dtx_font;

class Shader;

class Text
{
public:
	typedef enum TEXT_ALIGNMENT
	{
		ALIGN_LEFT = 0,
		ALIGN_CENTER,
		ALIGN_RIGHT
	} TextAlignment;

	static Text *getInstance();

	Text();
	~Text();

	void setViewProjection(const glm::mat4 &viewProjection);
	void render(const std::string &text, const glm::vec2 &pos, const glm::vec4 &color, TextAlignment = ALIGN_LEFT);

private:
	static const std::string FONT_FILENAME;
	static const int FONT_SIZE;

	static Text *instance;

	Shader *shader;
	struct dtx_font *font;

	void setupFont();
	void setupShader();
};
