#include "experiments/map/maptrialcontroller.h"

#include "sensing/dronedetection.h"
#include "sensing/sensinginterface.h"
#include "sensing/sensormap.h"

#include "glm/glm.hpp"
using namespace glm;

#include <vector>
using namespace std;

MapTrialController::MapTrialController(World *world, SensingInterface *sensingInterface, DroneInterface *droneInterface)
	: TrialController(world, sensingInterface, droneInterface)
{
	this -> sensingInterface = sensingInterface;

	sensorMap = new SensorMap(sensingInterface -> getSize(), 0.5f);
}

MapTrialController::~MapTrialController()
{
	// [todo]
}

void MapTrialController::update(float dt)
{
	vector<DroneDetection>::iterator i;
	vec2 pos;

	detectionsCurrentFrame.clear();
	sensingInterface -> getDrones(detectionsCurrentFrame);

	for(i = detectionsCurrentFrame.begin(); i != detectionsCurrentFrame.end(); ++ i)
	{
		pos = vec2(i -> pos.x, i -> pos.z);
		sensorMap -> add(pos);
	}
}

void MapTrialController::render(const mat4 &viewProjection)
{
	sensorMap -> render(viewProjection);
}

bool MapTrialController::start()
{
	return true;
}

void MapTrialController::pause()
{
	// [todo]
}

void MapTrialController::resume()
{
	// [todo]
}

bool MapTrialController::stopAndLand()
{
	return true;
}

bool MapTrialController::scram()
{
	return true;
}

bool MapTrialController::isComplete()
{
	return false;
}
