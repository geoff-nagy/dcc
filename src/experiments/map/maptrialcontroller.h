#include "experiments/trialcontroller.h"

#include "sensing/dronedetection.h"

#include "glm/glm.hpp"

#include <vector>

class SensingInterface;
class DroneInterface;
class SensorMap;

class MapTrialController : public TrialController
{
public:
	MapTrialController(World *world, SensingInterface *sensingInterface, DroneInterface *droneInterface);
	~MapTrialController();

	virtual void update(float dt);
	virtual bool start();
	virtual void pause();
	virtual void resume();
	virtual bool stopAndLand();
	virtual bool scram();
	virtual bool isComplete();

	// optional methods
	void render(const glm::mat4 &viewProjection);

private:
	// derived-class-local handles to superclass components for convenience
	SensingInterface *sensingInterface;

	// used to render where our detections or sensor blind spots are
	SensorMap *sensorMap;

	// list of drone detections we've received in the current frame only
	std::vector<DroneDetection> detectionsCurrentFrame;
};
