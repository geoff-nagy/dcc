#pragma once

#include "experiments/trialcontroller.h"

#include "sensing/dronedetection.h"

#include "objects/droneinterface.h"

#include "util/pidcontroller.h"

#include "glm/glm.hpp"

#include <stdint.h>
#include <vector>
#include <string>
#include <fstream>

class World;
class SensingInterface;
class DroneInterface;
class GumdropNode;
class LineRenderer;
class DroneFlightController;
class MultiHoverAgent;

// the Multi-Hover controller is designed to test detection and flight of multiple drones in a simple setting

class MultiHoverTrialController : public TrialController
{
public:
	typedef enum TRIAL_STATE
	{
		STATE_INITIAL = 0,							// basic initialization
		STATE_STARTING,								// transition to this is controlled by higher-level logic
		STATE_SCANNING,								// several frames of scanning to find the IDs of all drones we're working with
		STATE_TAKING_OFF,							// drones begin taking off to a designated height before their main logic kicks in
		STATE_FLYING,								// drones are now performing controller logic
		STATE_LAND_NOW,								// trial is over; drones are instructed to land
		STATE_LANDING,								// drones are currently landing
		STATE_COMPLETE								// drones have landed and we can safely end
	} TrialState;

	MultiHoverTrialController(World *world, SensingInterface *sensingInterface, DroneInterface *droneInterface, GumdropNode *trialConfig, const std::string &outputFilename);
	virtual ~MultiHoverTrialController();

	// required methods
	void update(float dt);
	bool start();
	void pause();
	void resume();
	bool stopAndLand();
	bool scram();
	bool isComplete();

	// optional methods
	void render(const glm::mat4 &viewProjection);

private:
	void updateStateInitial(float dt);
	void updateStateStarting(float dt);
	void updateStateScanning(float dt);
	void updateStateTakingOff(float dt);
	void updateStateGainingHeight(float dt);
	void updateStateFlying(float dt);
	void updateStateLandNow(float dt);
	void updateStateLanding(float dt);
	void updateStateComplete(float dt);

	void processDetections();
	void updateFlightControls(float dt);
	void issueFlightCommands();
	void deleteAgents();

	void openOutputFile();
	void closeOutputFile();
	void saveOutputStats();

	// derived-class-local handles to superclass components for convenience
	SensingInterface *sensingInterface;
	DroneInterface *droneInterface;
	GumdropNode *trialConfig;

	// rendering tools
	LineRenderer *lines;

	// generic properties
	float scanningTimer;
	float trialTime;

	// controller behaviour
	TrialState trialState;
	std::string outputFilename;
	std::ofstream outputFile;

	// drone control
	std::vector<DroneDetection> detectionsMaster;				// list of drone detections we received when priming our list of drones
	std::vector<DroneDetection> detectionsCurrentFrame;			// list of drone detections we've received in the current frame only
    std::vector<MultiHoverAgent*> agents;						// list of hovering agents we want to control, informed by detectionsMaster
    std::vector<FlightCommand> flightCommands;					// list of flight commands that will be sent to all drones
};
