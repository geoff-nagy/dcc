#include "experiments/multihover/multihovertrialcontroller.h"

#include "objects/droneinterface.h"
#include "objects/droneflightcontroller.h"
#include "objects/simulatedubee.h"

#include "objects/multihover/multihoveragent.h"

#include "sensing/sensinginterface.h"

#include "rendering/linerenderer.h"

#include "util/directories.h"
#include "util/mymath.h"
#include "util/log.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/random.hpp"
#include "glm/gtc/noise.hpp"
using namespace glm;

#include <string.h>
#include <vector>
#include <queue>
using namespace std;

// [todo]: make the hand-held remote send a broadcast, not a unicast, flight command! this is required in case we need to land all drones if something goes terribly wrong

MultiHoverTrialController::MultiHoverTrialController(World *world,
													 SensingInterface *sensingInterface,
													 DroneInterface *droneInterface,
													 GumdropNode *trialConfig,
													 const string &outputFilename)
	: TrialController(world, sensingInterface, droneInterface), outputFilename(outputFilename)
{
	this -> sensingInterface = sensingInterface;
	this -> droneInterface = droneInterface;

	// trial enters initial state
	trialState = STATE_INITIAL;

	// default stats
	trialTime = 0.0f;

	// prepare for stats output
	openOutputFile();

	// rendering tools
	lines = LineRenderer::getInstance();
}

MultiHoverTrialController::~MultiHoverTrialController()
{
	deleteAgents();
	closeOutputFile();
}

bool MultiHoverTrialController::start()
{
	bool result = false;

	LOG_INFO("[MHVR] starting controller");

	if(trialState == STATE_INITIAL || trialState == STATE_COMPLETE)
	{
		trialState = STATE_STARTING;
		result = true;
	}

	return result;
}

void MultiHoverTrialController::pause()
{
	// [todo]
}

void MultiHoverTrialController::resume()
{
	// [todo]
}

bool MultiHoverTrialController::stopAndLand()
{
	bool result = false;

	if(trialState == STATE_FLYING)
	{
		trialState = STATE_LAND_NOW;
		result = true;
	}

	return result;
}

bool MultiHoverTrialController::scram()
{
	vector<MultiHoverAgent*>::iterator i;

	// emergency stop
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		(*i) -> scram();
	}

	// transition to complete state
	trialState = STATE_COMPLETE;

	return true;
}

bool MultiHoverTrialController::isComplete()
{
	return false;//trialState == STATE_COMPLETE;
}

void MultiHoverTrialController::update(float dt)
{
	// update time tracking
	trialTime += dt;

	// scan for drones
	detectionsCurrentFrame.clear();
	sensingInterface -> getDrones(detectionsCurrentFrame);

	// control state-specific logic
	if     (trialState == STATE_INITIAL) updateStateInitial(dt);
	else if(trialState == STATE_STARTING) updateStateStarting(dt);
	else if(trialState == STATE_SCANNING) updateStateScanning(dt);
	else if(trialState == STATE_TAKING_OFF) updateStateTakingOff(dt);
	else if(trialState == STATE_FLYING) updateStateFlying(dt);
	else if(trialState == STATE_LAND_NOW) updateStateLandNow(dt);
	else if(trialState == STATE_LANDING) updateStateLanding(dt);
	else if(trialState == STATE_COMPLETE) updateStateComplete(dt);

	// log data
	saveOutputStats();
}

void MultiHoverTrialController::updateStateInitial(float dt)
{
	// nothing happens here until we receive a signal to start the trial
}

void MultiHoverTrialController::updateStateStarting(float dt)
{
	// clear our master list of detected drones that we will use
	detectionsMaster.clear();

	// start scanning the sensing region for drones
	trialState = STATE_SCANNING;
    scanningTimer = 0.0f;

    // [any other setup occurs here]
}

void MultiHoverTrialController::updateStateScanning(float dt)
{
	const float SCAN_TIME_SECONDS = 1.0f;
	const float TARGET_HEIGHT = 0.75f;

	vector<DroneDetection>::iterator i, j;
	vector<uint32_t>::iterator k;
	bool found;

	// go through all detections in the current frame
	for(i = detectionsCurrentFrame.begin(); i != detectionsCurrentFrame.end(); ++ i)
	{
        // is this drone already detected in our master list?
        found = false;
        j = detectionsMaster.begin();
        while(!found && j != detectionsMaster.end())
        {
			found = (*j++) == *i;
        }

        // if not, add it
        if(!found)
        {
			detectionsMaster.push_back(*i);
        }
	}

	// delete all previous agents
	deleteAgents();

	// elapse time and see if we can transition now
	scanningTimer += dt;
	if(scanningTimer > SCAN_TIME_SECONDS)
	{
		// transition to taking off state
		trialState = STATE_TAKING_OFF;

		// create the agents whose IDs we scanned
		LOG_INFO("[MHVR] scan complete -- detected " << detectionsMaster.size() << " drones");
		if(detectionsMaster.size() > 0)
		{
			// go through all detections
			for(i = detectionsMaster.begin(); i != detectionsMaster.end(); ++ i)
			{
				// enumerate drones and their IDs and positions
				LOG_INFO("    [" << i -> id << "] was found at (" << i -> pos.x << ", " << i -> pos.y << ", " << i -> pos.z << ")");

				// create flight controllers and target positions for all drones we found
				/*
								agents.push_back(new MultiHoverAgent(new DroneFlightController(PIDController(2.0f, 0.0f, 0.04f, 0.0f),				// pos x gains
																			   PIDController(3.0f, 0.3f, 0.0f, 2.0f),				// pos y gains
																			   PIDController(2.0f, 0.0f, 0.04f, 0.0f),				// pos z gains
																			   PIDController(0.5f, 0.0f, 0.06f, 0.0f),				// vel x gains
																			   PIDController(0.6f, 0.0f, 0.04f, 0.0f),				// vel y gains
																			   PIDController(0.5f, 0.0f, 0.06f, 0.0f),				// vel z gains
																			   PIDController(1.0f, 0.0f, 0.08f, 0.0f),				// heading gains
																			   SimulatedUBee::MIN_THRUST,							// min thrust so we don't plummet
																			   SimulatedUBee::BASE_THRUST),							// approx. hover thrust
																			   */

																			   // [todo]: try playing with derivative velocity gains here to smooth things out a bit
																			   // [todo]: the proportional gains might also be too high; consider turning them down a bit, too

				// these params get us into a hover radius of about 10cm, even in the center of the Vicon space where the detection is sloppy, which is good enough for demo purposes
				/*
				agents.push_back(new MultiHoverAgent(new DroneFlightController(PIDController(30.0f, 0.0f, 0.02f, 0.0f),				// pos x gains
																			   PIDController(3.0f, 0.2f, 0.0f, 0.25f),				// pos y gains
																			   PIDController(30.0f, 0.0f, 0.02f, 0.0f),				// pos z gains
																			   PIDController(0.15f, 0.0f, 0.00001f, 0.0f),				// vel x gains	//0.17f
																			   PIDController(0.5f, 0.0f, 0.05f, 0.0f),				// vel y gains
																			   PIDController(0.15f, 0.0f, 0.00001f, 0.0f),				// vel z gains
																			   PIDController(1.0f, 0.0f, 0.08f, 0.0f),				// heading gains
																			   SimulatedUBee::MIN_THRUST,							// min thrust so we don't plummet
																			   SimulatedUBee::BASE_THRUST),							// approx. hover thrust
                                                    *i,
                                                    TARGET_HEIGHT));*/

				agents.push_back(new MultiHoverAgent(new DroneFlightController(PIDController(4.3f, 0.0f, 0.08f, 0.0f),				// pos x gains
																			   PIDController(3.0f, 0.2f, 0.0f, 0.25f),				// pos y gains
																			   PIDController(4.3f, 0.0f, 0.08f, 0.0f),				// pos z gains



																			   PIDController(0.14f, 0.0f, 0.001f, 0.0f),				// vel x gains	//0.17f
																						   //0.9f	     0.008f

																			   PIDController(0.5f, 0.0f, 0.05f, 0.0f),				// vel y gains


																			   PIDController(0.14f, 0.0f, 0.001f, 0.0f),				// vel z gains
																							//0.23f



																			   PIDController(2.0f, 0.0f, 0.05f, 0.0f),				// heading gains
																			   SimulatedUBee::MIN_THRUST,							// min thrust so we don't plummet
																			   SimulatedUBee::BASE_THRUST),							// approx. hover thrust
                                                    *i,
                                                    TARGET_HEIGHT));

			}
		}
		else
		{
			LOG_ERROR("[MHVR] cannot proceed without at least 1 drone detection");
		}
	}
}

void MultiHoverTrialController::updateStateTakingOff(float dt)
{
	vector<MultiHoverAgent*>::iterator i;

	// issue take-off commands to drones
	LOG_INFO("[MHVR] taking off");

	// let the drones get to the target height
	trialState = STATE_FLYING;

	// all drones transition to take-off state
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		(*i) -> takeOff();
	}
}

void MultiHoverTrialController::updateStateFlying(float dt)
{
	// always control general agent logic
	updateFlightControls(dt);

	// send control commands to drones
	issueFlightCommands();
}

void MultiHoverTrialController::updateStateLandNow(float dt)
{
	vector<MultiHoverAgent*>::iterator i;

	// instruct all drones to land
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		(*i) -> landSafely();
	}

	// advance to waiting state
	trialState = STATE_LANDING;
	LOG_INFO("---------------------------------------[MHVR] landing all drones");

	// always control general agent logic
	updateFlightControls(dt);

	// send control commands to drones
	issueFlightCommands();
}

void MultiHoverTrialController::updateStateLanding(float dt)
{
	vector<MultiHoverAgent*>::iterator i;
	bool landed = false;

	// wait for all drones to land
	i = agents.begin();
	landed = true;
	while(landed && i != agents.end())
	{
		// we consider a drone landed either if it's at the landing altitude or we've lost the detection
		landed = (*i++) -> isLanded();
	}

	// notify user if everyone landed
	if(landed)
	{
		// done; transition to safe state and notify the user
		trialState = STATE_COMPLETE;
		LOG_INFO("---------------------------------------[MHVR] drones have landed successfully");
	}

	// always control general agent logic
	updateFlightControls(dt);

	// send control commands to drones
	issueFlightCommands();
}

void MultiHoverTrialController::updateStateComplete(float dt)
{
	vector<DroneDetection>::iterator i;
	FlightCommand nothing;

	// build flight command
	nothing.thrust = 0.0f;
	nothing.roll = 0.0f;
	nothing.pitch = 0.0f;
	nothing.yaw = 0.0f;

	// send NULL flight command to everyone so we don't move or anything
	flightCommands.clear();
	for(i = detectionsMaster.begin(); i != detectionsMaster.end(); ++ i)
	{
		nothing.id = i -> id;
		flightCommands.push_back(nothing);
	}

	// [do not control general agent logic here]

	// send control commands to drones
	issueFlightCommands();
}

void MultiHoverTrialController::updateFlightControls(float dt)
{
	vector<MultiHoverAgent*>::iterator i;
	vector<DroneDetection>::iterator j;
	bool found;
	MultiHoverAgent *curr;

	// update all agents we know should be present
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		// try to find a detection associated with this agent
		curr = *i;
		found = false;
		j = detectionsCurrentFrame.begin();
		while(!found && j != detectionsCurrentFrame.end())
		{
			found = (int)j -> id == curr -> getID();
			if(!found) ++ j;
		}

		// pass along a detection we found
		if(found)
		{
			curr -> setDroneDetection(*j);
		}

		// update general agent flight logic
		curr -> update(dt);
		flightCommands.push_back(curr -> getFlightCommand());
	}
}

void MultiHoverTrialController::issueFlightCommands()
{
	vector<FlightCommand>::iterator i;

	// send flight commands based on PID output
	// [note]: these flight commands should be sent as compound flight commands, if using the radio interface, wherever possible
	droneInterface -> clearCommands();
	for(i = flightCommands.begin(); i != flightCommands.end(); ++ i)
	{
		droneInterface -> addCommand(*i);
	}
}

void MultiHoverTrialController::deleteAgents()
{
	vector<MultiHoverAgent*>::iterator i;

	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		delete *i;
	}
	agents.clear();
}

void MultiHoverTrialController::render(const glm::mat4 &viewProjection)
{
	const vec4 TARGET_COLOR(0.0f, 1.0f, 1.0f, 1.0f);
	const float CROSSHAIR_SIZE = 0.1f;

	vector<MultiHoverAgent*>::iterator i;
	vec3 target;

	// show the box vertices and highlight our target vertex
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		target = (*i) -> getTargetPosition();
		target.y = 0.0f;

		lines -> add(target + vec3(-CROSSHAIR_SIZE, 0.0f, 0.0f), target + vec3(CROSSHAIR_SIZE, 0.0f, 0.0f), TARGET_COLOR);
		lines -> add(target + vec3(0.0f, 0.0f, -CROSSHAIR_SIZE), target + vec3(0.0f, 0.0f, CROSSHAIR_SIZE), TARGET_COLOR);
	}
}

void MultiHoverTrialController::openOutputFile()
{
    stringstream ss;
    string folder;
	uint32_t index;

	// create the directory indicated (if any) if it does not exist yet
	index = outputFilename.find_last_of('/');
	if(index != string::npos)
	{
		folder = outputFilename.substr(0, index);
		if(!dirDoesDirectoryExist(folder))
		{
			dirMakeDirectory(folder);
		}
	}

    // build the output file name and open the file for writing
    ss << outputFilename << ".txt";
    outputFilename = ss.str();
    outputFile.open(outputFilename.c_str());
    if(outputFile.is_open())
    {
		LOG_INFO("[MHVR] opened log file \"" << outputFilename << "\"");
    }
    else
    {
		LOG_ERROR("[MHVR] could not open output file \"" << outputFilename << "\" for writing");
    }
}

void MultiHoverTrialController::closeOutputFile()
{
	if(outputFile.is_open())
	{
		LOG_INFO("[MHVR] closing log file \"" << outputFilename << "\"");
		outputFile.close();
	}
}

void MultiHoverTrialController::saveOutputStats()
{
	if(outputFile.is_open())
	{
		/*
		outputFile << trialTime << ",";
		outputFile << detection.pos.x << ",";
		outputFile << detection.pos.y << ",";
		outputFile << detection.pos.z << ",";
		outputFile << detection.velocity.x << ",";
		outputFile << detection.velocity.y << ",";
		outputFile << detection.velocity.z << ",";
		outputFile << detection.dir << ",";
		outputFile << flightController -> getTargetXZSpeed().x << ",";
		outputFile << flightController -> getTargetYSpeed() << ",";
		outputFile << flightController -> getTargetXZSpeed().y;
		outputFile << endl;*/
	}
}
