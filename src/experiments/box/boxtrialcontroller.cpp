#include "experiments/box/boxtrialcontroller.h"

#include "objects/droneinterface.h"
#include "objects/droneflightcontroller.h"
#include "objects/simulatedubee.h"

#include "objects/box/boxagent.h"

#include "sensing/sensinginterface.h"

#include "rendering/linerenderer.h"

#include "util/directories.h"
#include "util/mymath.h"
#include "util/log.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/random.hpp"
#include "glm/gtc/noise.hpp"
using namespace glm;

#include <string.h>
#include <vector>
#include <queue>
using namespace std;

// [todo]: make the hand-held remote send a broadcast, not a unicast, flight command! this is required in case we need to land all drones if something goes terribly wrong

BoxTrialController::BoxTrialController(World *world,
									   SensingInterface *sensingInterface,
									   DroneInterface *droneInterface,
									   GumdropNode *trialConfig,
									   const string &outputFilename)
	: TrialController(world, sensingInterface, droneInterface), outputFilename(outputFilename)
{
	this -> sensingInterface = sensingInterface;
	this -> droneInterface = droneInterface;

	// trial enters initial state
	trialState = STATE_INITIAL;

	// default stats
	trialTime = 0.0f;

	// prepare for stats output
	openOutputFile();

	// rendering tools
	lines = LineRenderer::getInstance();

	// create target vertices
	buildTargetVertices();
}

BoxTrialController::~BoxTrialController()
{
	deleteAgents();
	closeOutputFile();
}

bool BoxTrialController::start()
{
	bool result = false;

	LOG_INFO("[BOX] starting controller");

	if(trialState == STATE_INITIAL || trialState == STATE_COMPLETE)
	{
		trialState = STATE_STARTING;
		result = true;
	}

	return result;
}

void BoxTrialController::pause()
{
	// [todo]
}

void BoxTrialController::resume()
{
	// [todo]
}

bool BoxTrialController::stopAndLand()
{
	bool result = false;

	if(trialState == STATE_FLYING)
	{
		trialState = STATE_LAND_NOW;
		result = true;
	}

	return result;
}

bool BoxTrialController::scram()
{
	vector<BoxAgent*>::iterator i;

	// emergency stop
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		(*i) -> scram();
	}

	// transition to complete state
	trialState = STATE_COMPLETE;

	return true;
}

bool BoxTrialController::isComplete()
{
	return false;//trialState == STATE_COMPLETE;
}

void BoxTrialController::update(float dt)
{
	// update time tracking
	trialTime += dt;

	// scan for drones
	detectionsCurrentFrame.clear();
	sensingInterface -> getDrones(detectionsCurrentFrame);

	// control state-specific logic
	if     (trialState == STATE_INITIAL) updateStateInitial(dt);
	else if(trialState == STATE_STARTING) updateStateStarting(dt);
	else if(trialState == STATE_SCANNING) updateStateScanning(dt);
	else if(trialState == STATE_TAKING_OFF) updateStateTakingOff(dt);
	else if(trialState == STATE_FLYING) updateStateFlying(dt);
	else if(trialState == STATE_LAND_NOW) updateStateLandNow(dt);
	else if(trialState == STATE_LANDING) updateStateLanding(dt);
	else if(trialState == STATE_COMPLETE) updateStateComplete(dt);

	// log data
	saveOutputStats();
}

void BoxTrialController::updateStateInitial(float dt)
{
	// nothing happens here until we receive a signal to start the trial
}

void BoxTrialController::updateStateStarting(float dt)
{
	detectionsMaster.clear();
	trialState = STATE_SCANNING;
    scanningTimer = 0.0f;
}

void BoxTrialController::updateStateScanning(float dt)
{
	const float SCAN_TIME_SECONDS = 1.0f;
	const float TARGET_HEIGHT = 0.75f;

	vector<DroneDetection>::iterator i, j;
	vector<uint32_t>::iterator k;
	bool found;

	// go through all detections in the current frame
	for(i = detectionsCurrentFrame.begin(); i != detectionsCurrentFrame.end(); ++ i)
	{
        // is this drone already detected in our master list?
        found = false;
        j = detectionsMaster.begin();
        while(!found && j != detectionsMaster.end())
        {
			found = (*j++) == *i;
        }

        // if not, add it
        if(!found)
        {
			detectionsMaster.push_back(*i);
        }
	}

	// delete all previous agents
	deleteAgents();

	// elapse time and see if we can transition now
	scanningTimer += dt;
	if(scanningTimer > SCAN_TIME_SECONDS)
	{
		// transition to taking off state
		trialState = STATE_TAKING_OFF;

		// create the agents whose IDs we scanned
		LOG_INFO("[BOX] scan complete -- detected " << detectionsMaster.size() << " drones");
		if(detectionsMaster.size() == 1)
		{
			// go through all detections
			for(i = detectionsMaster.begin(); i != detectionsMaster.end(); ++ i)
			{
				// enumerate drones and their IDs and positions
				LOG_INFO("    [" << i -> id << "] was found at (" << i -> pos.x << ", " << i -> pos.y << ", " << i -> pos.z << ")");

				// create flight controllers and target positions for all drones we found
				agents.push_back(new BoxAgent(new DroneFlightController(PIDController(9.0f, 0.5f, 6.0f, 0.05f),				// pos x gains
																	    PIDController(2.5f, 0.1f, 0.001f, 1.5f),				// pos y gains
																	    PIDController(9.0f, 0.5f, 6.0f, 0.05f),				// pos z gains
																	    PIDController(0.2f, 0.1f, 0.0f, 0.1f),				// vel x gains
																	    PIDController(0.45f, 0.0f, 0.001f, 0.0f),				// vel y gains
																	    PIDController(0.2f, 0.1f, 0.0f, 0.1f),				// vel z gains
																	    PIDController(1.0f, 0.0f, 0.08f, 0.0f),				// heading gains
																	    SimulatedUBee::MIN_THRUST,							// min thrust so we don't plummet
																	    SimulatedUBee::BASE_THRUST),							// approx. hover thrust
                                                    *i,
                                                    TARGET_HEIGHT,
                                                    boxVertices));

			}
		}
		else
		{
			LOG_ERROR("[BOX] requires exactly 1 drone detection, not " << detectionsMaster.size());
		}
	}
}

void BoxTrialController::updateStateTakingOff(float dt)
{
	vector<BoxAgent*>::iterator i;

	// issue take-off commands to drones
	LOG_INFO("[BOX] taking off");

	// let the drones get to the target height
	trialState = STATE_FLYING;

	// all drones transition to take-off state
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		(*i) -> takeOff();
	}
}

void BoxTrialController::updateStateFlying(float dt)
{
	// always control general agent logic
	updateFlightControls(dt);

	// send control commands to drones
	issueFlightCommands();
}

void BoxTrialController::updateStateLandNow(float dt)
{
	vector<BoxAgent*>::iterator i;

	// instruct all drones to land
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		(*i) -> landSafely();
	}

	// advance to waiting state
	trialState = STATE_LANDING;
	LOG_INFO("---------------------------------------[BOX] landing all drones");

	// always control general agent logic
	updateFlightControls(dt);

	// send control commands to drones
	issueFlightCommands();
}

void BoxTrialController::updateStateLanding(float dt)
{
	vector<BoxAgent*>::iterator i;
	bool landed = false;

	// wait for all drones to land
	i = agents.begin();
	landed = true;
	while(landed && i != agents.end())
	{
		// we consider a drone landed either if it's at the landing altitude or we've lost the detection
		landed = (*i++) -> isLanded();
	}

	// notify user if everyone landed
	if(landed)
	{
		// done; transition to safe state and notify the user
		trialState = STATE_COMPLETE;
		LOG_INFO("---------------------------------------[BOX] drones have landed successfully");
	}

	// always control general agent logic
	updateFlightControls(dt);

	// send control commands to drones
	issueFlightCommands();
}

void BoxTrialController::updateStateComplete(float dt)
{
	vector<DroneDetection>::iterator i;
	FlightCommand nothing;

	// build flight command
	nothing.thrust = 0.0f;
	nothing.roll = 0.0f;
	nothing.pitch = 0.0f;
	nothing.yaw = 0.0f;

	// send NULL flight command to everyone so we don't move or anything
	flightCommands.clear();
	for(i = detectionsMaster.begin(); i != detectionsMaster.end(); ++ i)
	{
		nothing.id = i -> id;
		flightCommands.push_back(nothing);
	}

	// [do not control general agent logic here]

	// send control commands to drones
	issueFlightCommands();
}

void BoxTrialController::updateFlightControls(float dt)
{
	vector<BoxAgent*>::iterator i;
	vector<DroneDetection>::iterator j;
	bool found;
	BoxAgent *curr;

	// update all agents we know should be present
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		// try to find a detection associated with this agent
		curr = *i;
		found = false;
		j = detectionsCurrentFrame.begin();
		while(!found && j != detectionsCurrentFrame.end())
		{
			found = (int)j -> id == curr -> getID();
			if(!found) ++ j;
		}

		// pass along a detection we found
		if(found)
		{
			curr -> setDroneDetection(*j);
		}

		// update general agent flight logic
		curr -> update(dt);
		flightCommands.push_back(curr -> getFlightCommand());
	}
}

void BoxTrialController::issueFlightCommands()
{
	vector<FlightCommand>::iterator i;

	// send flight commands based on PID output
	// [note]: these flight commands should be sent as compound flight commands, if using the radio interface, wherever possible
	droneInterface -> clearCommands();
	for(i = flightCommands.begin(); i != flightCommands.end(); ++ i)
	{
		droneInterface -> addCommand(*i);
	}
}

void BoxTrialController::deleteAgents()
{
	vector<BoxAgent*>::iterator i;

	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		delete *i;
	}
	agents.clear();
}

void BoxTrialController::render(const glm::mat4 &viewProjection)
{
	const vec4 NORMAL_COLOR(0.6f, 0.6f, 0.6f, 1.0f);
	const vec4 TARGET_COLOR(0.0f, 1.0f, 0.0f, 1.0f);
	const float CROSSHAIR_SIZE = 0.1f;

	vec4 color = NORMAL_COLOR;
	int i;

	// show the box vertices and highlight our target vertex
	for(i = 0; i != (int)boxVertices.size(); ++ i)
	{
		// make sure we actually have agents to question
		if(agents.size() > 0)
		{
			color = (i == agents[0] -> getTargetVertexIndex() ? TARGET_COLOR : NORMAL_COLOR);
		}

		// draw vertices with the target one highlighted
		lines -> add(boxVertices[i] + vec3(-CROSSHAIR_SIZE, 0.0f, 0.0f), boxVertices[i] + vec3(CROSSHAIR_SIZE, 0.0f, 0.0f), color);
		lines -> add(boxVertices[i] + vec3(0.0f, -CROSSHAIR_SIZE, 0.0f), boxVertices[i] + vec3(0.0f, CROSSHAIR_SIZE, 0.0f), color);
		lines -> add(boxVertices[i] + vec3(0.0f, 0.0f, -CROSSHAIR_SIZE), boxVertices[i] + vec3(0.0f, 0.0f, CROSSHAIR_SIZE), color);
	}
}

void BoxTrialController::buildTargetVertices()
{
	const vec3 BOX_CENTER(-2.0f, 0.0f, 0.0f);

	// build list of box vertices
	boxVertices.clear();
	boxVertices.push_back(BOX_CENTER + vec3(-0.5f, 1.25f, -0.5f));
	boxVertices.push_back(BOX_CENTER + vec3(-0.5f, 1.25f, 0.5f));
	boxVertices.push_back(BOX_CENTER + vec3(0.5f, 1.25f, 0.5f));
	boxVertices.push_back(BOX_CENTER + vec3(0.5f, 1.25f, -0.5f));
	boxVertices.push_back(BOX_CENTER + vec3(0.5f, 0.5f, -0.5f));
	boxVertices.push_back(BOX_CENTER + vec3(0.5f, 0.5f, 0.5f));
	boxVertices.push_back(BOX_CENTER + vec3(-0.5f, 0.5f, 0.5f));
	boxVertices.push_back(BOX_CENTER + vec3(-0.5f, 0.5f, -0.5f));
}

void BoxTrialController::openOutputFile()
{
    stringstream ss;
    string folder;
	uint32_t index;

	// create the directory indicated (if any) if it does not exist yet
	index = outputFilename.find_last_of('/');
	if(index != string::npos)
	{
		folder = outputFilename.substr(0, index);
		if(!dirDoesDirectoryExist(folder))
		{
			dirMakeDirectory(folder);
		}
	}

    // build the output file name and open the file for writing
    ss << outputFilename << ".txt";
    outputFilename = ss.str();
    outputFile.open(outputFilename.c_str());
    if(outputFile.is_open())
    {
		LOG_INFO("[BOX] opened log file \"" << outputFilename << "\"");
    }
    else
    {
		LOG_ERROR("[BOX] could not open output file \"" << outputFilename << "\" for writing");
    }
}

void BoxTrialController::closeOutputFile()
{
	if(outputFile.is_open())
	{
		LOG_INFO("[BOX] closing log file \"" << outputFilename << "\"");
		outputFile.close();
	}
}

void BoxTrialController::saveOutputStats()
{
	if(outputFile.is_open() && agents.size() > 0)
	{
		outputFile << trialTime << ",";
		outputFile << agents[0] -> getDetection().pos.x << ",";
		outputFile << agents[0] -> getDetection().pos.y << ",";
		outputFile << agents[0] -> getDetection().pos.z << ",";
		outputFile << agents[0] -> getDetection().velocity.x << ",";
		outputFile << agents[0] -> getDetection().velocity.y << ",";
		outputFile << agents[0] -> getDetection().velocity.z << ",";
		outputFile << agents[0] -> getDetection().dir << ",";
		outputFile << agents[0] -> getFlightController() -> getTargetXZSpeed().x << ",";
		outputFile << agents[0] -> getFlightController() -> getTargetYSpeed() << ",";
		outputFile << agents[0] -> getFlightController() -> getTargetXZSpeed().y;
		outputFile << endl;
	}
}
