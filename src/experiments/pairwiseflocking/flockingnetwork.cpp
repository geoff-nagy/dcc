#include "experiments/pairwiseflocking/flockingnetwork.h"
#include "experiments/pairwiseflocking/subflock.h"
#include "experiments/pairwiseflocking/subflockagent.h"

#include "util/log.h"

#include <stdint.h>
#include <string.h>
#include <list>
#include <set>
#include <vector>
#include <queue>
using namespace std;

FlockingNetwork::FlockingNetwork()
{
	resetStats();
}

FlockingNetwork::~FlockingNetwork()
{
	clear();
}

void FlockingNetwork::addAgents(const vector<int> &ids)
{
	SubFlockAgent *newAgent;
	vector<int>::const_iterator i;

	for(i = ids.cbegin(); i != ids.cend(); ++ i)
	{
		newAgent = new SubFlockAgent(*i);
		agents.push_back(newAgent);
		agentsByID[*i] = newAgent;
	}
}

void FlockingNetwork::connectAgents(int id1, int id2)
{
	map<int, SubFlockAgent*>::iterator i, j;

	// make sure first ID refers to a known agent
	i = agentsByID.find(id1);
	if(i == agentsByID.end())
	{
		LOG_ERROR("FlockingNetwork::connectAgents() does not know an agent with ID " << id1);
	}

	// make sure second ID refers to a known agent
	j = agentsByID.find(id2);
	if(j == agentsByID.end())
	{
		LOG_ERROR("FlockingNetwork::connectAgents() does not know an agent with ID " << id2);
	}

	// connect the two agents to each other
	i -> second -> connect(j -> second);
	j -> second -> connect(i -> second);
}

void FlockingNetwork::clear()
{
	vector<SubFlockAgent*>::iterator i;

	// free memory associated with agent entries
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		delete *i;
	}

	// clear list of agents and our stats
	agents.clear();
	agentsByID.clear();
	resetStats();
}

void FlockingNetwork::computeStats()
{
	resetStats();
	computeSubFlocks();
	computeDiameters();
	computeAverageDegree();
	computeNumConnections();
}

void FlockingNetwork::resetStats()
{
	vector<SubFlock*>::iterator i;

	averageSubFlockSize = -1.0;
	averageDiameter = -1.0;
	averageDegree = -1.0;
	numConnections = 0;

	// clear any old subflocks
	for(i = subFlocks.begin(); i != subFlocks.end(); ++ i)
	{
		delete *i;
	}
	subFlocks.clear();
}

void FlockingNetwork::computeSubFlocks()
{
	SubFlock *currSubFlock;
	vector<SubFlock*>::iterator k;
	uint32_t i, j;

	queue<uint32_t> searchGroup;
	uint32_t curr;

	// track which agents we have processed
	bool processed[agents.size()];
	memset(processed, 0, agents.size() * sizeof(bool));

	// make sure each agent has been assigned a sub flock---this is a basic non-recursive flood-fill algorithm
	for(i = 0; i < agents.size(); ++ i)
	{
		// we might have processed this agents' connections already
		if(!processed[i])
		{
			// build a queue of agents we need to search for direct or indirect connections to the current agent
			currSubFlock = new SubFlock();
			searchGroup.push(i);
			while(!searchGroup.empty())
			{
				// add to current flock and do not examine this agent again
				curr = searchGroup.front();
				searchGroup.pop();
				currSubFlock -> agents.insert(agents[curr]);

				// examine all neighbours of this agent
				for(j = 0; j < agents.size(); ++ j)
				{
					//if(curr != j && connections[curr][j] && !processed[j])
					//if(curr != j && !processed[j] && agentsByID[curr] -> isConnectedTo(agentsByID[j]))
					if(curr != j && !processed[j] && agents[curr] -> isConnectedTo(agents[j]))
					{
						searchGroup.push(j);
						processed[j] = true;
					}
				}
			}

			// subflock complete
			subFlocks.push_back(currSubFlock);
		}
	}
}

void FlockingNetwork::computeDiameters()
{
	SubFlockAgent **floodPath;
	vector<SubFlock*>::iterator i;
	set<SubFlockAgent*>::iterator j;
	int pathLength;

	// allocate space for flood fill algorithm
	floodPath = new SubFlockAgent*[agents.size()];

	// go through all connected components in this flocking graph
	averageSubFlockSize = 0.0;
	averageDiameter = 0.0;
	for(i = subFlocks.begin(); i != subFlocks.end(); ++ i)
	{
		// reset path and diameter tracking for this sub flock
		pathLength = -1;
		(*i) -> diameter = -1;

		// go through the agents in this subflock to compute the longest shortest path
		for(j = (*i) -> agents.begin(); j != (*i) -> agents.end(); ++ j)
		{
			// measure the length of the longest path from this agent
			pathLength = floodDistance(*j, floodPath) - 1;					// floodDistance() returns vertex count, not edge count, so we subtract 1
			if(pathLength > (*i) -> diameter)
			{
				(*i) -> diameter = pathLength;
				memcpy((*i) -> diameterPath, floodPath, sizeof(SubFlockAgent*) * pathLength);
			}
		}

		// add to running weighted flock size
		averageSubFlockSize += (double)(*i) -> agents.size() * ((double)(*i) -> agents.size() / (double)agents.size());

		// add to running weighted diameter
		averageDiameter += (double)(*i) -> diameter * ((double)(*i) -> agents.size() / (double)agents.size());
	}

	// free our flood fill path memory; we do not delete the SubFlockAgents referred to inside of it
	delete[] floodPath;
}

void FlockingNetwork::computeAverageDegree()
{
	vector<SubFlock*>::iterator i;
	set<SubFlockAgent*>::iterator j;

	// go through all connected components in this flocking graph
	averageDegree = 0.0;
	for(i = subFlocks.begin(); i != subFlocks.end(); ++ i)
	{
		// reset degree tracking for this sub flock
		(*i) -> averageDegree = 0.0;

		// total up all vertex degrees for this component
		for(j = (*i) -> agents.begin(); j != (*i) -> agents.end(); ++ j)
		{
			(*i) -> averageDegree += (*j) -> getNumConnections();
		}

		// now divide to get the average
		(*i) -> averageDegree /= (double)(*i) -> agents.size();

		// add to running weighted degree
		averageDegree += (double)(*i) -> averageDegree * ((double)(*i) -> agents.size() / (double)agents.size());
	}
}

void FlockingNetwork::computeNumConnections()
{
	vector<SubFlock*>::iterator i;
	set<SubFlockAgent*>::iterator j;

	// go through all connected components in this flocking graph
	numConnections = 0;
	for(i = subFlocks.begin(); i != subFlocks.end(); ++ i)
	{
		// total up all vertex degrees
		for(j = (*i) -> agents.begin(); j != (*i) -> agents.end(); ++ j)
		{
			numConnections += (*j) -> getNumConnections();
		}
	}
}

int FlockingNetwork::floodDistance(SubFlockAgent *start, SubFlockAgent **path)
{
	list<SubFlockAgent*> outer;
	list<SubFlockAgent*> inner;
	vector<SubFlockAgent*>::iterator i;
	SubFlockAgent *curr;

	int hop = 0;
	int highestHop = 0;
	SubFlockAgent *highestAgent = NULL;
	int result = -1;

	// clear node counts
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		(*i) -> setHopNumber(0);
	}

	path[hop] = start;
	outer.push_back(start);
	while(!outer.empty())
	{
		hop ++;
		while(!outer.empty())
		{
			curr = outer.front();
			outer.pop_front();
			if(curr -> getHopNumber() == 0)
			{
				curr -> setHopNumber(hop);
				for(i = curr -> getConnectionsBegin(); i != curr -> getConnectionsEnd(); ++ i)
				{
					if((*i) -> getHopNumber() == 0)
					{
						inner.push_back(*i);
					}
				}

				if(hop > highestHop)
				{
					highestHop = hop;
					highestAgent = curr;
				}
			}
		}

        hop ++;
        while(!inner.empty())
        {
			curr = inner.front();
			inner.pop_front();
			if(curr -> getHopNumber() == 0)
			{
				curr -> setHopNumber(hop);
				for(i = curr -> getConnectionsBegin(); i != curr -> getConnectionsEnd(); ++ i)
				{
					if((*i) -> getHopNumber() == 0)
					{
						outer.push_back(*i);
					}
				}

				if(hop > highestHop)
				{
					highestHop = hop;
					highestAgent = curr;
				}
			}
        }
	}

	// work backwards to find path
	result = highestHop;
	path[highestHop - 1] = highestAgent;
	while(highestHop >= 2 && highestAgent)
	{
		highestAgent = highestAgent -> getConnectionWithHopNumber(highestHop - 1);
		path[highestHop - 2] = highestAgent;
		highestHop --;
	}

	// we're done!
	return result;
}

int FlockingNetwork::getNumSubFlocks()
{
	return subFlocks.size();
}

int FlockingNetwork::getSubFlockSize(int index)
{
	return subFlocks[index] -> agents.size();
}

double FlockingNetwork::getAverageSubFlockSize()
{
	return averageSubFlockSize;
}

double FlockingNetwork::getAverageDiameter()
{
	return averageDiameter;
}

double FlockingNetwork::getAverageDegree()
{
	return averageDegree;
}

int FlockingNetwork::getNumConnections()
{
	return numConnections;
}
