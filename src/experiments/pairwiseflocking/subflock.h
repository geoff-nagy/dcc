#pragma once

#include <set>

class SubFlockAgent;

class SubFlock
{
public:
	SubFlock();
	~SubFlock();

	std::set<SubFlockAgent*> agents;
	int diameter;
	SubFlockAgent **diameterPath;
	double averageDegree;
};
