#include "experiments/pairwiseflocking/pairwiseflockingtrialcontroller.h"
#include "experiments/pairwiseflocking/flockingnetwork.h"

#include "world/world.h"

#include "objects/droneinterface.h"
#include "objects/pairwiseflocking/pairwiseflockingagent.h"
#include "objects/obstacle.h"
#include "objects/simulatedubee.h"

#include "sensing/sensinginterface.h"

#include "rendering/linerenderer.h"

#include "util/directories.h"
#include "util/log.h"
#include "util/wavefront.h"
#include "util/model.h"
#include "util/shader.h"
#include "util/mymath.h"
#include "util/rnd.h"

#include "GL/glew.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/random.hpp"
#include "glm/gtc/noise.hpp"
using namespace glm;

#include <string.h>
#include <vector>
#include <queue>
using namespace std;

const float PairwiseFlockingTrialController::SAVE_INTERVAL_SECS = 1.0f;

PairwiseFlockingTrialController::PairwiseFlockingTrialController(World *world,
																 SensingInterface *sensingInterface,
																 DroneInterface *droneInterface,
																 GumdropNode *trialConfig,
																 const string &trialOutputFilename,
																 vector<uint32_t> &pairs,
																 bool gui)
	: TrialController(world, sensingInterface, droneInterface), gui(gui)
{
	vec3 dronesOrigin;
	float cellSize;
	int horizontalCells;
	int verticalCells;

	uint32_t numPairs;
	uint32_t numSingle;
	trialState = STATE_INITIAL;

	this -> world = world;
	this -> sensingInterface = sensingInterface;
	this -> droneInterface = droneInterface;
	this -> trialOutputFilename = trialOutputFilename;

	if(gui)
	{
		loadAssets();
	}

	// read drone placement settings
	dronesOrigin = vec3(trialConfig -> getDouble("trial.drones.origin[0]", 0.0f, NULL, true),
						0.0f,
						trialConfig -> getDouble("trial.drones.origin[1]", 0.0f, NULL, true));
    cellSize = trialConfig -> getDouble("trial.drones.cell_size", 0.0f, NULL, true);
    horizontalCells = trialConfig -> getInt("trial.drones.cells[0]", 0, NULL, true);
	verticalCells = trialConfig -> getInt("trial.drones.cells[1]", 0, NULL, true);
	numPairs = trialConfig -> getInt("trial.drones.num_pairs", 0, NULL, true);
	numSingle = trialConfig -> getInt("trial.drones.num_single", 0, NULL, true);
	closePairing = trialConfig -> getBool("trial.drones.close_pairing", false, NULL, true);
	asymmetricTracking = trialConfig -> getBool("trial.drones.asymmetric_tracking", false, NULL, true);
	neighbourSensingNoiseStdDev = trialConfig -> getDouble("trial.drones.sensor_noise_std_dev", 0.0, NULL, true);
	neighbourSensingReliabilityThreshold = trialConfig -> getDouble("trial.drones.sensor_reliability_threshold", 0.0, NULL, true);
	obstacleOcclusion = trialConfig -> getBool("trial.drones.obstacle_occlusion", true, NULL, true);
	partnerSeparation = trialConfig -> getBool("trial.drones.partner_separation", true, NULL, true);

    // read trial properties
	maxTrialSeconds = trialConfig -> getInt("trial.time_sec", 0, NULL, true);
	numTrackedIfPaired = trialConfig -> getInt("trial.num_tracked_if_paired", 0, NULL, true);
	numTrackedIfSingle = trialConfig -> getInt("trial.num_tracked_if_single", 0, NULL, true);
	viewRange = (float)trialConfig -> getInt("trial.view_range_cm", 0, NULL, true) / 100.0f;
	boundsArea = vec2(trialConfig -> getDouble("trial.bounds.size[0]", 0.0, NULL, true),
					  trialConfig -> getDouble("trial.bounds.size[1]", 0.0, NULL, true));
	boundsEdge = trialConfig -> getDouble("trial.bounds.edge", 0.0, NULL, true);
	boundsHalfArea = boundsArea / 2.0f;
	agentAtEdge = false;

	// place drones
	addDrones(dronesOrigin, cellSize, horizontalCells, verticalCells, numPairs, numSingle, pairs);

	// place obstacles
	makeObstacles(trialConfig);

	// default stats
	uncompressedNetwork = NULL;
	compressedNetwork = NULL;
	trialTime = 0.0f;

	// prepare for stats output
	saveOutputTimer = SAVE_INTERVAL_SECS;
	openOutputFile();

	// track IDs of paired drones
	if(pairs.size() % 2 != 0)
	{
		LOG_ERROR("[PFTC] incoming pair list size is not an even number; this makes no sense");
	}
	pairedIDs.assign(pairs.begin(), pairs.end());
}

PairwiseFlockingTrialController::~PairwiseFlockingTrialController()
{
	closeOutputFile();
}

bool PairwiseFlockingTrialController::start()
{
	LOG_INFO("[PFTC] starting controller");
	trialState = STATE_STARTING;
	return true;
}

void PairwiseFlockingTrialController::pause()
{
	// [todo]
}

void PairwiseFlockingTrialController::resume()
{
	// [todo]
}

bool PairwiseFlockingTrialController::stopAndLand()
{
	return false;
}

bool PairwiseFlockingTrialController::scram()
{
	return false;
}

bool PairwiseFlockingTrialController::isComplete()
{
	return trialState == STATE_COMPLETE;
}

void PairwiseFlockingTrialController::update(float dt)
{
	// use the sensor object to read the drones, their IDs, and poses
	processDetections();

	// now control state-specific logic
	if     (trialState == STATE_INITIAL) updateStateInitial(dt);
	else if(trialState == STATE_STARTING) updateStateStarting(dt);
	else if(trialState == STATE_SCANNING) updateStateScanning(dt);
	else if(trialState == STATE_TAKING_OFF) updateStateTakingOff(dt);
	else if(trialState == STATE_GAINING_HEIGHT) updateStateGainingHeight(dt);
	else if(trialState == STATE_FLYING) updateStateFlying(dt);
	else if(trialState == STATE_LAND_NOW) updateStateLandNow(dt);
	else if(trialState == STATE_LANDING) updateStateLanding(dt);
	else if(trialState == STATE_COMPLETE) updateStateComplete(dt);

	// control general agent logic
	updateAgents(dt);

	// now issue the appropriate commands to the agents so they change their velocity to what we want
	issueFlightCommands();

	// build the flocking networks that are needed to compute the flock statistics
	buildUncompressedFlockingNetwork();
	buildCompressedFlockingNetwork();
}

void PairwiseFlockingTrialController::processDetections()
{
	vector<DroneDetection>::iterator i;

	sensingInterface -> getDrones(detections);
	detectionsByID.clear();
	for(i = detections.begin(); i != detections.end(); ++ i)
	{
		detectionsByID[i -> id] = *i;
	}
}

void PairwiseFlockingTrialController::render(const glm::mat4 &viewProjection)
{
	renderConnections(viewProjection);
	renderObstacles(viewProjection);
	renderBounds(viewProjection);
}

void PairwiseFlockingTrialController::updateStateInitial(float dt)
{
	// nothing happens here until we receive a signal to start the trial
}

void PairwiseFlockingTrialController::updateStateStarting(float dt)
{
	trialState = STATE_SCANNING;
    scanningTimer = 0.0f;

    // any other setup occurs here
}

void PairwiseFlockingTrialController::updateStateScanning(float dt)
{
	const float SCAN_TIME_SECONDS = 1.0f;
	const float TARGET_TAKEOFF_VELOCITY = 0.4f;//1.5f;//0.5f;
	const float TARGET_HEIGHT = 1.5f;

	PairwiseFlockingAgent *agent;
	vector<DroneDetection>::iterator i, j;
	vector<uint32_t>::iterator k;
	bool found;

	// go through all detections in the current frame
	for(i = detections.begin(); i != detections.end(); ++ i)
	{
        // is this drone already detected in our master list?
        found = false;
        j = masterList.begin();
        while(!found && j != masterList.end())
        {
			found = (*j++) == *i;
        }

        // if not, add it
        if(!found)
        {
			masterList.push_back(*i);
        }
	}

	// elapse time and see if we can transition now
	scanningTimer += dt;
	if(scanningTimer > SCAN_TIME_SECONDS)
	{
		// transition to taking off state
		trialState = STATE_TAKING_OFF;

		// create the agents whose IDs we scanned
		LOG_INFO("[PFTC] scan complete -- detected " << masterList.size() << " drones");
		for(i = masterList.begin(); i != masterList.end(); ++ i)
		{
			agent = new PairwiseFlockingAgent(i -> id, viewRange, closePairing);
			agents.push_back(agent);
			agentsByID[i -> id] = agent;

			//agent -> setMaxVelocity(MAX_TAKEOFF_VELOCITY);
			agent -> setTargetHeight(TARGET_HEIGHT, TARGET_TAKEOFF_VELOCITY);
			agent -> setBounds(boundsArea, boundsEdge);
		}

		// now assign pairs where they exist
		for(k = pairedIDs.begin(); k != pairedIDs.end(); k += 2)
		{
			agentsByID[*k] -> setPartnerID(*k + 1);
			agentsByID[*k + 1] -> setPartnerID(*k);
		}
	}
}

void PairwiseFlockingTrialController::updateStateTakingOff(float dt)
{
	vector<DroneDetection>::iterator i;
	vec3 pos;

	// issue take-off commands to drones
	LOG_INFO("[PFTC] taking off and setting target height");

	// send global sensor detections to other drones
	feedAgentSensors();

	// let the drones get to the target height
	trialState = STATE_GAINING_HEIGHT;
}

void PairwiseFlockingTrialController::updateStateGainingHeight(float dt)
{
	const float MIN_TRIAL_HEIGHT = 1.45f;

	vector<DroneDetection>::iterator i;
	bool allDronesReady;

	// send global sensor detections to other drones
	feedAgentSensors();

	// check if all drones are at the minimum trial altitude
	i = detections.begin();
	allDronesReady = true;
	while(i != detections.end() && allDronesReady)
	{
		allDronesReady = (i++) -> pos.y >= MIN_TRIAL_HEIGHT;
	}

	// go to the next state if all drones are high enough
	if(allDronesReady)
	{
		trialState = STATE_FLYING;
		LOG_INFO("[PFTC] min height reached; starting flocking");

		for(i = masterList.begin(); i != masterList.end(); ++ i)
		{
			agentsByID[i -> id] -> setInteractionsEnabled(true);
		}
	}
}

void PairwiseFlockingTrialController::updateStateFlying(float dt)
{
	// send global sensor detections to other drones
	feedAgentSensors();

	// control stats output
	saveOutputTimer += dt;
	if(saveOutputTimer >= SAVE_INTERVAL_SECS)
	{
		saveOutputTimer = 0.0f;
		saveOutputStats();
	}

	// trial timer runs only while all drones are flying
	trialTime += dt;
	if(maxTrialSeconds != 0 && trialTime >= maxTrialSeconds)
	{
		trialState = STATE_LAND_NOW;
	}
}

void PairwiseFlockingTrialController::updateStateLandNow(float dt)
{
	const float MAX_LAND_VELOCITY = 0.75f;
	const float TARGET_HEIGHT = 0.0f;

	vector<PairwiseFlockingAgent*>::iterator i;
	PairwiseFlockingAgent *curr;

	// send global sensor detections to other drones
	feedAgentSensors();

	// instruct all drones to land
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		curr = *i;
		curr -> setTargetHeight(TARGET_HEIGHT, MAX_LAND_VELOCITY);
		curr -> setInteractionsEnabled(false);
	}

	// advance to waiting state
	trialState = STATE_LANDING;
}

void PairwiseFlockingTrialController::updateStateLanding(float dt)
{
	const float MAX_LAND_HEIGHT = 0.01f;

	vector<DroneDetection>::iterator i;
	vector<PairwiseFlockingAgent*>::iterator j;
	bool allDronesReady;
	vec3 vel;

	// send global sensor detections to other drones
	feedAgentSensors();

	// check if all drones are at the minimum trial altitude
	i = detections.begin();
	allDronesReady = true;
	while(i != detections.end() && allDronesReady)
	{
		allDronesReady = (i++) -> pos.y <= MAX_LAND_HEIGHT;
	}

	// print velocities of drones
	for(j = agents.begin(); j != agents.end(); ++ j)
	{
		vel = (*j) -> getVelocity();
	}

	// move to the final state if the drones have all touched down
	if(allDronesReady)
	{
		trialState = STATE_COMPLETE;
		LOG_INFO("[PFTC] all drones have landed successfully");
	}
}

void PairwiseFlockingTrialController::updateStateComplete(float dt)
{
	// nothing
}

void PairwiseFlockingTrialController::updateAgents(float dt)
{
	vector<PairwiseFlockingAgent*>::iterator i;

	// update pairwise flocking agent logic; this also updates the height and velocity PID controllers and gives us a desired velocity vector
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		(*i) -> update(dt);
	}
}

void PairwiseFlockingTrialController::feedAgentSensors()
{
	vector<DroneDetection>::iterator i, k;
	vector<Obstacle*>::iterator m;
    map<uint32_t, PairwiseFlockingAgent*>::iterator j;
    map<uint32_t, DroneDetection>::iterator n;
	PairwiseFlockingAgent *curr;
	int partnerID;
	bool canSeePartner;
	float partnerDist;
	vec3 partnerVec;
	vec3 partnerVel;
	mat3 orientation;
	DroneDetection neighbour;
	vec3 velocity;

	// reset edge boundary tracking
	agentAtEdge = false;

	// reset line of sight visual debugging
	for(m = obstacles.begin(); m != obstacles.end(); ++ m)
	{
		(*m) -> obscuring = false;
	}

	// take the detections we received and inform each agent of whatever they need to know
    for(i = detections.begin(); i != detections.end(); ++ i)
    {
		j = agentsByID.find(i -> id);
		if(j != agentsByID.end())
		{
			// set information that the drone should know about itself; global position is not included in this, since the agent doesn't (and shouldn't!) need to know that
			curr = j -> second;
			curr -> setHeight(i -> pos.y);
			curr -> setPos(i -> pos);
			partnerID = curr -> getPartnerID();

			// transform velocity to drone's local frame of reference
			orientation[0] = i -> side;
			orientation[1] = i -> up;
			orientation[2] = i -> forward;

			//velocity = inverse(orientation) * i -> velocity;
			velocity = (orientation * i -> velocity);
			curr -> setVelocity(velocity);

			// prepare a new list of neighbours to track
			curr -> clearNeighbours();

			// do we have a partner? check if our global sensor picked them up; if they didn't, there's nothing we can do
			canSeePartner = false;
			if(partnerID >= 0)
			{
				// can we see them?
				n = detectionsByID.find(partnerID);
				if(n != detectionsByID.end())
				{
					canSeePartner = true;
				}
			}

			// make sure our partner is also within our own view range; whether or not we can see it determines how we interact with other neighbours
			partnerDist = 0.0f;
			if(partnerID >= 0 && canSeePartner)
			{
				// ignore the partner if it's too far away to see or blocked by an obstacle, or our sensor is being unreliable
				partnerVec = (n -> second.pos - i -> pos) + gaussRand(0.0f, neighbourSensingNoiseStdDev);
				partnerDist = length(vec2(partnerVec.x, partnerVec.z));
				partnerVel = n -> second.velocity - i -> velocity;

				// if partner separation is enabled, we allow the possibility that an agent lost its partner
				if(partnerSeparation)
				{
					if(partnerDist > viewRange || !hasLineOfSight(n -> second.pos, i -> pos) || !isSensorWorkingNow(i -> pos, neighbourSensingReliabilityThreshold))
					{
						canSeePartner = false;
					}
				}
			}

			// add our partner as an interaction neighbour
			if(partnerID >= 0 && canSeePartner)
			{
				neighbour.id = partnerID;
				neighbour.pos = partnerVec;
				neighbour.dist = partnerDist;
				neighbour.velocity = partnerVel;
				curr -> storeNeighbour(neighbour);
			}

			// drone also receives information about potential neighbours
			for(k = detections.begin(); k != detections.end(); ++ k)
			{
				// drone won't detect itself; also, skip any partner processing because we did that already
				if(i != k && i -> id != k -> id && (int)k -> id != partnerID)
				{
					// build neighbour detection info, and put in current agent's frame of reference
					neighbour.id = k -> id;
                    neighbour.pos = (k -> pos - i -> pos) + gaussRand(0.0f, neighbourSensingNoiseStdDev);
                    neighbour.velocity = k -> velocity - i -> velocity;					// [todo]: adjust local velocity sensing to be noisy as well
                    neighbour.dist = length(vec2(neighbour.pos.x, neighbour.pos.z));

                    // can we see this agent? note that view range, line of sight, and sensor reliability are all taken into account
                    if(neighbour.dist <= viewRange && hasLineOfSight(i -> pos, k -> pos) && isSensorWorkingNow(i -> pos, neighbourSensingReliabilityThreshold))
                    {
						// do we have/can we see a partner?
						if(partnerID >= 0 && canSeePartner)
						{
							// yes; if we're restricting ourselves to asymmetric tracking only, then only store neighbours at the correct orientation with respect to our partner;
							// the idea is that our partner acts as a proxy for other neighbours that we do not store here; otherwise, just add anyways
							if(dot(partnerVec, neighbour.pos) <= 0.0f || !asymmetricTracking)
							{
								curr -> storeNeighbour(neighbour);
							}
						}
						else
						{
							// no; just add the neighbour
							curr -> storeNeighbour(neighbour);
						}
                    }
				}
			}

			// drone limits its neighbour count by the number of neighbours it's supposed to track
			curr -> sortAndLimitNeighbours(numTrackedIfSingle, numTrackedIfPaired);

			// drone also receives information about obstacles
			curr -> clearObstacles();
			for(m = obstacles.begin(); m != obstacles.end(); ++ m)
			{
				curr -> storeObstacle(Obstacle((*m) -> pos - i -> pos, (*m) -> radius, (*m) -> avoidRadius));
			}

			// if an agent gets close to the world edge, note this
			if(curr -> isCloseToBounds()) agentAtEdge = true;
		}
		else
		{
			// this might not be a big deal, but let's throw a not-fatal warning if we get an ID we're not familiar with; it's just likely some sensor edge case; if you
			// get lots of these it's probably indicative of a bigger problem
			LOG_INFO("[PFTC] WARNING: drone ID " << i -> id << " was detected but no drone is known by that name");
		}
    }
}

void PairwiseFlockingTrialController::issueFlightCommands()
{
	vector<PairwiseFlockingAgent*>::iterator i;
	PairwiseFlockingAgent *curr;
	FlightCommand command;
	float thrust;

	// send flight commands based on PID output
	droneInterface -> clearCommands();
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		curr = *i;

		// compute our thrust level, but make sure we keep a minimum at all times to avoid dropping out of the sky
		thrust = SimulatedUBee::BASE_THRUST + curr -> getVelocityYPIDResult();
		if(thrust < SimulatedUBee::MIN_THRUST)
		{
			thrust = SimulatedUBee::MIN_THRUST;
		}

		// build the rest of the flight command
		command.id = curr -> getID();						// drone ID
		command.thrust = thrust;							// throttle
		command.roll = curr -> getVelocityXPIDResult();		// roll
		command.pitch = curr -> getVelocityZPIDResult();	// pitch
		command.yaw = 0.0f;									// [todo]: add yaw to compensate for yaw drift in real drones

		// add to interface for transmission later
		droneInterface -> addCommand(command);
	}
}

void PairwiseFlockingTrialController::buildUncompressedFlockingNetwork()
{
	vector<PairwiseFlockingAgent*>::iterator i;
	vector<DroneDetection>::iterator j;
	vector<int> ids;
	PairwiseFlockingAgent *curr;

	// clear the old uncompressed flocking network
	if(!uncompressedNetwork)
	{
		uncompressedNetwork = new FlockingNetwork();
	}
	uncompressedNetwork -> clear();

	// add the agents we have to the network
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		ids.push_back((*i) -> getID());
	}
	uncompressedNetwork -> addAgents(ids);

	// now add their connections---these are interpreted as bi-directional links by the FlockingNetwork
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		curr = *i;
		for(j = curr -> getNeighboursBegin(); j != curr -> getNeighboursEnd(); ++ j)
		{
			uncompressedNetwork -> connectAgents(curr -> getID(), j -> id);
		}
	}

	// compute flock statistics
	uncompressedNetwork -> computeStats();
}

void PairwiseFlockingTrialController::buildCompressedFlockingNetwork()
{
	map<int, int> mappedIDs;
	map<int, int>::iterator j;
	vector<PairwiseFlockingAgent*>::iterator i;
	PairwiseFlockingAgent *curr;
	vector<int> compressedIDs;
	vector<int>::iterator k;
	vector<DroneDetection>::iterator m;
	int myID, partnerID, minID;
	bool present;

	// clear the old compressed flocking network
	if(!compressedNetwork)
	{
		compressedNetwork = new FlockingNetwork();
	}
	compressedNetwork -> clear();

	// build an ID mapping for all agents
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		curr = *i;
		myID = curr -> getID();
		mappedIDs[myID] = myID;
	}

	// re-map paired agents' connections to the lowest ID in that pair; now each member of a pair is mapped to a single ID
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		curr = *i;
        if(curr -> getCanSeePartner())
        {
			// extract IDs
			myID = curr -> getID();
			partnerID = curr -> getPartnerID();

			// compute the minimum of those IDs and map both of our IDs to that minimum
			minID = glm::min(partnerID, myID);
            mappedIDs[partnerID] = minID;
        }
	}

	// build a complete list of all IDs that we have mapped to; only higher IDs belonging to mated pairs will not be present
	for(j = mappedIDs.begin(); j != mappedIDs.end(); ++ j)
	{
		// make sure the entry is not already present
		present = false;
		k = compressedIDs.begin();
		while(!present && k != compressedIDs.end())
		{
			present = (*k++) == j -> second;
		}

		// if not already present, add it
		if(!present)
		{
			compressedIDs.push_back(j -> second);
		}
	}

	// now add our list of compressed IDs to the network
	compressedNetwork -> addAgents(compressedIDs);

	// now connect these agents based on our ID mapping, not the actual IDs themselves
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		curr = *i;
		for(m = curr -> getNeighboursBegin(); m != curr -> getNeighboursEnd(); ++ m)
		{
			compressedNetwork -> connectAgents(mappedIDs[curr -> getID()], mappedIDs[m -> id]);
		}
	}

	// compute our network stats
	compressedNetwork -> computeStats();
}

void PairwiseFlockingTrialController::openOutputFile()
{
    stringstream ss;
    string folder;
	uint32_t index;

	// create the directory indicated (if any) if it does not exist yet
	index = trialOutputFilename.find_last_of('/');
	if(index != string::npos)
	{
		folder = trialOutputFilename.substr(0, index);
		if(!dirDoesDirectoryExist(folder))
		{
			dirMakeDirectory(folder);
		}
	}

    // build the output file name and open the file for writing
    ss << trialOutputFilename << ".txt";
    outputFileName = ss.str();
    outputFile.open(outputFileName.c_str());
    if(!outputFile.is_open())
    {
		LOG_ERROR("[PFTC] could not open output file \"" << outputFileName << "\" for writing");
    }
}

void PairwiseFlockingTrialController::closeOutputFile()
{
	if(outputFile.is_open())
	{
		outputFile.close();
	}
}

void PairwiseFlockingTrialController::saveOutputStats()
{
	outputFile << (int)round(trialTime) << ",";
	outputFile << uncompressedNetwork -> getNumSubFlocks() << ",";
	outputFile << uncompressedNetwork -> getAverageSubFlockSize() << ",";
	outputFile << uncompressedNetwork -> getAverageDiameter() << ",";
	outputFile << uncompressedNetwork -> getAverageDegree() << ",";
	outputFile << uncompressedNetwork -> getNumConnections() << ",";
	outputFile << compressedNetwork -> getAverageDiameter() << ",";
	outputFile << compressedNetwork -> getAverageDegree() << ",";
	outputFile << compressedNetwork -> getNumConnections();
	outputFile << endl;

	LOG_INFO("---------------------------------------------------------------");
	LOG_INFO("sub flock size  : " << uncompressedNetwork -> getAverageSubFlockSize() << "  |  " << compressedNetwork -> getAverageSubFlockSize());
	LOG_INFO("avg diameter    : " << uncompressedNetwork -> getAverageDiameter() << "  |  " << compressedNetwork -> getAverageDiameter());
	LOG_INFO("avg degree      : " << uncompressedNetwork -> getAverageDegree() << "  |  " << compressedNetwork -> getAverageDegree());
	LOG_INFO("num connections : " << uncompressedNetwork -> getNumConnections() << "  |  " << compressedNetwork -> getNumConnections());
}

bool PairwiseFlockingTrialController::hasLineOfSight(const vec3 &a, const vec3 &b)
{
	vector<Obstacle*>::iterator i;
	bool canSee = true;
	dvec2 p1, p2, center, intersect;

	// skip the occlusion check if occlusion is disabled
	if(obstacleOcclusion)
	{
		i = obstacles.begin();
		while(canSee && i != obstacles.end())
		{
			// do a simple ray-sphere intersection test for this obstacle
			p1 = vec2(a.x, a.z);
			p2 = vec2(b.x, b.z);
			center = vec2((*i) -> pos.x, (*i) -> pos.z);
			canSee = !rayIntersectsCircle(p1, p2, center, (*i) -> radius, intersect);

			// for debugging purposes, indicate visually later on that the obstacle is blocking our view
			if(!canSee)
			{
				(*i) -> obscuring = true;
			}

			// next obstacle
			++ i;
		}
	}

	return canSee;
}

bool PairwiseFlockingTrialController::isSensorWorkingNow(const vec3 &pos, float reliability)
{
	// a common misconception about Simplex noise is that it ranges from [-1, 1] when in fact it ranges from [-sqrt(N/4), sqrt(N/4)] where N is the number of
	// dimensions; thus, we need to scale our Simplex noise to fit in the range (0, 1] so that our sensor reliability theshold (from [0, 1]) makes sense
	const float SIMPLEX_MAX = sqrt(3.0f / 4.0f);
	const float SIMPLEX_RANGE = SIMPLEX_MAX * 2.0f;
	const float SCALING_FACTOR = 10.0f;

	// compute Simplex noise for the position we're trying to sense
	float val = simplex(pos * SCALING_FACTOR);

	// bring it into the range (0, 1]
	float scaled = clamp((val + SIMPLEX_MAX) / SIMPLEX_RANGE, 0.000001f, 1.0f);

	// see if our sensor meets the reliability test
	return reliability >= scaled;
}

void PairwiseFlockingTrialController::renderConnections(const glm::mat4 &viewProjection)
{
	const vec4 OUTGOING_COLOR(0.0f, 1.0f, 0.0f, 1.0f);
	const vec4 INCOMING_COLOR(0.0f, 0.0f, 0.0f, 0.0f);

	const vec4 PARTNER_COLOR(1.0f, 0.0f, 0.0f, 1.0f);
	const vec4 PARTNER_COLOR_INCOMING(1.0f, 0.0f, 0.0f, 0.0f);

	const vec4 CATCHING_UP_COLOR(0.2f, 1.0f, 1.0f, 1.0f);

	vector<DroneDetection>::iterator i, k;
	map<uint32_t, PairwiseFlockingAgent*>::iterator j;
	map<uint32_t, DroneDetection>::iterator m;
	PairwiseFlockingAgent *curr;

	// go through all agents we detected this frame
	for(i = detections.begin(); i != detections.end(); ++ i)
	{
		// make sure this is an agent ID we recognize
		j = agentsByID.find(i -> id);
		if(j != agentsByID.end())
		{
			// iterate through this agent's interaction partners and see if we can find them in the list of detections for this frame
			curr = j -> second;
			for(k = curr -> getNeighboursBegin(); k != curr -> getNeighboursEnd(); ++ k)
			{
				// find this agent in our detections
				m = detectionsByID.find(k -> id);
				if(m != detectionsByID.end())
				{
					// draw a line between the two of them
					if(j -> second -> getPartnerID() == (int)m -> second.id)
					{
						// partner connections are red
						if(j -> second -> getCatchingUpToPartner())
						{
							LineRenderer::getInstance() -> add(i -> pos, m -> second.pos, CATCHING_UP_COLOR, CATCHING_UP_COLOR);
						}
						else
						{
							LineRenderer::getInstance() -> add(i -> pos, m -> second.pos, PARTNER_COLOR, PARTNER_COLOR_INCOMING);
						}

					}
					else
					{
						// regular neighbour connections are green
						LineRenderer::getInstance() -> add(i -> pos, m -> second.pos, OUTGOING_COLOR, INCOMING_COLOR);
					}
				}
				else
				{
					//LOG_INFO("[PFTC] WARNING: renderConnections() could not find interaction partner with ID " << k -> id);
				}
			}
		}
		else
		{
			//LOG_INFO("[PFTC] WARNING: renderConnections() could not find a drone with ID " << i -> id);
		}
	}
}

void PairwiseFlockingTrialController::renderObstacles(const glm::mat4 &viewProjection)
{
	vector<Obstacle*>::iterator i;

	mat4 modelMat;

	// set appropriate blending and depth test modes
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_DEPTH_TEST);

	// position the camera
	sphereShader -> bind();
	sphereShader -> uniformMat4("u_VP", viewProjection);

	// render each obstacle and its repulsion radius
	for(i = obstacles.begin(); i != obstacles.end(); ++ i)
	{
		modelMat = mat4(1.0f);
		modelMat = translate(modelMat, (*i) -> pos);
		modelMat = scale(modelMat, vec3((*i) -> radius * 2.0f));

		sphereShader -> uniformMat4("u_Model", modelMat);
		sphereShader -> uniformVec4("u_Color", (*i) -> obscuring ? vec4(1.0f, 1.0f, 0.0f, 1.0f) : vec4(1.0f, 0.0f, 0.0f, 1.0f));
		sphereModel -> render();

		// repulsion radius does not write to depth buffer, so we avoid obscuring objects we should be able to see behind us
		glDepthMask(GL_FALSE);

		modelMat = mat4(1.0f);
		modelMat = translate(modelMat, (*i) -> pos);
		modelMat = scale(modelMat, vec3((*i) -> avoidRadius * 2.0f));

		sphereShader -> uniformMat4("u_Model", modelMat);
		sphereShader -> uniformVec4("u_Color", vec4(1.0f, 0.0f, 0.0f, 0.25f));
		sphereModel -> render();

		// restore depth writes
		glDepthMask(GL_TRUE);
	}

	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
}

void PairwiseFlockingTrialController::renderBounds(const mat4 &viewProjection)
{
	LineRenderer *lines = LineRenderer::getInstance();

	const float HEIGHT = 0.25f;

	const vec4 COLOR = (agentAtEdge ? vec4(1.0f, 0.0f, 0.0f, 0.75f) : vec4(0.0f, 1.0f, 0.0f, 0.75f));

	const vec3 CORNER_1(boundsHalfArea.x, HEIGHT, boundsHalfArea.y);
	const vec3 CORNER_2(-boundsHalfArea.x, HEIGHT, boundsHalfArea.y);
	const vec3 CORNER_3(-boundsHalfArea.x, HEIGHT, -boundsHalfArea.y);
	const vec3 CORNER_4(boundsHalfArea.x, HEIGHT, -boundsHalfArea.y);

	lines -> add(CORNER_1, CORNER_2, COLOR);
	lines -> add(CORNER_2, CORNER_3, COLOR);
	lines -> add(CORNER_3, CORNER_4, COLOR);
	lines -> add(CORNER_4, CORNER_1, COLOR);

	// now render everything
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glDepthMask(GL_FALSE);
	lines -> send();
	lines -> render(viewProjection);
	lines -> clear();

	glDepthMask(GL_TRUE);
	glDisable(GL_DEPTH_TEST);
}

void PairwiseFlockingTrialController::addDrones(const glm::vec3 &origin, float gridCellSize, int horizontalCells, int verticalCells, int numPairs, int numSingles, vector<uint32_t> &pairs)
{
	//const float PAIRED_DRONE_OFFSET = 0.5f;

	int numTotalCells = horizontalCells * verticalCells;
	SimulatedUBee **drones;
	vec3 dronePos;
	int pos1, pos2;
	int x1, y1, x2, y2;
	int i;
	uint32_t id = 0;
	vec3 topLeft = vec3(-gridCellSize * (float)(horizontalCells - 1) * 0.5f, 0.0f, -gridCellSize * (float)(verticalCells - 1) * 0.5f);

	// allocate memory for non-overlapping drone positions we will spawn in
	drones = new SimulatedUBee*[numTotalCells];
    for(i = 0; i < numTotalCells; ++ i)
    {
		drones[i] = NULL;
    }

	// spawn the pairs first; they spawn next to each other and thus each pair takes up more space than a single drone; that way,
	// when we spawn the singles after the pairs, the singles can just fit into the unused spots
	i = 0;
	while(i < numPairs)
	{
		pos1 = rndLinear(0, numTotalCells - horizontalCells - 1);			// spawn on any row but the last
		x1 = pos1 % horizontalCells;
		y1 = pos1 / horizontalCells;
		if(drones[pos1] == NULL)
		{
			pos2 = pos1 + horizontalCells;
			x2 = pos2 % horizontalCells;
			y2 = pos2 / horizontalCells;
			if(drones[pos1] == NULL && drones[pos2] == NULL)
			{
				// add one partner
				drones[pos1] = new SimulatedUBee(id++);
				dronePos = topLeft + vec3(x1 * gridCellSize, 0.0f, y1 * gridCellSize);
				world -> addSimulatedDrone(drones[pos1], dronePos);

				// now add the other
				drones[pos2] = new SimulatedUBee(id++);
				dronePos = topLeft + vec3(x2 * gridCellSize, 0.0f, (y2 * gridCellSize));// - PAIRED_DRONE_OFFSET);
				world -> addSimulatedDrone(drones[pos2], dronePos);

				// assign partnership
				pairs.push_back(drones[pos1] -> getID());
				pairs.push_back(drones[pos2] -> getID());

				// next pair
				++ i;
			}
		}
	}

    // now spawn the single drones
    i = 0;
	while(i < numSingles)
	{
		pos1 = rndLinear(0, numTotalCells - 1);
		x1 = pos1 % horizontalCells;
		y1 = pos1 / horizontalCells;
		if(drones[pos1] == NULL)
		{
			drones[pos1] = new SimulatedUBee(id++);
			dronePos = topLeft + vec3(x1 * gridCellSize, 0.0f, y1 * gridCellSize);
			world -> addSimulatedDrone(drones[pos1], dronePos);
			++ i;
		}
	}
}

void PairwiseFlockingTrialController::makeObstacles(GumdropNode *trialConfig)
{
	const float HEIGHT = 1.5f;

	vec3 origin;
	float cellSize;
	uint32_t horizontalCells;
	uint32_t verticalCells;
	float softRadius;
	float hardRadius;

	uint32_t i, j;
	vec3 topLeft;

	// obstacles might be turned off
	if(trialConfig -> getBool("trial.obstacles_enabled", false, NULL, true))
	{
		// pull obstacle field characteristics
		origin = vec3(trialConfig -> getDouble("trial.obstacle_field.origin[0]", 0.0f, NULL, true),
					  0.0f,
					  trialConfig -> getDouble("trial.obstacle_field.origin[1]", 0.0f, NULL, true));
		cellSize = trialConfig -> getDouble("trial.obstacle_field.cell_size", 0.0f, NULL, true);
		horizontalCells = trialConfig -> getInt("trial.obstacle_field.cells[0]", 0, NULL, true);
		verticalCells = trialConfig -> getInt("trial.obstacle_field.cells[1]", 0, NULL, true);
		softRadius = trialConfig -> getDouble("trial.obstacle_field.soft_radius", 0.0f, NULL, true);
		hardRadius = trialConfig -> getDouble("trial.obstacle_field.hard_radius", 0.0f, NULL, true);

		// position the obstacles
		topLeft = origin + vec3(-cellSize * (float)(horizontalCells - 1) * 0.5f, 0.0f, -cellSize * (float)(verticalCells - 1) * 0.5f);
		for(i = 0; i < verticalCells; ++ i)
		{
			for(j = 0; j < horizontalCells; ++ j)
			{
				obstacles.push_back(new Obstacle(topLeft + vec3(j * cellSize, HEIGHT, i * cellSize), hardRadius, softRadius));
			}
		}
	}
}

void PairwiseFlockingTrialController::loadAssets()
{
	WavefrontModel *sphereWavefront = WavefrontModel::fromFile("../mesh/sphere.obj");
	sphereModel = Model::fromWavefront(sphereWavefront);
	delete sphereWavefront;

	// load our sphere shader
	sphereShader = Shader::fromFile("../shaders/solid-plain.vert", "../shaders/solid-plain.frag");
	sphereShader -> bindAttrib("a_Vertex", 0);
	sphereShader -> bindAttrib("a_Normal", 1);
	sphereShader -> link();
	sphereShader -> bind();
	sphereShader -> uniformVec3("u_SunColor", vec3(1.0f, 1.0f, 1.0f));
	sphereShader -> uniformVec3("u_SunDirection", vec3(0.0f, -1.0f, 0.0f));
	sphereShader -> uniformVec3("u_AmbientColor", vec3(0.2f, 0.2f, 0.2f));
}
