#include "experiments/pairwiseflocking/pairwiseflockingneighbourentry.h"

#include "objects/pairwiseflocking/pairwiseflockingagent.h"

#include "glm/glm.hpp"
using namespace glm;

#include <string>
#include <sstream>
using namespace std;

PairwiseFlockingNeighbourEntry::PairwiseFlockingNeighbourEntry()
	: agent(NULL), pos(0.0f), vel(0.0f), dist(0.0f), isPartner(false)
{ }

PairwiseFlockingNeighbourEntry::PairwiseFlockingNeighbourEntry(PairwiseFlockingAgent *n_agent, const vec2 &n_pos, const vec2 &n_vel, float n_dist, bool n_isPartner)
	: agent(n_agent), pos(n_pos), vel(n_vel), dist(n_dist), isPartner(n_isPartner)
{
	// anything else...?
}

bool PairwiseFlockingNeighbourEntry::operator < (const PairwiseFlockingNeighbourEntry &other) const
{
	return isPartner || (!isPartner && dist < other.dist);
}

string PairwiseFlockingNeighbourEntry::toString() const
{
	stringstream ss;
	ss << "ID: " << agent -> getID() << "    dist: " << dist << "      isPartner? " << isPartner;
	return ss.str();
}
