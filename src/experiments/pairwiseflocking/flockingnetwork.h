#pragma once

#include <map>
#include <vector>

class SubFlock;
class SubFlockAgent;

class FlockingNetwork
{
public:
	FlockingNetwork();
	~FlockingNetwork();

	// agent connection management
	void addAgents(const std::vector<int> &ids);
	void connectAgents(int a, int b);
	void clear();

	// stats computation and retrieval
    void computeStats();
	int getNumSubFlocks();
	int getSubFlockSize(int subFlock);
	double getAverageSubFlockSize();
	double getAverageDiameter();
	double getAverageDegree();
	int getNumConnections();

private:
	// stats calculations; note that when computing connected components, diameters, and degrees, directional edges are converted to bi-directional edges;
	// also, in the case of computeNumConnections(), every *directional* edge counts as 1 connection; therefore, a mutually-tracking pair of agents will
	// count as 2 connections
	void resetStats();
	void computeSubFlocks();
	void computeDiameters();
	void computeAverageDegree();
	void computeNumConnections();

	// useful utility functions
	int floodDistance(SubFlockAgent *start, SubFlockAgent **path);
	bool contains(std::vector<SubFlockAgent*> agents, SubFlockAgent *agent);
	void moveConnectionsFrom(SubFlockAgent *from, SubFlockAgent *to);
	void removeAllConnections(SubFlockAgent *agent);

	// list of agents
	std::vector<SubFlockAgent*> agents;
	std::map<int, SubFlockAgent*> agentsByID;

	// some stats
	std::vector<SubFlock*> subFlocks;
	double averageSubFlockSize;
	double averageDiameter;
	double averageDegree;
	int numConnections;
};
