#pragma once

#include "glm/glm.hpp"

#include <string>

class PairwiseFlockingAgent;

class PairwiseFlockingNeighbourEntry
{
public:
	PairwiseFlockingNeighbourEntry();
	PairwiseFlockingNeighbourEntry(PairwiseFlockingAgent *agent, const glm::vec2 &n_pos, const glm::vec2 &n_vel, float n_dist, bool isPartner);

	bool operator < (const PairwiseFlockingNeighbourEntry &other) const;

	std::string toString() const;

	PairwiseFlockingAgent *agent;
	glm::vec2 pos;
	glm::vec2 vel;
	float dist;
	bool isPartner;
};
