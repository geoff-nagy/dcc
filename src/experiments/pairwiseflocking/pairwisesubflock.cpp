#include "experiments/pairwiseflocking/pairwisesubflock.h"

#include <set>
using namespace std;

PairwiseSubFlock::PairwiseSubFlock()
{
	diameter = -1;
	diameterPath = new PairwiseFlockingAgent*[100];
	averageDegree = 0.0;
}

PairwiseSubFlock::~PairwiseSubFlock()
{
	delete[] diameterPath;
}
