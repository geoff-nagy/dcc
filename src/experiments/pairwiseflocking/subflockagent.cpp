#include "experiments/pairwiseflocking/subflockagent.h"

#include <vector>
using namespace std;

SubFlockAgent::SubFlockAgent(int id)
	: id(id)
{
	hopNumber = 0;
}

int SubFlockAgent::getID()
{
	return id;
}

void SubFlockAgent::setHopNumber(int hopNumber)
{
	this -> hopNumber = hopNumber;
}

int SubFlockAgent::getHopNumber()
{
	return hopNumber;
}

void SubFlockAgent::connect(SubFlockAgent *other)
{
	connections.push_back(other);
}

void SubFlockAgent::clearConnections()
{
	connections.clear();
}

void SubFlockAgent::removeConnectionsTo(SubFlockAgent *other)
{
	vector<SubFlockAgent*>::iterator i = connections.begin();
	while(i != connections.end())
	{
		if((*i) -> getID() == other -> getID())
		{
			i = connections.erase(i);
		}
		else
		{
			++ i;
		}
	}
}

bool SubFlockAgent::isConnectedTo(SubFlockAgent *other)
{
	vector<SubFlockAgent*>::iterator i = connections.begin();
	bool result = false;

	while(!result && i != connections.end())
	{
		result = (*i++) -> getID() == other -> getID();
	}

	return result;
}

bool SubFlockAgent::hasConnections()
{
	return connections.size();
}

vector<SubFlockAgent*>::iterator SubFlockAgent::getConnectionsBegin()
{
	return connections.begin();
}

vector<SubFlockAgent*>::iterator SubFlockAgent::getConnectionsEnd()
{
	return connections.end();
}

SubFlockAgent *SubFlockAgent::getConnectionWithHopNumber(int hop)
{
	vector<SubFlockAgent*>::iterator i = connections.begin();
	SubFlockAgent *result = NULL;

	while(!result && i != connections.end())
	{
		if((*i) -> getHopNumber() == hop)
		{
			result = *i;
		}
		++ i;
	}

	return result;
}

int SubFlockAgent::getNumConnections()
{
	// [note]: we divide by 2 here because of the way that connections are handled in higher-level logic for the sake
	//         of building our flocking networks; this results in us registering twice the number of connections that
	//         we actually have; so, we just divide by 2 here to get the actual amount
	return connections.size() / 2;
}
