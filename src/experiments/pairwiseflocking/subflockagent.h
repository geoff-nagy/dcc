#pragma once

#include <vector>

class SubFlockAgent
{
public:
	SubFlockAgent(int id);

	int getID();

	void setHopNumber(int hopNumber);
	int getHopNumber();

	void connect(SubFlockAgent *other);
	void clearConnections();
	void removeConnectionsTo(SubFlockAgent *other);
	bool isConnectedTo(SubFlockAgent *other);
	bool hasConnections();
	std::vector<SubFlockAgent*>::iterator getConnectionsBegin();
	std::vector<SubFlockAgent*>::iterator getConnectionsEnd();
	SubFlockAgent *getConnectionWithHopNumber(int count);
	int getNumConnections();

private:

	int id;
	std::vector<SubFlockAgent*> connections;				// used to compute network diameter
	int hopNumber;
};
