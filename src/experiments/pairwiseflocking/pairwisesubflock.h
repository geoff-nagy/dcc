#pragma once

#include <set>

class PairwiseFlockingAgent;

class PairwiseSubFlock
{
public:
	PairwiseSubFlock();
	~PairwiseSubFlock();

	std::set<PairwiseFlockingAgent*> agents;
	int diameter;
	PairwiseFlockingAgent **diameterPath;
	double averageDegree;
};
