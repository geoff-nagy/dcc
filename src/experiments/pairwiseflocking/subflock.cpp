#include "subflock.h"

#include <set>
using namespace std;

SubFlock::SubFlock()
{
	diameter = -1;
	diameterPath = new SubFlockAgent*[100];
	averageDegree = 0.0;
}

SubFlock::~SubFlock()
{
	delete[] diameterPath;
}
