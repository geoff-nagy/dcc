#pragma once

#include "experiments/trialcontroller.h"

#include "sensing/dronedetection.h"

#include <stdint.h>
#include <fstream>
#include <string>
#include <vector>
#include <map>

class FlockingNetwork;
class PairwiseFlockingAgent;
class Obstacle;
class Graph;

class GumdropNode;
class WavefrontModel;
class Model;
class Shader;

class PairwiseFlockingTrialController : public TrialController
{
public:
	typedef enum TRIAL_STATE
	{
		STATE_INITIAL = 0,							// basic initialization
		STATE_STARTING,								// transition to this is controlled by higher-level logic
		STATE_SCANNING,								// several frames of scanning to find the IDs of all drones we're working with
		STATE_TAKING_OFF,							// drones begin taking off to a designated height before their main logic kicks in
		STATE_GAINING_HEIGHT,						// drones are currently gaining altitude to the designated height
		STATE_FLYING,								// drones are now performing pairwise flocking logic and flying together
		STATE_LAND_NOW,								// trial is over; drones are instructed to land
		STATE_LANDING,								// drones are currently landing
		STATE_COMPLETE								// drones have landed and we can safely end
	} TrialState;

	PairwiseFlockingTrialController(World *world,
									SensingInterface *sensingInterface,
									DroneInterface *droneInterface,
									GumdropNode *trialConfig,
									const std::string &trialName,
									std::vector<uint32_t> &pairs,
									bool gui);
	~PairwiseFlockingTrialController();

	virtual void update(float dt);
	virtual bool start();
	virtual void pause();
	virtual void resume();
	virtual bool stopAndLand();
	virtual bool scram();
	virtual bool isComplete();

	virtual void render(const glm::mat4 &viewProjection);

private:
	// - - - private methods - - - //
	void updateStateInitial(float dt);
	void updateStateStarting(float dt);
	void updateStateScanning(float dt);
	void updateStateTakingOff(float dt);
	void updateStateGainingHeight(float dt);
	void updateStateFlying(float dt);
	void updateStateLandNow(float dt);
	void updateStateLanding(float dt);
	void updateStateComplete(float dt);

	void updateAgents(float dt);

	void processDetections();
	void feedAgentSensors();
	void issueFlightCommands();
	void buildUncompressedFlockingNetwork();
	void buildCompressedFlockingNetwork();

	void openOutputFile();
	void closeOutputFile();
	void saveOutputStats();

	bool hasLineOfSight(const glm::vec3 &a, const glm::vec3 &b);
	bool isSensorWorkingNow(const glm::vec3 &pos, float reliability);

	void makeObstacles(GumdropNode *trialConfig);
	void addDrones(const glm::vec3 &origin, float gridCellSize, int horizontalCells, int verticalCells, int numPairs, int numSingles, std::vector<uint32_t> &pairs);

	void renderConnections(const glm::mat4 &viewProjection);
	void renderObstacles(const glm::mat4 &viewProjection);
	void renderBounds(const glm::mat4 &viewProjection);

	void loadAssets();

	// - - - private vars - - - //

	static const float SAVE_INTERVAL_SECS;

	bool gui;
	std::string trialOutputFilename;
	uint32_t maxTrialSeconds;
	float trialTime;

	TrialState trialState;
	uint32_t numTrackedIfPaired;
	uint32_t numTrackedIfSingle;
	float viewRange;

	bool closePairing;
	bool asymmetricTracking;
	float neighbourSensingNoiseStdDev;
	float neighbourSensingReliabilityThreshold;

	bool obstacleOcclusion;
	bool partnerSeparation;

	glm::vec2 boundsArea;
	glm::vec2 boundsHalfArea;
	float boundsEdge;
	bool agentAtEdge;

	World *world;												// interface to world (simulated or otherwise)
	SensingInterface *sensingInterface;							// interface to drone detector (e.g., Vicon)
	DroneInterface *droneInterface;								// interface to drone communications (e.g., radio)

	std::string outputFileName;
	std::ofstream outputFile;									// for outputting trial data to file
	float saveOutputTimer;

	std::vector<uint32_t> pairedIDs;							// list of IDs that should be paired together

	std::vector<PairwiseFlockingAgent*> agents;					// list of pairwise flocking agents whose logic commands the drones
	std::map<uint32_t, PairwiseFlockingAgent*> agentsByID;		// same as above, but indexed by ID

	std::vector<DroneDetection> masterList;						// drone detections accumulated over several frames, to determine the IDs of all drones present in the trial
	float scanningTimer;										// tracks how long we need to be scanning for drones before the trial starts

	std::vector<DroneDetection> detections;						// drone detections in current frame
	std::map<uint32_t, DroneDetection> detectionsByID;			// same as above, but indexed by ID
	std::vector<Obstacle*> obstacles;							// simulated obstacles that the agents must avoid

	FlockingNetwork *uncompressedNetwork;						// graph representing the entire flock, where agents are vertices and tracking interactions are edges
	FlockingNetwork *compressedNetwork;							// same as above, except pairs are represented as single nodes

	Model *sphereModel;                                    		// GL mesh for drone model
	Shader *sphereShader;
};
