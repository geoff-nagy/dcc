#include "experiments/box/boxtrialcontroller.h"

#include "world/world.h"

#include "objects/droneinterface.h"
#include "objects/droneflightcontroller.h"
#include "objects/obstacle.h"
#include "objects/simulatedubee.h"

#include "sensing/sensinginterface.h"

#include "rendering/linerenderer.h"

#include "util/directories.h"
#include "util/log.h"
#include "util/wavefront.h"
#include "util/model.h"
#include "util/shader.h"
#include "util/mymath.h"
#include "util/rnd.h"

#include "GL/glew.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/random.hpp"
#include "glm/gtc/noise.hpp"
using namespace glm;

#include <string.h>
#include <vector>
#include <queue>
using namespace std;

// notes:
// X re-balance the uBee in the flight panel so it doesn't list to its left
// - figure out what the fuck is going on with that goddamn Vicon detection in that one quadrant of the lab
// X finish tuning those parameters for the box controller

const vec3 BoxTrialController::BOX_CENTER = vec3(-0.3f, 0.0f, 0.0f);

const vec3 BoxTrialController::TARGET_VERTICES[8] = {vec3(-0.5f, 1.25f, -0.5f),//vec3(-1.0f, 2.0f, -1.0f),
													 vec3(-0.5f, 1.25f, 0.5f),
													 vec3(0.5f, 1.25f, 0.5f),
													 vec3(0.5f, 1.25f, -0.5f),
													 vec3(0.5f, 0.5f, -0.5f),
													 vec3(0.5f, 0.5f, 0.5f),
													 vec3(-0.5f, 0.5f, 0.5f),
													 vec3(-0.5f, 0.5f, -0.5f)};

BoxTrialController::BoxTrialController(World *world,
									   SensingInterface *sensingInterface,
									   DroneInterface *droneInterface,
									   GumdropNode *trialConfig,
									   const string &outputFilename,
									   bool simulated)
	: TrialController(world, sensingInterface, droneInterface), simulated(simulated), outputFilename(outputFilename)
{
	this -> world = world;
	this -> sensingInterface = sensingInterface;
	this -> droneInterface = droneInterface;

	// trial enters initial state
	trialState = STATE_INITIAL;

    // read trial properties
	repetitions = trialConfig -> getInt("trial.repetitions", 0, NULL, true);
	currentRepetition = 0;

	// default stats
	trialTime = 0.0f;

	// trial type-specific properties
	targetVertexIndex = 0;

	// build the flight controller
	flightController = new DroneFlightController(PIDController(2.0f, 0.0f, 0.04f, 0.0f),				// pos x gains
												 PIDController(3.0f, 0.3f, 0.0f, 2.0f),					// pos y gains
												 PIDController(2.0f, 0.0f, 0.04f, 0.0f),				// pos z gains
												 PIDController(0.5f, 0.0f, 0.06f, 0.0f),				// vel x gains
												 PIDController(0.6f, 0.0f, 0.04f, 0.0f),				// vel y gains
												 PIDController(0.5f, 0.0f, 0.06f, 0.0f),				// vel z gains
												 PIDController(1.0f, 0.0f, 0.08f, 0.0f),				// heading gains
												 SimulatedUBee::MIN_THRUST,
												 SimulatedUBee::BASE_THRUST);

	// set null flight command
	flightCommand.id = 0;
	flightCommand.thrust = 0.0f;
	flightCommand.roll = 0.0f;
	flightCommand.pitch = 0.0f;
	flightCommand.yaw = 0.0f;

	// prepare for stats output
	openOutputFile();

	// add a single drone, if we're in simulation mode
	if(simulated)
	{
		world -> addSimulatedDrone(new SimulatedUBee(0), vec3(0.0f));
	}

	// rendering tools
	lines = LineRenderer::getInstance();
}

BoxTrialController::~BoxTrialController()
{
	closeOutputFile();
}

bool BoxTrialController::start()
{
	bool result = false;

	LOG_INFO("[BOX] starting controller");

	if(trialState == STATE_INITIAL || trialState == STATE_COMPLETE)
	{
		trialState = STATE_STARTING;
		result = true;
	}

	return result;
}

void BoxTrialController::pause()
{
	// [todo]
}

void BoxTrialController::resume()
{
	// [todo]
}

bool BoxTrialController::stopAndLand()
{
	bool result = false;

	if(trialState == STATE_FLYING || trialState == STATE_GAINING_HEIGHT)
	{
		trialState = STATE_LAND_NOW;
		result = true;
	}

	return result;
}

bool BoxTrialController::scram()
{
	trialState = STATE_COMPLETE;
	flightController -> setBaseThrust(0.0f);
	flightController -> setMinThrust(0.0f);
	return true;
}

bool BoxTrialController::isComplete()
{
	return false;//trialState == STATE_COMPLETE;
}

void BoxTrialController::update(float dt)
{
	// update time tracking
	trialTime += dt;

	// scan for drones
	detections.clear();
	sensingInterface -> getDrones(detections);

	// control state-specific logic
	if     (trialState == STATE_INITIAL) updateStateInitial(dt);
	else if(trialState == STATE_STARTING) updateStateStarting(dt);
	else if(trialState == STATE_SCANNING) updateStateScanning(dt);
	else if(trialState == STATE_TAKING_OFF) updateStateTakingOff(dt);
	else if(trialState == STATE_GAINING_HEIGHT) updateStateGainingHeight(dt);
	else if(trialState == STATE_FLYING) updateStateFlying(dt);
	else if(trialState == STATE_LAND_NOW) updateStateLandNow(dt);
	else if(trialState == STATE_LANDING) updateStateLanding(dt);
	else if(trialState == STATE_COMPLETE) updateStateComplete(dt);

	// log data
	saveOutputStats();
}

void BoxTrialController::updateStateInitial(float dt)
{
	// nothing happens here until we receive a signal to start the trial
}

void BoxTrialController::updateStateStarting(float dt)
{
	trialState = STATE_SCANNING;
    scanningTimer = 0.0f;

    // any other setup occurs here
    targetVertexIndex = 0;
    targetPosition = TARGET_VERTICES[targetVertexIndex] + BOX_CENTER;
    flightController -> setTargetPosition(targetPosition);
    flightController -> setMaxTargetYSpeed(0.5f);
    flightController -> setMaxTargetYawSpeed(1.0f);
}

void BoxTrialController::updateStateScanning(float dt)
{
	const float SCAN_TIME_SECONDS = 1.0f;

	vector<DroneDetection> scanned;
	vector<DroneDetection>::iterator i, j;
	vector<uint32_t>::iterator k;
	bool found;

	// go through all detections in the current frame
	for(i = detections.begin(); i != detections.end(); ++ i)
	{
        // is this drone already detected in our master list?
        found = false;
        j = scanned.begin();
        while(!found && j != scanned.end())
        {
			found = (*j++) == *i;
        }

        // if not, add it
        if(!found)
        {
			scanned.push_back(*i);
        }
	}

	// elapse time and see if we can transition now
	scanningTimer += dt;
	if(scanningTimer > SCAN_TIME_SECONDS)
	{
		// transition to taking off state
		trialState = STATE_TAKING_OFF;

		// create the agents whose IDs we scanned
		LOG_INFO("[BOX] scan complete -- detected " << scanned.size() << " drones");
		if(scanned.size() > 0)
		{
			LOG_INFO("[BOX] is using the drone ID " << scanned[0].id);
			detectedDroneID = scanned[0].id;
		}
		else
		{
			LOG_ERROR("[BOX] cannot proceed without at least 1 drone detection");
		}
	}
}

void BoxTrialController::updateStateTakingOff(float dt)
{
	vector<DroneDetection>::iterator i;
	vec3 pos;

	// issue take-off commands to drones
	LOG_INFO("[BOX] taking off and setting target height");

	// send global sensor detections to other drones
	updateDronePose(dt);

	// let the drones get to the target height
	trialState = STATE_GAINING_HEIGHT;

	// assign hover-level and minimum-level thrust amounts
	flightController -> setBaseThrust(SimulatedUBee::BASE_THRUST);
	flightController -> setMinThrust(SimulatedUBee::MIN_THRUST);

	// drone just flies straight up---do not control RPY just yet
	//updateVertexTargetPositions(dt);
	//updateFlightControls(dt);
	flightCommand.id = detectedDroneID;
	flightCommand.thrust = 1.0f;
	flightCommand.roll = 0.0f;
	flightCommand.pitch = 0.0f;
	flightCommand.yaw = 0.0f;

	// send control commands to drones
	issueFlightCommands();
}

void BoxTrialController::updateStateGainingHeight(float dt)
{
	const float MIN_TRIAL_HEIGHT = 0.35f;//0.5f;

	vector<DroneDetection>::iterator i;
	bool allDronesReady;

	// send global sensor detections to other drones
	updateDronePose(dt);

	// check if all drones are at the minimum trial altitude
	i = detections.begin();
	allDronesReady = true;
	while(i != detections.end() && allDronesReady)
	{
		allDronesReady = (i++) -> pos.y >= MIN_TRIAL_HEIGHT;
	}

	// go to the next state if all drones are high enough
	if(allDronesReady)
	{
		trialState = STATE_FLYING;
		LOG_INFO("[BOX] min height reached; starting pattern");
	}

	// drone just flies straight up---do not control RPY just yet
	//updateVertexTargetPositions(dt);
	//updateFlightControls(dt);
	flightCommand.id = detectedDroneID;
	flightCommand.thrust = 1.0f;
	flightCommand.roll = 0.0f;
	flightCommand.pitch = 0.0f;
	flightCommand.yaw = 0.0f;

	// send control commands to drones
	issueFlightCommands();
}

void BoxTrialController::updateStateFlying(float dt)
{
	// send global sensor detections to other drones
	updateDronePose(dt);

	// always control general agent logic
	updateVertexTargetPositions(dt);
	updateFlightControls(dt);

	// send control commands to drones
	issueFlightCommands();
}

void BoxTrialController::updateStateLandNow(float dt)
{
	const float MAX_LAND_VELOCITY = 0.25f;

	// send global sensor detections to other drones
	updateDronePose(dt);

	// instruct all drones to land
	flightController -> setTargetPosition(vec3(detection.pos.x, 0.017f, detection.pos.z));
	flightController -> setMaxTargetYSpeed(MAX_LAND_VELOCITY);

	// advance to waiting state
	trialState = STATE_LANDING;
	LOG_INFO("---------------------------------------LANDING---------------------------------------");

	// always control general agent logic
	updateFlightControls(dt);

	// send control commands to drones
	issueFlightCommands();
}

void BoxTrialController::updateStateLanding(float dt)
{
	const float MAX_LAND_HEIGHT = 0.17f;

	// find drone
	updateDronePose(dt);

	// check if all drones are at the desired trial altitude
	if(detection.pos.y <= MAX_LAND_HEIGHT)
	{
		trialState = STATE_COMPLETE;
		LOG_INFO("--------------------------------------------------------[BOX] drone has landed successfully");

		flightController -> setBaseThrust(0.0f);
		flightController -> setMinThrust(0.0f);
	}

	// always control general agent logic
	updateFlightControls(dt);

	// send control commands to drones
	issueFlightCommands();
}

void BoxTrialController::updateStateComplete(float dt)
{
	// [do not send global sensor detections]

	// find drone
	updateDronePose(dt);

	// send NULL flight command so we don't move or anything
	flightCommand.id = detectedDroneID;
	flightCommand.thrust = 0.0f;
	flightCommand.roll = 0.0f;
	flightCommand.pitch = 0.0f;
	flightCommand.yaw = 0.0f;

	// [do not control general agent logic here]

	// send control commands to drones
	issueFlightCommands();
}

void BoxTrialController::updateDronePose(float dt)
{
	vector<DroneDetection>::iterator i;

	// search for the drone with our target ID; record its pose
    for(i = detections.begin(); i != detections.end(); ++ i)
    {
		if((int)i -> id == detectedDroneID)
		{
			detection = *i;
		}
    }
}

void BoxTrialController::updateVertexTargetPositions(float dt)
{
	const float CLOSE_ENOUGH_DIST = 0.1f;

	float dist;

	// if we're close enough to the current target, advance to next target
	dist = length((TARGET_VERTICES[targetVertexIndex] + BOX_CENTER) - detection.pos);
	if(dist <= CLOSE_ENOUGH_DIST)
	{
		++ targetVertexIndex;
		if(targetVertexIndex >= 8)
		{
			targetVertexIndex = 0;
		}

		targetPosition = BOX_CENTER + TARGET_VERTICES[targetVertexIndex];
		flightController -> setTargetPosition(targetPosition);
	}
}

void BoxTrialController::updateFlightControls(float dt)
{
	const float MAX_TARGET_XZ_SPEED = 0.4f;//3.0f;//1.0f;//1.5f;
	const float MAX_TARGET_Y_SPEED = 0.4f;

	const float MAX_YAW_SPEED = 1.0f;//500.0f;//0.5f;
	const float TARGET_HEADING = 0.0f;

	// set desired flight params
	flightController -> setMaxTargetXZSpeed(MAX_TARGET_XZ_SPEED);
	flightController -> setMaxTargetYSpeed(MAX_TARGET_Y_SPEED);
	flightController -> setMaxTargetYawSpeed(MAX_YAW_SPEED);
	flightController -> setTargetHeading(TARGET_HEADING);

	// update flight control
	flightController -> update(detection, dt);
	flightCommand = flightController -> getLastFlightCommand();
}

void BoxTrialController::issueFlightCommands()
{
	// send flight commands based on PID output
	droneInterface -> clearCommands();
	droneInterface -> addCommand(flightCommand);
}

void BoxTrialController::render(const glm::mat4 &viewProjection)
{
	const vec4 NORMAL_COLOR(0.6f, 0.6f, 0.6f, 1.0f);
	const vec4 TARGET_COLOR(0.0f, 1.0f, 0.0f, 1.0f);
	const float CROSSHAIR_SIZE = 0.1f;

	vec4 color;
	uint32_t i;

	// show the box vertices and highlight our target vertex
	for(i = 0; i < 8; ++ i)
	{
		color = (i == targetVertexIndex ? TARGET_COLOR : NORMAL_COLOR);
		lines -> add(BOX_CENTER + TARGET_VERTICES[i] + vec3(-CROSSHAIR_SIZE, 0.0f, 0.0f), BOX_CENTER + TARGET_VERTICES[i] + vec3(CROSSHAIR_SIZE, 0.0f, 0.0f), color);
		lines -> add(BOX_CENTER + TARGET_VERTICES[i] + vec3(0.0f, -CROSSHAIR_SIZE, 0.0f), BOX_CENTER + TARGET_VERTICES[i] + vec3(0.0f, CROSSHAIR_SIZE, 0.0f), color);
		lines -> add(BOX_CENTER + TARGET_VERTICES[i] + vec3(0.0f, 0.0f, -CROSSHAIR_SIZE), BOX_CENTER + TARGET_VERTICES[i] + vec3(0.0f, 0.0f, CROSSHAIR_SIZE), color);
	}
}

void BoxTrialController::openOutputFile()
{
    stringstream ss;
    string folder;
	uint32_t index;

	// create the directory indicated (if any) if it does not exist yet
	index = outputFilename.find_last_of('/');
	if(index != string::npos)
	{
		folder = outputFilename.substr(0, index);
		if(!dirDoesDirectoryExist(folder))
		{
			dirMakeDirectory(folder);
		}
	}

    // build the output file name and open the file for writing
    ss << outputFilename << ".txt";
    outputFilename = ss.str();
    outputFile.open(outputFilename.c_str());
    if(outputFile.is_open())
    {
		LOG_INFO("[BOX] opened log file \"" << outputFilename << "\"");
    }
    else
    {
		LOG_ERROR("[BOX] could not open output file \"" << outputFilename << "\" for writing");
    }
}

void BoxTrialController::closeOutputFile()
{
	if(outputFile.is_open())
	{
		LOG_INFO("[BOX] closing log file \"" << outputFilename << "\"");
		outputFile.close();
	}
}

void BoxTrialController::saveOutputStats()
{
	if(outputFile.is_open())
	{
		outputFile << trialTime << ",";
		outputFile << detection.pos.x << ",";
		outputFile << detection.pos.y << ",";
		outputFile << detection.pos.z << ",";
		outputFile << detection.velocity.x << ",";
		outputFile << detection.velocity.y << ",";
		outputFile << detection.velocity.z << ",";
		outputFile << detection.dir << ",";
		outputFile << flightController -> getTargetXZSpeed().x << ",";
		outputFile << flightController -> getTargetYSpeed() << ",";
		outputFile << flightController -> getTargetXZSpeed().y;
		outputFile << endl;
	}
}
