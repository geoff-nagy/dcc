#pragma once

#include "experiments/trialcontroller.h"

#include "sensing/dronedetection.h"

#include "objects/droneinterface.h"

#include "util/pidcontroller.h"
#include "util/circularbuffer.h"

#include "glm/glm.hpp"

#include <stdint.h>
#include <vector>
#include <string>
#include <fstream>

class World;
class SensingInterface;
class DroneInterface;
class GumdropNode;
class LineRenderer;
class DroneFlightController;

// the BoxTrialController is a simple controller for making a single drone move in a box-like pattern, from one vertex to the next;
// the main purpose is to assess uBee control performance and demonstrate proof-of-concept using some sensor like the Vicon

class BoxTrialController : public TrialController
{
public:
	typedef enum TRIAL_STATE
	{
		STATE_INITIAL = 0,							// basic initialization
		STATE_STARTING,								// transition to this is controlled by higher-level logic
		STATE_SCANNING,								// several frames of scanning to find the IDs of all drones we're working with
		STATE_TAKING_OFF,							// drones begin taking off to a designated height before their main logic kicks in
		STATE_GAINING_HEIGHT,						// drones are currently gaining altitude to the designated height
		STATE_FLYING,								// drones are now performing flying logic
		STATE_LAND_NOW,								// trial is over; drones are instructed to land
		STATE_LANDING,								// drones are currently landing
		STATE_COMPLETE								// drones have landed and we can safely end
	} TrialState;

	static const glm::vec3 TARGET_VERTICES[8];
	static const glm::vec3 BOX_CENTER;

	BoxTrialController(World *world, SensingInterface *sensingInterface, DroneInterface *droneInterface, GumdropNode *trialConfig, const std::string &outputFilename, bool simulated);
	virtual ~BoxTrialController();

	// required methods
	void update(float dt);
	bool start();
	void pause();
	void resume();
	bool stopAndLand();
	bool scram();
	bool isComplete();

	// optional methods
	void render(const glm::mat4 &viewProjection);

private:
	void updateStateInitial(float dt);
	void updateStateStarting(float dt);
	void updateStateScanning(float dt);
	void updateStateTakingOff(float dt);
	void updateStateGainingHeight(float dt);
	void updateStateFlying(float dt);
	void updateStateLandNow(float dt);
	void updateStateLanding(float dt);
	void updateStateComplete(float dt);

	void processDetections();
	void updateDronePose(float dt);
	void updateVertexTargetPositions(float dt);
	void updateFlightControls(float dt);
	void issueFlightCommands();

	void openOutputFile();
	void closeOutputFile();
	void saveOutputStats();

	// derived-class-local handles to superclass components for convenience
	World *world;
	SensingInterface *sensingInterface;
	DroneInterface *droneInterface;
	GumdropNode *trialConfig;
	bool simulated;

	// rendering tools
	LineRenderer *lines;

	// generic properties
	float scanningTimer;
	float trialTime;
	std::vector<DroneDetection> detections;

	// box controller behaviour
	TrialState trialState;
	std::string outputFilename;
	std::ofstream outputFile;
	uint32_t targetVertexIndex;
	int detectedDroneID;
	uint32_t repetitions;
	uint32_t currentRepetition;

	// single drone control
	glm::vec3 targetPosition;
	DroneDetection detection;
	DroneFlightController *flightController;
	FlightCommand flightCommand;
};
