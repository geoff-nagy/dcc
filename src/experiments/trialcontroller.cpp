#include "experiments/trialcontroller.h"

TrialController::TrialController(World *world, SensingInterface *sensingInterface, DroneInterface *droneInterface)
	: world(world), sensingInterface(sensingInterface), droneInterface(droneInterface)
{
	// anything?
}

TrialController::~TrialController()
{
	// anything?
}

World *TrialController::getWorld()
{
	return world;
}

SensingInterface *TrialController::getSensingInterface()
{
	return sensingInterface;
}

DroneInterface *TrialController::getDroneInterface()
{
	return droneInterface;
}

void TrialController::render(const glm::mat4 &viewProjection)
{
	// overridden by derived classes
}
