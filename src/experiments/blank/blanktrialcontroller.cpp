#include "experiments/blank/blanktrialcontroller.h"

BlankTrialController::BlankTrialController(World *world, SensingInterface *sensingInterface, DroneInterface *droneInterface)
	: TrialController(world, sensingInterface, droneInterface)
{
	// [todo]
}

BlankTrialController::~BlankTrialController()
{
	// [todo]
}

void BlankTrialController::update(float dt)
{
	// [todo]
}

bool BlankTrialController::start()
{
	return true;
}

void BlankTrialController::pause()
{
	// [todo]
}

void BlankTrialController::resume()
{
	// [todo]
}

bool BlankTrialController::stopAndLand()
{
	return true;
}

bool BlankTrialController::scram()
{
	return true;
}

bool BlankTrialController::isComplete()
{
	return false;
}
