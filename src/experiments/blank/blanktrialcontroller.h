#include "experiments/trialcontroller.h"

class SensingInterface;
class DroneInterface;

class BlankTrialController : public TrialController
{
public:
	BlankTrialController(World *world, SensingInterface *sensingInterface, DroneInterface *droneInterface);
	~BlankTrialController();

	virtual void update(float dt);
	virtual bool start();
	virtual void pause();
	virtual void resume();
	virtual bool stopAndLand();
	virtual bool scram();
	virtual bool isComplete();

private:
	// [todo]
};
