#pragma once

#include "glm/glm.hpp"

class World;
class SensingInterface;
class DroneInterface;

// the TrialController is an abstract class for defining the experimental trial logic you want to use; it allows you to run the
// same trials or sets of drone behaviours with any kind of sensing device or drone, simulated or otherwise

class TrialController
{
public:
	TrialController(World *world, SensingInterface *sensingInterface, DroneInterface *droneInterface);
	virtual ~TrialController() = 0;

	World *getWorld();
	SensingInterface *getSensingInterface();
	DroneInterface *getDroneInterface();

	virtual void update(float dt) = 0;
	virtual bool start() = 0;
	virtual void pause() = 0;
	virtual void resume() = 0;
	virtual bool stopAndLand() = 0;
	virtual bool scram() = 0;
	virtual bool isComplete() = 0;
	virtual void render(const glm::mat4 &viewProjection);

private:
	World *world;
	SensingInterface *sensingInterface;
	DroneInterface *droneInterface;
};
