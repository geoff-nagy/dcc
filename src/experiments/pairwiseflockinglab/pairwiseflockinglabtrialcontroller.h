#pragma once

#include "experiments/trialcontroller.h"

#include "sensing/dronedetection.h"

#include "objects/droneinterface.h"

#include <stdint.h>
#include <fstream>
#include <string>
#include <vector>
#include <map>

class FlockingNetwork;
class PairwiseFlockingLabAgent;
class Obstacle;
class Graph;

class GumdropNode;
class WavefrontModel;
class Model;
class Shader;

class PairwiseFlockingLabTrialController : public TrialController
{
public:
	typedef enum TRIAL_STATE
	{
		STATE_INITIAL = 0,										// basic initialization
		STATE_STARTING,											// transition to this is controlled by higher-level logic
		STATE_SCANNING,											// several frames of scanning to find the IDs of all drones we're working with
		STATE_TAKING_OFF,										// drones begin taking off to a designated height before their main logic kicks in
		STATE_GAINING_HEIGHT,									// drones are currently gaining altitude to the designated height
		STATE_POSITIONING,										// drones are getting into their starting positions
		STATE_FLYING,											// drones are now performing pairwise flocking logic and flying together
		STATE_LAND_NOW,											// trial is over; drones are instructed to land
		STATE_LANDING,											// drones are currently landing
		STATE_COMPLETE											// drones have landed and we can safely end
	} TrialState;

	PairwiseFlockingLabTrialController(World *world,
									   SensingInterface *sensingInterface,
									   DroneInterface *droneInterface,
									   GumdropNode *trialConfig,
									   const std::string &trialName,
									   bool gui);
	~PairwiseFlockingLabTrialController();

	virtual void update(float dt);
	virtual bool start();
	virtual void pause();
	virtual void resume();
	virtual bool stopAndLand();
	virtual bool scram();
	virtual bool isComplete();

	virtual void render(const glm::mat4 &viewProjection);

private:
	// - - - private methods - - - //

	void updateStateInitial(float dt);
	void updateStateStarting(float dt);
	void updateStateScanning(float dt);
	void updateStateTakingOff(float dt);
	void updateStateGainingHeight(float dt);
	void updateStatePositioning(float dt);
	void updateStateFlying(float dt);
	void updateStateLandNow(float dt);
	void updateStateLanding(float dt);
	void updateStateComplete(float dt);

	void updateFlightControls(float dt);
	void processDetections();
	void feedAgentSensors();
	void issueFlightCommands();
	void deleteAgents();
	void buildUncompressedFlockingNetwork();
	void buildCompressedFlockingNetwork();

	void openOutputFile();
	void closeOutputFile();
	void saveOutputStats();

	bool hasLineOfSight(const glm::vec3 &a, const glm::vec3 &b);
	bool isSensorWorkingNow(const glm::vec3 &pos, float reliability);

	void makeObstacleField(GumdropNode *trialConfig);
	void makeObstaclesIndividual(GumdropNode *trialConfig);

	void renderConnections(const glm::mat4 &viewProjection);
	void renderObstacles(const glm::mat4 &viewProjection);
	void renderBounds(const glm::mat4 &viewProjection);
	void renderVelocities(const glm::mat4 &viewProjection);
	void renderPositioning(const glm::mat4 &viewProjection);
	void renderRadioCommands(const glm::mat4 &viewProjection);

	void loadAssets();

	// - - - private vars - - - //

	static const float SAVE_INTERVAL_SECS;						// how often we write to the trial output file, in seconds

	static const float BASE_TARGET_FLYING_HEIGHT_METERS;				// how high we want the drones to be when they're flocking, in meters

	bool gui;
	std::string trialOutputFilename;
	uint32_t maxTrialSeconds;

	float scanningTimer;
	float trialTime;

	TrialState trialState;
	uint32_t numTrackedIfPaired;
	uint32_t numTrackedIfSingle;
	float viewRange;

	bool closePairing;
	bool asymmetricTracking;
	float neighbourSensingNoiseStdDev;
	float neighbourSensingReliabilityThreshold;

	bool obstacleOcclusion;
	bool partnerSeparation;

	glm::vec2 boundsArea;
	glm::vec2 boundsHalfArea;
	float boundsEdge;
	bool agentAtEdge;

	World *world;												// interface to world (simulated or otherwise)
	SensingInterface *sensingInterface;							// interface to drone detector (e.g., Vicon)
	DroneInterface *droneInterface;								// interface to drone communications (e.g., radio)

	// trial output control
	std::string outputFileName;
	std::ofstream outputFile;									// for outputting trial data to file
	float saveOutputTimer;

	// drone control
	std::vector<DroneDetection> detectionsMaster;				// list of drone detections we received when priming our list of drones
	std::vector<DroneDetection> detectionsCurrentFrame;			// list of drone detections we've received in the current frame only
    std::map<uint32_t, DroneDetection> detectionsByID;			// same as detectionsCurrentFrame, except indexed by ID
    std::vector<PairwiseFlockingLabAgent*> agents;				// list of hovering agents we want to control, informed by detectionsMaster
    std::map<uint32_t, PairwiseFlockingLabAgent*> agentsByID;	// same as above, but indexed by ID
    std::vector<uint32_t> pairedIDs;							// list of IDs that should be paired together
    std::vector<FlightCommand> flightCommands;					// list of flight commands that will be sent to all drones

	//std::map<uint32_t, DroneDetection> detectionsByID;			// same as above, but indexed by ID
	std::vector<Obstacle*> obstacles;							// simulated obstacles that the agents must avoid

	FlockingNetwork *uncompressedNetwork;						// graph representing the entire flock, where agents are vertices and tracking interactions are edges
	FlockingNetwork *compressedNetwork;							// same as above, except pairs are represented as single nodes

	Model *sphereModel;                                    		// GL mesh for drone model
	Shader *sphereShader;
};
