#include "experiments/pairwiseflockinglab/pairwiseflockinglabtrialcontroller.h"

#include "experiments/pairwiseflocking/flockingnetwork.h"

#include "world/world.h"

#include "objects/pairwiseflockinglab/pairwiseflockinglabagent.h"

#include "objects/droneinterface.h"
#include "objects/obstacle.h"
#include "objects/simulatedubee.h"
#include "objects/droneflightcontroller.h"
#include "objects/ubeeradiointerface.h"

#include "sensing/sensinginterface.h"

#include "rendering/linerenderer.h"

#include "util/directories.h"
#include "util/log.h"
#include "util/wavefront.h"
#include "util/model.h"
#include "util/shader.h"
#include "util/mymath.h"
#include "util/rnd.h"

#include "GL/glew.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/random.hpp"
#include "glm/gtc/noise.hpp"
#include "glm/gtx/rotate_vector.hpp"
using namespace glm;

#include <string.h>
#include <sstream>
#include <vector>
#include <queue>
using namespace std;

// how often we log trial data
const float PairwiseFlockingLabTrialController::SAVE_INTERVAL_SECS = 1.0f;

// each agent flies at its own height to help reduce the risk of collisions, but will be at least this high off the ground
const float PairwiseFlockingLabTrialController::BASE_TARGET_FLYING_HEIGHT_METERS = 0.5f;

PairwiseFlockingLabTrialController::PairwiseFlockingLabTrialController(World *world,
																	   SensingInterface *sensingInterface,
																	   DroneInterface *droneInterface,
																	   GumdropNode *trialConfig,
																	   const string &trialOutputFilename,
																	   bool gui)
	: TrialController(world, sensingInterface, droneInterface), gui(gui), trialState(STATE_INITIAL)
{
	int numPaired;
	int i;
	stringstream ss;

	this -> world = world;
	this -> sensingInterface = sensingInterface;
	this -> droneInterface = droneInterface;
	this -> trialOutputFilename = trialOutputFilename;

	if(gui)
	{
		loadAssets();
	}

	// read flocking settings
	closePairing = trialConfig -> getBool("trial.close_pairing", false, NULL, true);
	asymmetricTracking = trialConfig -> getBool("trial.asymmetric_tracking", false, NULL, true);
	neighbourSensingNoiseStdDev = trialConfig -> getDouble("trial.sensor_noise_std_dev", 0.0, NULL, true);
	neighbourSensingReliabilityThreshold = trialConfig -> getDouble("trial.sensor_reliability_threshold", 0.0, NULL, true);
	obstacleOcclusion = trialConfig -> getBool("trial.obstacle_occlusion", true, NULL, true);
	partnerSeparation = trialConfig -> getBool("trial.partner_separation", true, NULL, true);
	numTrackedIfPaired = trialConfig -> getInt("trial.num_tracked_if_paired", 0, NULL, true);
	numTrackedIfSingle = trialConfig -> getInt("trial.num_tracked_if_single", 0, NULL, true);
	viewRange = (float)trialConfig -> getInt("trial.view_range_cm", 0, NULL, true) / 100.0f;

	// read paired count and check for sanity
	numPaired = trialConfig -> getInt("trial.paired_count", 0, NULL, true);
	if(numPaired % 2 == 1)
	{
		LOG_ERROR("[PCTFC] number of paired agents is " << numPaired << ", but this makes no sense because it is not an even number");
	}

	// assign paired IDs
	LOG_INFO("[PFTC] " << numPaired << " agents are paired");
	for(i = 0; i < numPaired; ++ i)
	{
		// add to the list of paired IDs
		ss.str("");
		ss << "trial.paired[" << i << "]";
        pairedIDs.push_back(trialConfig -> getInt(ss.str(), 0, NULL, true));

        // print out IDs of paired agents
        if(i % 2 == 1)
        {
			LOG_INFO("    agents " << pairedIDs[i - 1] << " and " << pairedIDs[i] << " are paired");
		}
	}

    // read trial properties
	maxTrialSeconds = trialConfig -> getInt("trial.time_sec", 0, NULL, true);
	boundsArea = vec2(trialConfig -> getDouble("trial.bounds.size[0]", 0.0, NULL, true),
					  trialConfig -> getDouble("trial.bounds.size[1]", 0.0, NULL, true));
	boundsEdge = trialConfig -> getDouble("trial.bounds.edge", 0.0, NULL, true);
	boundsHalfArea = boundsArea / 2.0f;
	agentAtEdge = false;

	// place obstacles
	makeObstacleField(trialConfig);
	makeObstaclesIndividual(trialConfig);

	// default stats
	uncompressedNetwork = NULL;
	compressedNetwork = NULL;
	trialTime = 0.0f;

	// prepare for stats output
	saveOutputTimer = SAVE_INTERVAL_SECS;
	openOutputFile();
}

PairwiseFlockingLabTrialController::~PairwiseFlockingLabTrialController()
{
	deleteAgents();
	closeOutputFile();
}

bool PairwiseFlockingLabTrialController::start()
{
	LOG_INFO("[PFTC] starting controller");
	trialState = STATE_STARTING;
	return true;
}

void PairwiseFlockingLabTrialController::pause()
{
	// [todo]
}

void PairwiseFlockingLabTrialController::resume()
{
	// [todo]
}

bool PairwiseFlockingLabTrialController::stopAndLand()
{
	bool result = false;

	// only allow gently landings if we're flying and already in control
	// [note]: we don't allow safe landings for agents that have already lost tracking, etc., because we don't have detections for them
	if(trialState == STATE_TAKING_OFF || trialState == STATE_GAINING_HEIGHT || trialState == STATE_POSITIONING || trialState == STATE_FLYING)
	{
		trialState = STATE_LAND_NOW;
		result = true;
	}

	return result;
}

bool PairwiseFlockingLabTrialController::scram()
{
	vector<PairwiseFlockingLabAgent*>::iterator i;

	// emergency stop
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		(*i) -> scram();
	}

	// transition to complete state
	trialState = STATE_COMPLETE;

	return true;
}

bool PairwiseFlockingLabTrialController::isComplete()
{
	return false;//trialState == STATE_COMPLETE;
}

void PairwiseFlockingLabTrialController::update(float dt)
{
	// use the sensor object to read the drones, their IDs, and poses
	processDetections();

	// now control state-specific logic
	if     (trialState == STATE_INITIAL) updateStateInitial(dt);
	else if(trialState == STATE_STARTING) updateStateStarting(dt);
	else if(trialState == STATE_SCANNING) updateStateScanning(dt);
	else if(trialState == STATE_TAKING_OFF) updateStateTakingOff(dt);
	else if(trialState == STATE_GAINING_HEIGHT) updateStateGainingHeight(dt);
	else if(trialState == STATE_POSITIONING) updateStatePositioning(dt);
	else if(trialState == STATE_FLYING) updateStateFlying(dt);
	else if(trialState == STATE_LAND_NOW) updateStateLandNow(dt);
	else if(trialState == STATE_LANDING) updateStateLanding(dt);
	else if(trialState == STATE_COMPLETE) updateStateComplete(dt);
}

void PairwiseFlockingLabTrialController::processDetections()
{
	vector<DroneDetection>::iterator i;

	// scan for drones
	detectionsCurrentFrame.clear();
	sensingInterface -> getDrones(detectionsCurrentFrame);

	// build a list of detections by ID, for easier reference later on
	detectionsByID.clear();
	for(i = detectionsCurrentFrame.begin(); i != detectionsCurrentFrame.end(); ++ i)
	{
		detectionsByID[i -> id] = *i;
	}
}

void PairwiseFlockingLabTrialController::render(const glm::mat4 &viewProjection)
{
	// don't bother with connections unless we're in the air
	if(trialState == STATE_FLYING)
	{
		renderConnections(viewProjection);
	}

	renderObstacles(viewProjection);
	renderBounds(viewProjection);
	renderVelocities(viewProjection);
	renderRadioCommands(viewProjection);

	if(trialState == STATE_POSITIONING)
	{
		renderPositioning(viewProjection);
	}
}

void PairwiseFlockingLabTrialController::updateStateInitial(float dt)
{
	// [nothing happens here until we receive a signal to start the trial]
}

void PairwiseFlockingLabTrialController::updateStateStarting(float dt)
{
	// clear our master list of detected drones that we will use
	detectionsMaster.clear();

	// start scanning the sensing region for drones
	trialState = STATE_SCANNING;
    scanningTimer = 0.0f;

    // [any other setup occurs here]
}

void PairwiseFlockingLabTrialController::updateStateScanning(float dt)
{
	const float SCAN_TIME_SECONDS = 1.0f;
	const float HEIGHT_INCREMENT = 0.12f;

	PairwiseFlockingLabAgent *curr;
	vector<DroneDetection>::iterator i, j;
	vector<uint32_t>::iterator k;
	bool found;
	float individualHeightAdd;

	// go through all detections in the current frame
	for(i = detectionsCurrentFrame.begin(); i != detectionsCurrentFrame.end(); ++ i)
	{
        // is this drone already detected in our master list?
        found = false;
        j = detectionsMaster.begin();
        while(!found && j != detectionsMaster.end())
        {
			found = (*j++) == *i;
        }

        // if not, add it
        if(!found)
        {
			detectionsMaster.push_back(*i);
        }
	}

	// delete all previous agents
	deleteAgents();

	// elapse time and see if we can transition now
	scanningTimer += dt;
	if(scanningTimer > SCAN_TIME_SECONDS)
	{
		// transition to taking off state
		trialState = STATE_TAKING_OFF;

		// create the agents whose IDs we scanned
		LOG_INFO("[PFTC] scan complete -- detected " << detectionsMaster.size() << " drones");
		if(detectionsMaster.size() > 0)
		{
			// go through all detections
			individualHeightAdd = HEIGHT_INCREMENT * detectionsMaster.size();
			for(i = detectionsMaster.begin(); i != detectionsMaster.end(); ++ i)
			{
				// enumerate drone IDs and positions
				LOG_INFO("    [" << i -> id << "] was found at (" << i -> pos.x << ", " << i -> pos.y << ", " << i -> pos.z << ")");

				// create flight controllers and target positions for all drones we found
				curr = new PairwiseFlockingLabAgent(new DroneFlightController(PIDController(4.3f, 0.0f, 0.08f, 0.0f),				// pos x gains
																			   PIDController(3.5f, 0.2f, 0.0f, 0.25f),				// pos y gains
																			   PIDController(4.3f, 0.0f, 0.08f, 0.0f),				// pos z gains

																			   PIDController(0.32f, 0.0f, 0.02f, 0.0f),				// vel x gains	//0.17f
																						   //0.9f	     0.008f
																			   PIDController(0.5f, 0.0f, 0.05f, 0.0f),				// vel y gains

																			   PIDController(0.32f, 0.0f, 0.02f, 0.0f),				// vel z gains
																							//0.23f

																			   PIDController(2.0f, 0.0f, 0.05f, 0.0f),
																			   SimulatedUBee::MIN_THRUST,							// min thrust so we don't plummet
																			  SimulatedUBee::BASE_THRUST),							// approx. hover thrust
												   *i,
												   BASE_TARGET_FLYING_HEIGHT_METERS + individualHeightAdd,
												   viewRange,
												   closePairing);

				// set world bounds
				curr -> setBounds(boundsArea, boundsEdge);

				// add the agent
				agents.push_back(curr);
				agentsByID[i -> id] = curr;

				// increase height for next agent
				individualHeightAdd -= HEIGHT_INCREMENT;
			}

			// now assign pairs where they exist
			for(k = pairedIDs.begin(); k < pairedIDs.end(); k += 2)
			{
				agentsByID[*k] -> setPartnerID(*(k + 1));
				agentsByID[*(k + 1)] -> setPartnerID(*k);
				LOG_INFO("pairing " << *k << " and " << *(k + 1));
			}
		}
		else
		{
			LOG_ERROR("[PFTC] cannot proceed without at least 1 drone detection");
		}
	}
}

void PairwiseFlockingLabTrialController::updateStateTakingOff(float dt)
{
	vector<PairwiseFlockingLabAgent*>::iterator i;

	// issue take-off commands to drones
	LOG_INFO("[PFTC] taking off");

	// let the drones get to the target height
	trialState = STATE_GAINING_HEIGHT;

	// all drones transition to take-off state
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		(*i) -> takeOff();
	}
}

void PairwiseFlockingLabTrialController::updateStateGainingHeight(float dt)
{
	const float TARGET_HEIGHT_TOLERANCE = 0.1f;				// how close is close enough to the desired flying height, in meters

	vector<DroneDetection>::iterator i, j;
	vector<DroneDetection>::iterator droneItr;
	bool allDronesReady;
	bool droneFound;

	// send global sensor detections to other drones
	feedAgentSensors();

	// control general agent logic
	updateFlightControls(dt);

	// check if *all* drones are at the minimum trial altitude
	i = detectionsMaster.begin();
	allDronesReady = true;
	droneFound = true;
	while(droneFound && i != detectionsMaster.end() && allDronesReady)
	{
		// search for a drone in the current detections that we know should exist from our scanning phase
		droneItr = detectionsCurrentFrame.end();
		j = detectionsCurrentFrame.begin();
		while(droneItr == detectionsCurrentFrame.end() && j != detectionsCurrentFrame.end())
		{
			// if we find the desired drone within the current frame, save an iterator to it so we can exit the inner loop early
			// and check its altitude later on
			if(i -> id == j -> id)
			{
				droneItr = j;
			}
			++ j;
		}

		// only proceed if we found the current drone; this requires all drones that we know of to be both found
		// and at a minimum altitude, for us to proceed to the next trial state
		allDronesReady = droneItr != detectionsCurrentFrame.end() && droneItr -> pos.y >= agentsByID[i -> id] -> getTargetHeight() - TARGET_HEIGHT_TOLERANCE;

		// next drone to look for
		++ i;
	}

	// go to the next state if all drones are high enough
	if(allDronesReady)
	{
		trialState = STATE_POSITIONING;
		LOG_INFO("[PFTC] min height reached; moving into positions");
	}

	// send control commands to drones
	issueFlightCommands();
}

void PairwiseFlockingLabTrialController::updateStatePositioning(float dt)
{
	vector<DroneDetection>::iterator i, j;
	vector<DroneDetection>::iterator droneItr;
	bool allDronesReady;
	bool droneFound;

	// send global sensor detections to other drones
	feedAgentSensors();

	// control general agent logic
	updateFlightControls(dt);

	// check if *all* drones are at their desired starting positions
	i = detectionsMaster.begin();
	allDronesReady = true;
	droneFound = true;
	while(droneFound && i != detectionsMaster.end() && allDronesReady)
	{
		// search for a drone in the current detections that we know should exist from our scanning phase
		droneItr = detectionsCurrentFrame.end();
		j = detectionsCurrentFrame.begin();
		while(droneItr == detectionsCurrentFrame.end() && j != detectionsCurrentFrame.end())
		{
			// if we find the desired drone within the current frame, save an iterator to it so we can exit the inner loop early
			// and check its altitude later on
			if(i -> id == j -> id)
			{
				droneItr = j;
			}
			++ j;
		}

		// only proceed if we found the current drone; this requires all drones that we know of to be both found
		// and at their starting positions, for us to proceed to the next trial state
		allDronesReady = droneItr != detectionsCurrentFrame.end() && agentsByID[i -> id] -> isInPosition();

		// next drone to look for
		++ i;
	}

	// go to the next state if all drones are high enough
	if(allDronesReady)
	{
		trialState = STATE_FLYING;
		LOG_INFO("[PFTC] positions reached; starting flocking");

		// enable agent flocking interactions
		for(i = detectionsMaster.begin(); i != detectionsMaster.end(); ++ i)
		{
			agentsByID[i -> id] -> setInteractionsEnabled(true);
		}
	}

	// send control commands to drones
	issueFlightCommands();
}

void PairwiseFlockingLabTrialController::updateStateFlying(float dt)
{
	// send global sensor detections to other drones
	feedAgentSensors();

	// always control general agent logic
	updateFlightControls(dt);

	// build flocking networks
	buildUncompressedFlockingNetwork();
	buildCompressedFlockingNetwork();

	// control stats output
	saveOutputTimer += dt;
	if(saveOutputTimer >= SAVE_INTERVAL_SECS)
	{
		saveOutputTimer = 0.0f;
		saveOutputStats();
	}

	// trial timer runs only while all drones are flying
	trialTime += dt;
	if(maxTrialSeconds != 0 && trialTime >= maxTrialSeconds)
	{
		trialState = STATE_LAND_NOW;
	}

	// send control commands to drones
	issueFlightCommands();
}

void PairwiseFlockingLabTrialController::updateStateLandNow(float dt)
{
	vector<PairwiseFlockingLabAgent*>::iterator i;
	PairwiseFlockingLabAgent *curr;

	// [no need to send global sensor detections to other drones]

	// instruct all drones to land
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		curr = *i;
		curr -> setInteractionsEnabled(false);
		curr -> clearNeighbours();
		curr -> landSafely();
	}

	// always control general agent logic
	updateFlightControls(dt);

	// advance to waiting state
	trialState = STATE_LANDING;

	// send control commands to drones
	issueFlightCommands();
}

void PairwiseFlockingLabTrialController::updateStateLanding(float dt)
{
	vector<PairwiseFlockingLabAgent*>::iterator i;
	bool landed = false;

	// wait for all drones to land
	i = agents.begin();
	landed = true;
	while(landed && i != agents.end())
	{
		// we consider a drone landed either if it's at the landing altitude or we've lost the detection
		landed = (*i++) -> isLanded();
	}

	// notify user if everyone landed
	if(landed)
	{
		// done; transition to safe state and notify the user
		trialState = STATE_COMPLETE;
		LOG_INFO("---------------------------------------[PFTC] drones have landed successfully");
	}

	// always control general agent logic
	updateFlightControls(dt);

	// send control commands to drones
	issueFlightCommands();
}

void PairwiseFlockingLabTrialController::updateStateComplete(float dt)
{
	vector<DroneDetection>::iterator i;
	FlightCommand nothing;

	// build flight command
	nothing.thrust = 0.0f;
	nothing.roll = 0.0f;
	nothing.pitch = 0.0f;
	nothing.yaw = 0.0f;

	// send NULL flight command to everyone so we don't move or anything
	flightCommands.clear();
	for(i = detectionsMaster.begin(); i != detectionsMaster.end(); ++ i)
	{
		nothing.id = i -> id;
		flightCommands.push_back(nothing);
	}

	// [do not control general agent logic here]

	// send control commands to drones
	issueFlightCommands();
}

void PairwiseFlockingLabTrialController::updateFlightControls(float dt)
{
	vector<PairwiseFlockingLabAgent*>::iterator i;
	vector<DroneDetection>::iterator j;
	bool found;
	PairwiseFlockingLabAgent *curr;

	// update all agents we know should be present
	//LOG_INFO("-------------------------------------------");

	// clear previous flight commands
	flightCommands.clear();

	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		// try to find a detection associated with this agent; note that because we use the TrackingMatcher, a detection for a previously-known drone
		// should *always* be detected in the detectionsCurrentFrame list, but we check anyways to make it future-proof
		curr = *i;
		found = false;
		j = detectionsCurrentFrame.begin();
		while(!found && j != detectionsCurrentFrame.end())
		{
			found = (int)j -> id == curr -> getID();
			if(found)
			{
				// assign the drone detection
				LOG_INFO("[PFLC] matching detection " << j -> id << " with agent " << curr -> getID());
				curr -> setDroneDetection(*j);
				if(j -> detectionFail)
				{
					curr -> setDetectionFail(true);
				}

				// update general agent flight logic
				curr -> update(dt);
				flightCommands.push_back(curr -> getFlightCommand());
			}
			++ j;
		}
	}
}

void PairwiseFlockingLabTrialController::issueFlightCommands()
{
	vector<FlightCommand>::iterator i;

	// send flight commands based on PID output
	droneInterface -> clearCommands();
	for(i = flightCommands.begin(); i != flightCommands.end(); ++ i)
	{
		droneInterface -> addCommand(*i);
	}
}

void PairwiseFlockingLabTrialController::deleteAgents()
{
	vector<PairwiseFlockingLabAgent*>::iterator i;

	// free memory associated with pairwise flocking agents
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		delete *i;
	}

	// now clear the lists that now have dangling pointers
	agents.clear();
	agentsByID.clear();
}

void PairwiseFlockingLabTrialController::feedAgentSensors()
{
	vector<DroneDetection>::iterator i, k;
	vector<Obstacle*>::iterator m;
    map<uint32_t, PairwiseFlockingLabAgent*>::iterator j;
    map<uint32_t, DroneDetection>::iterator n;
	PairwiseFlockingLabAgent *curr;
	int partnerID;
	bool canSeePartner;
	float partnerDist;
	vec3 partnerVec;
	vec3 partnerVel;
	mat3 orientation;
	DroneDetection focal;
	DroneDetection neighbour;
	vec3 velocity;

	// reset edge boundary tracking
	agentAtEdge = false;

	// take the detections we received and inform each agent of whatever they need to know
    for(i = detectionsMaster.begin(); i != detectionsMaster.end(); ++ i)
    {
		j = agentsByID.find(i -> id);
		if(j != agentsByID.end())
		{
			// set information that the drone should know about itself; global position is not included in this, since the agent doesn't (and shouldn't!) need to know that
			curr = j -> second;
			partnerID = curr -> getPartnerID();

			// we must use the latest detection for this agent, not the one in detectionsMaster which is out of date
			focal = curr -> getDroneDetection();

			// transform velocity to drone's local frame of reference
			orientation[0] = focal.side;
			orientation[1] = focal.up;
			orientation[2] = focal.forward;
			velocity = (orientation * focal.velocity);

			// prepare a new list of neighbours to track
			curr -> clearNeighbours();

			// [todo]: handle case where partner gets lost and has landed because of lost tracking or a drone malfunction, etc.

			// [todo]: figure out a way to make statistics make sense if we lose a drone---can this be done?

			// do we have a partner? check if our global sensor picked them up; if they didn't, there's nothing we can do
			canSeePartner = false;
			if(partnerID >= 0)
			{
				// can we see them?
				n = detectionsByID.find(partnerID);
				if(n != detectionsByID.end())
				{
					canSeePartner = true;
				}
			}

			// make sure our partner is also within our own view range; whether or not we can see it determines how we interact with other neighbours
			partnerDist = 0.0f;
			if(partnerID >= 0 && canSeePartner)
			{
				// ignore the partner if it's too far away to see or blocked by an obstacle, or our sensor is being unreliable
				partnerVec = (n -> second.pos - focal.pos) + gaussRand(0.0f, neighbourSensingNoiseStdDev);
				partnerDist = length(vec2(partnerVec.x, partnerVec.z));
				partnerVel = n -> second.velocity - focal.velocity;

				// if partner separation is enabled, we allow the possibility that an agent lost its partner
				if(partnerSeparation)
				{
					if(partnerDist > viewRange || !hasLineOfSight(n -> second.pos, focal.pos) || !isSensorWorkingNow(focal.pos, neighbourSensingReliabilityThreshold))
					{
						canSeePartner = false;
					}
				}
			}

			// add our partner as an interaction neighbour
			if(partnerID >= 0 && canSeePartner)
			{
				neighbour.id = partnerID;
				neighbour.pos = partnerVec;
				neighbour.dist = partnerDist;
				neighbour.velocity = partnerVel;
				curr -> storeNeighbour(neighbour);
			}

			// drone also receives information about neighbours
			for(k = detectionsCurrentFrame.begin(); k != detectionsCurrentFrame.end(); ++ k)
			{
				// drone won't detect itself; also, skip any partner processing because we did that already
				if(i != k && i -> id != k -> id && (int)k -> id != partnerID)
				{
					// build neighbour detection info, and put in current agent's frame of reference
					neighbour.id = k -> id;
                    neighbour.pos = (k -> pos - focal.pos) + gaussRand(0.0f, neighbourSensingNoiseStdDev);
                    neighbour.velocity = k -> velocity - focal.velocity;					// [todo]: adjust local velocity sensing to be noisy as well
                    neighbour.dist = length(vec2(neighbour.pos.x, neighbour.pos.z));

                    // can we see this agent? note that view range, line of sight, and sensor reliability are all taken into account
                    if(neighbour.dist <= viewRange && hasLineOfSight(focal.pos, k -> pos) && isSensorWorkingNow(focal.pos, neighbourSensingReliabilityThreshold))
                    {
						// do we have/can we see a partner?
						if(partnerID >= 0 && canSeePartner)
						{
							// yes; if we're restricting ourselves to asymmetric tracking only, then only store neighbours at the correct orientation with respect to our partner;
							// the idea is that our partner acts as a proxy for other neighbours that we do not store here; otherwise, just add anyways
							if(dot(partnerVec, neighbour.pos) <= 0.0f || !asymmetricTracking)
							{
								curr -> storeNeighbour(neighbour);
							}
						}
						else
						{
							// no; just add the neighbour
							curr -> storeNeighbour(neighbour);
						}
                    }
				}
			}

			// drone limits its neighbour count by the number of neighbours it's supposed to track
			curr -> sortAndLimitNeighbours(numTrackedIfSingle, numTrackedIfPaired);

			// drone also receives information about obstacles
			curr -> clearObstacles();
			for(m = obstacles.begin(); m != obstacles.end(); ++ m)
			{
				curr -> storeObstacle(Obstacle((*m) -> pos - focal.pos, (*m) -> radius, (*m) -> avoidRadius));
			}

			// if an agent gets close to the world edge, note this
			if(curr -> isCloseToBounds()) agentAtEdge = true;
		}
		else
		{
			// this might not be a big deal, but let's throw a not-fatal warning if we get an ID we're not familiar with; it's just likely some sensor edge case; if you
			// get lots of these it's probably indicative of a bigger problem
			LOG_INFO("[PFTC] WARNING: drone ID " << i -> id << " was detected but no drone is known by that name");
		}
    }
}

void PairwiseFlockingLabTrialController::buildUncompressedFlockingNetwork()
{
	vector<PairwiseFlockingLabAgent*>::iterator i;
	vector<DroneDetection>::iterator j;
	vector<int> ids;
	PairwiseFlockingLabAgent *curr;

	// clear the old uncompressed flocking network
	if(!uncompressedNetwork)
	{
		uncompressedNetwork = new FlockingNetwork();
	}
	uncompressedNetwork -> clear();

	// add the agents we have to the network
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		ids.push_back((*i) -> getID());
	}
	uncompressedNetwork -> addAgents(ids);

	// now add their connections---these are interpreted as bi-directional links by the FlockingNetwork
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		curr = *i;
		for(j = curr -> getNeighboursBegin(); j != curr -> getNeighboursEnd(); ++ j)
		{
			uncompressedNetwork -> connectAgents(curr -> getID(), j -> id);
		}
	}

	// compute flock statistics
	uncompressedNetwork -> computeStats();
}

void PairwiseFlockingLabTrialController::buildCompressedFlockingNetwork()
{
	map<int, int> mappedIDs;
	map<int, int>::iterator j;
	vector<PairwiseFlockingLabAgent*>::iterator i;
	PairwiseFlockingLabAgent *curr;
	vector<int> compressedIDs;
	vector<int>::iterator k;
	vector<DroneDetection>::iterator m;
	int myID, partnerID, minID;
	bool present;

	// clear the old compressed flocking network
	if(!compressedNetwork)
	{
		compressedNetwork = new FlockingNetwork();
	}
	compressedNetwork -> clear();

	// build an ID mapping for all agents
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		curr = *i;
		myID = curr -> getID();
		mappedIDs[myID] = myID;
	}

	// re-map paired agents' connections to the lowest ID in that pair; now each member of a pair is mapped to a single ID
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		curr = *i;
        if(curr -> getCanSeePartner())
        {
			// extract IDs
			myID = curr -> getID();
			partnerID = curr -> getPartnerID();

			// compute the minimum of those IDs and map both of our IDs to that minimum
			minID = glm::min(partnerID, myID);
            mappedIDs[partnerID] = minID;
        }
	}

	// build a complete list of all IDs that we have mapped to; only higher IDs belonging to mated pairs will not be present
	for(j = mappedIDs.begin(); j != mappedIDs.end(); ++ j)
	{
		// make sure the entry is not already present
		present = false;
		k = compressedIDs.begin();
		while(!present && k != compressedIDs.end())
		{
			present = (*k++) == j -> second;
		}

		// if not already present, add it
		if(!present)
		{
			compressedIDs.push_back(j -> second);
		}
	}

	// now add our list of compressed IDs to the network
	compressedNetwork -> addAgents(compressedIDs);

	// now connect these agents based on our ID mapping, not the actual IDs themselves
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		curr = *i;
		for(m = curr -> getNeighboursBegin(); m != curr -> getNeighboursEnd(); ++ m)
		{
			compressedNetwork -> connectAgents(mappedIDs[curr -> getID()], mappedIDs[m -> id]);
		}
	}

	// compute our network stats
	compressedNetwork -> computeStats();
}

void PairwiseFlockingLabTrialController::openOutputFile()
{
    stringstream ss;
    string folder;
	uint32_t index;

	// create the directory indicated (if any) if it does not exist yet
	index = trialOutputFilename.find_last_of('/');
	if(index != string::npos)
	{
		folder = trialOutputFilename.substr(0, index);
		if(!dirDoesDirectoryExist(folder))
		{
			dirMakeDirectory(folder);
		}
	}

    // build the output file name and open the file for writing
    ss << trialOutputFilename << ".txt";
    outputFileName = ss.str();
    outputFile.open(outputFileName.c_str());
    if(!outputFile.is_open())
    {
		LOG_ERROR("[PFTC] could not open output file \"" << outputFileName << "\" for writing");
    }
}

void PairwiseFlockingLabTrialController::closeOutputFile()
{
	if(outputFile.is_open())
	{
		outputFile.close();
	}
}

void PairwiseFlockingLabTrialController::saveOutputStats()
{
	outputFile << (int)round(trialTime) << ",";
	outputFile << uncompressedNetwork -> getNumSubFlocks() << ",";
	outputFile << uncompressedNetwork -> getAverageSubFlockSize() << ",";
	outputFile << uncompressedNetwork -> getAverageDiameter() << ",";
	outputFile << uncompressedNetwork -> getAverageDegree() << ",";
	outputFile << uncompressedNetwork -> getNumConnections() << ",";
	outputFile << compressedNetwork -> getAverageDiameter() << ",";
	outputFile << compressedNetwork -> getAverageDegree() << ",";
	outputFile << compressedNetwork -> getNumConnections();
	outputFile << endl;

	LOG_INFO("---------------------------------------------------------------");
	LOG_INFO("sub flock size  : " << uncompressedNetwork -> getAverageSubFlockSize() << "  |  " << compressedNetwork -> getAverageSubFlockSize());
	LOG_INFO("avg diameter    : " << uncompressedNetwork -> getAverageDiameter() << "  |  " << compressedNetwork -> getAverageDiameter());
	LOG_INFO("avg degree      : " << uncompressedNetwork -> getAverageDegree() << "  |  " << compressedNetwork -> getAverageDegree());
	LOG_INFO("num connections : " << uncompressedNetwork -> getNumConnections() << "  |  " << compressedNetwork -> getNumConnections());
}

bool PairwiseFlockingLabTrialController::hasLineOfSight(const vec3 &a, const vec3 &b)
{
	vector<Obstacle*>::iterator i;
	bool canSee = true;
	dvec2 p1, p2, center, intersect;
	float radius;

	// skip the occlusion check if occlusion is disabled
	if(obstacleOcclusion)
	{
		i = obstacles.begin();
		while(canSee && i != obstacles.end())
		{
			// do a simple ray-sphere intersection test for this obstacle
			p1 = vec2(a.x, a.z);
			p2 = vec2(b.x, b.z);
			center = vec2((*i) -> pos.x, (*i) -> pos.z);
			radius = (*i) -> radius;
			canSee = !rayIntersectsCircle(p1, p2, center, (*i) -> radius, intersect) &&
			         length(p1 - center) >= radius &&
			         length(p2 - center) >= radius;

			// for debugging purposes, indicate visually later on that the obstacle is blocking our view
			if(!canSee)
			{
				(*i) -> obscuring = true;
			}

			++ i;
		}
	}

	return canSee;
}

bool PairwiseFlockingLabTrialController::isSensorWorkingNow(const vec3 &pos, float reliability)
{
	// a common misconception about Simplex noise is that it ranges from [-1, 1] when in fact it ranges from [-sqrt(N/4), sqrt(N/4)] where N is the number of
	// dimensions; thus, we need to scale our Simplex noise to fit in the range (0, 1] so that our sensor reliability theshold (from [0, 1]) makes sense
	const float SIMPLEX_MAX = sqrt(3.0f / 4.0f);
	const float SIMPLEX_RANGE = SIMPLEX_MAX * 2.0f;
	const float SCALING_FACTOR = 10.0f;

	// compute Simplex noise for the position we're trying to sense
	float val = simplex(pos * SCALING_FACTOR);

	// bring it into the range (0, 1]
	float scaled = clamp((val + SIMPLEX_MAX) / SIMPLEX_RANGE, 0.000001f, 1.0f);

	// see if our sensor meets the reliability test
	return reliability >= scaled;
}

void PairwiseFlockingLabTrialController::renderConnections(const glm::mat4 &viewProjection)
{
	const vec4 OUTGOING_COLOR(0.0f, 1.0f, 0.0f, 0.5f);
	const vec4 INCOMING_COLOR(0.0f, 0.0f, 0.0f, 0.0f);

	const vec4 PARTNER_COLOR(1.0f, 0.0f, 0.0f, 0.5f);
	const vec4 PARTNER_COLOR_INCOMING(1.0f, 0.0f, 0.0f, 0.0f);

	const vec4 CATCHING_UP_COLOR(0.2f, 1.0f, 1.0f, 1.0f);

	vector<DroneDetection>::iterator i, k;
	map<uint32_t, PairwiseFlockingLabAgent*>::iterator j;
	map<uint32_t, DroneDetection>::iterator m;
	PairwiseFlockingLabAgent *curr;

	// go through all agents we detected this frame
	for(i = detectionsCurrentFrame.begin(); i != detectionsCurrentFrame.end(); ++ i)
	{
		// make sure this is an agent ID we recognize
		j = agentsByID.find(i -> id);
		if(j != agentsByID.end())
		{
			// iterate through this agent's interaction partners and see if we can find them in the list of detections for this frame
			curr = j -> second;
			for(k = curr -> getNeighboursBegin(); k != curr -> getNeighboursEnd(); ++ k)
			{
				// find this agent in our detections
				m = detectionsByID.find(k -> id);
				if(m != detectionsByID.end())
				{
					// draw a line between the two of them
					if(j -> second -> getPartnerID() == (int)m -> second.id)
					{
						// partner connections are red
						if(j -> second -> getCatchingUpToPartner())
						{
							LineRenderer::getInstance() -> add(i -> pos, m -> second.pos, CATCHING_UP_COLOR, CATCHING_UP_COLOR);
						}
						else
						{
							LineRenderer::getInstance() -> add(i -> pos, m -> second.pos, PARTNER_COLOR, PARTNER_COLOR_INCOMING);
						}

					}
					else
					{
						// regular neighbour connections are green
						LineRenderer::getInstance() -> add(i -> pos, m -> second.pos, OUTGOING_COLOR, INCOMING_COLOR);
					}
				}
				else
				{
					LOG_INFO("[PFTC] WARNING: renderConnections() could not find interaction partner with ID " << k -> id);
				}
			}
		}
		else
		{
			LOG_INFO("[PFTC] WARNING: renderConnections() could not find a drone with ID " << i -> id);
		}
	}
}

void PairwiseFlockingLabTrialController::renderObstacles(const glm::mat4 &viewProjection)
{
	vector<Obstacle*>::iterator i;

	mat4 modelMat;

	// set appropriate blending and depth test modes
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_DEPTH_TEST);

	// position the camera
	sphereShader -> bind();
	sphereShader -> uniformMat4("u_VP", viewProjection);

	// render each obstacle and its repulsion radius
	for(i = obstacles.begin(); i != obstacles.end(); ++ i)
	{
		modelMat = mat4(1.0f);
		modelMat = translate(modelMat, (*i) -> pos);
		modelMat = scale(modelMat, vec3((*i) -> radius * 2.0f));

		sphereShader -> uniformMat4("u_Model", modelMat);
		sphereShader -> uniformVec4("u_Color", (*i) -> obscuring ? vec4(1.0f, 1.0f, 0.0f, 1.0f) : vec4(1.0f, 0.0f, 0.0f, 1.0f));
		sphereModel -> render();

		// repulsion radius does not write to depth buffer, so we avoid obscuring objects we should be able to see behind us
		glDepthMask(GL_FALSE);

		modelMat = mat4(1.0f);
		modelMat = translate(modelMat, (*i) -> pos);
		modelMat = scale(modelMat, vec3((*i) -> avoidRadius * 2.0f));

		sphereShader -> uniformMat4("u_Model", modelMat);
		sphereShader -> uniformVec4("u_Color", vec4(1.0f, 0.0f, 0.0f, 0.25f));
		sphereModel -> render();

		// restore depth writes
		glDepthMask(GL_TRUE);

		(*i) -> obscuring = false;
	}

	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
}

void PairwiseFlockingLabTrialController::renderBounds(const mat4 &viewProjection)
{
	LineRenderer *lines = LineRenderer::getInstance();

	const float HEIGHT = 0.25f;

	const vec4 COLOR = (agentAtEdge ? vec4(1.0f, 0.0f, 0.0f, 0.75f) : vec4(0.0f, 1.0f, 0.0f, 0.75f));

	const vec3 CORNER_1(boundsHalfArea.x, HEIGHT, boundsHalfArea.y);
	const vec3 CORNER_2(-boundsHalfArea.x, HEIGHT, boundsHalfArea.y);
	const vec3 CORNER_3(-boundsHalfArea.x, HEIGHT, -boundsHalfArea.y);
	const vec3 CORNER_4(boundsHalfArea.x, HEIGHT, -boundsHalfArea.y);

	lines -> add(CORNER_1, CORNER_2, COLOR);
	lines -> add(CORNER_2, CORNER_3, COLOR);
	lines -> add(CORNER_3, CORNER_4, COLOR);
	lines -> add(CORNER_4, CORNER_1, COLOR);

	// now render everything
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glDepthMask(GL_FALSE);
	lines -> send();
	lines -> render(viewProjection);
	lines -> clear();

	glDepthMask(GL_TRUE);
	glDisable(GL_DEPTH_TEST);
}

void PairwiseFlockingLabTrialController::renderVelocities(const mat4 &viewProjection)
{
	const vec4 TARGET_COLOR(1.0f, 1.0f, 0.0f, 1.0f);
	const vec4 ACTUAL_COLOR(0.0f, 1.0f, 1.0f, 1.0f);
	const vec4 STEER_COLOR(1.0f, 0.0f, 0.0f, 1.0f);
	const vec4 PITCH_AND_ROLL_COLOR(1.0f, 0.6f, 1.0f, 1.0f);

	const float LENGTH_MULTIPLIER = 0.7f;
	const float AVOID_LENGTH_MULTIPLIER = 2.5f;
	const float PITCH_AND_ROLL_LENGTH_MULTIPLIER = 5.0f;
	const float FAIL_LENGTH = 0.5f;

	LineRenderer *lines = LineRenderer::getInstance();
	vector<PairwiseFlockingLabAgent*>::iterator i;
	PairwiseFlockingLabAgent *curr;
	vec3 currPos;
	vec3 currVel;
	vec3 currTargetVel;
	vec3 currSteerAway;
	vec3 currPitchAndRoll;

	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		curr = *i;
		currPos = curr -> getPos();
		currVel = curr -> getVelocity();
		currTargetVel = vec3(curr -> getTargetXZVelocity().x, 0.0f, curr -> getTargetXZVelocity().y);
		currSteerAway = vec3(curr -> getSteerAway().x, 0.0f, curr -> getSteerAway().y);
		currPitchAndRoll = vec3(curr -> getPitchAndRollOutput().x, 0.0f, curr -> getPitchAndRollOutput().y);

		lines -> add(currPos, currPos + (currTargetVel * LENGTH_MULTIPLIER), TARGET_COLOR);
		lines -> add(currPos, currPos + (currVel * LENGTH_MULTIPLIER), ACTUAL_COLOR);

		lines -> add(currPos, currPos + (currSteerAway * AVOID_LENGTH_MULTIPLIER), STEER_COLOR);

		// rotate and render pitch and roll output
		//currPitchAndRoll = rotate(currPitchAndRoll, curr -> getDroneDetection().dir, vec3(0.0f, 1.0f, 0.0f));
		lines -> add(currPos, currPos + (currPitchAndRoll * PITCH_AND_ROLL_LENGTH_MULTIPLIER), PITCH_AND_ROLL_COLOR);

		if(curr -> getDetectionFail())
		{
			lines -> add(currPos + vec3(-FAIL_LENGTH, 0.0f, -FAIL_LENGTH), currPos + vec3(FAIL_LENGTH, 0.0f, FAIL_LENGTH), vec4(1.0f, 0.0f, 0.0f, 1.0f));
			lines -> add(currPos + vec3(FAIL_LENGTH, 0.0f, -FAIL_LENGTH), currPos + vec3(-FAIL_LENGTH, 0.0f, FAIL_LENGTH), vec4(1.0f, 0.0f, 0.0f, 1.0f));
			curr -> setDetectionFail(false);
		}
	}

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glDepthMask(GL_FALSE);

	lines -> send();
	lines -> render(viewProjection);
	lines -> clear();

	glDepthMask(GL_TRUE);
	glDisable(GL_DEPTH_TEST);
}

void PairwiseFlockingLabTrialController::renderPositioning(const mat4 &viewProjection)
{
	const vec4 TARGET_COLOR(1.0f, 0.75f, 1.0f, 1.0f);
	const float CROSSHAIR_SIZE = 0.1f;

	LineRenderer *lines = LineRenderer::getInstance();
	vector<PairwiseFlockingLabAgent*>::iterator i;
	vec3 target;

	// show the box vertices and highlight our target vertex
	for(i = agents.begin(); i != agents.end(); ++ i)
	{
		target = (*i) -> getStartingPosition();
		target.y = 0.0f;

		lines -> add(target + vec3(-CROSSHAIR_SIZE, 0.0f, 0.0f), target + vec3(CROSSHAIR_SIZE, 0.0f, 0.0f), TARGET_COLOR);
		lines -> add(target + vec3(0.0f, 0.0f, -CROSSHAIR_SIZE), target + vec3(0.0f, 0.0f, CROSSHAIR_SIZE), TARGET_COLOR);
	}
}

void PairwiseFlockingLabTrialController::renderRadioCommands(const mat4 &viewProjection)
{
	const float ROLL_DEMAND_GAIN = 40.0f;		// for this debugging task just use hard-coded values that we configured the ubee to use
	const float PITCH_DEMAND_GAIN = 40.0f;

	const float DEMAND_MULTIPLIER = 0.5f;

	const vec4 DEMAND_COLOR(1.0f, 1.0f, 1.0f, 1.0f);

	LineRenderer *lines = LineRenderer::getInstance();

	vector<PairwiseFlockingLabAgent*>::iterator i;
	PairwiseFlockingLabAgent *curr;
	vec3 currPos;
	vec3 currDemand;
	vector<uint8_t> bytes;

	uint8_t byteIndex;
	uint8_t bytesProcessed;
	uint8_t msgLen;
	uint8_t msgID;

	//uint8_t yawThrust;
	//uint8_t throttle;
	uint8_t rollThrust;
	uint8_t pitchThrust;

	float pitchDemand;
	float rollDemand;

	UBeeRadioInterface *radio;

	// ignore this visualization if we're not using a real ubee radio
	radio = dynamic_cast<UBeeRadioInterface*>(droneInterface);
	if(radio)
	{
		radio -> getLastSent(bytes);
		if(bytes.size())
		{
			// ignore everything other than compound flight commands
			if(bytes[0] == UBeeRadioInterface::MSG_COMPOUND_FLIGHT_COMMAND)
			{
				// pretend that each agent got this compound flight command
				for(i = agents.begin(); i != agents.end(); ++ i)
				{
					curr = *i;
					currPos = curr -> getPos();

					bytesProcessed = 0;
					msgLen = bytes[1];
					byteIndex = 2;

					while(bytesProcessed < msgLen)
					{
						msgID = bytes[byteIndex];
						if(msgID == curr -> getID())
						{
							// pull demands in this compound flight command that are addressed to us
							++ byteIndex;						// skip over ID
							//yawThrust = bytes[byteIndex++];
							//throttle = bytes[byteIndex++];
							byteIndex ++;		// skip yawThrust
							byteIndex ++;		// skip throttle
							rollThrust = bytes[byteIndex++];
							pitchThrust = bytes[byteIndex++];

							// early exit
							bytesProcessed = msgLen;

							// convert demands into floating-point values we can use
							pitchDemand = (((float)pitchThrust - 128.0f) / 128.0f) * PITCH_DEMAND_GAIN;
							rollDemand = (((float)rollThrust - 128.0f) / 128.0f) * ROLL_DEMAND_GAIN;

							// render those demands
							currDemand = vec3(-rollDemand, 0.0f, pitchDemand);
							lines -> add(currPos, currPos + (currDemand * DEMAND_MULTIPLIER), DEMAND_COLOR);
						}
						else
						{
							// advance to next segment of compound flight command
							byteIndex += 5;					// skip over ID and flight command bytes
							bytesProcessed += 5;			// factor into what we skipped over
						}
					}
				}
			}
		}
	}
}

void PairwiseFlockingLabTrialController::makeObstacleField(GumdropNode *trialConfig)
{
	vec3 origin;
	float cellSize;
	uint32_t horizontalCells;
	uint32_t verticalCells;
	float softRadius;
	float hardRadius;

	uint32_t i, j;
	vec3 topLeft;

	// obstacles might be turned off
	if(trialConfig -> getBool("trial.obstacles_enabled", false, NULL, true))
	{
		// pull obstacle field characteristics
		origin = vec3(trialConfig -> getDouble("trial.obstacle_field.origin[0]", 0.0f, NULL, true),
					  0.0f,
					  trialConfig -> getDouble("trial.obstacle_field.origin[1]", 0.0f, NULL, true));
		cellSize = trialConfig -> getDouble("trial.obstacle_field.cell_size", 0.0f, NULL, true);
		horizontalCells = trialConfig -> getInt("trial.obstacle_field.cells[0]", 0, NULL, true);
		verticalCells = trialConfig -> getInt("trial.obstacle_field.cells[1]", 0, NULL, true);
		softRadius = trialConfig -> getDouble("trial.obstacle_field.soft_radius", 0.0f, NULL, true);
		hardRadius = trialConfig -> getDouble("trial.obstacle_field.hard_radius", 0.0f, NULL, true);

		// position the obstacles
		topLeft = origin + vec3(-cellSize * (float)(horizontalCells - 1) * 0.5f, 0.0f, -cellSize * (float)(verticalCells - 1) * 0.5f);
		for(i = 0; i < verticalCells; ++ i)
		{
			for(j = 0; j < horizontalCells; ++ j)
			{
				obstacles.push_back(new Obstacle(topLeft + vec3(j * cellSize, BASE_TARGET_FLYING_HEIGHT_METERS, i * cellSize), hardRadius, softRadius));
			}
		}
	}
}

void PairwiseFlockingLabTrialController::makeObstaclesIndividual(GumdropNode *trialConfig)
{
	int numObstacles;
	float softRadius;
	float hardRadius;
	vec3 pos;
	stringstream ss;
	int i;

	// obstacles might be turned off
	if(trialConfig -> getBool("trial.obstacles_enabled", false, NULL, true))
	{
		// pull number and size info
		numObstacles = trialConfig -> getInt("trial.obstacles.num_obstacles", 0, NULL, true);
		softRadius = trialConfig -> getDouble("trial.obstacles.soft_radius", 0.0f, NULL, true);
		hardRadius = trialConfig -> getDouble("trial.obstacles.hard_radius", 0.0f, NULL, true);

		// add however many obstacles were specified
		for(i = 0; i < numObstacles; ++ i)
		{
			// read properties
			ss.str("");
			ss << "trial.obstacles.obstacle[" << i << "]";
            pos.x = trialConfig -> getDouble(ss.str() + ".pos[0]", 0.0f, NULL, true);
            pos.y = BASE_TARGET_FLYING_HEIGHT_METERS;//trialConfig -> getDouble(ss.str() + ".pos[1]", 0.0f, NULL, true);
            pos.z = trialConfig -> getDouble(ss.str() + ".pos[2]", 0.0f, NULL, true);

            // add the obstacle
            obstacles.push_back(new Obstacle(pos, hardRadius, softRadius));
		}
	}
}

void PairwiseFlockingLabTrialController::loadAssets()
{
	WavefrontModel *sphereWavefront = WavefrontModel::fromFile("../mesh/sphere.obj");
	sphereModel = Model::fromWavefront(sphereWavefront);
	delete sphereWavefront;

	// load our drone shader

	sphereShader = Shader::fromFile("../shaders/solid-plain.vert", "../shaders/solid-plain.frag");
	sphereShader -> bindAttrib("a_Vertex", 0);
	sphereShader -> bindAttrib("a_Normal", 1);
	sphereShader -> link();
	sphereShader -> bind();
	sphereShader -> uniformVec3("u_SunColor", vec3(1.0f, 1.0f, 1.0f));
	sphereShader -> uniformVec3("u_SunDirection", vec3(0.0f, -1.0f, 0.0f));
	sphereShader -> uniformVec3("u_AmbientColor", vec3(0.2f, 0.2f, 0.2f));
}
