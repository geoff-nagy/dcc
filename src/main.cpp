// Drone Command Center (DCC) Tool
// Geoff Nagy
// A tool for both simulated and real drone experiments

#include "core/application.h"
#include "core/version.h"

#include "util/log.h"

#include <string.h>
#include <string>
#include <iostream>
using namespace std;

// - - - constants - - - //

// - - - prototypes - - - //

void printUsage(char *argv[]);

// - - - main function - - - //

int main(int args, char *argv[])
{
	Application *app;

	// open logging facilities
	Log::open();

	// parse and log the provided arguments
	LOG_INFO("Drone Command Center v" << VERSION_MAJOR << "." << VERSION_MINOR << "." << VERSION_REVISION);
	LOG_INFO("Geoff Nagy, Simon Fraser University");
	LOG_INFO("gnagy@sfu.ca");
	LOG_INFO("------------------------------------------------------------------------");

	// start the application with the arguments provided
	app = Application::getInstance();
	app -> init(args, argv);
	app -> run();
	delete app;

	// and we're done
	return 0;
}

// - - - other functions - - - //
