#include "gui/form.h"

#include "glm/glm.hpp"
using namespace glm;

Form::Form(const glm::vec2 &size)
{
	setSize(size);
}

Form::~Form() { }

void Form::update(Cursor *cursor, float dt)
{
	Container::update(cursor, dt);
}

void Form::render()
{
	Container::render();
}
