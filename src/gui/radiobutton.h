#pragma once

#include "gui/clickable.h"
#include "gui/form.h"

#include "glm/glm.hpp"

#include <string>

class RadioButton : public Clickable
{
public:
	typedef enum SELECTION_TYPE
	{
		SELECTION_TYPE_SINGLE = 0,
		SELECTION_TYPE_GROUPED
	} SelectionType;

	RadioButton(const std::string &text, const glm::vec2 &size, SelectionType selectionType, bool selected, Form *form, void (Form::*onRelease)(), int keyboardKey = -1, int keyboardModifier = -1);
	~RadioButton();

	void setSelected(bool selected);
	bool getSelected();

	void setText(const std::string &text);

	bool isSingle();

	void update(Cursor *cursor, float dt);
	void render();

private:
	static const glm::vec4 BUTTON_UNSELECTED_NORMAL_COLOR;
	static const glm::vec4 BUTTON_UNSELECTED_HOVER_COLOR;
	static const glm::vec4 BUTTON_UNSELECTED_DOWN_COLOR;

	static const glm::vec4 BUTTON_SELECTED_NORMAL_COLOR;
	static const glm::vec4 BUTTON_SELECTED_HOVER_COLOR;
	static const glm::vec4 BUTTON_SELECTED_DOWN_COLOR;

	static const glm::vec4 BUTTON_DISABLED_COLOR;

	static const glm::vec4 TEXT_COLOR;

	std::string text;
	SelectionType selectionType;
	Form *form;
	void (Form::*onRelease)();
	bool selected;
	int keyboardKey;
	int keyboardModifier;
};
