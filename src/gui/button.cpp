#include "gui/button.h"

#include "rendering/quad.h"
#include "rendering/text.h"

#include "glm/glm.hpp"
using namespace glm;

#include <string>
using namespace std;

const vec4 Button::BUTTON_NORMAL_COLOR(0.35f, 0.35f, 0.35f, 0.5f);
const vec4 Button::BUTTON_HOVER_COLOR(0.45f, 0.45f, 0.45f, 0.5f);
const vec4 Button::BUTTON_DOWN_COLOR(1.0f, 0.71f, 0.0f, 0.75f);
const vec4 Button::TEXT_COLOR(1.0f);

const vec4 Button::BUTTON_DISABLED_COLOR(0.1f, 0.1f, 0.1f, 0.5f);
const vec4 Button::TEXT_DISABLED_COLOR(0.5f, 0.5f, 0.5f, 1.0f);

Button::Button(const string &text, const vec2 &size, Form *form, void (Form::*onRelease)(), int keyboardKey, int keyboardModifier)
{
	this -> text = text;
	this -> form = form;
	this -> onRelease = onRelease;

	setSize(size);
	setKeys(keyboardKey, keyboardModifier);
}

Button::~Button()
{
	// nothing
}

void Button::setText(const string &text)
{
	this -> text = text;
}

void Button::update(Cursor *cursor, float dt)
{
	// get user action
	Clickable::update(cursor, dt);

	// if user released button, invoke event handler if one is assigned
	if(getRelease() && form && onRelease)
	{
		(form ->* onRelease)();
	}
}

void Button::render()
{
	vec2 size = getSize();
	vec2 textTweak = (size / 2.0f) + vec2(0.0f, 4.0f);
	vec2 pos = getPos();

	vec4 backColor;
	vec4 textColor;

	if(getEnabled())
	{
		if     (getDown())  backColor = BUTTON_DOWN_COLOR;
		else if(getHover()) backColor = BUTTON_HOVER_COLOR;
		else                backColor = BUTTON_NORMAL_COLOR;

		textColor = TEXT_COLOR;
	}
	else
	{
		backColor = BUTTON_DISABLED_COLOR;
		textColor = TEXT_DISABLED_COLOR;
	}

	Quad::getInstance() -> render(pos, size, backColor);
	Text::getInstance() -> render(text, pos + textTweak, textColor, Text::ALIGN_CENTER);
}
