#include "gui/label.h"

#include "rendering/quad.h"
#include "rendering/text.h"

#include "libdrawtext/drawtext.h"

#include "glm/glm.hpp"
using namespace glm;

#include <string>
using namespace std;

const vec4 Label::TEXT_COLOR(1.0f);
const vec4 Label::SHADOW_COLOR(0.0f, 0.0f, 0.0f, 1.0f);
const vec4 Label::BACKGROUND_COLOR(0.0f, 0.0f, 0.0f, 0.75f);

Label::Label(const string &text, bool background, Text::TextAlignment textAlign)
{
	setText(text);
	setSize(vec2(dtx_string_width(text.c_str()), 16.0f));
	this -> background = background;
	this -> textAlign = textAlign;
}

Label::Label(const string &text, const vec2 &size, bool background, Text::TextAlignment textAlign)
{
	setText(text);
	setSize(size);
	this -> background = background;
	this -> textAlign = textAlign;
}

Label::~Label() { }

void Label::setText(const string &text)
{
	this -> text = text;
}

void Label::render()
{
	const vec2 SHADOW_POS_TWEAK(1, 1);

	vec2 size = getSize();
	vec2 textTweak = vec2(0.0f, (size.y / 2.0f) + 4.0f);
	vec2 pos = getPos();

	// background is optional
	if(background)
	{
		Quad::getInstance() -> render(pos, size, BACKGROUND_COLOR);
	}

	// shadow always renders
	Text::getInstance() -> render(text, pos + textTweak + SHADOW_POS_TWEAK, SHADOW_COLOR, textAlign);

	// text always renders
	Text::getInstance() -> render(text, pos + textTweak, TEXT_COLOR, textAlign);
}
