#include "gui/radiobutton.h"

#include "rendering/quad.h"
#include "rendering/text.h"

#include "glm/glm.hpp"
using namespace glm;

#include <string>
using namespace std;

const vec4 RadioButton::BUTTON_UNSELECTED_NORMAL_COLOR(0.35f, 0.35f, 0.35f, 0.5f);
const vec4 RadioButton::BUTTON_UNSELECTED_HOVER_COLOR(0.45f, 0.45f, 0.45f, 0.5f);
const vec4 RadioButton::BUTTON_UNSELECTED_DOWN_COLOR(1.0f, 0.71f, 0.0f, 0.5f);

const vec4 RadioButton::BUTTON_SELECTED_NORMAL_COLOR(1.0f, 0.71f, 0.0f, 0.75f);
const vec4 RadioButton::BUTTON_SELECTED_HOVER_COLOR(1.0f, 0.71f, 0.0f, 0.9f);
const vec4 RadioButton::BUTTON_SELECTED_DOWN_COLOR(1.0f, 0.71f, 0.0f, 1.0f);

const vec4 RadioButton::BUTTON_DISABLED_COLOR(0.0f, 0.0f, 0.0f, 0.9f);

const vec4 RadioButton::TEXT_COLOR(1.0f);

RadioButton::RadioButton(const string &text, const vec2 &size, SelectionType selectionType, bool selected, Form *form, void (Form::*onRelease)(), int keyboardKey, int keyboardModifier)
{
	this -> selectionType = selectionType;
	this -> selected = selected;
	this -> form = form;
	this -> onRelease = onRelease;

	setText(text);
	setSize(size);
	setKeys(keyboardKey, keyboardModifier);
}

RadioButton::~RadioButton()
{
	// nothing
}

void RadioButton::setSelected(bool selected)
{
	this -> selected = selected;
}

bool RadioButton::getSelected()
{
	return selected;
}

void RadioButton::setText(const string &text)
{
	this -> text = text;
}

bool RadioButton::isSingle()
{
	return selectionType == SELECTION_TYPE_SINGLE;
}

void RadioButton::update(Cursor *cursor, float dt)
{
	// get user action
	Clickable::update(cursor, dt);

	// if user released button, invoke event handler if one is assigned
	if(getRelease())
	{
		selected = !selected;
		if(form && onRelease)
		{
			(form ->* onRelease)();
		}
	}
}

void RadioButton::render()
{
	vec4 color = BUTTON_DISABLED_COLOR;
	vec2 size = getSize();
	vec2 textTweak = (size / 2.0f) + vec2(0.0f, 4.0f);
	vec2 pos = getPos();

	if(getEnabled())
	{
		if(selected)
		{
			color = BUTTON_SELECTED_NORMAL_COLOR;
			if     (getDown())  color = BUTTON_SELECTED_DOWN_COLOR;
			else if(getHover()) color = BUTTON_SELECTED_HOVER_COLOR;
		}
		else
		{
			color = BUTTON_UNSELECTED_NORMAL_COLOR;
			if     (getDown())  color = BUTTON_UNSELECTED_DOWN_COLOR;
			else if(getHover()) color = BUTTON_UNSELECTED_HOVER_COLOR;
		}
	}

	Quad::getInstance() -> render(pos, size, color);
	Text::getInstance() -> render(text, pos + textTweak, TEXT_COLOR, Text::ALIGN_CENTER);
}
