#include "gui/slider.h"
#include "gui/cursor.h"

#include "rendering/quad.h"
#include "rendering/text.h"

#include "glm/glm.hpp"
using namespace glm;

#include <string>
#include <sstream>
using namespace std;

const vec4 Slider::BACKGROUND_NORMAL_COLOR(0.0f, 0.0f, 0.0f, 0.5f);
const vec4 Slider::BACKGROUND_HOVER_COLOR(0.1f, 0.1f, 0.1f, 0.5f);
const vec4 Slider::BACKGROUND_DOWN_COLOR(0.2f, 0.2f, 0.2f, 0.5f);

const vec4 Slider::SLIDER_NORMAL_COLOR(0.55f, 0.55f, 0.55f, 0.5f);
const vec4 Slider::SLIDER_HOVER_COLOR(0.65f, 0.65f, 0.65f, 0.5f);
const vec4 Slider::SLIDER_DOWN_COLOR(1.0f, 0.71f, 0.0f, 0.75f);

const vec4 Slider::TEXT_COLOR(1.0f);

Slider::Slider(const string &text, const vec2 &size, Form *form, void (Form::*onChange)(int), int min, int max)
{
	init(text, size, form, onChange, min, max, max);
}

Slider::Slider(const string &text, const vec2 &size, Form *form, void (Form::*onChange)(int), int min, int max, int value)
{
	init(text, size, form, onChange, min, max, value);
}

Slider::~Slider()
{
	// nothing
}

void Slider::init(const string &text, const vec2 &size, Form *form, void (Form::*onChange)(int), int min, int max, int value)
{
	this -> text = text;
	this -> form = form;
	this -> onChange = onChange;
	this -> min = min;
	this -> max = max;
	this -> value = value;
	setSize(size);
}

void Slider::setValue(int value)
{
	this -> value = value;
	if(form && onChange)
	{
		(form->*onChange)(value);
	}
}

int Slider::getValue()
{
	return value;
}

void Slider::update(Cursor *cursor, float dt)
{
	vec2 mousePos;
	vec2 pos;
	vec2 size;
	float xFactor;

	// get user action
	Clickable::update(cursor, dt);

	// compute slider value based on mouse position relative to our position and size
	if(getDown())
	{
		mousePos = cursor -> getPos();
		pos = getPos();
		size = getSize();

		// our new value factor is the mouse x offset from our origin, divided by our width
		xFactor = (mousePos.x - pos.x) / size.x;
		xFactor = clamp(xFactor, 0.0f, 1.0f);
		value = (float)min + (float)(max - min) * xFactor;
	}

	// if user clicks slider, invoke event handler if one is assigned
	if(getDown() && form && onChange)
	{
		(form ->* onChange)(value);
	}
}

void Slider::render()
{
	vec4 backgroundColor = BACKGROUND_NORMAL_COLOR;
	vec4 sliderColor = SLIDER_NORMAL_COLOR;

	vec2 size = getSize();
	vec2 textTweak = (size / 2.0f) + vec2(0.0f, 4.0f);
	vec2 pos = getPos();
	float fillFactor = (float)(value - min) / (float)(max - min);

	stringstream ss;

	// adjust colour based on cursor action
	if(getDown())
	{
		sliderColor = SLIDER_DOWN_COLOR;
		backgroundColor = BACKGROUND_DOWN_COLOR;
	}
	else if(getHover())
	{
		sliderColor = SLIDER_HOVER_COLOR;
		backgroundColor = BACKGROUND_DOWN_COLOR;
	}

	// render background and slider
	Quad::getInstance() -> render(pos, size, backgroundColor);
	Quad::getInstance() -> render(pos, vec2(size.x * fillFactor, size.y), sliderColor);

	// render text value on top
	ss << value << text;
	Text::getInstance() -> render(ss.str(), pos + textTweak, TEXT_COLOR, Text::ALIGN_CENTER);
}
