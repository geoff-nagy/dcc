#pragma once

#include "gui/widget.h"

#include "glm/glm.hpp"

class Cursor;

class Clickable : public Widget
{
public:
	Clickable();
	~Clickable();

	void update(Cursor *cursor, float dt);
	void render() = 0;

	void setKeys(int keyboardKey, int keyboardModifier);

	bool getHover();
	bool getDown();
	bool getClick();
	bool getRelease();

	void setEnabled(bool enabled);
	bool getEnabled();

private:
	bool getKeyboardCommandDown();

	int keyboardKey;
	int keyboardModifier;

	bool hover;
	bool down;
	bool click;
	bool release;

	bool enabled;
};
