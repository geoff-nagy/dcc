#include "gui/clickable.h"
#include "gui/cursor.h"

#include "core/application.h"

#include "util/log.h"

#include "glm/glm.hpp"
using namespace glm;

Clickable::Clickable()
{
	// no cursor action yet
	hover = false;
	down = false;
	click = false;
	release = false;

	// no assigned keyboard keys
	keyboardKey = -1;
	keyboardModifier = -1;

	// enabled
	setEnabled(true);
}

Clickable::~Clickable() { }

void Clickable::update(Cursor *cursor, float dt)
{
	bool keyboardCommandDown = getKeyboardCommandDown();

	// base widget update routine
	Widget::update(cursor, dt);

	// determine state based on cursor position and button state
	if(cursor -> isInArea(getPos(), getSize()))
	{
		hover = true;

		// cursor press
		release = false;
		if(enabled)
		{
			if(cursor -> isLeftButtonDown())
			{
				// first press
				if(!down)
				{
					click = true;
				}
				down = true;
			}
			else		// cursor release
			{
				// first release
				if(down)
				{
					release = true;
				}
				down = false;
			}
		}
	}
	else if(keyboardCommandDown)
	{
		if(enabled)
		{
			release = !down;
			down = true;
		}
	}
	else
	{
		// mouse not in area, hover inactive
		hover = false;
		down = false;
		click = false;
		release = false;
	}

	// enable tool-tip if hover is active
	setToolTipActive(hover);
}

void Clickable::setKeys(int keyboardKey, int keyboardModifier)
{
	this -> keyboardKey = keyboardKey;
	this -> keyboardModifier = keyboardModifier;
}

bool Clickable::getHover()
{
	return hover;
}

bool Clickable::getDown()
{
	return down;
}

bool Clickable::getClick()
{
	return click;
}

bool Clickable::getRelease()
{
	return release;
}

void Clickable::setEnabled(bool enabled)
{
	this -> enabled = enabled;
}

bool Clickable::getEnabled()
{
	return enabled;
}

bool Clickable::getKeyboardCommandDown()
{
	Application *app = Application::getInstance();
	bool result = false;

	// we only care if we have an assigned keyboard command
	if(keyboardKey >= 0)
	{
		result = app -> getKeyDown(keyboardKey);
		if(result && keyboardModifier >= 0)
		{
			result &= app -> getKeyDown(keyboardModifier);
		}
		else
		{
			result &= (!app -> getKeyDown(GLFW_KEY_LEFT_SHIFT) && !app -> getKeyDown(GLFW_KEY_LEFT_ALT) && !app -> getKeyDown(GLFW_KEY_LEFT_CONTROL));
		}
	}

	return result;
}
