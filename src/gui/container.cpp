#include "gui/container.h"
#include "gui/radiobutton.h"

#include "rendering/quad.h"
#include "rendering/text.h"

#include "libdrawtext/drawtext.h"

#include "glm/glm.hpp"
using namespace glm;

#include <vector>
using namespace std;

Container::Container()
{
	resetPadding(0);
}

Container::~Container()
{
	vector<Widget*>::iterator i;
	for(i = widgets.begin(); i != widgets.end(); ++ i)
	{
		delete *i;
	}
}

void Container::resetPlacement()
{
	widgetPos = vec2(padding);
	newLine = true;
	lineHeight = 0;
}

void Container::resetPadding(int padding)
{
	setPadding(padding);
	resetPlacement();
}

void Container::setWidgetPos(const glm::vec2 &widgetPos)
{
	this -> widgetPos = widgetPos;
}

void Container::setPadding(int padding)
{
	this -> padding = padding;
}

void Container::padHorizontal(int padding)
{
	widgetPos += vec2(padding, 0.0f);
}

void Container::padVertical(int padding)
{
	widgetPos += vec2(0.0f, padding);
}

void Container::addWidget(Widget *widget, const string &toolTip)
{
	// advance to next line if the next widget doesn't fit on the current (non-empty) one
	if(!newLine && widgetPos.x + widget -> getSize().x > getSize().x)
	{
		widgetPos = vec2(padding, widgetPos.y + lineHeight + padding);
		newLine = true;
		lineHeight = 0;
	}

	// place the widget, record the line as non-empty, and record the height of the line so far
	addWidget(widget, widgetPos, toolTip);
	widgetPos += vec2(widget -> getSize().x + padding, 0.0f);
	newLine = false;
	lineHeight = glm::max(lineHeight, (int)widget -> getSize().y);
}

void Container::addWidget(Widget *widget, const vec2 &pos, const string &toolTip)
{
	RadioButton *rdo;

	// set the widget's position and add to the master list for this container
	widget -> setPos(getPos() + pos);
	widget -> setToolTip(toolTip);
	widgets.push_back(widget);

	// if this is a radio button, add to the current group (whatever that happens to be)
	rdo = dynamic_cast<RadioButton*>(widget);
	if(rdo && !rdo -> isSingle())
	{
		// add first if not already there
		if(radioButtons.size() == 0)
		{
			radioButtons.push_back(vector<RadioButton*>());
		}

		// add radio button to current group
		radioButtons[radioButtons.size() - 1].push_back(rdo);
		if(radioButtons[radioButtons.size() - 1].size() == 1)
		{
			rdo -> setSelected(true);
		}
	}
}

void Container::endRadioButtonGroup()
{
	radioButtons.push_back(vector<RadioButton*>());
}

void Container::update(Cursor *cursor, float dt)
{
	vector<Widget*>::iterator i;
	vector<vector<RadioButton*> >::iterator j;
	vector<RadioButton*>::iterator k;

	// handle widget updating
	for(i = widgets.begin(); i != widgets.end(); ++ i)
	{
		(*i) -> update(cursor, dt);
	}

	// if a radio button in a group is clicked, unselect all the others
	for(j = radioButtons.begin(); j != radioButtons.end(); ++ j)
	{
        for(k = j -> begin(); k != j -> end(); ++ k)
        {
			if((*k) -> getRelease())
			{
				unselectAllRadioButtons(*j);
				(*k) -> setSelected(true);
			}
        }
	}
}

void Container::render()
{
	Widget *curr;
	vector<Widget*>::iterator i;
	vector<Widget*> toolTips;

	for(i = widgets.begin(); i != widgets.end(); ++ i)
	{
		curr = *i;
		if(curr -> getVisible())
		{
			curr -> render();

			// save tool tip for later if enabled
			if(curr -> getToolTipActive())
			{
				toolTips.push_back(curr);
			}
		}
	}

	// render tool tips on top of the container
	for(i = toolTips.begin(); i != toolTips.end(); ++ i)
	{
		showToolTip(*i);
	}
}

void Container::showToolTip(Widget *widget)
{
	const vec4 BACKGROUND_COLOR(0.15f, 0.15f, 0.15f, 0.85f);
	const vec4 TEXT_COLOR(0.9f, 0.9f, 0.9f, 1.0f);

	const float TOOL_TIP_HEIGHT = 26.0f;
	const vec2 BACKGROUND_SIZE_TWEAK(16.0f, 0.0f);

	const vec2 TEXT_POS_TWEAK(18.0f, 17.0f);
	const vec2 BACKGROUND_POS_TWEAK(10.0f, 0.0f);

	vec2 toolTipSize;
	vec2 pos;

	string toolTip = widget -> getToolTip();

	// don't bother rendering if the tool tip text is blank
	if(toolTip.length())
	{
		// compute base tool tip position, as well as size based on rendered string size
		pos = vec2(getPos().x + getSize().x, widget -> getPos().y);
		toolTipSize = vec2(dtx_string_width(toolTip.c_str()), TOOL_TIP_HEIGHT);

		// render background quad and foreground text
		Quad::getInstance() -> render(pos + BACKGROUND_POS_TWEAK, toolTipSize + BACKGROUND_SIZE_TWEAK, BACKGROUND_COLOR);
		Text::getInstance() -> render(toolTip, pos + TEXT_POS_TWEAK, TEXT_COLOR, Text::ALIGN_LEFT);
	}
}

void Container::unselectAllRadioButtons(vector<RadioButton*> &radios)
{
	vector<RadioButton*>::iterator i;

	for(i = radios.begin(); i != radios.end(); ++ i)
	{
		(*i) -> setSelected(false);
	}
}
