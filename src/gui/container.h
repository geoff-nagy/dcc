#pragma once

#include "gui/widget.h"

#include "glm/glm.hpp"

#include <vector>

class Cursor;
class RadioButton;

class Container : public Widget
{
public:
	Container();
	~Container();

	void setWidgetPos(const glm::vec2 &widgetPos);
	void resetPlacement();
	void resetPadding(int padding);
	void setPadding(int padding);
	void padHorizontal(int padding);
	void padVertical(int padding);

	void addWidget(Widget *widget, const std::string &toolTip = "");
	void addWidget(Widget *widget, const glm::vec2 &pos, const std::string &toolTip = "");
	void endRadioButtonGroup();

	void update(Cursor *cursor, float dt);
	void render();

private:
	std::vector<Widget*> widgets;							// list of all widgets in this container
	std::vector<std::vector<RadioButton*> > radioButtons;	// used to ensure that only one radio button per group can be selected

	int padding;											// refers to inner margin and minimum spacing between widgets
	glm::vec2 widgetPos;									// the origin of where the next widget will be placed
	bool newLine;											// widgets are placed horizontally, and then vertically---so is this a fresh line of widgets?
	int lineHeight;											// minimum length we need to skip when advancing to the next line of widgets for placements

	void showToolTip(Widget *widget);
	void unselectAllRadioButtons(std::vector<RadioButton*> &radios);
};
