#pragma once

#include "gui/clickable.h"
#include "gui/form.h"

#include "glm/glm.hpp"

#include <string>

class Button : public Clickable
{
public:
	Button(const std::string &text, const glm::vec2 &size, Form *form, void (Form::*onRelease)(), int keyboardKey = -1, int keyboardModifier = -1);
	~Button();

	void setText(const std::string &text);

	void update(Cursor *cursor, float dt);
	void render();

private:
	static const glm::vec4 BUTTON_NORMAL_COLOR;
	static const glm::vec4 BUTTON_HOVER_COLOR;
	static const glm::vec4 BUTTON_DOWN_COLOR;
	static const glm::vec4 TEXT_COLOR;

	static const glm::vec4 BUTTON_DISABLED_COLOR;
	static const glm::vec4 TEXT_DISABLED_COLOR;

	std::string text;
	Form *form;
	void (Form::*onRelease)();
	int keyboardKey;
	int keyboardModifier;
};
