#pragma once

#include "glm/glm.hpp"

#include <string>

class Cursor;

class Widget
{
public:
	Widget();
	virtual ~Widget() = 0;

	void setPos(const glm::vec2 &pos);
	glm::vec2 getPos();

	void setSize(const glm::vec2 &size);
	glm::vec2 getSize();

	void setToolTip(const std::string &toolTip);
	std::string getToolTip();

	void setToolTipActive(bool toolTipActive);
	bool getToolTipActive();

	void setVisible(bool visible);
	bool getVisible();

	virtual void update(Cursor *cursor, float dt);
	virtual void render();

private:
	glm::vec2 pos;
	glm::vec2 size;
	std::string toolTip;
	bool toolTipActive;
	bool visible;
};
