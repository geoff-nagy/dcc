#include "gui/cursor.h"

#include "GL/glew.h"
#include "GLFW/glfw3.h"

#include "glm/glm.hpp"
using namespace glm;

Cursor::Cursor(GLFWwindow *window)
{
	this -> window = window;

	pos = vec2(0.0f);
	oldPos = vec2(0.0f);
	vel = vec2(0.0f);
	leftButtonDown = false;
	middleButtonDown = false;
}

Cursor::~Cursor() { }

void Cursor::update()
{
	// get cursor position from GLFW window
	double x, y;
	glfwGetCursorPos(window, &x, &y);
	oldPos = pos;
	pos = vec2(x, y);
	vel = pos - oldPos;

	// get cursor mouse button status
    leftButtonDown = (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS);
    middleButtonDown = (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE) == GLFW_PRESS);
}

vec2 Cursor::getPos()
{
	return pos;
}

vec2 Cursor::getVel()
{
	return vel;
}

bool Cursor::isInArea(const vec2 &areaPos, const vec2 &areaSize)
{
	return pos.x >= areaPos.x &&
		   pos.y >= areaPos.y &&
		   pos.x <= (areaPos.x + areaSize.x) &&
		   pos.y <= (areaPos.y + areaSize.y);
}

bool Cursor::isLeftButtonDown()
{
	return leftButtonDown;
}

bool Cursor::isMiddleButtonDown()
{
	return middleButtonDown;
}
