#include "gui/histogram.h"

#include "rendering/quad.h"
#include "rendering/text.h"

#include "util/log.h"

#include "glm/glm.hpp"
using namespace glm;

#include <vector>
#include <sstream>
#include <iostream>
#include <iomanip>
using namespace std;

Histogram::Histogram(const vec2 &size)
{
    setSize(size);
}

Histogram::~Histogram() { }

void Histogram::setData(vector<float> &values, float binSize)
{
	vector<float>::iterator i;

	// reset
	minValue = FLT_MAX;
	maxValue = -FLT_MAX;

	// compute data range
	maxFrequency = 0;
	for(i = values.begin(); i != values.end(); ++i)
	{
		minValue = glm::min(*i, minValue);
		maxValue = glm::max(*i, maxValue);
	}

	// bin everything
	zoomFactor = 0.0f;
	range = maxValue - minValue;
	this -> binSize = binSize;
	this -> values = values;
	rebuildInRange(minValue, maxValue);
}

void Histogram::update(Cursor *cursor, float dt)
{
	// nothing?
}

void Histogram::render()
{
	// colouring
	const vec4 BACKGROUND_COLOR(0.15f, 0.15f, 0.15f, 0.75f);
	const vec4 USABLE_AREA_COLOR(0.05f, 0.05f, 0.05f, 0.75f);
	const vec4 TEXT_COLOR(1.0f, 1.0f, 1.0f, 1.0f);
	const vec4 BIN_COLOR(0.3f, 0.3f, 0.3f, 0.8f);

	// spacing
	const vec2 POS = getPos();
	const vec2 SIZE = getSize();
	const vec2 MARGIN(40.0f, 30.0f);
	const vec2 USABLE_POS(getPos() + MARGIN);
	const vec2 USABLE_SIZE(SIZE - (MARGIN * 2.0f));
	const vec2 USABLE_AREA_POS_TWEAK(0.0f, -20.0f);
	const vec2 USABLE_AREA_SIZE_TWEAK(0.0f, 20.0f);
	const vec2 Y_AXIS_TEXT_START(10.0f, MARGIN.y + USABLE_SIZE.y);
	const vec2 Y_AXIS_TEXT_END(10.0f, MARGIN.y - 3.0f);
	const vec2 BIN_DELIMITER_TWEAK(MARGIN.x, MARGIN.y + 13.0f);
	const vec2 BIN_SPACING(USABLE_SIZE.x / (float)(binValues.size() - 1), 0.0f);
	const vec2 BIN_SIZE((USABLE_SIZE.x / (float)frequencies.size()) - 2, USABLE_SIZE.y);
	const bool SKIP_COLS = frequencies.size() > 60;

	vec2 binPos;
	vec2 binSize;

	stringstream ss;
	vector<float>::iterator i;
	vector<int>::iterator j;
	int k = 0;

	// print background and graph area
	Quad::getInstance() -> render(POS, SIZE, BACKGROUND_COLOR);
	Quad::getInstance() -> render(USABLE_POS + USABLE_AREA_POS_TWEAK, USABLE_SIZE + USABLE_AREA_SIZE_TWEAK, USABLE_AREA_COLOR);

	if(frequencies.size() > 0)
	{
		// print frequencies
		Text::getInstance() -> render("0", POS + Y_AXIS_TEXT_START, TEXT_COLOR);
		ss.str("");
		ss << maxFrequency;
		Text::getInstance() -> render(ss.str(), POS + Y_AXIS_TEXT_END, TEXT_COLOR);

		// render bin delimiters
		binPos = POS + vec2(0.0f, USABLE_SIZE.y) + BIN_DELIMITER_TWEAK;
		for(i = binValues.begin(), k = 0; i != binValues.end(); ++ i, ++ k)
		{
			// print text of bin delimiters
			if(!SKIP_COLS || k % 5 == 0)
			{
				ss.str("");
				ss << setprecision(3) << ((abs(*i - 0.0f) < 0.001f) ? 0.0f : *i);					// annnoying
				Text::getInstance() -> render(ss.str(), binPos, TEXT_COLOR);
			}

			// advance
			binPos += BIN_SPACING;
		}

		// render frequency values
		binPos = POS + vec2(0.0f, USABLE_SIZE.y) + MARGIN;
		for(j = frequencies.begin(), k = 0; j != frequencies.end(); ++ j, ++ k)
		{
			// compute physical size of bin
			binSize = vec2(BIN_SIZE.x, -BIN_SIZE.y * ((float)*j / (maxFrequency + 1)));

			// render the bin as a quad, and the frequency on top as text
			Quad::getInstance() -> render(binPos, binSize, BIN_COLOR);
			if(*j && !SKIP_COLS)
			{
				ss.str("");
				ss << *j;
				Text::getInstance() -> render(ss.str(), binPos + vec2(binSize.x / 2.0f, binSize.y - 6.0f), TEXT_COLOR, Text::ALIGN_CENTER);
			}

			// advance
			binPos += BIN_SPACING;
		}
	}
	else
	{
		Text::getInstance() -> render("[No data]", POS + (SIZE / 2.0f), TEXT_COLOR, Text::ALIGN_CENTER);
	}
}

void Histogram::rebuildInRange(float newMin, float newMax)
{
	int numBins;
	int j;
	float binValue;
	vector<float>::iterator i;

	// reset
	binValues.clear();
	frequencies.clear();

	// sort data into bins
	numBins = ceil((newMax - newMin) / binSize) + 1;
	binValue = floor(newMin / binSize) * binSize;
	for(j = 0; j < numBins; ++ j)
	{
		frequencies.push_back(0);
		for(i = values.begin(); i != values.end(); ++ i)
		{
			if(*i >= binValue && *i < binValue + binSize)
			{
				frequencies[j] ++;
				maxFrequency = glm::max(frequencies[j], maxFrequency);
			}
		}

		// next bin
		binValues.push_back(binValue);
		binValue += binSize;
	}

	// add last delimiting bin value for completeness
	binValues.push_back(binValue);

	// sanity check
	/*
	unsigned int m;
	for(m = 0; m < frequencies.size(); ++ m)
	{
		cout << "[" << binValues[m] << " : " << binValues[m + 1] << "]:  ";
		cout << frequencies[m] << endl;
	}*/
}

void Histogram::zoomIn(const glm::vec2 &cursorPos)
{
	const vec2 topLeft = getPos();
	const vec2 size = getSize();
	const vec2 bottomRight = topLeft + size;

	float cursorOffset;
	float newMin;
	float newMax;
	float minScaleDist;
	float maxScaleDist;

	// make sure mouse is in histogram area
	if(cursorPos.x >= topLeft.x &&
	   cursorPos.y >= topLeft.y &&
	   cursorPos.x <= bottomRight.x &&
	   cursorPos.y <= bottomRight.y)
	{
		if(zoomFactor < 0.9f)
		{
			zoomFactor = glm::min(0.9f, zoomFactor + 0.1f);

			// compute area to zoom to based on mouse position inside the widget
			cursorOffset = (cursorPos.x - topLeft.x) / size.x;
			minScaleDist = range * cursorOffset;
			maxScaleDist = range * (1.0f - cursorOffset);

			// compute new min and max values to display
			newMin = minValue + (zoomFactor * minScaleDist);
			newMax = maxValue - (zoomFactor * maxScaleDist);
			rebuildInRange(newMin, newMax);
		}
	}
}

void Histogram::zoomOut(const glm::vec2 &cursorPos)
{
	const vec2 topLeft = getPos();
	const vec2 size = getSize();
	const vec2 bottomRight = topLeft + size;

	float cursorOffset;
	float newMin;
	float newMax;
	float minScaleDist;
	float maxScaleDist;

	// make sure mouse is in histogram area
	if(cursorPos.x >= topLeft.x &&
	   cursorPos.y >= topLeft.y &&
	   cursorPos.x <= bottomRight.x &&
	   cursorPos.y <= bottomRight.y)
	{
		zoomFactor = glm::max(0.0f, zoomFactor - 0.1f);

		// compute area to zoom to based on mouse position inside the widget
		cursorOffset = (cursorPos.x - topLeft.x) / size.x;
		minScaleDist = range * cursorOffset;
		maxScaleDist = range * (1.0f - cursorOffset);

		// compute new min and max values to display
		newMin = minValue + (zoomFactor * minScaleDist);
		newMax = maxValue - (zoomFactor * maxScaleDist);
		rebuildInRange(newMin, newMax);
	}
}
