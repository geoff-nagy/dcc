#pragma once

#include "gui/container.h"

#include "glm/glm.hpp"

class Cursor;

class Panel : public Container
{
public:
	Panel(const glm::vec2 &size, bool background = true);
	~Panel();

	void setVisible(bool visible);
	bool getVisible();

	void update(Cursor *cursor, float dt);
	void render();

private:
	static const glm::vec4 BACKGROUND_COLOR;

	bool background;
	bool visible;
};
