#include "gui/panel.h"

#include "rendering/quad.h"

#include "util/log.h"

#include "glm/glm.hpp"
using namespace glm;

#include <string>
using namespace std;

const vec4 Panel::BACKGROUND_COLOR(0.15f, 0.15f, 0.15f, 0.85f);

Panel::Panel(const vec2 &size, bool background)
	: Container()
{
	setSize(size);
	this -> background = background;

	visible = true;
}

Panel::~Panel()
{
	// nothing
}

void Panel::setVisible(bool visible)
{
	this -> visible = visible;
}

bool Panel::getVisible()
{
	return visible;
}

void Panel::update(Cursor *cursor, float dt)
{
	if(getVisible())
	{
		Container::update(cursor, dt);
	}
}

void Panel::render()
{
	// don't do anything if not visible
	if(getVisible())
	{
		// optionally render background
		if(background)
		{
			Quad::getInstance() -> render(getPos(), getSize(), BACKGROUND_COLOR);
		}

		// render contained elements
		Container::render();
	}
}
