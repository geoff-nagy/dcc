#pragma once

#include "gui/widget.h"

#include "GL/glew.h"

#include "glm/glm.hpp"

class PictureBox : public Widget
{
public:
	PictureBox(GLuint texture, const glm::vec2 &size);
	~PictureBox();

	void update(Cursor *cursor, float dt);
	void render();

private:
	GLuint texture;
};
