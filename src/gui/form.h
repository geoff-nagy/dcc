#pragma once

#include "gui/container.h"

#include "glm/glm.hpp"

class Cursor;

class Form : public Container
{
public:
	Form(const glm::vec2 &size);
	virtual ~Form() = 0;

	virtual void initForm() = 0;

	virtual void update(Cursor *cursor, float dt);
	virtual void render();

	typedef void (Form::*ButtonPressHandler)();
	typedef void (Form::*SliderChangeHandler)(int);
};
