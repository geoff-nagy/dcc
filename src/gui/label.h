#pragma once

#include "gui/widget.h"

#include "rendering/text.h"

#include "glm/glm.hpp"

#include <string>

class Label : public Widget
{
public:
	Label(const std::string &text, bool background = false, Text::TextAlignment = Text::ALIGN_LEFT);
	Label(const std::string &text, const glm::vec2 &size, bool background = false, Text::TextAlignment = Text::ALIGN_LEFT);
	~Label();

	void setText(const std::string &text);

	void render();

private:
	static const glm::vec4 TEXT_COLOR;
	static const glm::vec4 SHADOW_COLOR;
	static const glm::vec4 BACKGROUND_COLOR;

	std::string text;
	bool background;
	Text::TextAlignment textAlign;
};
