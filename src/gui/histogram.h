#pragma once

#include "gui/widget.h"

#include "glm/glm.hpp"

#include <vector>

class Cursor;

class Histogram : public Widget
{
public:
	Histogram(const glm::vec2 &size);
	~Histogram();

	void setData(std::vector<float> &values, float binSize);

	void update(Cursor *cursor, float dt);
	void render();

	void zoomIn(const glm::vec2 &cursorPos);
	void zoomOut(const glm::vec2 &cursorPos);

private:
	void rebuildInRange(float minRange, float maxRange);

	float minValue;
	float maxValue;
	float range;
	float zoomFactor;
	float binSize;

	std::vector<float> values;
	std::vector<float> binValues;
	std::vector<int> frequencies;
	int maxFrequency;
};
