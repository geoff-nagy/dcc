#include "gui/picturebox.h"

#include "rendering/quad.h"

#include "glm/glm.hpp"
using namespace glm;

#include <string>
using namespace std;

PictureBox::PictureBox(GLuint texture, const vec2 &size)
{
	this -> texture = texture;
	setSize(size);
}

PictureBox::~PictureBox()
{
	// nothing
}

void PictureBox::update(Cursor *cursor, float dt)
{
	// nothing
}

void PictureBox::render()
{
	Quad::getInstance() -> render(getPos(), getSize(), vec4(1.0f), texture);
}
