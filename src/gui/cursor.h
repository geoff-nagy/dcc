#pragma once

#include "glm/glm.hpp"

struct GLFWwindow;

class Cursor
{
public:
	Cursor(GLFWwindow *window);
	~Cursor();

	void update();

	glm::vec2 getPos();
	glm::vec2 getVel();

	bool isInArea(const glm::vec2 &areaPos, const glm::vec2 &areaSize);
	bool isLeftButtonDown();
	bool isMiddleButtonDown();

private:
	GLFWwindow *window;
	glm::vec2 pos;
	glm::vec2 oldPos;
	glm::vec2 vel;
	bool leftButtonDown;
	bool middleButtonDown;
};
