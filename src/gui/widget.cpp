#include "gui/widget.h"

#include "glm/glm.hpp"
using namespace glm;

#include <string>
using namespace std;

Widget::Widget()
{
	toolTipActive = false;
	visible = true;
}

Widget::~Widget() { }

void Widget::setPos(const vec2 &pos)
{
	this -> pos = pos;
}

vec2 Widget::getPos()
{
	return pos;
}

void Widget::setSize(const vec2 &size)
{
	this -> size = size;
}

vec2 Widget::getSize()
{
	return size;
}

void Widget::setToolTip(const string &toolTip)
{
	this -> toolTip = toolTip;
}

string Widget::getToolTip()
{
	return toolTip;
}

void Widget::setToolTipActive(bool toolTipActive)
{
	this -> toolTipActive = toolTipActive;
}

bool Widget::getToolTipActive()
{
	return toolTipActive;
}

void Widget::setVisible(bool visible)
{
	this -> visible = visible;
}

bool Widget::getVisible()
{
	return visible;
}

void Widget::update(Cursor *cursor, float dt) { }

void Widget::render() { }
