#pragma once

#include "gui/clickable.h"
#include "gui/form.h"

#include "glm/glm.hpp"

#include <string>

class Slider : public Clickable
{
public:
	Slider(const std::string &text, const glm::vec2 &size, Form *form, void (Form::*onChange)(int), int min, int max);
	Slider(const std::string &text, const glm::vec2 &size, Form *form, void (Form::*onChange)(int), int min, int max, int value);
	~Slider();

	void setValue(int value);
	int getValue();

	void update(Cursor *cursor, float dt);
	void render();

private:
	static const glm::vec4 BACKGROUND_NORMAL_COLOR;
	static const glm::vec4 BACKGROUND_HOVER_COLOR;
	static const glm::vec4 BACKGROUND_DOWN_COLOR;

	static const glm::vec4 SLIDER_NORMAL_COLOR;
	static const glm::vec4 SLIDER_HOVER_COLOR;
	static const glm::vec4 SLIDER_DOWN_COLOR;

	static const glm::vec4 TEXT_COLOR;

	std::string text;
	Form *form;
	void (Form::*onChange)(int);
	int min;
	int max;
	int value;

	void init(const std::string &text, const glm::vec2 &size, Form *form, void (Form::*onChange)(int), int min, int max, int value);
};

