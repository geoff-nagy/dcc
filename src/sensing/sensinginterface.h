#pragma once

#include "sensing/dronedetection.h"

#include "objects/marker.h"

#include "glm/glm.hpp"

#include <vector>

class SensingInterface
{
public:
	SensingInterface(const glm::vec2 &size);
	virtual ~SensingInterface() = 0;

	virtual void update(float dt) = 0;
	virtual void getDrones(std::vector<DroneDetection> &drones) = 0;
	virtual void getMarkers(std::vector<Marker> &markers);

	glm::vec2 getSize();

private:
	glm::vec2 size;
};
