#include "sensing/dronedetection.h"

DroneDetection::DroneDetection()
{
	name = "";
	id = 0;
	dir = 0.0f;
	dist = 0.0f;
	age = 0.0f;
	dirty = false;
	lastDetectionTime = 0.0;
	detectionFail = false;
}

bool DroneDetection::operator < (const DroneDetection &other) const
{
	return id < other.id;
}

bool DroneDetection::operator == (const DroneDetection &other) const
{
	return id == other.id;
}

bool DroneDetection::operator != (const DroneDetection &other) const
{
	return id != other.id;
}

DroneDetection DroneDetection::operator + (const DroneDetection &other)
{
	DroneDetection result = *this;
	result.pos += other.pos;
	result.oldPos += other.oldPos;
	result.velocity += other.velocity;
	result.dir += other.dir;
	return result;
}

DroneDetection DroneDetection::operator / (float val)
{
	DroneDetection result = *this;
	result.pos /= val;
	result.oldPos /= val;
	result.velocity /= val;
	result.dir /= val;
	return result;
}
