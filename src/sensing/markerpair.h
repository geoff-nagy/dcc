#pragma once

#include "glm/glm.hpp"

class Marker;

class MarkerPair
{
public:
	MarkerPair();
	MarkerPair(Marker *marker1, Marker* marker2);

	Marker *marker1;
	Marker *marker2;
	glm::vec3 vec;			// vector describing the edge between them
	float dist;				// distance between markers in meters
};
