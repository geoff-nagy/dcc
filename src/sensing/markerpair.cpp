#include "sensing/markerpair.h"

#include "objects/marker.h"

#include "glm/glm.hpp"
using namespace glm;

MarkerPair::MarkerPair()
	: dist(0.0f)
{ }

MarkerPair::MarkerPair(Marker *marker1, Marker *marker2)
{
	this -> marker1 = marker1;
	this -> marker2 = marker2;
	vec = marker2 -> pos - marker1 -> pos;
	dist = length(vec);
}
