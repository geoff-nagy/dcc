#include "sensing/magicsimulatorsensinginterface.h"
#include "sensing/dronedetection.h"

#include "objects/simulateddrone.h"
#include "objects/simulateddroneswarm.h"

#include "util/log.h"

#include "glm/glm.hpp"
#include "glm/gtc/random.hpp"
using namespace glm;

#include <stdint.h>
#include <vector>
#include <map>
using namespace std;

MagicSimulatorSensingInterface::MagicSimulatorSensingInterface(const vec2 &size, SimulatedDroneSwarm *droneSwarm)
	: SensingInterface(size), droneSwarm(droneSwarm)
{ }

MagicSimulatorSensingInterface::~MagicSimulatorSensingInterface() { }

void MagicSimulatorSensingInterface::update(float dt)
{
	DroneDetection detection;
	vector<SimulatedDrone*>::iterator i;
	SimulatedDrone *curr;

	// grab the drone positions from the simulated world
	droneDetections.clear();
	for(i = droneSwarm -> getDronesBegin(); i != droneSwarm -> getDronesEnd(); ++ i)
	{
		curr = *i;
		detection.id = curr -> getID();
		detection.pos = curr -> getPos();
		detection.velocity = (curr -> getPos() - curr -> getOldPos()) / dt;
		detection.side = curr -> getSide();
		detection.forward = curr -> getForward();
		detection.up = curr -> getUp();
		droneDetections[detection.id] = detection;
	}
}

void MagicSimulatorSensingInterface::getDrones(vector<DroneDetection> &droneList)
{
	map<uint32_t, DroneDetection>::iterator i;

	droneList.clear();
	for(i = droneDetections.begin(); i != droneDetections.end(); ++ i)
	{
		droneList.push_back(i -> second);
	}
}

void MagicSimulatorSensingInterface::getMarkers(vector<vec3> &markerList) { }
