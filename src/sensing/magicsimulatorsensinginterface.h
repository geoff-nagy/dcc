#pragma once

#include "sensing/sensinginterface.h"
#include "sensing/dronedetection.h"

#include <stdint.h>
#include <vector>
#include <map>

class SimulatedDroneSwarm;

// this is a straight-up magic sensor that detects all the drones
class MagicSimulatorSensingInterface : public SensingInterface
{
public:
	MagicSimulatorSensingInterface(const glm::vec2 &size, SimulatedDroneSwarm *droneSwarm);
	~MagicSimulatorSensingInterface();

	virtual void update(float dt);
	virtual void getDrones(std::vector<DroneDetection> &poses);
	virtual void getMarkers(std::vector<glm::vec3> &markers);

private:
	SimulatedDroneSwarm *droneSwarm;
	std::map<uint32_t, DroneDetection> droneDetections;
};
