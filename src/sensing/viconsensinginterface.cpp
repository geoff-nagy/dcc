#include "sensing/viconsensinginterface.h"
#include "sensing/resolvemarkers.h"
#include "sensing/dronedetection.h"
#include "sensing/resolvemarkers.h"
#include "sensing/trackingmatcher.h"

#include "util/log.h"

#include "glm/glm.hpp"
#include "glm/gtc/random.hpp"
using namespace glm;

#include "ViconDriver/Client.h"
using namespace ViconDataStreamSDK::CPP;

#include <sstream>
#include <time.h>
using namespace std;

const float ViconSensingInterface::MM_TO_METERS = 0.001f;

ViconSensingInterface::ViconSensingInterface(const vec2 &size, vector<int> &expectedIDs)
	: SensingInterface(size), expectedIDs(expectedIDs)
{
	const float TRACKING_SEARCH_RADIUS = 0.5f;//1000.1f;
	const float MAX_DETECTION_AGE_TO_FAIL = 4.0f;//300.0f;

	// initialize Vicon interface
	client = new Client();
	//connect("192.168.1.146", 801);
	connect("vicon.autolab", 801);

	// build our tracking matcher object that tracks identical drone marker configurations over time
	trackingMatcher = new TrackingMatcher(TRACKING_SEARCH_RADIUS, MAX_DETECTION_AGE_TO_FAIL);
	firstFrame = true;
}

ViconSensingInterface::~ViconSensingInterface()
{
	if(isConnected())
	{
		disconnect();
	}
	delete client;

	delete trackingMatcher;
}

void ViconSensingInterface::update(float dt)
{
	uint32_t frameResult;

	// clear list of markers and drone detections
	markers.clear();
	drones.clear();

    // get a new frame; notify the user if we failed
    //if(linearRand(1, 50) > 1)
    {
		frameResult = client -> GetFrame().Result;
		if(frameResult != Result::Success)
		{
			LOG_INFO("ViconSensingInterface::update() failed to retrieve a new frame; result was " << frameResult);
		}
		else
		{
			// perform marker detection, and resolve those into actual drones
			//if(linearRand(1, 50) > 15)
			{
				detectMarkers();
				detectDrones();
			}
		}
    }

    // perform tracking over time; we do this regardless of any Vicon input so we can age detections accordingly
    trackAndMatchDrones(dt);
}

void ViconSensingInterface::getDrones(vector<DroneDetection> &detections)
{
	detections.clear();
	detections.assign(drones.begin(), drones.end());
}

void ViconSensingInterface::getMarkers(std::vector<Marker> &markerList)
{
	markerList.clear();
	markerList.assign(markers.begin(), markers.end());
}

void ViconSensingInterface::detectMarkers()
{
    markers = getUnlabeledMarkers();
}

void ViconSensingInterface::detectDrones()
{
	resolveUBeeMarkers(markers, drones);
}

void ViconSensingInterface::trackAndMatchDrones(float dt)
{
	// if this is the first frame we've seen, initialize the tracking matcher first
	if(firstFrame)
	{
		// find at least one drone before we promise the system that everything is visible
		if(markers.size() >= 3)
		{
			if(trackingMatcher -> promise(drones, expectedIDs))
			{
				// promise was successfully accepted; we can now perform tracking over time
				firstFrame = false;
			}
			else
			{
				// promise failed; erase drone detections
				drones.clear();
			}
		}
	}
	else
	{
		trackingMatcher -> match(drones, dt);		// perform tracking over time
		trackingMatcher -> retrieve(drones);		// erase recent drone detection list and replace with tracked-matched ones
	}
}

bool ViconSensingInterface::connect(const string &host, unsigned int port)
{
	stringstream ss;
	bool result = false;

    // attempt to connect to the Vicon host
    ss << host << ":" << port;
    LOG_INFO("ViconSensingInterface::connect(): connecting to vicon @ " << ss.str() << "...");
    Output_Connect err = client -> Connect(ss.str());
    if(isConnected())
    {
		LOG_INFO("ViconSensingInterface::connect(): connection successful");
		client -> EnableSegmentData();
		client -> EnableUnlabeledMarkerData();
		client -> EnableMarkerData();
		client -> SetStreamMode(ViconDataStreamSDK::CPP::StreamMode::ClientPullPreFetch);
		//client -> SetStreamMode(ViconDataStreamSDK::CPP::StreamMode::ServerPush);
		//client -> SetStreamMode(ViconDataStreamSDK::CPP::StreamMode::ClientPull);

		// Set the global axes to match our coordinate system
		client -> SetAxisMapping(Direction::Backward, Direction::Up, Direction::Left);
		result = true;
    }
    else
    {
		LOG_INFO("ViconSensingInterface::connect(): could not connect");
		LOG_INFO("ViconSensingInterface::connect(): result code is: " << err.Result);
    }

	return result;
}

bool ViconSensingInterface::isConnected()
{
    return client -> IsConnected().Connected;
}

void ViconSensingInterface::disconnect()
{
    LOG_INFO("ViconSensingInterface::disconnect(): disconnecting from Vicon");
    client -> Disconnect();
}

vector<Marker> ViconSensingInterface::getUnlabeledMarkers()
{
    vector<Marker> result;
    vec3 pos;
    Marker marker;

    Output_GetUnlabeledMarkerGlobalTranslation unlabelled;
    Output_GetUnlabeledMarkerCount unlabelledMarkerCount;
    //unsigned int frame;// = client -> GetFrame().Result;
    unsigned int count;
    //unsigned int count = client -> GetUnlabeledMarkerCount().MarkerCount;
    //unsigned int code = client -> GetUnlabeledMarkerCount().Result;
    unsigned int i;

    // get unlabelled Vicon markers; notify the user if we failed or got zero markers
    unlabelledMarkerCount = client -> GetUnlabeledMarkerCount();
    count = unlabelledMarkerCount.MarkerCount;
    if(unlabelledMarkerCount.Result != Result::Success || count == 0)
    {
		LOG_INFO("ViconSensingInterface::getUnlabeledMarkers(): getUnlabelledMarkerCount() returned result of " << unlabelledMarkerCount.Result << " with marker count " << count);
    }

	// build a list of vectors containing the unlabelled markers we find
    for(i = 0; i < count; ++ i)
    {
		unlabelled = client -> GetUnlabeledMarkerGlobalTranslation(i);
		pos = vec3(unlabelled.Translation[0], unlabelled.Translation[1], unlabelled.Translation[2]) * MM_TO_METERS;
		marker.id = i;
		marker.pos = pos;
		marker.seen = false;
		marker.color = vec3(1.0f, 0.0f, 0.0f);
		result.push_back(marker);
		//result.push_back(vec3(-unlabelled.Translation[2], unlabelled.Translation[1], unlabelled.Translation[0]) * MM_TO_METERS);
    }

    return result;
}
