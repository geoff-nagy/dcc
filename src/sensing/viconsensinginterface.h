#pragma once

// modified version of Vicon drivers found at: https://github.com/rock-drivers/drivers-vicon
// changed to use the GLM library and otherwise remove some nasty dependencies and other things I don't like

#include "sensing/sensinginterface.h"

#include "objects/marker.h"

#include "ViconDriver/Client.h"

#include "glm/glm.hpp"

#include <vector>
#include <string>
#include <map>

class TrackingMatcher;

class ViconSensingInterface : public SensingInterface
{
public:
	ViconSensingInterface(const glm::vec2 &size, std::vector<int> &expectedIDs);
	~ViconSensingInterface();

	virtual void update(float dt);
	virtual void getDrones(std::vector<DroneDetection> &drones);
	virtual void getMarkers(std::vector<Marker> &markers);

	void registerDroneName(const std::string &name);

private:
	bool connect(const std::string &host, unsigned int port = 801);
	bool isConnected();

	void disconnect();

	void detectMarkers();
	void detectDrones();
	void trackAndMatchDrones(float dt);

	std::vector<Marker> getUnlabeledMarkers();

	static const float MM_TO_METERS;

	ViconDataStreamSDK::CPP::Client *client;

	std::vector<int> expectedIDs;
	std::vector<Marker> markers;
	std::vector<DroneDetection> drones;
	std::map<uint32_t, DroneDetection> droneDetections;

	TrackingMatcher *trackingMatcher;
	bool firstFrame;
};
