#pragma once

#include "glm/glm.hpp"

class Quad;

class SensorMap
{
public:
	SensorMap(const glm::vec2 &size, float resolution);
	~SensorMap();

	void clear();
	void add(const glm::vec2 &pos);

	void render(const glm::mat4 &viewProjection);

private:
	Quad *quad;

	glm::vec2 size;
	glm::vec2 halfSize;
	int numCellsX;
	int numCellsY;
	int numCellsTotal;
	int maxOccupancy;
	float resolution;
	int *cells;
};
