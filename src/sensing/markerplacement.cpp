#include "sensing/markerplacement.h"

#include "util/log.h"

#include "glm/glm.hpp"
using namespace glm;

#include <stdint.h>

void verifyUBeeFocalMarkerDistances()
{
    uint32_t i;
	float dist;

    // print focal marker positions and the center pos
    LOG_INFO("focal marker 1   : " << UBEE_FOCAL_MARKER_1.x << ", " << UBEE_FOCAL_MARKER_1.y << ", " << UBEE_FOCAL_MARKER_1.z);
    LOG_INFO("focal marker 2   : " << UBEE_FOCAL_MARKER_2.x << ", " << UBEE_FOCAL_MARKER_2.y << ", " << UBEE_FOCAL_MARKER_2.z);
	LOG_INFO("focal marker avg : " << UBEE_FOCAL_MARKER_AVG.x << ", " << UBEE_FOCAL_MARKER_AVG.y << ", " << UBEE_FOCAL_MARKER_AVG.z);

	// test distance from the focal center to each ID marker
    for(i = 0; i < NUM_UBEE_ID_MARKERS; ++ i)
    {
		dist = length(UBEE_FOCAL_MARKER_AVG - UBEE_ID_MARKERS[i]);
		LOG_INFO("ID marker " << i << " : " << UBEE_ID_MARKERS[i].x << ", " << UBEE_ID_MARKERS[i].y << ", " << UBEE_ID_MARKERS[i].z);
		LOG_INFO("    dist : " << dist);
    }
}
