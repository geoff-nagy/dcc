#include "sensing/trackingmatcher.h"
#include "sensing/dronedetection.h"
#include "sensing/resolvemarkers.h"

#include "util/log.h"

#include "glm/glm.hpp"
using namespace glm;

#include <vector>
#include <algorithm>
using namespace std;

// - - - private function prototypes - - - //

static bool compareLeftToRightAndTopToBottom(const DroneDetection &a, const DroneDetection &b, float cellSize);
static void swap(vector<DroneDetection> &detections, int a, int b);
static void sortInGrid(vector<DroneDetection> &detections, float cellSize);

// - - - TrackingMatcher implementation - - - //

TrackingMatcher::TrackingMatcher(float detectionSearchRadius, float maxAgeToFail)
	: detectionSearchRadius(detectionSearchRadius), maxAgeToFail(maxAgeToFail), currentTime(0.0)
{
	// [todo]: anything else?
}

TrackingMatcher::~TrackingMatcher()
{
	// [todo]: free any dynamically-allocated memory
}

bool TrackingMatcher::promise(const vector<DroneDetection> &detections, const vector<int> &ids)
{
	const int MARKER_CIRCULAR_BUFFER_SIZE = 2;

	vector<DroneDetection>::iterator i, j;
	vector<int>::const_iterator k;
	bool result = false;
	float dist;
	float minDist;

	// clear any previously promise()'d listings
	master.clear();

	// the whole idea of promise() is that the detections should match the IDs perfectly; if they don't, we can't proceed
	if(detections.size() == ids.size())
	{
		// build the master list of detections we've been promise()'d, and sort them in left-right, top-bottom fashion
		master.assign(detections.begin(), detections.end());

		// find minimum separation between detections
		minDist = FLT_MAX;
		for(i = master.begin(); i != master.end(); ++ i)
		{
			for(j = i + 1; j != master.end(); ++ j)
			{
				dist = length(i -> pos - j -> pos);
				if(dist < minDist)
				{
					minDist = dist;
				}
			}
		}
		minDist /= 2.0f;			// [note]: is this necessary?

		// now sort them on a grid from left-right, top-down fashion, with cell sizes equal to half that minimum distance
		LOG_INFO("[TRMT]: TrackingMatcher is sorting " << ids.size() << " promises using a cell size of " << minDist);
		sortInGrid(master, minDist);

		// assign IDs to the detections that we were promise()'d, and indicate order in which they were matched
		result = true;
		for(i = master.begin(), k = ids.begin(); i != master.end(); ++ i, ++ k)
		{
			// print match
			i -> id = *k;
			LOG_INFO("    ID " << i -> id << " was matched at pos (" << i -> pos.x << ", " << i -> pos.y << ", " << i -> pos.z << ")");

			// build circular buffers for filtering marker positions over time
			leftMarkerBuffer[i -> id] = CircularBuffer(MARKER_CIRCULAR_BUFFER_SIZE);
			rightMarkerBuffer[i -> id] = CircularBuffer(MARKER_CIRCULAR_BUFFER_SIZE);
			frontMarkerBuffer[i -> id] = CircularBuffer(MARKER_CIRCULAR_BUFFER_SIZE);
			posBuffer[i -> id] = CircularBuffer(4);
		}
	}
	else
	{
		LOG_INFO("[TRMT]: [WARNING]: " << ids.size() << " drones were promise()'d, but " << detections.size() << " detections were found");
	}

	return result;
}

void TrackingMatcher::match(vector<DroneDetection> &detections, float dt)
{
	vector<DroneDetection>::iterator i, j, closest;
	//vector<vec3> markers;
	//vector<Marker> markerObjs;
	//vector<DroneDetection> resolutions;
	float dist;
	float minDist;

	// increase time tracking; this is always called, so this will always increase
	currentTime += dt;

	// go through all drones we should be tracking
	for(i = master.begin(); i != master.end(); ++ i)
	{
		// is this drone still considered present?
		if(i -> age <= maxAgeToFail)
		{
			// find the closest new detection, within a certain limit
			minDist = FLT_MAX;
			closest = detections.end();

			// now process all detections we've seen
			for(j = detections.begin(); j != detections.end(); ++ j)
			{
				// skip detections that have been matched already
				if(!j -> dirty)
				{
					// look for the closest match so far, within our search radius
					dist = length(j -> pos - i -> pos);
					if(dist <= detectionSearchRadius && dist < minDist)
					{
						minDist = dist;
						closest = j;
					}
				}
			}

			// if we found a detection we think is the current drone we're trying to match
			if(closest != detections.end())
			{
				posBuffer[i -> id].addEntry(closest -> pos);
/*
				// add new detections to circular buffer of markers associated with this detection
                leftMarkerBuffer[i -> id].addEntry(closest -> leftMarker.pos);
                rightMarkerBuffer[i -> id].addEntry(closest -> rightMarker.pos);
                frontMarkerBuffer[i -> id].addEntry(closest -> frontMarker.pos);

				// average out marker positions using the circular buffer filter
				markers.clear();
                markers.push_back(leftMarkerBuffer[i -> id].getFilteredOutput());
                markers.push_back(rightMarkerBuffer[i -> id].getFilteredOutput());
                markers.push_back(frontMarkerBuffer[i -> id].getFilteredOutput());

                // build a list of three markers that we will want resolved into a single uBee detection later on
                markerObjs.clear();
                markerObjs.push_back(Marker(markers[0]));
				markerObjs.push_back(Marker(markers[1]));
				markerObjs.push_back(Marker(markers[2]));

                // resolve those averaged markers into a single uBee detection
                resolutions.clear();
                resolveUBeeMarkers(markerObjs, resolutions);*/

                // prevent hideous velocity errors
                if(i -> lastDetectionTime < 0.0000001)
                {
					i -> lastDetectionTime = currentTime - dt;
                }

                i -> oldPos = i -> pos;
                i -> pos = posBuffer[i -> id].getFilteredOutput();//closest -> pos;
                i -> velocity = (i -> pos - i -> oldPos) / (float)(currentTime - i -> lastDetectionTime);

                i -> forward = closest -> forward;
                i -> side = closest -> side;
                i -> up = closest -> up;
                i -> dir = closest -> dir;

                i -> age = 0.0f;
                i -> lastDetectionTime = currentTime;
                i -> detectionFail = false;

                closest -> dirty = true;

                // make sure we only got one detection back
                //if(resolutions.size() == 1)
                {
					/*
					i -> oldPos = i -> pos;
					i -> pos = //resolutions[0].pos;
					i -> velocity = (i -> pos - i -> oldPos) / (float)(currentTime - i -> lastDetectionTime);//(dt * (currentTime;

					// I think this might be wrong
					// the velocity has to be determined from the controlling code, not here

					LOG_INFO("[TRMT] drone " << i -> id << " has a time constant of " << (float)(currentTime - i -> lastDetectionTime));
					//LOG_INFO("vel: " << i -> velocity.x << ", " << i -> velocity.y << ", " << i -> velocity.z);
					i -> forward = resolutions[0].forward;
					i -> side = resolutions[0].side;
					i -> up = resolutions[0].up;
					i -> dir = resolutions[0].dir;

					// update the master detection with the new marker setup we just detected
					i -> leftMarker = closest -> leftMarker;
					i -> rightMarker = closest -> rightMarker;
					i -> frontMarker = closest -> frontMarker;
					i -> age = 0.0f;
					i -> lastDetectionTime = currentTime;
					i -> detectionFail = false;

					// pseudo-delete the recent detection
					closest -> dirty = true;
					*/
                }
                //else
                {
					/*
					// otherwise, age the detection
					i -> age += dt;
					i -> detectionFail = true;
					LOG_INFO("[TRMT]: ID " << i -> id << " has been aged to " << i -> age);
					*/
                }
                /*
                else
                {
					// we have to use non-filtered output because our resolution failed for some reason; I don't think that this should happen very often;
					// update the detection position in our master list; note that the ID and names stay the same; we only update position and orientation
					i -> oldPos = i -> pos;
					i -> pos = closest -> pos;
					i -> velocity = (i -> pos - i -> oldPos) / (float)(currentTime - i -> lastDetectionTime);//dt;
					//LOG_INFO("vel: " << i -> velocity.x << ", " << i -> velocity.y << ", " << i -> velocity.z);
					i -> forward = closest -> forward;
					i -> side = closest -> side;
					i -> up = closest -> up;
					i -> dir = closest -> dir;

					// notify the user
					LOG_INFO("[TRMT]: [WARNING]: could not produce filtered marker positions for ID " << i -> id << " (" << resolutions.size() << " were produced)");
				}*/

                // uncomment to see what's happening
                /*
				LOG_INFO("--------------------------------");
				LOG_INFO("[TRMT] TRACKING FOR AGENT " << i -> id << ": ");
				LOG_INFO("pos   : " << i -> pos.x << ", " << i -> pos.y << ", " << i -> pos.z);
				LOG_INFO("old   : " << i -> oldPos.x << ", " << i -> oldPos.y << ", " << i -> oldPos.z);
				LOG_INFO("pdiff : " << (i -> pos.x - i -> oldPos.x) << ", " << (i -> pos.y - i -> oldPos.y) << ", " << (i -> pos.z - i -> oldPos.z));
				LOG_INFO("vel   : " << i -> velocity.x << ", " << i -> velocity.y << ", " << i -> velocity.z);
				LOG_INFO("speed : " << length(i -> velocity) << " m/s");
				LOG_INFO("time  : " << i -> lastDetectionTime);
				LOG_INFO("c - l : " << (currentTime - i -> lastDetectionTime));
				*/
			}
			else
			{
				// otherwise, age the detection
				i -> age += dt;
				i -> detectionFail = true;
				LOG_INFO("[TRMT]: ID " << i -> id << " has been aged to " << i -> age);
			}
		}
		else
		{
			LOG_INFO("[TRMT]: ID " << i -> id << " has been permanently lost at age " << i -> age);
			//i -> detectionFail = true;
		}
	}
}

void TrackingMatcher::retrieve(vector<DroneDetection> &detections)
{
	detections.clear();
	detections.assign(master.begin(), master.end());
}

// - - - private function implementations - - - //

bool compareLeftToRightAndTopToBottom(const DroneDetection &a, const DroneDetection &b, float cellSize)
{
	int cellAX = (int)round(a.pos.x / cellSize);
	int cellAZ = (int)round(a.pos.z / cellSize);
	int cellBX = (int)round(b.pos.x / cellSize);
	int cellBZ = (int)round(b.pos.z / cellSize);

	if     (cellAZ <  cellBZ)                     return true;
	else if(cellAZ == cellBZ && cellAX < cellBX)  return true;
	else					                      return false;

}

void swap(vector<DroneDetection> &detections, int a, int b)
{
	DroneDetection temp = detections[a];
    detections[a] = detections[b];
    detections[b] = temp;
}

void sortInGrid(vector<DroneDetection> &detections, float cellSize)
{
    int i, j, minIndex;

    // One by one move boundary of unsorted subarray
    for (i = 0; i < (int)detections.size() - 1; ++ i)
    {
        // Find the minimum element in unsorted array
        minIndex = i;
        for(j = i + 1; j < (int)detections.size(); ++ j)
        {
			if(compareLeftToRightAndTopToBottom(detections[j], detections[minIndex], cellSize))
			{
				minIndex = j;
			}
		}

        // Swap the found minimum element with the first element
        swap(detections, minIndex, i);
    }
}
