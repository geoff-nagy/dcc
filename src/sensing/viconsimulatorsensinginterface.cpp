#include "sensing/viconsimulatorsensinginterface.h"
#include "sensing/resolvemarkers.h"
#include "sensing/dronedetection.h"
#include "sensing/trackingmatcher.h"

#include "objects/simulateddrone.h"
#include "objects/simulateddroneswarm.h"

#include "util/log.h"

#include "glm/glm.hpp"
#include "glm/gtc/random.hpp"
using namespace glm;

#include <stdint.h>
#include <vector>
#include <map>
#include <algorithm>
using namespace std;

ViconSimulatorSensingInterface::ViconSimulatorSensingInterface(const vec2 &size, SimulatedDroneSwarm *droneSwarm, vector<int> &expectedIDs)
	: SensingInterface(size), droneSwarm(droneSwarm), expectedIDs(expectedIDs)
{
	const float TRACKING_SEARCH_RADIUS = 0.25f;
	const float MAX_DETECTION_AGE_TO_FAIL = 3.0f;

	// build our tracking matcher object that tracks identical drone marker configurations over time
	trackingMatcher = new TrackingMatcher(TRACKING_SEARCH_RADIUS, MAX_DETECTION_AGE_TO_FAIL);
	firstFrame = true;
}

ViconSimulatorSensingInterface::~ViconSimulatorSensingInterface()
{
	delete trackingMatcher;
}

void ViconSimulatorSensingInterface::update(float dt)
{
	// clear list of markers and drone detections
	markers.clear();
	drones.clear();

    // in the simulated Vicon we always succeed in getting frames, so there's no need to take into account the
    // case where we don't (like we do in the real ViconSensingInterface)
	detectMarkers();
	detectDrones();

    // perform tracking over time; we do this regardless of any Vicon input so we can age detections accordingly
    trackAndMatchDrones(dt);
}

void ViconSimulatorSensingInterface::getDrones(vector<DroneDetection> &detections)
{
	detections.clear();
	detections.assign(drones.begin(), drones.end());
}

void ViconSimulatorSensingInterface::getMarkers(std::vector<Marker> &markerList)
{
	markerList.clear();
	markerList.assign(markers.begin(), markers.end());
}

void ViconSimulatorSensingInterface::detectMarkers()
{
	const float POS_ERROR_STD_DEV = 0.05f;				// this is probably way worse than what we'd get in the real Vicon lab

	const int SINGLE_MARKER_FAILURE_PERCENT = 1;		// percentage of time we fail to detect a marker
	const int SINGLE_MARKER_ERROR_PERCENT = 10;			// percentage of time we get a marker position slightly wrong
	const int FRAME_ERROR_PERCENT = 1;					// percentage of time we fail to get any new markers at all

	vector<DroneDetection> newDetections;
	vector<DroneDetection>::iterator k;
	map<uint32_t, DroneDetection>::iterator m;
	vector<SimulatedDrone*>::iterator i;
	vec3 error;
	uint32_t j;
	SimulatedDrone *curr;

	// clear our list of markers we've detected
	markers.clear();

	// there's a chance we miss a frame
	if(linearRand(1, 100) > FRAME_ERROR_PERCENT)
	{
		// read the Vicon marker positions from the simulated world
		for(i = droneSwarm -> getDronesBegin(); i != droneSwarm -> getDronesEnd(); ++ i)
		{
			curr = *i;
			for(j = 0; j < curr -> getNumViconMarkers(); ++ j)
			{
				// there's a small chance we fail to see this marker
				if(linearRand(1, 100) > SINGLE_MARKER_FAILURE_PERCENT)
				{
					// there's a small chance we get a marker in a wrong position
					if(linearRand(1, 100) <= SINGLE_MARKER_ERROR_PERCENT)
					{
						error = vec3(gaussRand(0.0f, POS_ERROR_STD_DEV), gaussRand(0.0f, POS_ERROR_STD_DEV), gaussRand(0.0f, POS_ERROR_STD_DEV));
					}
					else
					{
						error = vec3(0.0f);
					}

					// add the marker to our list of detections
					markers.push_back(curr -> getViconMarker(j) + error);
				}
			}
		}
	}
	else
	{
		LOG_INFO("ViconSimulatorSensingInterface::update() failed to retrieve a new frame");
	}

	// shuffle the markers; users shouldn't make any assumptions about order
	random_shuffle(markers.begin(), markers.end());
}

void ViconSimulatorSensingInterface::detectDrones()
{
	resolveUBeeMarkers(markers, drones);
}

void ViconSimulatorSensingInterface::trackAndMatchDrones(float dt)
{
	// if this is the first frame we've seen, initialize the tracking matcher first
	if(firstFrame)
	{
		// find at least one drone before we promise the system that everything is visible
		if(markers.size() >= 3)
		{
			if(trackingMatcher -> promise(drones, expectedIDs))
			{
				// promise was successfully accepted; we can now perform tracking over time
				firstFrame = false;
			}
			else
			{
				// promise failed; erase drone detections
				drones.clear();
			}
		}
	}
	else
	{
		trackingMatcher -> match(drones, dt);		// perform tracking over time
		trackingMatcher -> retrieve(drones);		// erase recent drone detection list and replace with tracked-matched ones
	}
}
