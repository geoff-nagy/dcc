#pragma once

#include "sensing/dronedetection.h"

#include "util/circularbuffer.h"

#include <vector>
#include <map>

class TrackingMatcher
{
public:
	TrackingMatcher(float detectionSearchRadius, float maxAgeToFail);
	~TrackingMatcher();

	// this method should be called at the beginning of your trial, telling the tracker that you promise
	// that there are drones placed in a left-to-right, top-to-bottom fashion with the IDs provided, in
	// that order; this will create DroneDetections at those detected positions matching those IDs to
	// prime the matching algorithm; promise() returns true iff the prime was successful
	bool promise(const std::vector<DroneDetection> &detections, const std::vector<int> &ids);

	// given a series of detections, match them (in some intelligent way) to the drones that have already
	// been previously promised to the TrackingMatcher
	void match(std::vector<DroneDetection> &detections, float dt);

	// after calling match(), use this method to retrieve a list of (matched) DroneDetections, where the
	// positions detected received by match() are now (hopefully) matched to agents we know exist
	void retrieve(std::vector<DroneDetection> &detections);

private:
	float detectionSearchRadius;				// maximum distance we expect consecutive detections to be away from each other
	float maxAgeToFail;							// how many seconds of failed detections it takes for a drone to be considered lost
	std::vector<DroneDetection> master;			// master list of detections formed by call to promise()
	double currentTime;

	std::map<int, CircularBuffer> leftMarkerBuffer;
	std::map<int, CircularBuffer> rightMarkerBuffer;
	std::map<int, CircularBuffer> frontMarkerBuffer;

	std::map<int, CircularBuffer> posBuffer;
};
