#include "sensing/resolvemarkers.h"
#include "sensing/dronedetection.h"
#include "sensing/markerpair.h"

#include "objects/marker.h"

#include "util/log.h"

#include "glm/glm.hpp"
using namespace glm;

#include <vector>
#include <algorithm>
using namespace std;

// - - - private function prototypes

static bool sortDistLargeToSmall(const MarkerPair &a, const MarkerPair &b);

// - - - public functions - - - //

void resolveUBeeMarkers(vector<Marker> &markers, vector<DroneDetection> &drones)
{
	const vec2 MARKER_1(0.0f, 0.045f);												// positions of markers, in meters
	const vec2 MARKER_2(-0.025f, -0.025f);
	const vec2 MARKER_3(0.025f, -0.025f);

	const float HEAD_EDGE_LENGTH = length(MARKER_1 - MARKER_2);						// ideal length of any edge pointing to head marker
	const float SIDE_EDGE_LENGTH = length(MARKER_2 - MARKER_3);						// ideal length of the edge connecting the two side markers
	const float EDGE_LENGTH_TOLERANCE = 0.03f;										// reject any edges that have length this far away from ideal/expected length

	const float MIN_CLUSTER_DIST = SIDE_EDGE_LENGTH - EDGE_LENGTH_TOLERANCE;		// markers in a cluster cannot be too close together
	const float MAX_CLUSTER_DIST = HEAD_EDGE_LENGTH + EDGE_LENGTH_TOLERANCE;		// markers in a cluster might read at most 5cm or so apart, but let's be generous

	DroneDetection drone;
	vector<Marker>::iterator k;
	vector<Marker*> master;
	vector<Marker*>::iterator i, j;
	vector<Marker*> cluster;
	Marker *curr;
	Marker *head, *left, *right;
	vec3 leftPos;
	vector<MarkerPair> edges;
	float dist;

	// clear the outgoing list to avoid duplicates
	drones.clear();

	// re-build the list of markers using pointers so we can refer to them later
	for(k = markers.begin(); k != markers.end(); ++ k)
	{
		master.push_back(new Marker(*k));
	}

	// keep iterating until we've found all triplets or we run out of markers
	i = master.begin();
	while(i < master.end())
	{
		// acquire the focal marker that we will look around
		curr = *i;
		if(!curr -> dirty)				// make sure we haven't processed it yet in a previous cluster
		{
			// start re-building the cluster, using the focal marker as the first entry
			cluster.clear();
			cluster.push_back(curr);

			// cluster as many other markers around the focal marker as we can find
			j = i + 1;
			while(j < master.end())
			{
				// without considering markers we've already determined belong to a uBee...
				if(!(*j) -> dirty)
				{
					// is this new marker close to our focal marker?
					dist = length(curr -> pos - (*j) -> pos);
					if(dist >= MIN_CLUSTER_DIST && dist <= MAX_CLUSTER_DIST)
					{
						// close enough; add it
						cluster.push_back(*j);
					}
				}

				// advance
				++ j;
			}

			// only consider clusters we've found that have exactly 3 markers
			if(cluster.size() == 3)
			{
				// build a drone detection around those markers
				drone.name = "uBee";
				drone.id = 0;
				drone.pos = (cluster[0] -> pos + cluster[1] -> pos + cluster[2] -> pos) / 3.0f;		// simple average
				drone.oldPos = drone.pos;
				drone.velocity = vec3(0.0f);

				// build a set of marker pairs; these represent edges connecting each marker
				edges.clear();
				edges.push_back(MarkerPair(cluster[0], cluster[1]));
				edges.push_back(MarkerPair(cluster[0], cluster[2]));
				edges.push_back(MarkerPair(cluster[1], cluster[2]));

				// sort the pairs based on intra-pair distance
				sort(edges.begin(), edges.end(), sortDistLargeToSmall);

				// do a more careful check to make sure edge lengths make sense
				if(fabs(edges[0].dist - HEAD_EDGE_LENGTH) <= EDGE_LENGTH_TOLERANCE &&
				   fabs(edges[1].dist - HEAD_EDGE_LENGTH) <= EDGE_LENGTH_TOLERANCE &&
				   fabs(edges[2].dist - SIDE_EDGE_LENGTH) <= EDGE_LENGTH_TOLERANCE)
				{


					// the first two edges (i.e., the two longest ones) should have the head marker as a common point; find it
					head = NULL;
					if     (edges[0].marker1 == edges[1].marker1) head = edges[0].marker1;
					else if(edges[0].marker1 == edges[1].marker2) head = edges[0].marker1;
					else if(edges[0].marker2 == edges[1].marker1) head = edges[0].marker2;
					else if(edges[0].marker2 == edges[1].marker2) head = edges[0].marker2;

					// a head marker doesn't exist if we can't find a common point; this can happen if edge lengths are wrong (such as with a false positive)
					if(head != NULL)
					{
						// "delete" the markers we've built the cluster with---we will now consider them to be a part of a marker ubee triangle configuration
						// [todo]: put this into the head != NULL conditional below?
						for(j = cluster.begin(); j != cluster.end(); ++ j)
						{
							(*j) -> dirty = true;
						}

						// compute forward vector
						drone.forward = normalize(head -> pos - drone.pos);

						// compute a position to the left of the center and find the side marker closest to it; this will indicate where our "side" vector is
						// [note]: this doesn't take into consideration the roll or pitch of the ubee, but if it's too extreme we have other problems anyways
						leftPos = drone.pos + vec3(drone.forward.z, 0.0f, -drone.forward.x);		// crude cross product
						if(length(leftPos - edges[2].marker1 -> pos) < length(leftPos - edges[2].marker2 -> pos))
						{
							left = edges[2].marker1;
							right = edges[2].marker2;
						}
						else
						{
							left = edges[2].marker2;
							right = edges[2].marker1;
						}

						// side vector is formed by the left- and right-most markers
						drone.side = normalize(left -> pos - right -> pos);

						// re-compute forward vector
						drone.forward = normalize(head -> pos - ((left -> pos + right -> pos) / 2.0f));

						// compute side vector
						//drone.side = -normalize(cross(drone.forward, vec3(0.0f, 1.0f, 0.0f)));

						// up vector is the cross product of the forward and side
						drone.up = normalize(cross(drone.forward, drone.side));

						// compute direction we're facing along yaw axis, in radians
						drone.dir = -(M_PI_2 - atan2(drone.forward.z, drone.forward.x));

						// save marker information in case we need it later
						drone.leftMarker = *left;
						drone.rightMarker = *right;
						drone.frontMarker = *head;

						// all tests passed; add the detection
						drones.push_back(drone);
					}
				}

				// [wtf? why was this here?]
				// [now moved it to inside both upper conditionals; d'oh!]
				//drones.push_back(drone);
			}
		}

		// advance to next focal marker
		++ i;
	}

	// free our memory
	for(i = master.begin(); i != master.end(); ++ i)
	{
		delete *i;
	}
}

// - - - private functions - - - //

bool sortDistLargeToSmall(const MarkerPair &a, const MarkerPair &b)
{
	return a.dist > b.dist;
}
