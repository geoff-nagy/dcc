#pragma once

#include "objects/marker.h"

#include "glm/glm.hpp"

#include <string>

class DroneDetection
{
public:
	DroneDetection();

	bool operator  < (const DroneDetection &other) const;
	bool operator == (const DroneDetection &other) const;
	bool operator != (const DroneDetection &other) const;

	DroneDetection operator+(const DroneDetection &other);
	DroneDetection operator/(float val);

	// ID information
	std::string name;
	uint32_t id;

	// positional and orientation info
	glm::vec3 pos;					// global pos
	glm::vec3 oldPos;				// global old pos
	glm::vec3 velocity;				// global velocity
	float dir;						// global yaw orientation in radians
	float dist;						// distance to reference object
	float age;						// age of this detection in seconds
	bool dirty;						// used to ignore entries in lists of DroneDetections
	double lastDetectionTime;		// when this detection was made, in seconds
	bool detectionFail;

	// global orientation vectors
	glm::vec3 forward;
	glm::vec3 side;
	glm::vec3 up;

	// marker information
	Marker leftMarker;
	Marker rightMarker;
	Marker frontMarker;
};
