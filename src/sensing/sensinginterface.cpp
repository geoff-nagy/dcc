#include "sensing/sensinginterface.h"

#include "objects/marker.h"

#include "glm/glm.hpp"
using namespace glm;

#include <vector>
using namespace std;

SensingInterface::SensingInterface(const vec2 &size)
	: size(size)
{
	// anything else?
}

SensingInterface::~SensingInterface() { }

void SensingInterface::getMarkers(vector<Marker> &markers)
{
	// should be overridden by any sub class wanting access to Vicon-like marker data, if applicable, for debugging purposes
}

vec2 SensingInterface::getSize()
{
	return size;
}
