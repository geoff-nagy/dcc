#pragma once

#include "sensing/dronedetection.h"

#include "objects/marker.h"

#include "glm/glm.hpp"

#include <vector>

// this is function which, given a list of Vicon marker detections, resolves them into detections
// that contain position, orientation, and ID information; it assumes that markers are placed on
// a drone in the UBee format, described by the constants in markerplacement.h
void resolveUBeeMarkers(std::vector<Marker> &markers, std::vector<DroneDetection> &drones);
