#include "sensing/sensormap.h"

#include "rendering/quad.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
using namespace glm;

#include <string.h>
using namespace std;

SensorMap::SensorMap(const vec2 &size, float resolution)
	: size(size), resolution(resolution)
{
	quad = Quad::getInstance();

	halfSize = size / 2.0f;
	numCellsX = size.x / resolution;
	numCellsY = size.y / resolution;
	numCellsTotal = numCellsX * numCellsY;
	cells = new int[numCellsTotal];
	clear();
}

SensorMap::~SensorMap()
{
	delete[] cells;
}

void SensorMap::clear()
{
	memset(cells, 0, sizeof(int) * numCellsTotal);
	maxOccupancy = 0;
}

void SensorMap::add(const vec2 &pos)
{
	// determine (translated) location on grid
	int index;
	int cellX = (pos.x + halfSize.x) / resolution;
	int cellY = (pos.y + halfSize.y) / resolution;

	// increase the count at this cell, if in range
	if(cellX >= 0 && cellX < numCellsX &&
	   cellY >= 0 && cellY < numCellsY)
	{
		index = (cellY * numCellsX) + cellX;
		++ cells[index];
		maxOccupancy = max(maxOccupancy, cells[index]);
	}
}

void SensorMap::render(const mat4 &viewProjection)
{
	const vec4 MIN_COLOR(1.0f, 0.0f, 0.0f, 0.5f);
	const vec4 MID_COLOR(1.0f, 1.0f, 0.0f, 0.5f);
	const vec4 MAX_COLOR(0.0f, 1.0f, 0.0f, 0.5f);

	const int MAX_OCCUPANCY = 500;

	int x, y;
	vec2 pos;
	float factor;
	int index;
	mat4 modelMat;

	quad -> setViewProjection(viewProjection);

	for(y = 0; y < numCellsY; ++ y)
	{
		for(x = 0; x < numCellsX; ++ x)
		{
			index = (y * numCellsX) + x;
			pos = vec2(x * resolution, y * resolution) - halfSize;
			factor = (float)cells[index] / MAX_OCCUPANCY;//maxOccupancy;

			modelMat = mat4(1.0f);
			modelMat = translate(modelMat, vec3(pos.x, 0.0f, pos.y));
			modelMat = scale(modelMat, vec3(resolution));
			modelMat = rotate(modelMat, (float)M_PI_2, vec3(1.0f, 0.0f, 0.0f));

			if(factor <= 0.5f)
			{
				quad -> render(modelMat, MIN_COLOR + (MID_COLOR - MIN_COLOR) * (factor * 2.0f));
			}
			else
			{
				quad -> render(modelMat, MID_COLOR + (MAX_COLOR - MID_COLOR) * ((factor - 0.5f) * 2.0f));
			}
		}
	}
}
