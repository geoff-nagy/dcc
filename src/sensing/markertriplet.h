#pragma once

#include "objects/marker.h"

class MarkerTriplet
{
public:
	MarkerTriplet();
	MarkerTriplet(Marker focalMarker1, Marker focalMarker2, Marker idMarker, int id, float dist);

	bool operator< (const MarkerTriplet &other) const;

	bool containsAnyMarkersOf(const MarkerTriplet &other) const;

	Marker focalMarker1;
	Marker focalMarker2;
	Marker idMarker;
	int id;
	float dist;
};
