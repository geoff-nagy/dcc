#pragma once

#include "sensing/sensinginterface.h"
#include "sensing/dronedetection.h"

#include <stdint.h>
#include <vector>
#include <map>

class SimulatedDroneSwarm;
class TrackingMatcher;

class ViconSimulatorSensingInterface : public SensingInterface
{
public:
	ViconSimulatorSensingInterface(const glm::vec2 &size, SimulatedDroneSwarm *droneSwarm, std::vector<int> &expectedIDs);
	~ViconSimulatorSensingInterface();

	virtual void update(float dt);
	virtual void getDrones(std::vector<DroneDetection> &poses);
	virtual void getMarkers(std::vector<Marker> &markers);

private:
	SimulatedDroneSwarm *droneSwarm;
	std::vector<Marker> markers;
	std::vector<DroneDetection> drones;
	std::map<uint32_t, DroneDetection> droneDetections;

	void detectMarkers();
	void detectDrones();
	void trackAndMatchDrones(float dt);

	std::vector<int> expectedIDs;
	TrackingMatcher *trackingMatcher;
	bool firstFrame;
};
