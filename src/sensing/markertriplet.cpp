#include "sensing/markertriplet.h"

#include "objects/marker.h"

MarkerTriplet::MarkerTriplet()
	: id(-1), dist(0.0f)
{ }

MarkerTriplet::MarkerTriplet(Marker focalMarker1, Marker focalMarker2, Marker idMarker, int id, float dist)
	: focalMarker1(focalMarker1), focalMarker2(focalMarker2), idMarker(idMarker), id(id), dist(dist)
{ }

bool MarkerTriplet::operator< (const MarkerTriplet &other) const
{
	return dist < other.dist;
}

bool MarkerTriplet::containsAnyMarkersOf(const MarkerTriplet &other) const
{
	return focalMarker1.id == other.focalMarker1.id ||
	       focalMarker1.id == other.focalMarker2.id ||
	       focalMarker1.id == other.idMarker.id ||
	       focalMarker2.id == other.focalMarker2.id ||
	       focalMarker2.id == other.idMarker.id ||
	       idMarker.id == other.idMarker.id;
}
