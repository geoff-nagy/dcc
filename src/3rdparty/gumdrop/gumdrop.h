/*
Gumdrop data format
Geoff Nagy

Having worked with JSON, XML, and a few other data formats and their parsers
(including my own weird ones) I haven't come across any that satisfy my desires
for (a) a simple, general-purpose data format, and (b) an easy way of accessing
the data programmatically in a simple one-line-ish way that also allows for
error-checking and missing-data-checking, if necessary.

The Gumdrop format is my answer to both (a) and (b). The data format is simple
and C-esque, and comes naturally without me having to think too hard about it.
It's general-purpose enough that I can use it for pretty much anything.
Accessing the data is also mind-numbingly simple; you can specify C-esque paths
to the data both during run-time or compile-time, in a single line of code.
Optional parameters to those methods allow the user to check for missing values
or enable error messages, if desired. Thus, the user of this library can select
the level of flexibility or error-handling they desire.
*/

#pragma once

#include <string>
#include <vector>
#include <stdint.h>
#include <sstream>

// this is a useful macro for building paths during run timewithout having to
// use the stringstream class
#define GUMDROP(path) \
	static_cast<std::ostringstream&>(std::ostringstream().flush() << path).str()

// - - - GumdropNode class - - - //

// the GumdropNode is the primary class used to access the Gumdrop data format;
// no other classes are exposed or needed
class GumdropNode
{
public:
	// this will create a blank GumdropNode with no data; it's not really useful
	GumdropNode();

	// the user is responsible for invoking the destructor, via delete, to free
	// up all memory used by a GumdropNode
	~GumdropNode();

	// creates a GumdropNode based on the contents of a given filename
	static GumdropNode *fromFile(const std::string &filename);

	// creates a GumdropNode based on the contents of the given string
	static GumdropNode *fromMemory(uint8_t *data, uint32_t len);

	// writes the GumdropNode contents to a string
	std::string toString();

	// writes the GumdropNode contents to a file
	void toFile(const std::string &filename);

	// accessor methods for the different data types that Gumdrop supports
	bool getBool(const std::string &path, bool defaultVal, bool *found = NULL, bool enableError = false);
	double getDouble(const std::string &path, double defaultVal, bool *found = NULL, bool enableError = false);
	int32_t getInt(const std::string &path, int32_t defaultVal, bool *found = NULL, bool enableError = false);
	std::string getString(const std::string &path, const std::string &defaultVal, bool *found = NULL, bool enableError = false);

private:

	// - - - DataType enum - - - //

	// every derived class of the GumdropVariable class has an assigned type
	typedef enum DATA_TYPE
	{
		DATA_TYPE_INVALID = 0,
		DATA_TYPE_BOOL,
		DATA_TYPE_DOUBLE,
		DATA_TYPE_INT,
		DATA_TYPE_STRING,
		DATA_TYPE_ARRAY
	} DataType;

	// - - - GumdropVariable class - - - //

	// the abstract base class for the Gumdrop variable type
	class GumdropVariable
	{
	public:
		GumdropVariable();
		virtual ~GumdropVariable() = 0;
		virtual std::string toString(uint32_t indent) = 0;

		std::string name;			// every var has an assigned name
		DataType dataType;			// every var has an assigned type
	};

	// - - - GumdropSingleVariable class - - - //

	// derived variable class that represents a single (non-array) variable
	class GumdropSingleVariable : public GumdropVariable
	{
	public:
		GumdropSingleVariable();
		virtual ~GumdropSingleVariable();
		virtual std::string toString(uint32_t indent);

		// only one of these will really be used, since instantiations of this
		// class represent only a single variable; this is a somewhat wasteful
		// but simple solution that I may change in the future
		bool boolValue;
		double doubleValue;
		int32_t intValue;
		std::string strValue;
	};

	// - - - GumdropArrayVariable class - - - //

	// derived array class that represents an array of GumdropSingleVariables
	class GumdropArrayVariable : public GumdropVariable
	{
	public:
		GumdropArrayVariable();
		virtual ~GumdropArrayVariable();
		virtual std::string toString(uint32_t indent);

		// elements in this array; note that types are allowed to be mixed
		std::vector<GumdropSingleVariable*> elements;
	};

	// - - - static functions - - - //

	// useful function that parses the incoming string and determines the
	// type of data it will parse to
	static DataType getDataType(const std::string &value);

	// after calling getDataType(), you can call one of these to actually
	// perform the conversion/parsing to the requested data format; note that
	// these functions assume that the provided string has already been verified
	// as the given type by getDataType()
	static bool convertToBool(const std::string &value);
	static double convertToDouble(const std::string &value);
	static int32_t convertToInt(const std::string &value);
	static std::string convertToString(const std::string &value);

	// determines if a variable name is syntactically allowed
	static bool isValidName(const std::string &name);

	// low-level method for parsing individual tokens of the Gumdrop data format
	static std::string getNextToken(uint8_t *data, uint32_t len, uint32_t *index, uint32_t *lineNum);

	// - - - methods - - - //

	// output this node as a string with a given indentation level
	std::string toStringChild(uint32_t indent);

	// return a variable that has the given data type on the given path
	GumdropSingleVariable *getVar(const std::string &path, DataType dataType);

	// return a node with the given name, contained in this node
	GumdropNode *getNodeByName(const std::string &name, uint32_t index);

	// return a var with the given name and data type, contained in this node
	GumdropSingleVariable *getVarByName(const std::string &name, uint32_t index, DataType dataType);

	// - - - data members - - - //

	std::string name;							// the name of this node
	GumdropNode *parent;						// the immediate parent of this node
	std::vector<GumdropNode*> nodes;			// immediate child nodes of this node
	std::vector<GumdropVariable*> variables;	// data variables contained in this node
};
