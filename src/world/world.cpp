#include "world/world.h"

#include "core/application.h"

#include "objects/camera.h"
#include "objects/simulateddroneswarm.h"
#include "objects/simulationubeeinterface.h"
#include "objects/grid.h"

#include "gui/panel.h"
#include "gui/label.h"
#include "gui/button.h"
#include "gui/radiobutton.h"
#include "gui/histogram.h"

#include "util/wavefront.h"
#include "util/model.h"
#include "util/shader.h"
#include "util/gldebugging.h"
#include "util/mymath.h"
#include "util/log.h"

#include "rendering/quad.h"
#include "rendering/linerenderer.h"
#include "rendering/pointrenderer.h"
#include "rendering/text.h"

#include "sensing/sensinginterface.h"
#include "sensing/viconsimulatorsensinginterface.h"
#include "sensing/magicsimulatorsensinginterface.h"
#include "sensing/viconsensinginterface.h"
#include "sensing/dronedetection.h"

#include "objects/simulationubeeinterface.h"
#include "objects/ubeeradiointerface.h"
#include "objects/simulatedubee.h"

#include "experiments/pairwiseflocking/pairwiseflockingtrialcontroller.h"
#include "experiments/pairwiseflockinglab/pairwiseflockinglabtrialcontroller.h"
#include "experiments/blank/blanktrialcontroller.h"
#include "experiments/box/boxtrialcontroller.h"
#include "experiments/multihover/multihovertrialcontroller.h"
#include "experiments/map/maptrialcontroller.h"

#include "util/image.h"
#include "util/rnd.h"
#include "util/arguments.h"
#include "util/stringutils.h"

#include "libdrawtext/drawtext.h"

#include "GL/glew.h"

#include "GLFW/glfw3.h"

#include "glm/glm.hpp"
#include "glm/gtc/random.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/quaternion.hpp"
#include "glm/gtx/rotate_vector.hpp"
using namespace glm;

#include <limits>
#include <ctime>
#include <cstdlib>
#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;

World::World(const vec2 &windowSize, int toolbarWidth, bool gui, const string &worldConfigFilename, const string &trialConfigFilename)
	: Form(windowSize), gui(gui)
{
	Arguments a;

	stringstream ss;
	string trialOutputFilename;
	string sensorStr;
	string commsStr;
	string commsDevice;
	uint32_t commsDelay;

	vector<int> pairs;
	//uint32_t numPairs;
	//uint32_t numTrackedIfPaired;
	//uint32_t numSingle;
	//uint32_t numTrackedIfSingle;
	string trialStr;
	string outputFolder;
	uint32_t index;

	uint32_t numExpectedIDs;
	vector<int> expectedIDs;
	bool idsPresent;
	int i;

	this -> windowSize = windowSize;
	this -> toolbarWidth = toolbarWidth;
	this -> gui = gui;
	paused = false;

	// load world specifications
	worldConfigFile = GumdropNode::fromFile(worldConfigFilename);
	if(!worldConfigFile)
	{
		LOG_ERROR("the specified world file \"" << worldConfigFilename << "\" could not be loaded");
	}

	// load trial specifications
	trialConfigFile = GumdropNode::fromFile(trialConfigFilename);
	if(!trialConfigFile)
	{
		LOG_ERROR("the specified trial file \"" << trialConfigFilename << "\" could not be loaded");
	}

	// determine trial output file name from the trial config file
	index = trialConfigFilename.find_last_of('/');
	outputFolder = "";
	if(index != string::npos)
	{
		outputFolder = trialConfigFilename.substr(0, index);
	}
	trialOutputFilename = outputFolder + "/results/" + trialConfigFile -> getString("trial.output", "", NULL, true);

	// report on configuration
	LOG_INFO("world config file: \"" << worldConfigFilename << "\"");
	LOG_INFO("trial config file: \"" << trialConfigFilename << "\"");
	LOG_INFO("trial output file: \"" << trialOutputFilename << "\"");

	// is the world simulated or real?
	worldName = worldConfigFile -> getString("world.name", "", NULL, true);
	simulated = worldConfigFile -> getBool("world.simulated", false, NULL, true);
	worldSize = vec2(worldConfigFile -> getDouble("world.size[0]", 0.0f, NULL, true),
					 worldConfigFile -> getDouble("world.size[1]", 0.0f, NULL, true));

	// create our simulated drones
	droneSwarm = NULL;
	if(simulated)
	{
		droneSwarm = new SimulatedDroneSwarm();
		addSimulatedDronesFromWorldConfig();
	}

	// create our sensing interface
	sensingInterface = NULL;
	sensorStr = worldConfigFile -> getString("world.sensor.name", "", NULL, true);
	sensorSize = vec2(worldConfigFile -> getDouble("world.sensor.size[0]", 0.0f, NULL, true),
					  worldConfigFile -> getDouble("world.sensor.size[1]", 0.0f, NULL, true));
	if(sensorStr.compare("simulated_vicon") == 0)
	{
		// check to see how many IDs are expected in the world
		numExpectedIDs = trialConfigFile -> getInt("trial.id_count", 0, &idsPresent, true);
		if(numExpectedIDs == 0 || !idsPresent)
		{
			LOG_ERROR("trial file \"" << trialConfigFilename << "\" has specified that a simulated Vicon sensor is to be used, but no expected IDs were listed");
		}

		// build list of IDs so we can track them over time
		for(i = 0; i < (int)numExpectedIDs; ++ i)
		{
			ss.str("");
			ss << "trial.ids" << "[" << i << "]";
			index = trialConfigFile -> getInt(ss.str(), 0, NULL, true);
			expectedIDs.push_back(index);
		}

		// initialize our Vicon tracker with the IDs we expect to find
		sensingInterface = new ViconSimulatorSensingInterface(sensorSize, droneSwarm, expectedIDs);
	}
	else if(sensorStr.compare("magic") == 0)
	{
		sensingInterface = new MagicSimulatorSensingInterface(sensorSize, droneSwarm);
	}
	else if(sensorStr.compare("vicon") == 0)
	{
		// check to see how many IDs are expected in the world
		numExpectedIDs = trialConfigFile -> getInt("trial.id_count", 0, &idsPresent, true);
		if(numExpectedIDs == 0 || !idsPresent)
		{
			LOG_ERROR("trial file \"" << trialConfigFilename << "\" has specified that a Vicon sensor is to be used, but no expected IDs were listed");
		}

		// build list of IDs so we can track them over time
		for(i = 0; i < (int)numExpectedIDs; ++ i)
		{
			ss.str("");
			ss << "trial.ids" << "[" << i << "]";
			index = trialConfigFile -> getInt(ss.str(), 0, NULL, true);
			expectedIDs.push_back(index);
		}

		// initialize our Vicon tracker with the IDs we expect to find
		sensingInterface = new ViconSensingInterface(sensorSize, expectedIDs);
	}
	// add other sensing interfaces here as you implement them
	else
	{
		LOG_ERROR("sensor \"" << sensorStr << "\" is not known");
	}

    // create our communication interface
    droneInterface = NULL;
    commsStr = worldConfigFile -> getString("world.comms.name", "", NULL, true);
    commsDevice = worldConfigFile -> getString("world.comms.device", "", NULL, true);
    commsDelay = worldConfigFile -> getInt("world.comms.delay_ms", 0, NULL, true);
    if(commsStr.compare("simulated_ubee_radio") == 0)
    {
		droneInterface = new SimulationUBeeInterface(droneSwarm, (float)commsDelay / 1000.0f);
    }
    else if(commsStr.compare("ubee_radio") == 0)
    {
		droneInterface = new UBeeRadioInterface(commsDevice);
    }
    // add other drone communication interfaces here as you implement them
    else
    {
		LOG_ERROR("comms \"" << commsStr << "\" is not known");
	}

	// configure our trial
	trialStr = trialConfigFile -> getString("trial.type", "", NULL, true);
	if(trialStr.compare("pairwise_simulated") == 0)
	{
		LOG_ERROR("the pairwise_simulated trial controller is no longer supported and is out of date; use the pairwise_lab trial controller instead");
		//trial = new PairwiseFlockingTrialController(this, sensingInterface, droneInterface, trialConfigFile, trialOutputFilename, pairs, gui);
	}
	else if(trialStr.compare("pairwise_lab") == 0)
	{
		trial = new PairwiseFlockingLabTrialController(this, sensingInterface, droneInterface, trialConfigFile, trialOutputFilename, gui);
	}
	else if(trialStr.compare("blank") == 0)
	{
		trial = new BlankTrialController(this, sensingInterface, droneInterface);
	}
	else if(trialStr.compare("box") == 0)
	{
		trial = new BoxTrialController(this, sensingInterface, droneInterface, trialConfigFile, trialOutputFilename);
	}
	else if(trialStr.compare("multi_hover") == 0)
	{
		trial = new MultiHoverTrialController(this, sensingInterface, droneInterface, trialConfigFile, trialOutputFilename);
	}
	else if(trialStr.compare("map") == 0)
	{
		trial = new MapTrialController(this, sensingInterface, droneInterface);
	}
	// add other trial types here as you implement them
	else
	{
		LOG_ERROR("trial type \"" << trialStr << "\" is not known");
	}

	// start the trial
	currentTime = 0.0;
	lastInterfaceUpdateTime = 0.0;
	if(!gui && trial)
	{
		//trial -> start();
		startTrial();
	}

	// start our GUI elements if requested
	if(gui)
	{
		initForm();
		zoomIn = false;
		zoomOut = false;
		mouseLeftButtonDown = false;
		onViewDefault();
		cameraDistance = 0.0f;
		cameraAngleX = 0.0f;
		cameraAngleY = 0.0f;

		cameraYaw = 0.0f;
		cameraPitch = 0.0f;

		lineRenderer = LineRenderer::getInstance();
		textRenderer = Text::getInstance();
	}

	droneInterfaceTimer = 0.0f;
}

World::~World()
{
	// delete controller objects
	delete droneInterface;
	delete sensingInterface;
	delete trial;

	// delete our graphical objects and utilities if we started the GUI
	if(gui)
	{
		delete worldGridFloor;
		delete Quad::getInstance();
		delete Text::getInstance();
		delete PointRenderer::getInstance();
		delete LineRenderer::getInstance();
	}

	delete trialConfigFile;
	delete worldConfigFile;
}

void World::initForm()
{
	const vec2 QUARTER_BUTTON_SIZE(43.0f, 26.0f);
	const vec2 THIRD_BUTTON_SIZE(59.0f, 26.0f);
	const vec2 HALF_BUTTON_SIZE(91.0f, 26.0f);
	const vec2 FULL_BUTTON_SIZE(187.0f, 26.0f);
	const vec2 MEGA_BUTTON_SIZE(187.0f, 52.0f);

	vec2 size = getSize();

	// OpenGL camera setup

	setupViewingParams();

	// place GUI elements

	pnlToolbar = new Panel(vec2(toolbarWidth, size.y), true);
	pnlToolbar -> resetPadding(5);
	addWidget(pnlToolbar);

	pnlMainArea = new Panel(vec2(windowSize.x - toolbarWidth, size.y), false);
	pnlMainArea -> resetPadding(5);
	addWidget(pnlMainArea);

	pnlMainArea -> addWidget(lblFPS = new Label("FPS XXXXXXX"));
	pnlMainArea -> addWidget(new Label("MMB + drag to pan or tilt     |     Shift + MMB to translate along ground plane     |     Mouse scroll to zoom to cursor     |     "));
	if(simulated)
	{
		pnlMainArea -> addWidget(new Label("Simulated mode"));
	}
	else
	{
		pnlMainArea -> addWidget(new Label("Live mode"));
	}

	pnlMainArea -> padHorizontal(2000.0f);
	pnlMainArea -> addWidget(lblTime = new Label("Time: XX:XX:XX"));

	pnlToolbar -> padVertical(10.0f);
	pnlToolbar -> addWidget(new Label("Camera view"));
	pnlToolbar -> padHorizontal(200.0f);
	pnlToolbar -> addWidget(new Button("Default (F1)", HALF_BUTTON_SIZE, this, static_cast<Form::ButtonPressHandler>(&World::onViewDefault), GLFW_KEY_F1), "View scene from default starting pose");
	pnlToolbar -> addWidget(new Button("Top (F2)", HALF_BUTTON_SIZE, this, static_cast<Form::ButtonPressHandler>(&World::onViewTop), GLFW_KEY_F2), "View scene from above");

	pnlToolbar -> padVertical(10.0f);
	pnlToolbar -> addWidget(new Label("Select"));
	pnlToolbar -> padHorizontal(200.0f);
	pnlToolbar -> addWidget(new Button("All/None (A)", HALF_BUTTON_SIZE, this, static_cast<Form::ButtonPressHandler>(&World::toggleSelection), 'A'), "Select all drones, or clear selection if any drones are selected");
	pnlToolbar -> addWidget(new Button("Invert (I)", HALF_BUTTON_SIZE, this, static_cast<Form::ButtonPressHandler>(&World::invertSelection), 'I'), "Invert drone selection");

	pnlToolbar -> padVertical(10.0f);
	pnlToolbar -> addWidget(new Label("Drone Control"));
	pnlToolbar -> padHorizontal(200.0f);
	pnlToolbar -> addWidget(new Button("Take Off", HALF_BUTTON_SIZE, this, NULL, 'A'), "Command drone(s) to take off");
	pnlToolbar -> addWidget(new Button("Land", HALF_BUTTON_SIZE, this, NULL, 'I'), "Command drone(s) to land immediately");
	pnlToolbar -> addWidget(new Button("Ping", FULL_BUTTON_SIZE, this, static_cast<Form::ButtonPressHandler>(&World::ping), 'P'), "Command all drones to flash their LED (this command is only obeyed if not in flight)");

	pnlToolbar -> padVertical(20.0f);
	pnlToolbar -> addWidget(new Label("Trial Logic Control"));
	pnlToolbar -> padHorizontal(200.0f);
	pnlToolbar -> addWidget(btnStart = new Button("Start Trial", FULL_BUTTON_SIZE, this, static_cast<Form::ButtonPressHandler>(&World::startTrial), 'A'), "Start the trial");
	pnlToolbar -> addWidget(btnPause = new Button("Pause", FULL_BUTTON_SIZE, this, static_cast<Form::ButtonPressHandler>(&World::pauseTrial), 'I'), "Pause or resume the trial while keeping the drones in the air");
	pnlToolbar -> addWidget(btnStopAndLand = new Button("Stop & Land", FULL_BUTTON_SIZE, this, static_cast<Form::ButtonPressHandler>(&World::stopAndLand), 'I'), "Land all drones and stop the trial immediately");
	pnlToolbar -> addWidget(btnScram = new Button("SCRAM", MEGA_BUTTON_SIZE, this, static_cast<Form::ButtonPressHandler>(&World::scram), 'S'), "EMERGENCY STOP");

	// set button enable defaults
	btnStart -> setEnabled(true);
	btnPause -> setEnabled(false);
	btnStopAndLand -> setEnabled(false);
	btnScram -> setEnabled(false);

	// load our drone model

	droneWavefront = WavefrontModel::fromFile("../mesh/ubee.obj");
	droneModel = Model::fromWavefront(droneWavefront);

	markerWavefront = WavefrontModel::fromFile("../mesh/marker.obj");
	markerModel = Model::fromWavefront(markerWavefront);

	// load our drone shader

	solidShader = Shader::fromFile("../shaders/solid-plain.vert", "../shaders/solid-plain.frag");
	solidShader -> bindAttrib("a_Vertex", 0);
	solidShader -> bindAttrib("a_Normal", 1);
	solidShader -> link();
	solidShader -> bind();
	solidShader -> uniformVec3("u_SunColor", vec3(1.0f, 1.0f, 1.0f));
	solidShader -> uniformVec3("u_SunDirection", vec3(0.4f, -1.0f, 0.4f));
	solidShader -> uniformVec3("u_AmbientColor", vec3(0.4f, 0.4f, 0.4f));
}

bool World::shouldClose()
{
	return trial && trial -> isComplete();
}

void World::setupViewingParams()
{
	const float FOV = radians(45.0f);
	const float NEAR = 0.001f;
	const float FAR = 100.0f;
	//const vec2 SIZE = sensingInterface -> getSize();

	// prepare mouse scroll callback
	cameraDistance = 15.0;
	cameraAngleX = -0.4;//-M_PI / 8.0f;
	cameraAngleY = -M_PI_2;

	// build the world floor
	//grid = new Grid(SIZE.x + 1.0f, SIZE.y + 1.0f, 1.0f);
	worldGridFloor = new Grid(worldSize.x + 1.0f, worldSize.y + 1.0f, 1.0f);

	// set up camera projection
	perspectiveProjection = perspective(FOV, windowSize.x / windowSize.y, NEAR, FAR);
	viewport = vec4(0.0f, 0.0f, windowSize.x, windowSize.y);
}

void World::addSimulatedDrone(SimulatedDrone *drone, const vec3 &pos)
{
	if(droneSwarm)
	{
		LOG_INFO("[world] added simulated drone with ID " << drone -> getID() << " to (" << pos.x << ", " << pos.y << ", " << pos.z << ")");
		droneSwarm -> addDrone(drone, pos);
	}
	else
	{
		LOG_ERROR("World::addSimulatedDrone() cannot add a drone to position " << pos.x << ", " << pos.y << ", " << pos.z << " because the simulated drone manager is NULL");
	}
}

void World::addSimulatedDronesFromWorldConfig()
{
	SimulatedDrone *drone;
	int numDrones;
	vec3 dronePosOffset;
	stringstream ss1, ss2;
	string droneType;
	int droneID;
	vec3 dronePos;
	int numMarkers;
	vec3 markerPos;
	int i, j;

	// make sure we have a valid drone handler first
	if(droneSwarm)
	{
		// count the number of drones
		numDrones = worldConfigFile -> getInt("world.drone_count", 0, NULL, false);
		dronePosOffset.x = worldConfigFile -> getDouble("world.drone_pos_offset[0]", 0.0f, NULL, false);
		dronePosOffset.y = worldConfigFile -> getDouble("world.drone_pos_offset[1]", 0.0f, NULL, false);
		dronePosOffset.z = worldConfigFile -> getDouble("world.drone_pos_offset[2]", 0.0f, NULL, false);

		// add however many drones were specified
		for(i = 0; i < numDrones; ++ i)
		{
			// read drone properties
			ss1.str("");
			ss1 << "world.drones.drone[" << i << "]";
			droneType = worldConfigFile -> getString(ss1.str() + ".type", "", NULL, true);
			droneID = worldConfigFile -> getInt(ss1.str() + ".id", 0, NULL, true);
            dronePos.x = worldConfigFile -> getDouble(ss1.str() + ".pos[0]", 0.0f, NULL, true);
            dronePos.y = worldConfigFile -> getDouble(ss1.str() + ".pos[1]", 0.0f, NULL, true);
            dronePos.z = worldConfigFile -> getDouble(ss1.str() + ".pos[2]", 0.0f, NULL, true);
			numMarkers = worldConfigFile -> getInt(ss1.str() + ".marker_count", 0, NULL, false);

			// make the drone
			drone = NULL;
			if(droneType.compare("ubee") == 0)
			{
				drone = new SimulatedUBee(droneID);
				droneSwarm -> addDrone(drone, dronePosOffset + dronePos);
			}
			else
			{
				LOG_ERROR("drone type " << droneType << " is not recognized");
			}

			// add any motion capture markers
			if(drone)
			{
				for(j = 0; j < numMarkers; ++ j)
				{
					ss2.str("");
					ss2 << ss1.str() + ".marker[" << j << "]";
					markerPos.x = worldConfigFile -> getDouble(ss2.str() + ".pos[0]", 0.0f, NULL, true);
					markerPos.y = worldConfigFile -> getDouble(ss2.str() + ".pos[1]", 0.0f, NULL, true);
					markerPos.z = worldConfigFile -> getDouble(ss2.str() + ".pos[2]", 0.0f, NULL, true);
					drone -> addViconMarker(markerPos);
				}
			}
		}
	}
	else
	{
		LOG_ERROR("World::addSimulatedDrone() cannot add drones because no drone swarm manager was initialized");
	}
}

void World::update(Cursor *cursor, float dt)
{
	vector<DroneDetection> drones;
	stringstream ss;

	// update GUI elements if enabled
	if(gui)
	{
		// update widgets
		Form::update(cursor, dt);
		updateTimeLabel();

		// move the camera
		computeBrushVector(cursor);
		controlMouseSelection(cursor);
		controlCamera(cursor, dt);
	}

	// update any simulated world elements we have
	if(simulated)
	{
		droneSwarm -> update(dt);
	}

	// update sensing components
	drones.clear();
	sensingInterface -> update(dt);
	sensingInterface -> getDrones(drones);

	// update experiment logic

	// update timer
	currentTime += dt;

	// update command interface logic; we only send commands every 30th of a second or so
	droneInterfaceTimer += dt;
	//if(droneInterfaceTimer >= (1.0f / 30.0f))//0.02f)
	if((currentTime - lastInterfaceUpdateTime) >= (1.0f / 30.0f))
	{
		trial -> update(currentTime - lastInterfaceUpdateTime);//dt);

		LOG_INFO("---- trial update: tick = " << (currentTime - lastInterfaceUpdateTime));

		lastInterfaceUpdateTime = currentTime;

		//droneInterfaceTimer = 0.0f;
		droneInterface -> sendCommands(currentTime);
	}
	droneInterface -> clearCommands();

	// update FPS
	if(gui)
	{
		ss.str("");
		ss << "FPS: " << (int)round(1.0f / dt) << "        |      ";
		lblFPS -> setText(ss.str());
	}
}

void World::render()
{
	// render grid floor
	glEnable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	renderWorld();
	renderDrones();
	//renderDroneMarkers();

	// render camera data
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	renderDroneMarkers();
	//renderCamerasAndPoints();

	// render selection box
	renderMouseSelectionBox();

	// render trial-specific objects
	if(trial)
	{
		trial -> render(perspectiveViewProjection);
	}

	// render GUI elements
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	Quad::getInstance() -> setViewProjection(Application::getInstance() -> getScreenViewProjection());
	Form::render();
}

void World::renderWorld()
{
	// render grid floor
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	worldGridFloor -> render(mat4(perspectiveViewProjection));
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
}

void World::renderDrones()
{
	const vec4 X_AXIS_COLOR(1.0f, 0.0f, 0.0f, 1.0f);
	const vec4 Y_AXIS_COLOR(0.0f, 0.0f, 1.0f, 1.0f);
	const vec4 Z_AXIS_COLOR(0.0f, 1.0f, 0.0f, 1.0f);
	const float AXIS_LENGTH = 0.2f;

	const float DRONE_MODEL_SIZE = 0.09f;						// uBee drone is 9cm on a side

	const vec4 DRONE_ID_COLOR(1.0f, 1.0f, 1.0f, 1.0f);
	const float DRONE_ID_HEIGHT_TWEAK = 30.0f;

	vector<DroneDetection> drones;
	vector<DroneDetection>::iterator i;
	vec3 windowCoords;
	stringstream ss;

	vec3 pos;
	mat4 modelMat;

	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);

	solidShader -> bind();
	solidShader -> uniformMat4("u_VP", perspectiveViewProjection);

	// get drone detections
	sensingInterface -> getDrones(drones);

	// shadows should not write to depth buffer and do not self-occlude in the depth buffer
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// render fake shadows
	for(i = drones.begin(); i != drones.end(); ++ i)
	{
		// render fake shadow
		modelMat[0] = vec4(i -> side, 0.0f);
		modelMat[1] = vec4(i -> up, 0.0f);
		modelMat[2] = vec4(i -> forward, 0.0f);
		modelMat[3] = vec4(i -> pos.x, 0.01f, i -> pos.z, 1.0f);
		modelMat = scale(modelMat, vec3(DRONE_MODEL_SIZE * 1.1f, 0.000001f, DRONE_MODEL_SIZE * 1.1f));
		solidShader -> uniformVec4("u_Color", vec4(0.2f, 0.2f, 0.2f, 0.8f));
		solidShader -> uniformMat4("u_Model", modelMat);
		droneModel -> render();
	}

	// prepare to render solid object
	glDepthMask(GL_TRUE);
	glEnable(GL_DEPTH_TEST);

	// render drone model and axes
	for(i = drones.begin(); i != drones.end(); ++ i)
	{
		// render drone models
		modelMat[0] = vec4(i -> side, 0.0f);
		modelMat[1] = vec4(i -> up, 0.0f);
		modelMat[2] = vec4(i -> forward, 0.0f);
		modelMat[3] = vec4(i -> pos, 1.0f);
		modelMat = scale(modelMat, vec3(DRONE_MODEL_SIZE));
		solidShader -> uniformVec4("u_Color", vec4(1.0f, 1.0f, 1.0f, 1.0f));
		solidShader -> uniformMat4("u_Model", modelMat);
		droneModel -> render();

		// render drone axes
		lineRenderer -> add(i -> pos, i -> pos + (i -> forward * AXIS_LENGTH), Z_AXIS_COLOR);
		lineRenderer -> add(i -> pos, i -> pos + (i -> side * AXIS_LENGTH), X_AXIS_COLOR);
		lineRenderer -> add(i -> pos, i -> pos + (i -> up * AXIS_LENGTH), Y_AXIS_COLOR);
	}

	// enable blending and turn off depth testing
	glDepthMask(GL_TRUE);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	// render drone IDs
	for(i = drones.begin(); i != drones.end(); ++ i)
	{
		// build ID string
		ss.str("");
		ss << "[drone " << i -> id << "]";// " << (int)round(i -> pos.x / 0.422124f) << ", " << (int)round(i -> pos.z / 0.422124f);

		// project into window space
        windowCoords = project(i -> pos, mat4(1.0f), perspectiveViewProjection, viewport);
        textRenderer -> render(ss.str(), vec2(windowCoords.x, viewport.w - windowCoords.y - DRONE_ID_HEIGHT_TWEAK), DRONE_ID_COLOR, Text::ALIGN_CENTER);
	}
}

void World::renderDroneMarkers()
{
	const float MARKER_SIZE = 0.02f;						// Vicon markers we use are about 9.5mm in diameter, but make them larger so we can see them
	const vec4 MARKER_COLOR(1.0f, 1.0f, 1.0f, 1.0f);

	vector<Marker> markers;
	vector<Marker>::iterator i;

	vec3 pos;
	mat4 modelMat;

	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);

	solidShader -> bind();
	solidShader -> uniformMat4("u_VP", perspectiveViewProjection);
	solidShader -> uniformVec4("u_Color", MARKER_COLOR);

	sensingInterface -> getMarkers(markers);
	//LOG_INFO("detected " << markers.size() << " markers");
	for(i = markers.begin(); i != markers.end(); ++ i)
	{
		modelMat = mat4(1.0f);
		modelMat = translate(modelMat, i -> pos);
		modelMat = scale(modelMat, vec3(MARKER_SIZE));

		solidShader -> uniformMat4("u_Model", modelMat);
		//solidShader -> uniformVec4("u_Color", vec4(i -> color, 1.0f));
		markerModel -> render();
	}

	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
}

void World::renderMouseSelectionBox()
{
	const vec4 SELECTION_BOX_COLOR(0.1f, 0.5f, 1.0f, 0.5f);

	vec2 pos;
	vec2 size;

	if(mouseLeftButtonDown)
	{
		pos = mouseSelectionStart;
		size = mouseSelectionEnd - pos;
		Quad::getInstance() -> render(pos, size, SELECTION_BOX_COLOR);
	}
}

void World::mouseScrollCallback(Cursor *cursor, double x, double y)
{
	if	   (y < 0.0f) zoomOut = true;
	else if(y > 0.0f) zoomIn = true;
}

vec3 World::worldToScreen(const glm::vec3 &world)
{
	vec3 result = project(world, perspectiveView, perspectiveProjection, viewport);
	result.y = windowSize.y - result.y;				// GUI uses inverted y axis
    return result;
}

void World::updateTimeLabel()
{
	stringstream ss;

	ss << "Time: " << strFormatSeconds(currentTime);
	lblTime -> setText(ss.str());
}

void World::computeBrushVector(Cursor *cursor)
{
	const vec2 SIZE = getSize();
	const vec4 VIEWPORT(0.0f, 0.0f, SIZE.x, SIZE.y);

	vec2 cursorPos = cursor -> getPos();

	// correct mouse inverted y-axis and unproject between near and far planes to get the 3D vector of the mouse into the world from the camera
	cursorPos = vec2(cursorPos.x, SIZE.y - cursorPos.y);
	brushVector = -normalize(unProject(vec3(cursorPos, 0.0f), perspectiveView, perspectiveProjection, VIEWPORT) -
							 unProject(vec3(cursorPos, 1.0f), perspectiveView, perspectiveProjection, VIEWPORT));
}

void World::controlMouseSelection(Cursor *cursor)
{
/*
	vec2 topLeft, bottomRight;

	if(mouseLeftButtonDown)
	{
		mouseSelectionEnd = cursor -> getPos();
		if(!cursor -> isLeftButtonDown())
		{
			mouseLeftButtonDown = false;

			// adjust selection box so that the origin is top right
			topLeft = vec2(glm::min(mouseSelectionStart.x, mouseSelectionEnd.x), glm::min(mouseSelectionStart.y, mouseSelectionEnd.y));
			bottomRight = vec2(glm::max(mouseSelectionStart.x, mouseSelectionEnd.x), glm::max(mouseSelectionStart.y, mouseSelectionEnd.y));

			// shift adds to the selection
			if(Application::getInstance() -> getKeyDown(GLFW_KEY_LEFT_SHIFT))
			{
				addToSelection(topLeft, bottomRight);
			}
			else
			{
				setSelection(topLeft, bottomRight);
			}
		}
	}
	else
	{
		if(cursor -> isLeftButtonDown() && cursor -> getPos().x > 200.0f)
		{
			mouseSelectionStart = cursor -> getPos();
			mouseSelectionEnd = mouseSelectionStart;
			mouseLeftButtonDown = true;
		}
	}*/
}

void World::controlCamera(Cursor *cursor, float dt)
{
	const vec2 SIZE = getSize();
	const vec3 CAMERA_UP(0.0f, 1.0f, 0.0f);
	const float ZOOM_SPEED = 0.5f;
	const float TRANSLATE_SPEED = 0.01f;
	const float ROTATE_SPEED = 0.15f;

	vec3 cameraLook;
	vec2 cursorPos;
	vec2 cursorVel;
	vec3 cameraMoveLeft;
	vec3 cameraMoveForward;

	// determine if the cursor is in the screen
	cursorPos = cursor -> getPos();
	if(cursorPos.x >= 0.0f && cursorPos.x < SIZE.x && cursorPos.y >= 0.0f && cursorPos.y < SIZE.y)
	{
		// compute the left and forwards 3D camera vectors that we will translate our camera by
		cameraMoveLeft = vec3(-1.0f, 0.0f, 0.0f);
		cameraMoveLeft = rotate(cameraMoveLeft, radians(cameraYaw), vec3(0.0f, 1.0f, 0.0f));
		cameraMoveForward = vec3(-cameraMoveLeft.z, 0.0f, cameraMoveLeft.x);

		// zoom in to brush direction?
		if(zoomIn)
		{
			cameraPosTarget += brushVector * ZOOM_SPEED;
			zoomIn = false;
		}

		// zoom out to brush direction?
		if(zoomOut)
		{
			cameraPosTarget -= brushVector * ZOOM_SPEED;
			zoomOut = false;
		}

		// move, pitch, and yaw the camera
		if(cursor -> isMiddleButtonDown())
		{
			cursorVel = cursor -> getVel();
			if(Application::getInstance() -> getKeyDown(GLFW_KEY_LEFT_SHIFT))
			{
				//  hold down shift for translation
				cameraPosTarget += cameraMoveLeft * cursorVel.x * TRANSLATE_SPEED;
				cameraPosTarget += cameraMoveForward * cursorVel.y * TRANSLATE_SPEED;
			}
			else
			{
				// otherwise rotate
				cameraPitchTarget += cursorVel.y * ROTATE_SPEED;
				cameraYawTarget += cursorVel.x * ROTATE_SPEED;
			}
		}
	}

	// smooth motion to target camera position
	cameraPos = cameraPos + (cameraPosTarget - cameraPos) * 0.8f;

	// constrain pitch
	cameraPitchTarget = clamp(cameraPitchTarget, -89.0f, 89.0f);

	// smooth rotation to target camera orientation
	cameraPitch = curveAngle(cameraPitch, cameraPitchTarget, 0.8f);
	cameraYaw = curveAngle(cameraYaw, cameraYawTarget, 0.8f);

	// rotate the camera
	cameraLook = vec3(0.0f, 0.0f, -1.0f);
	cameraLook = rotate(cameraLook, radians(cameraPitch), vec3(1.0f, 0.0f, 0.0f));
	cameraLook = rotate(cameraLook, radians(cameraYaw), vec3(0.0f, 1.0f, 0.0f));

	// compute lookat matrix
	perspectiveView = lookAt(cameraPos, cameraPos + cameraLook, CAMERA_UP);
	perspectiveViewProjection = perspectiveProjection * perspectiveView;
}

void World::toggleSelection()
{
	/*if(selectedBirds.size())
	{
		clearSelection();
	}
	else
	{
		setSelectionAll();
	}*/
}

void World::setSelectionAll()
{/*
	vector<Bird*>::iterator i;

	selectedBirds.clear();
	for(i = allBirds.begin(); i != allBirds.end(); ++ i)
	{
		(*i) -> selected = true;
		selectedBirds.push_back(*i);
	}*/
}

void World::clearSelection()
{/*
	vector<Bird*>::iterator i;

	for(i = selectedBirds.begin(); i != selectedBirds.end(); ++ i)
	{
		(*i) -> selected = false;
	}
	selectedBirds.clear();*/
}

void World::invertSelection()
{/*
	vector<Bird*>::iterator i;

	selectedBirds.clear();
	for(i = allBirds.begin(); i != allBirds.end(); ++ i)
	{
		(*i) -> selected = !((*i) -> selected);
		if((*i) -> selected)
		{
			selectedBirds.push_back(*i);
		}
	}*/
}

void World::addToSelection(const vec2 &boxStart, const vec2 &boxEnd)
{/*
	vector<Bird*>::iterator i;
	vec3 screenPos;

	for(i = allBirds.begin(); i != allBirds.end(); ++ i)
	{
		screenPos = worldToScreen((*i) -> pos);
		if(isInBox(vec2(screenPos), boxStart, boxEnd) &&
		   !isSelected(*i))
		{
			(*i) -> selected = true;
			selectedBirds.push_back(*i);
		}
	}*/
}

void World::setSelection(const vec2 &boxStart, const vec2 &boxEnd)
{/*
	vector<Bird*>::iterator i;
	vec3 screenPos;

	selectedBirds.clear();
	for(i = allBirds.begin(); i != allBirds.end(); ++ i)
	{
		screenPos = worldToScreen((*i) -> pos);
		if(isInBox(vec2(screenPos), boxStart, boxEnd))
		{
			(*i) -> selected = true;
			selectedBirds.push_back(*i);
		}
		else
		{
			(*i) -> selected = false;
		}
	}*/
}

bool World::isSelected(Drone *drone)
{/*
	vector<Bird*>::iterator i = selectedBirds.begin();
	bool result = false;
	while(!result && i != selectedBirds.end())
	{
		result = bird == (*i++);
	}

	return result;*/
	return false;
}

void World::rebuildSelectionList()
{/*
	vector<Bird*>::iterator i;

	selectedBirds.clear();
	for(i = allBirds.begin(); i != allBirds.end(); ++ i)
	{
		if((*i) -> selected)
		{
			selectedBirds.push_back(*i);
		}
	}*/
}

bool World::isInBox(const glm::vec2 &pos, const glm::vec2 &topLeft, const glm::vec2 &bottomRight)
{
	return pos.x >= topLeft.x &&
		   pos.y >= topLeft.y &&
		   pos.x <= bottomRight.x &&
		   pos.y <= bottomRight.y;
}

// - - - GUI event handlers - - - //

void World::onViewDefault()
{
	//cameraPosTarget = vec3(-3.5f, 1.5f, 2.0f);
	cameraPosTarget = vec3(0.0f, 4.0f, 7.0f);
	cameraPitchTarget = -25.0f;
	//cameraPitch = 0.0f;
	cameraYawTarget = 0.0f;
	//cameraYaw = 0.0f;
}

void World::onViewTop()
{
	cameraPosTarget = vec3(0.0f, 8.0f, 0.0f);
	cameraPitchTarget = -90.0f;
	//cameraPitch = 0.0f;
	cameraYawTarget = 0.0f;
	//cameraYaw = 0.0f;
}

void World::ping()
{
	// this is a broadcasted ping
	if(droneInterface)
	{
		droneInterface -> ping(255);
	}
}

void World::startTrial()
{
	bool result = false;

	// unpause
	paused = false;

	// start the trial controller
	if(trial)
	{
		result = trial -> start();
	}

	// update GUI elements
	if(gui && result)
	{
		btnStart -> setEnabled(false);
		btnPause -> setEnabled(true);
		btnStopAndLand -> setEnabled(true);
		btnScram -> setEnabled(true);
	}
}

void World::pauseTrial()
{
	// set pause status
	paused = !paused;

	// pause the drone controller
	if(paused)
	{
		trial -> pause();
	}
	else
	{
		trial -> resume();
	}

	// update GUI elements
	if(gui)
	{
		if(paused)
		{
			btnPause -> setText("Resume");
		}
		else
		{
			btnPause -> setText("Pause");
		}
	}
}

void World::stopAndLand()
{
	bool result = false;

	// notify the user
	LOG_INFO("[WORLD] ***** STOP AND LAND *****");

	// unpause
	paused = false;

	// drone controller lands
	result = trial -> stopAndLand();

	// update GUI elements
	if(gui && result)
	{
		btnStart -> setEnabled(true);
		btnPause -> setEnabled(false);
		btnStopAndLand -> setEnabled(false);
	}
}

void World::scram()
{
	bool result = false;

	// notify the user
	LOG_INFO("[WORLD] ***** SCRAM *****");

	// unpause
	paused = false;

	// drone controller shuts down
	result = trial -> scram();

	// update GUI elements
	if(gui && result)
	{
		btnStart -> setEnabled(true);
		btnPause -> setEnabled(false);
		btnStopAndLand -> setEnabled(false);
	}
}
