#pragma once

#include "gumdrop/gumdrop.h"

#include "gui/form.h"
#include "gui/cursor.h"

#include "objects/point.h"

#include "experiments/trialcontroller.h"

#include "glm/glm.hpp"

#include <string>
#include <vector>

class GumdropNode;
class Shader;
class WavefrontModel;
class Model;

class CaptureCamera;
class Frame;
class Drone;
class Grid;

class Panel;
class Label;
class Button;
class Histogram;

class LineRenderer;
class Text;

class SimulatedDroneSwarm;
class SimulatedDrone;
class ViconInterface;
class DroneInterface;

class World : public Form
{
public:
	static World *getInstance();

	World(const glm::vec2 &windowSize, int toolbarSize, bool gui, const std::string &worldConfigFile, const std::string &trialConfigFile);
	~World();

	// initialize GUI elements, required by base class
	void initForm();

	// is the form requesting that the application close?
	bool shouldClose();

	// regular main loop processing, optionally required by base class
	void update(Cursor *cursor, float dt);
	void render();

	// input processing
	void mouseScrollCallback(Cursor *cursor, double x, double y);

	// simulation handling
	void addSimulatedDrone(SimulatedDrone *drone, const glm::vec3 &pos);
	void addSimulatedDronesFromWorldConfig();

	// misc
	glm::vec3 worldToScreen(const glm::vec3 &world);

private:
	static void mouseScrollCallback(GLFWwindow* window, double x, double y);

	std::string worldName;
	glm::vec2 worldSize;
	glm::vec2 sensorSize;

	GumdropNode *worldConfigFile;							// data file describing world properties
	GumdropNode *trialConfigFile;							// data file describing type of experiment and params

	TrialController *trial;									// controls the logic of each experimental trial
	SimulatedDroneSwarm *droneSwarm;						// simulated drones; NULL if not using the simulator
	SensingInterface *sensingInterface;						// interface to Vicon data; this might be for either simulated or real drones
	DroneInterface *droneInterface;							// interface to drone controller; this can be for either simulated or real drones
	Grid *worldGridFloor;									// grid floor indicating world size
	Grid *sensorGridFloor;									// grid floor indicating sensing area size
	bool gui;												// is the GUI enabled, or are we running headless?
	bool simulated;											// are we using a simulated Vicon?
	double currentTime;										// current time in seconds, starting from when application started
	double lastInterfaceUpdateTime;							// last time we issued a control update, in seconds
	bool paused;											// are we currently paused?

	float droneInterfaceTimer;

	Button *btnStart;
	Button *btnPause;
	Button *btnStopAndLand;
	Button *btnScram;

	LineRenderer *lineRenderer;
	Text *textRenderer;

	Shader *solidShader;									// simple shader for lighting solid objects

	WavefrontModel *droneWavefront;							// wavefront data associated with drone model
	Model *droneModel;										// GL mesh for drone model

	WavefrontModel *markerWavefront;						// wavefront data associated with marker model
	Model *markerModel;										// GL mesh for Vicon marker model

	glm::mat4 perspectiveProjection;
	glm::mat4 perspectiveView;
	glm::mat4 perspectiveViewProjection;
	glm::vec4 viewport;

	float cameraDistance;									// used to control camera distance and viewing angle
	float cameraAngleX;
	float cameraAngleY;

	bool mouseLeftButtonDown;
	glm::vec2 mouseSelectionStart;
	glm::vec2 mouseSelectionEnd;

	glm::vec3 cameraPos;
	glm::vec3 cameraPosTarget;
	float cameraPitchTarget;
	float cameraYawTarget;
	float cameraPitch;
	float cameraYaw;
	bool zoomIn;
	bool zoomOut;
	glm::vec3 brushVector;

	// GUI configuration
	glm::vec2 windowSize;
	int toolbarWidth;

	// GUI elements
	Panel *pnlToolbar;
	Panel *pnlMainArea;
	Label *lblFPS;
	Label *lblTime;

	// intialization functions
	void setupGUI();
	void openWindow();
	void prepareOpenGL();
	void prepareCameras();
	void setupViewingParams();

	// input handling
	void getKeyboardInput();

	// GUI handling
	void updateTimeLabel();

	// camera control
	void computeBrushVector(Cursor *cursor);
	void controlMouseSelection(Cursor *cursor);
	void controlCamera(Cursor *cursor, float dt);

	// rendering functions
	void renderWorld();
	void renderDrones();
	void renderDroneMarkers();
	void renderMouseSelectionBox();
	void renderGUI();

	// selection control
	void toggleSelection();
	void setSelectionAll();
	void clearSelection();
	void invertSelection();
	void addToSelection(const glm::vec2 &boxStart, const glm::vec2 &boxEnd);
	void setSelection(const glm::vec2 &boxStart, const glm::vec2 &boxEnd);
	bool isSelected(Drone *drone);
	void rebuildSelectionList();
	bool isInBox(const glm::vec2 &screenPos, const glm::vec2 &boxStart, const glm::vec2 &boxEnd);

	// - - - GUI event handlers - - - //

	void onViewDefault();
	void onViewTop();

	void ping();

	void startTrial();
	void pauseTrial();
	void stopAndLand();
	void scram();
};
