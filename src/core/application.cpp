#include "core/application.h"
#include "world/world.h"
#include "core/version.h"

#include "gui/cursor.h"

#include "rendering/quad.h"
#include "rendering/text.h"

#include "sensing/viconsensinginterface.h"

#include "util/log.h"
#include "util/rnd.h"
#include "util/progressbar.h"
#include "util/directories.h"
#include "util/gldebugging.h"
#include "util/arguments.h"

#include "glm/glm.hpp"
#include "glm/gtc/quaternion.hpp"
#include "glm/gtc/matrix_transform.hpp"
using namespace glm;

#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
using namespace std;

const int Application::FRAMES_PER_SECOND = 60;
const float Application::TARGET_FRAMERATE = 1.0 / (float)FRAMES_PER_SECOND;

const vec2 Application::CAMERA_IMAGE_SIZE(1620, 768);
const int Application::TOOLBAR_WIDTH = 200;
const vec2 Application::WINDOW_SIZE(CAMERA_IMAGE_SIZE.x + TOOLBAR_WIDTH, CAMERA_IMAGE_SIZE.y);

Application *Application::instance = NULL;

Application::Application()
	: cursor(NULL)
{
	reseed();

	// fire up the vicon system
	/*
	viconHostName = "vicon:801";
	vicon = new Vicon();
    if(!vicon -> connect(viconHostName))
    {
		LOG_ERROR("could not connect to vicon host \"" << viconHostName << "\"");
    }*/
}

Application::~Application()
{
	LOG_INFO("shutting down...");

	delete form;

	if(gui)
	{
		delete cursor;
	}
}

Application *Application::getInstance()
{
	if(instance == NULL)
	{
		instance = new Application();
	}

	return instance;
}

void Application::scrollCallback(GLFWwindow *window, double x, double y)
{
	if(instance && instance -> form)
	{
		instance -> form -> mouseScrollCallback(instance -> cursor, x, y);
	}
}

void Application::init(int args, char *argv[])
{
	Arguments a;
	string worldConfigFilename;
	string trialConfigFilename;

	a.registerBool("gui", true);
	a.registerString("world", true);
	a.registerString("trial", true);
	a.process(args, argv);

	gui = a.getBool("gui");
	worldConfigFilename = a.getString("world");
	trialConfigFilename = a.getString("trial");

	if(gui)
	{
		openWindow();
		prepareOpenGL();
		configureUtilities();
		cursor = new Cursor(window);
	}

	form = new World(WINDOW_SIZE, TOOLBAR_WIDTH, gui, worldConfigFilename, trialConfigFilename);
/*
	Arguments a;

	a.registerBool("gui", true);
	a.process(args, argv);

	gui = a.getBool("gui");
	if(gui)
	{
		openWindow();
		prepareOpenGL();
		configureUtilities();
		cursor = new Cursor(window);
	}

	form = new World(WINDOW_SIZE, TOOLBAR_WIDTH, gui, args, argv);

	// any other setup takes place here
*/
}

void Application::run()
{
	const double GUI_UPDATE_RATE = 0.016667f;
	const double LOGIC_UPDATE_RATE = 1.0 / 200.0;//0.004f;//0.008334f;

	// these handle our smooth frame timing if the GUI is enabled
	float dt;											// time since last cycle through loop (*not* time between update/render steps)
	double currentTime;									// current time according to GLFW
	double oldTime;										// what the value of currentTime was on the last cycle through loop
	double guiUpdateTimer = 0.0f;
	double logicUpdateTimer = 0.0f;
	double lastLogicTime;
	float tick;

	// update GUI elements if required; otherwise, just update program logic
	LOG_INFO("beginning main processing...");
	if(gui)
	{
		// prime our time tracking
		currentTime = glfwGetTime();
		oldTime = currentTime;
		lastLogicTime = currentTime;

		// loop until we're done
		while((!glfwWindowShouldClose(window) && /*!glfwGetKey(window, GLFW_KEY_ESCAPE)) && */!form -> shouldClose()))
		{
			// compute a time multiplier based on our frame rate
			oldTime = currentTime;
			currentTime = glfwGetTime();
			dt = currentTime - oldTime;

			// do we update the logic?
			logicUpdateTimer += dt;
			if(logicUpdateTimer >= LOGIC_UPDATE_RATE)
			{
				// compute delta time for update step
				tick = currentTime - lastLogicTime;
				lastLogicTime = currentTime;

				LOG_INFO("--------- LOGIC UPDATE STEP: dt is " << tick);

				// always update main elements
				cursor -> update();
				form -> update(cursor, tick);

				// subtract time remaining until next frame
				//logicUpdateTimer -= LOGIC_UPDATE_RATE;
				logicUpdateTimer = 0.0;
			}

			// do we update the GUI?
			guiUpdateTimer += dt;
			if(guiUpdateTimer >= GUI_UPDATE_RATE)
			{
				// clear our colour and depth buffers
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

				// render main program elements
				form -> render();

				// poll for new events and swap our back buffers
				glfwPollEvents();
				glfwSwapBuffers(window);

				// subtract time remaining until next frame
				//guiUpdateTimer -= GUI_UPDATE_RATE;
				guiUpdateTimer = 0.0;
			}
		}
	}
	else
	{
		while(!form -> shouldClose())
		{
			form -> update(NULL, TARGET_FRAMERATE);
		}
	}
}

bool Application::getKeyDown(int key)
{
	return glfwGetKey(window, key) == GLFW_PRESS;
}

void Application::getMousePos(double *mouseX, double *mouseY)
{
	glfwGetCursorPos(window, mouseX, mouseY);
}

bool Application::getMouseButton(int index)
{
	return glfwGetMouseButton(window, index);
}

float Application::getJoystickAxis(int axis)
{
	float value = 0.0f;
	int count;
	const float *axes = glfwGetJoystickAxes(GLFW_JOYSTICK_1, &count);

	// make sure axis index is valid
	if(axis >= 0 && axis < count)
	{
		value = axes[axis];
	}

	return value;
}

vec3 Application::worldToScreen(const glm::vec3 &world)
{
	return form -> worldToScreen(world);
}

mat4 Application::getScreenViewProjection()
{
	return orthoViewProj;
}

void Application::openWindow()
{
	const char *TITLE = "uBee Command Center";

	stringstream ss;
	GLenum error;

	// we need to intialize GLFW before we create a GLFW window
	if(!glfwInit())
	{
		LOG_ERROR("Viewer::openWindow() could not initialize GLFW");
	}

	// explicitly set our OpenGL context to something that doesn't support any old-school shit
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, false);
	glfwWindowHint(GLFW_REFRESH_RATE, 60);
	#ifdef DEBUG
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
	#endif

	// build title string
	ss << TITLE << " v" << VERSION_MAJOR << "." << VERSION_MINOR << "." << VERSION_REVISION;

	// create our OpenGL window using GLFW
    window = glfwCreateWindow(WINDOW_SIZE.x,				// specify width
							  WINDOW_SIZE.y,				// specify height
							  ss.str().c_str(),				// title of window
							  #ifdef DEBUG
								NULL,
							  #else
								NULL,//glfwGetPrimaryMonitor(),		// fullscreen mode
							  #endif
							  NULL);						// not sharing resources across monitors
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);

	// disable the cursor
	//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// configure our viewing area
	glViewport(0, 0, WINDOW_SIZE.x, WINDOW_SIZE.y);

	// enable our extensions handler
	LOG_INFO("initializing GLEW...");
	glewExperimental = true;		// GLEW bug: glewInit() doesn't get all extensions, so we have it explicitly search for everything it can find
	error = glewInit();
	if(error != GLEW_OK)
	{
		cerr << glewGetErrorString(error) << endl;
		exit(1);
	}

	// clear the OpenGL error code that results from initializing GLEW
	glGetError();

	// print our OpenGL version info
	LOG_INFO("-- GL version:   " << (char*)glGetString(GL_VERSION));
    LOG_INFO("-- GL vendor:    " << (char*)glGetString(GL_VENDOR));
    LOG_INFO("-- GL renderer:  " << (char*)glGetString(GL_RENDERER));
	LOG_INFO("-- GLSL version: " << (char*)glGetString(GL_SHADING_LANGUAGE_VERSION));

	// OpenGL callback debugging is extremely useful, so enable it if we're in debug mode
	#ifdef DEBUG
		initGLDebugger();
	#else
		LOG_INFO("-- GL callback debugging is disabled in non-Debug configurations");
	#endif
}

void Application::prepareOpenGL()
{
	// some sane rendering settings
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glEnable(GL_CULL_FACE);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// black background
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void Application::configureUtilities()
{
	const float NEAR = -1000.0f;
	const float FAR = 1000.0f;

	mat4 proj = ortho(0.0f, WINDOW_SIZE.x,  WINDOW_SIZE.y, 0.0f, NEAR, FAR);
	mat4 view = lookAt(vec3(0.0f, 0.0f, 1.0f), vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f));

	orthoViewProj = view * proj;

	Quad::getInstance() -> setViewProjection(orthoViewProj);
	Text::getInstance() -> setViewProjection(orthoViewProj);

	glfwSetScrollCallback(window, scrollCallback);
}

void Application::reseed()
{
	FILE *fp;
	uint32_t seed;

	// open random device and read from it
	fp = fopen("/dev/urandom", "r");
	if(fp)
	{
		if(fread(&seed, 1, sizeof(uint32_t), fp) == 0)
		{
			LOG_INFO("[WARN] Application::reseed() could not open /dev/urandom; using time(NULL)");
			seed = time(NULL);
		}
		fclose(fp);
	}
	else
	{
		LOG_INFO("[WARN] Application::reseed() could not open /dev/urandom; using time(NULL)");
		seed = time(NULL);
	}

	// reseed random number generator
	rndSeed(seed);
	srand(seed);
}
