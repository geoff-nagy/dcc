#pragma once

#include "util/arguments.h"

#include "GL/glew.h"

#include "GLFW/glfw3.h"

#include "glm/glm.hpp"

#include <string>
#include <map>

class Arguments;
class Vicon;
class World;
class Cursor;

class Application
{
public:
	static Application *getInstance();
	static void scrollCallback(GLFWwindow *window, double x, double y);

	~Application();

	// core functionality
	void init(int args, char *argv[]);
	void run();

	// input
	bool getKeyDown(int key);
	void getMousePos(double *x, double *y);
	bool getMouseButton(int button);
	float getJoystickAxis(int axis);

	// misc
	glm::vec3 worldToScreen(const glm::vec3 &world);
	glm::mat4 getScreenViewProjection();

private:
	static const int FRAMES_PER_SECOND;
	static const float TARGET_FRAMERATE;

	static const glm::vec2 CAMERA_IMAGE_SIZE;
	static const int TOOLBAR_WIDTH;
	static const glm::vec2 WINDOW_SIZE;

	static Application *instance;

	Application();

	void openWindow();
	void prepareOpenGL();
	void configureUtilities();
	void reseed();

	bool gui;
	std::string trialName;
	GLFWwindow *window;										// handle to GL/GLFW window and input reading
	glm::mat4 orthoViewProj;

	Vicon *vicon;
	World *form;
	Cursor *cursor;
	std::string viconHostName;
};
