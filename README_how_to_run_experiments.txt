How to Mass-Run DCC Experiments/Simulations
-------------------------------------------

First, change into the directory of the DCC executable:

	cd dcc/linux-gcc

Now, invoke the Python script that runs the experiments:

	python3 ../scripts/run_experiments.py 12 ../config/worlds/open_environment_mar_3.gum ../scripts/trials/mar_3/trial

You should get a confirmation prompt. If everything looks good, enter 'y' to proceed.

Final results, in the case of the example below, will appear in the directory:

	../scripts/trials/mar_3/results

Finished trials will appear in a similar directory, because they are moved there, one-at-a-time, when they are completed:

	../scripts/trials/mar_3/finished

The top-level trials directory,

	../scripts/trials/mar_3

should be empty once the trials have completed.

End of documentation
--------------------
