#version 330 core

uniform mat4 u_VP;											// our view-projection matrix
uniform mat4 u_Model;

layout(location = 0) in vec2 a_Vertex;

out vec2 v_TexCoord;

void main()
{
	gl_Position = u_VP * u_Model * vec4(a_Vertex.x, a_Vertex.y, 0.0, 1.0);
}
