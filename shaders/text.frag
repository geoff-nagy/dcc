#version 330 core

uniform sampler2D u_Texture;
uniform vec4 u_Color;

in vec2 v_TexCoord;

out vec4 f_Color;

void main()
{
	float luminance = texture(u_Texture, v_TexCoord).r;
    f_Color = vec4(u_Color.rgb, luminance * u_Color.a);
}
