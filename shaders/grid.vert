#version 330

uniform mat4 u_ViewProjection;							// camera projection (e.g., orthographic, perspective)

layout(location = 0) in vec3 a_Vertex;					// incoming position of vertex

void main()
{
	// matrix transformations are always evaluated in reverse order, so the "vertex" vector
	// multiplication here is done first, which is what we want, and so on...
	gl_Position = u_ViewProjection * vec4(a_Vertex, 1.0);
}
