#version 330

in vec4 v_Color;

out vec4 f_FragColor;

void main()
{
	// all we need to do is assign the outgoing fragment color to the interpolated vertex color value
	f_FragColor = v_Color;
}
