#version 330

uniform mat4 u_ViewProjection;

layout(location = 0) in vec3 a_Vertex;				// incoming position of vertex
layout(location = 1) in vec4 a_Color;				// incoming colour of vertex
layout(location = 2) in float a_Size;				// incoming size of vertex point

out vec4 v_Color;

void main()
{
	// assign vertex colour
	v_Color = a_Color;

	// matrix transformations are always evaluated in reverse order, so the "vertex" vector
	// multiplication here is done first, which is what we want, and so on...
	gl_Position = u_ViewProjection * vec4(a_Vertex, 1.0);

	// set the point size
	gl_PointSize = a_Size;
}
