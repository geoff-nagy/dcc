#version 330

uniform mat4 u_ViewProjection;

layout(location = 0) in vec3 a_Vertex;
layout(location = 1) in vec4 a_Color;

out vec4 v_Color;

void main()
{
	v_Color = a_Color;
    gl_Position = u_ViewProjection * vec4(a_Vertex, 1.0);
}
