#version 330 core

uniform mat4 u_VP;											// our view-projection matrix
uniform mat4 u_Model;

layout(location = 0) in vec2 a_Vertex;
layout(location = 1) in vec2 a_TexCoord;

out vec2 v_TexCoord;

void main()
{
	v_TexCoord = a_TexCoord;
	gl_Position = u_VP * u_Model * vec4(a_Vertex.x, a_Vertex.y, 0.0, 1.0);
}
