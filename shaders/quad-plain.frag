#version 330 core

uniform vec4 u_Color;

out vec4 f_FragColor;

void main()
{
	f_FragColor = u_Color;
}
