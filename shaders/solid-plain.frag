#version 330 core

uniform vec4 u_Color;
uniform vec3 u_SunColor;
uniform vec3 u_SunDirection;
uniform vec3 u_AmbientColor;

in vec3 v_Normal;

out vec4 f_FragColor;

void main()
{
	vec3 N = normalize(v_Normal);
	vec3 L = normalize(-u_SunDirection);
	float NdotL = max(0.0, dot(N, L));

	f_FragColor = vec4((u_AmbientColor * u_Color.rgb) + (u_Color.rgb * u_SunColor * NdotL), u_Color.a);
}
