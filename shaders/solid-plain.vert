#version 330 core

uniform mat4 u_VP;											// our view-projection matrix
uniform mat4 u_Model;

layout(location = 0) in vec3 a_Vertex;
layout(location = 1) in vec3 a_Normal;

out vec3 v_Normal;

void main()
{
	v_Normal = normalize(mat3(u_Model) * a_Normal);
	gl_Position = u_VP * u_Model * vec4(a_Vertex.x, a_Vertex.y, a_Vertex.z, 1.0);
}
