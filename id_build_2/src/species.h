#pragma once

#include "constants.h"

#include "glm/glm.hpp"

#include <string>

class Species
{
public:
	static Species *fromRandom();
	static Species *fromMutation(Species *species);
	static Species *fromCrossover(Species *s1, Species *s2);

	Species();
	~Species();

	//bool operator == (const Species &other) const;

	std::string toStr() const;
	std::string getPlacementStr() const;
	char getToken() const;

	void savePositionsToFile(const std::string &name);

//private:

	static void getRandomFocalPos(glm::vec3 *v1, glm::vec3 *v2);
	static glm::vec3 getRandomIDPos();

	static const glm::vec3 FOCAL_MARKER_1_BASE;
	static const glm::vec3 FOCAL_MARKER_2_BASE;
	static const glm::vec3 ID_MARKER_BASE;

	void computeHash();

	glm::vec3 focalMarker1;
	glm::vec3 focalMarker2;
	glm::vec3 idMarkerOffsets[10];

	char token;
	uint32_t hash;
};
