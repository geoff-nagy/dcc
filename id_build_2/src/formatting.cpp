#include "formatting.h"

#include <string>
#include <sstream>
using namespace std;

string formatSeconds(uint64_t seconds)
{
	uint32_t secs = seconds % 60;
	uint32_t mins = seconds / 60;
	uint32_t hrs = mins / 60;
	uint32_t days = hrs / 24;
	stringstream ss;

	// now apply modulus to get properly-wrapped amounts
	mins %= 60;
	hrs %= 24;
	days %= 365;

	// days, hours, and minutes are optional
	if(days)
	{
		ss << days << "d " << hrs << "h " << mins << "m ";
	}
	else if(hrs)
	{
		ss << hrs << "h " << mins << "m ";
	}
	else if(mins)
	{
		ss << mins << "m ";
	}

	// always print seconds
	ss << secs << "s";
	return ss.str();
}

string formatNumber(uint64_t number)
{
	stringstream ss;
	string str;
	string result = "";
	int64_t i;

	ss << number;
	str = ss.str();
	for(i = str.size() - 1; i >= 0; -- i)
	{
		result = str[i] + result;
		if(((int)str.size() - i) % 3 == 0 && i > 0) result = "," + result;
	}

	return result;
}
