#include "species.h"
#include "evaluator.h"
#include "rnd.h"

#include "xxhash/xxhash.h"

#include "glm/glm.hpp"
#include "glm/gtc/random.hpp"
using namespace glm;

#include <string.h>
#include <string>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <iostream>
using namespace std;

const vec3 Species::FOCAL_MARKER_1_BASE = vec3(-17.5f, 0.0f, -17.5f);
const vec3 Species::FOCAL_MARKER_2_BASE = vec3(17.5f, 0.0f, -17.5f);
const vec3 Species::ID_MARKER_BASE = vec3(0.0f, 0.0f, 22.0f);

Species::Species()
{
	token = '0';
	hash = 0;
}

Species::~Species() { }

Species *Species::fromRandom()
{
	Species *result = new Species();
	uint32_t i;

	// randomize base offset
	getRandomFocalPos(&(result -> focalMarker1), &(result -> focalMarker2));

	// randomize ID offsets
	for(i = 0; i < 10; ++ i)
	{
		result -> idMarkerOffsets[i] = getRandomIDPos();
	}

	// assign token to indicate how we generated this species and compute its hash
	result -> token = 'R';
	result -> computeHash();

	return result;
}

Species *Species::fromMutation(Species *species)
{
	Species *result = new Species();
	uint32_t i;

	// half chance we randomize focal markers
	if(rndBool())
	{
		getRandomFocalPos(&(result -> focalMarker1), &(result -> focalMarker2));
	}
	else
	{
		result -> focalMarker1 = species -> focalMarker1;
		result -> focalMarker2 = species -> focalMarker2;
	}

	// randomize ID offsets
	for(i = 0; i < 10; ++ i)
	{
		if(rndBool())
		{
			result -> idMarkerOffsets[i] = getRandomIDPos();
		}
		else
		{
			result -> idMarkerOffsets[i] = species -> idMarkerOffsets[i];
		}
	}

	// assign token to indicate how we generated this species and compute its hash
	result -> token = 'M';
	result -> computeHash();

	return result;
}

Species *Species::fromCrossover(Species *s1, Species *s2)
{
/*
	Species *result = new Species(s1 -> numStates);
	uint32_t i;

	for(i = 0; i < result -> numRules; ++ i)
	{
		result -> rules[i] = rndBool() ? s1 -> rules[i] : s2 -> rules[i];
	}

	// assign token to indicate how we generated this species and compute its hash
	result -> token = 'C';
	result -> correct();
	result -> computeHash();

	return result;*/
	return NULL;
}
/*
bool Species::operator == (const Species &other) const
{
	return memcmp(hash, other.hash, sizeof(uint32_t)) == 0;
}*/

string Species::toStr() const
{
	stringstream ss;

    // print a token marker, the hex identifier, etc.
    ss << token << "          ";
	ss << hex << setw(8) << setfill('0') << hash << "      ";

    return ss.str();
}

string Species::getPlacementStr() const
{
	stringstream ss;
	vec3 focalPoint;
	uint32_t i;

	// print focal marker positions
	ss << fixed << setprecision(2);
	ss << "        marker         x      y      z" << endl;
	ss << "--------------------------------------" << endl;
	ss << "Focal marker 1 : " << setw(6) << focalMarker1.x << ", " << setw(6) << focalMarker1.y << ", " << setw(6) << focalMarker1.z << endl;
	ss << "Focal marker 2 : " << setw(6) << focalMarker2.x << ", " << setw(6) << focalMarker2.y << ", " << setw(6) << focalMarker2.z << "    (dist: " << setw(6) << length(focalMarker1 - focalMarker2) << ")" << endl;
	ss << "---------------------------------------" << endl;

	// print ID marker positions
	focalPoint = (focalMarker1 + focalMarker2) / 2.0f;
	for(i = 0; i < 10; ++ i)
	{
		ss << "          ID " << (int)i << " : " << setw(6) << idMarkerOffsets[i].x << ", " << setw(6) << idMarkerOffsets[i].y << ", " << setw(6) << idMarkerOffsets[i].z << "    (dist: " << setw(6) << length(focalPoint - idMarkerOffsets[i]) << ")" << endl;
	}

	return ss.str();
}

char Species::getToken() const
{
	return token;
}

void Species::savePositionsToFile(const string &name)
{
	ofstream file;

	file.open(name.c_str());
	if(file.is_open())
	{
		file << getPlacementStr();
		file.close();
	}
	else
	{
		cout << "[WARNING]: cannot open \"" << name << "\" for writing" << endl;
	}
}

void Species::computeHash()
{
	uint32_t totalSize = sizeof(vec3) * 12;
	uint8_t *data = new uint8_t[totalSize];
	uint8_t *ptr = data;

	memcpy(ptr, &focalMarker1,   sizeof(vec3));       ptr += sizeof(vec3);
	memcpy(ptr, &focalMarker2,   sizeof(vec3));       ptr += sizeof(vec3);
	memcpy(ptr, idMarkerOffsets, sizeof(vec3) * 10);

	hash = XXH32(data, totalSize, 0);
	delete[] data;
}

void Species::getRandomFocalPos(vec3 *v1, vec3 *v2)
{
	const float MIN_OFFSET_X = -10.0f;
	const float MAX_OFFSET_X = 10.0f;

	const float MIN_OFFSET_Y = 0.0f;
	const float MAX_OFFSET_Y = 15.0f;

	vec3 offset = vec3(linearRand(MIN_OFFSET_X, MAX_OFFSET_X), linearRand(MIN_OFFSET_Y, MAX_OFFSET_Y), 0.0f);
	*v1 = FOCAL_MARKER_1_BASE + offset;
	*v2 = FOCAL_MARKER_2_BASE + offset;
}

vec3 Species::getRandomIDPos()
{
	const float MIN_OFFSET_X = -10.0f;
	const float MAX_OFFSET_X = 10.0f;

	const float MIN_OFFSET_Y = 0.0f;
	const float MAX_OFFSET_Y = 15.0f;

	const float MIN_OFFSET_Z = 0.0f;
	const float MAX_OFFSET_Z = 20.0f;

    return ID_MARKER_BASE + vec3(linearRand(MIN_OFFSET_X, MAX_OFFSET_X),
								 linearRand(MIN_OFFSET_Y, MAX_OFFSET_Y),
								 linearRand(MIN_OFFSET_Z, MAX_OFFSET_Z));
}
