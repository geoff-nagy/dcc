#pragma once

#include "species.h"

#include <stdint.h>
#include <string>

class SpeciesEval
{
public:
	SpeciesEval();
	SpeciesEval(Species *species);
	SpeciesEval(const SpeciesEval &eval);
	~SpeciesEval();

	SpeciesEval &operator = (const SpeciesEval &other);
	bool operator < (const SpeciesEval &other) const;
	bool operator == (const SpeciesEval &other);

	void reset();

	std::string toStr();

	Species *species;
	float minDistance;			// minimum distance separating any two IDs
	uint32_t age;				// how many generations we've been around for
};
