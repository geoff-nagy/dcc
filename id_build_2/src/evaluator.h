#pragma once

#include <ostream>
#include <stdint.h>

class Species;

float evaluate(Species *species);

//bool evaluateFSSP(Species *species, uint32_t numMachines, uint32_t *earlyFirings, uint32_t *perfectFirings, bool print = false);
//uint32_t evaluateFSSP(uint8_t **machines, uint32_t *firingTimes, uint32_t numMachines, Species *species, bool print);
//bool evaluate(Species *species, uint32_t numMachines, std::ostream *stream);
