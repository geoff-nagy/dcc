#include "evaluator.h"
#include "species.h"

#include "glm/glm.hpp"
using namespace glm;

#include <string.h>
#include <iostream>
#include <iomanip>
#include <algorithm>
using namespace std;

float evaluate(Species *species)
{
	const float MIN_DIFF_FROM_FOCAL_SPREAD = 2.0f;
	const float MIN_DIFF_FROM_FOCAL_POINT = 35.0f;

	uint32_t i, j;
	vec3 pos1, pos2;
	float dist1, dist2;
	float pairDist;
	float minDist = FLT_MAX;
	vec3 focalCenter = (species -> focalMarker1 + species -> focalMarker2) / 2.0f;
	float focalSpread = length(species -> focalMarker1 - species -> focalMarker2);

	for(i = 0; i < 10; ++ i)
	{
		// make sure our distance to the focal center is not too close to the
		// distance between the focal markers, and that all of our markers are
		// at least a certain distance apart
		pos1 = species -> idMarkerOffsets[i];//getMarkerPos(i);
		dist1 = length(pos1 - focalCenter);
		if(fabs(dist1 - focalSpread) >= MIN_DIFF_FROM_FOCAL_SPREAD &&					// distance must be far enough away from focal spread
		   length(pos1 - species -> focalMarker1) >= MIN_DIFF_FROM_FOCAL_POINT &&		// marker must be far enough away from focal point 1
		   length(pos1 - species -> focalMarker2) >= MIN_DIFF_FROM_FOCAL_POINT)			// marker must be far enough away from focal point 2
		{
			// now check inter-ID distances
			for(j = i + 1; j < 10; ++ j)
			{
				// get the distance to the focal center for the current pair of IDs
				pos2 = species -> idMarkerOffsets[j];//getMarkerPos(j);
				dist2 = length(pos2 - focalCenter);

				// track the lowest difference in distance between all pairs of
				// ID markers from the focal center
				pairDist = fabs(dist1 - dist2);
				if(pairDist < minDist)
				{
					minDist = pairDist;
				}
			}
		}
		else
		{
			return 0.0f;
		}
	}

	return minDist;
}
