#include "engine.h"
#include "specieseval.h"
#include "species.h"
#include "constants.h"
#include "rnd.h"
#include "progressbar.h"
#include "formatting.h"
#include "evaluator.h"
#include "util.h"

#include <string.h>
#include <unistd.h>
#include <float.h>

#include <cmath>
#include <ctime>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <sys/random.h>
using namespace std;

// - - - class implementation - - - //

Engine::Engine(uint32_t numGenerations)
	: numGenerations(numGenerations)
{
	uint32_t randomSeed;

	// seed the global Mersenne Twister using /dev/urandom
	if(getrandom(&randomSeed, sizeof(uint32_t), 0) == 0)
	{
		cout << "[WARNING]: getrandom() returned 0" << endl;
	}
	rndSeed(randomSeed);
}

Engine::~Engine() { }

void printBytes(uint8_t *data, uint32_t len)
{
	uint64_t i;

	cout << "[";
	for(i = 0; i < len; ++ i)
	{
		cout << setw(3) << (int)data[i];
		if(i < len - 1)
		{
			cout << "  ";
		}
	}
	cout << "]";
}

void Engine::terminate()
{
	quit = true;
}

void Engine::run()
{
	const int MAX_SPECIES = 1000000;					// each generation should have this many species
	const int PRINT_COUNT = 50;							// print the top PRINT_COUNT after every epoch
	const int ELITE_SIZE = 1000;
	const int ELITE_CHILDREN = (ELITE_SIZE * (ELITE_SIZE + 1)) / 2;

	stringstream ss;
	uint32_t gen;
	uint32_t j, k;
	uint32_t children;
	uint64_t startTime;
	SpeciesEval mutated;

	vector<Species*> garbage;
	vector<SpeciesEval> species;
	vector<SpeciesEval>::iterator i;

	// intro
	cout << "--------------------------------------------------------------------------" << endl;
	cout << "IDBuildGA: A genetic algorithm for finding marker placements for the uBee" << endl;

	// print details before we start
	cout << "--------------------------------------------------------------------------" << endl;
	cout << "number of generations : " << numGenerations << endl;
	cout << "--------------------------------------------------------------------------" << endl;

	// start the timer
	startTime = utGetTimeSeconds();

	// initialize the population
	while(species.size() < MAX_SPECIES)
	{
		if(species.size() % 5000 == 0)
		{
			cout << "initializing specimens..." << (species.size() * 100) / MAX_SPECIES << "%   \r";
		}
		species.push_back(SpeciesEval(Species::fromRandom()));
	}
	cout << "initializing specimens...done" << endl;
	cout << "--------------------------------------------------------------------------" << endl;

	// run evolution
	gen = 0;
	quit = false;
	while(!quit && gen < numGenerations)
	{
		// everybody produces a mutation, and the better one of each pair survives (or the newer one, in the case of ties)
		for(i = species.begin(), j = 0; i != species.end(); ++ i, ++ j)
		{
			// show progress
			if(j % 60000 == 0)
			{
				ss.str("");
				ss << "  evaluating generation " << left << setw(5) << (gen + 1) << ": ";
				printProgressBar((j * 100) / species.size(), ss.str());
				fflush(stdout);
			}

			// evaluate both; note that if a species has already been evaluated it is not re-evaluated
			evaluateFitness(*i);
			mutated = SpeciesEval(Species::fromMutation(i -> species));
			evaluateFitness(mutated);

			// keep the better of the two, or whichever is newest in the case of ties; this allows us to move along a flat fitness landscape;
			// note also that our comparison operator < ranks *higher scoring* species lower
			if(*i < mutated)
			{
				delete mutated.species;
			}
			else
			{
				delete i -> species;
				*i = mutated;
				i -> species -> token = 'M';
			}

			// age the survivor
			++ (i -> age);
		}

		// final progress bar
		printProgressBar(100, ss.str());

		// sort the population
		sort(species.begin(), species.end());

		// output the sorted population
		cout << "\r";
		print(species, gen, PRINT_COUNT - 16, startTime);

		// output the evaluation of the best
		ss.str("");
		//evaluate(species[0].species, species[0].maxMachines, &ss);
		cout << "--------------------------------------------------------------------------" << endl;
		cout << "BEST SO FAR (" << hex << setw(8) << setfill('0') << species[0].species -> hash << dec << ", dist = " << species[0].minDistance << ", a = " << species[0].age << "):" << endl;
		cout << species[0].species -> getPlacementStr() << endl;
		cout << ss.str();
		ss.str("");

		// advance generation
		++ gen;

		// - - - elitism - - - //

		// only reproduce if this is not the final generation
		if(gen < numGenerations)
		{
			// remove all but the elite
			eraseAndCollect(species, species.begin() + ELITE_SIZE, species.end(), garbage);

			// mutate existing elite, with number of children inversely proportional to fitness
			children = 0;
			for(j = 0; j < ELITE_SIZE; ++ j)
			{
				for(k = 0; k <= j; ++ k)
				{
					mutated = SpeciesEval(Species::fromMutation(species[j].species));
					species.push_back(mutated);

					++ children;
					if(children % 60000 == 0)
					{
						ss.str("");
						ss << "  mutating generation   " << left << setw(5) << (gen + 1) << ": ";
						printProgressBar((children * 100) / ELITE_CHILDREN, ss.str());
						fflush(stdout);
					}
				}
			}
			printProgressBar(100, ss.str());

			// now populate the remaining space with random individuals that will generate offspring and compete with later
			while(species.size() < MAX_SPECIES)
			{
				species.push_back(SpeciesEval(Species::fromRandom()));
			}
		}

		// clean up after this generation (goddamn kids!)
		destroyGarbage(garbage);
		fflush(stdout);
	}

	// report
	cout << "--------------------------------------------------------------------------" << endl;
	if(quit) cout << "ABORTED AT GENERATION " << gen + 1 << ". ";
	else     cout << "COMPLETE. ";
	cout << "BEST DISTANCE WAS " << bestDistance << "." << endl;

	// save to file
	saveToReportFile(species[0].species -> hash, getReportString(species[0], startTime));

	// final clean up
	eraseAndCollect(species, species.begin(), species.end(), garbage);
	destroyGarbage(garbage);
}

// - - - private methods - - - //

void Engine::evaluateFitness(SpeciesEval &eval)
{
	if(eval.minDistance <= 0.0f)
	{
		eval.minDistance = evaluate(eval.species);
		if(eval.minDistance > bestDistance)
		{
			bestDistance = eval.minDistance;
		}
	}
	// [todo]
/*
	const uint32_t MAX_MACHINES = 1000;

	uint32_t earlyFirings;
	uint32_t perfectFirings;
	bool keepEvaluating;
	uint32_t i;
	stringstream ss;

	// don't bother evaluating if we've done it already
	if(eval.maxMachines == 0)
	{
		// evaluate the current species with consecutively increasing number of machines
		keepEvaluating = true;
		i = 2;
		while(keepEvaluating && i < MAX_MACHINES)
		{
			keepEvaluating = evaluateFSSP(eval.species, i, &earlyFirings, &perfectFirings, false);
			if(keepEvaluating)
			{
				++ i;
			}
		}

		// assign score
		eval.maxMachines = i;
		eval.earlyFirings = earlyFirings;
		eval.perfectFirings = perfectFirings;

		// save best machine count for final report; note that we subtract -1 because eval.maxMachines
		// is the current number of machines we're trying to solve, but haven't yet
		if(eval.minDistance > bestDistance)
		{
			bestDistance = eval.minDistance;
		}
	}*/
}

void Engine::eraseAndCollect(vector<SpeciesEval> &values, vector<SpeciesEval>::iterator begin, vector<SpeciesEval>::iterator end, vector<Species*> &garbage)
{
	vector<SpeciesEval>::iterator i;

	for(i = begin; i != end; ++ i)
	{
		garbage.push_back(i -> species);
	}
	values.erase(begin, end);
}

void Engine::destroyGarbage(vector<Species*> &garbage)
{
	vector<Species*>::iterator i;

	for(i = garbage.begin(); i != garbage.end(); ++ i)
	{
		delete *i;
	}
	garbage.clear();
}

void Engine::print(vector<SpeciesEval> &evals, uint32_t generationIndex, uint32_t cutoff, time_t startTime)
{
	uint32_t i;

	cout << "=========================================================================" << endl;
	cout << "           TOP " << cutoff << " IN GENERATION " << (generationIndex + 1) << " ";
	cout << "(time: " << utFormatTime(utGetTimeSeconds() - startTime) << "):" << endl;
	cout << "-------------------------------------------------------------------------" << endl;
	cout << "  token      hash            d      a";
	cout << endl;
	cout << "-------------------------------------------------------------------------" << endl;

	for(i = 0; i < cutoff; ++ i)
	{
		cout << evals[i].toStr() << endl;
	}
}

string Engine::getReportString(SpeciesEval &species, time_t startTime)
{
	stringstream ss;

	// summary
	ss << "POST-RUN IDBUILDGA SUMMARY" << endl;
	ss << "-----------------------" << endl;
	ss << "number of generations : " << numGenerations << endl;
	ss << "time to completion    : " << utFormatTime(utGetTimeSeconds() - startTime) << endl;
	ss << "best distance         : " << bestDistance << endl;
	ss << endl;

	// show evaluation stats
	ss << "TOP SPECIMEN STATS" << endl;
	ss << "------------------" << endl;
	ss << "token           : " << species.species -> token << endl;
	ss << "hash            : " << hex << setw(8) << setfill('0') << species.species -> hash << endl;
	ss << dec << setfill(' ');
	ss << "min distance    : " << species.minDistance << endl;
	ss << "age             : " << species.age << endl;
	ss << endl;

	// print resulting positions
	ss << "MARKER PLACEMENT" << endl;
	ss << "----------------" << endl;
	ss << species.species -> getPlacementStr() << endl;

	// done
	ss << "END OF REPORT" << endl;
	return ss.str();
}

void Engine::saveToReportFile(uint32_t hash, const string &report)
{
	stringstream ss;
	ofstream file;

	// build the filename
	ss << "reports/report_" << bestDistance << "_" << hex << setw(8) << setfill('0') << hash << ".txt";
	ss << dec << setfill(' ');

	// open and save the file
	file.open(ss.str().c_str(), ofstream::out);
	if(file.is_open())
	{
		file << report;
		file.close();
	}
	else
	{
		cout << "[WARNING]: could not save output file " << ss.str() << endl;
	}
}
