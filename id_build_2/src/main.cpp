#include "engine.h"
#include "species.h"

#include <signal.h>
#include <cmath>
#include <ctime>
#include <iostream>
using namespace std;

// - - - prototypes - - - //

void sigIntHandlerFunc(int s);

// - - - globals - - - //

Engine *engine = NULL;

// - - - main function - - - //

int main(int args, char *argv[])
{
	struct sigaction sigIntHandler;

	if(args == 2)
	{
		// set up an interrupt catcher
		sigIntHandler.sa_handler = sigIntHandlerFunc;
		sigemptyset(&sigIntHandler.sa_mask);
		sigIntHandler.sa_flags = 0;
		sigaction(SIGINT, &sigIntHandler, NULL);

		// fire up the genetic engine
		engine = new Engine(atoi(argv[1]));
		engine -> run();
		delete engine;
	}
	else
	{
		cout << "   usage: " << argv[0] << " <num generations>" << endl;
	}

	return 0;
}

// - - - other functions - - - //

void sigIntHandlerFunc(int)
{
	engine -> terminate();
}
