#pragma once

#include <time.h>
#include <string>

// get current time in seconds
time_t utGetTimeSeconds();

// get time expressed in, e.g., "1h 13m 12s"
std::string utFormatTime(int seconds);
