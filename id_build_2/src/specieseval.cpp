#include "specieseval.h"
#include "species.h"
#include "constants.h"

#include <math.h>
#include <string.h>
#include <cfloat>
#include <cstdlib>
#include <string>
#include <sstream>
#include <iomanip>
using namespace std;

SpeciesEval::SpeciesEval()
{
	reset();
}

SpeciesEval::SpeciesEval(Species *species)
{
	reset();
	this -> species = species;
}

SpeciesEval::SpeciesEval(const SpeciesEval &other)
{
	species = other.species;
	minDistance = other.minDistance;
	age = other.age;
}

SpeciesEval::~SpeciesEval()
{
	//delete species;
}

SpeciesEval &SpeciesEval::operator = (const SpeciesEval &other)
{
	species = other.species;
	minDistance = other.minDistance;
	age = other.age;

	return *this;
}

// in this context, '<' means better (lower scores are better)
bool SpeciesEval::operator < (const SpeciesEval &other) const
{
	if(minDistance > other.minDistance) return true;
	return false;
}

bool SpeciesEval::operator == (const SpeciesEval &other)
{
	return species -> hash == other.species -> hash;
}

void SpeciesEval::reset()
{
	species = NULL;
	minDistance = -1.0f;
	age = 0;
}

string SpeciesEval::toStr()
{
	stringstream ss;

	ss << "  " << species -> toStr();
	ss << "  " << left << setw(5) << fixed << setprecision(2) << minDistance;
	ss << "  " << left << setw(4) << age;

	return ss.str();
}
