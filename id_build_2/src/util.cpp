#include "util.h"

#include <time.h>
#include <errno.h>
#include <string>
#include <sstream>
#include <iostream>
using namespace std;

time_t utGetTimeSeconds()
{
	struct timespec t;
	int rc;
	time_t result = 0;

	// retrieve value from real-time clock; this is supported in all implementations
	rc = clock_gettime(CLOCK_REALTIME, &t);

	// indicate clock failure; this should never fail but would be a serious problem if it did
	if(rc != 0)
	{
		cerr << "[WARN]: utGetTimeSeconds() called clock_gettime() but this call failed with error code " << rc << " and errno " << errno << endl;
	}
	else
	{
		result = t.tv_sec;
	}

	return result;
}

string utFormatTime(int seconds)
{
	int secs = seconds % 60;
	int mins = seconds / 60;
	int hrs = mins / 60;
	int days = hrs / 24;
	stringstream ss;

	// now apply modulus to get properly-wrapped amounts
	mins %= 60;
	hrs %= 24;
	days %= 365;

	// days, hours, and minutes are optional
	if(days) ss << days << "d ";
	if(hrs)  ss << hrs << "h ";
	if(mins) ss << mins << "m ";

	// always print seconds
	ss << secs << "s";

	return ss.str();
}
