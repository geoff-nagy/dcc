#pragma once

#include <string>

std::string formatSeconds(uint64_t seconds);
std::string formatNumber(uint64_t number);
