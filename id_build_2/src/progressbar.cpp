#include <iostream>
#include <iomanip>
using namespace std;

// - - - public functions - - - //

void printProgressBar(int percent, const string &message)
{
	int i;
	int halfPercent = percent / 2;

	cout << "\r" << message << "[";
	for(i = 0; i <= halfPercent; i ++)
		cout << "=";

	if(halfPercent < 50)
		cout << ">";

	for(; i < 50; i ++)
		cout << " ";
	cout << "]: " << percent << "%  \r";//%d%%\r", percent);
	fflush(stdout);						// print immediately
}
