#pragma once

#include <stdint.h>

// global convenience PRNG that uses a Mersenne Twister as the underlying PRNG

void rndSeed(uint32_t seed);
uint32_t rnd();
uint32_t rndLinear(uint32_t minVal, uint32_t maxVal);
bool rndBool();
