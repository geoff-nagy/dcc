#pragma once

#include "species.h"
#include "specieseval.h"

#include <time.h>
#include <stdint.h>
#include <string>
#include <vector>
#include <atomic>

class Engine
{
public:
	Engine(uint32_t numGenerations);
	~Engine();

	void run();
	void terminate();

private:
	void evaluateFitness(SpeciesEval &speciesEval);

	void eraseAndCollect(std::vector<SpeciesEval> &values, std::vector<SpeciesEval>::iterator begin, std::vector<SpeciesEval>::iterator end, std::vector<Species*> &garbage);
	void destroyGarbage(std::vector<Species*> &garbage);

	void print(std::vector<SpeciesEval> &evals, uint32_t generationIndex, uint32_t cutoff, time_t startTime);

	std::string getReportString(SpeciesEval &species, time_t startTime);
	void saveToReportFile(uint32_t hash, const std::string &report);

	uint32_t numGenerations;
	float bestDistance;

	std::atomic<bool> quit;
};
