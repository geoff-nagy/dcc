POST-RUN IDBUILDGA SUMMARY
-----------------------
number of generations : 1000
time to completion    : 8s
best distance         : 1.76217

TOP SPECIMEN STATS
------------------
token           : M
hash            : 68528c59
min distance    : 1.76217
age             : 1

MARKER PLACEMENT
----------------
        marker         x      y      z
--------------------------------------
Focal marker 1 : -24.33,  12.24, -17.50
Focal marker 2 :  10.67,  12.24, -17.50    (dist:  35.00)
---------------------------------------
          ID 0 :   8.58,  14.37,  41.61    (dist:  61.12)
          ID 1 :  -0.14,  11.75,  39.29    (dist:  57.19)
          ID 2 :  -0.30,   0.60,  34.50    (dist:  53.68)
          ID 3 :   6.85,  11.31,  32.23    (dist:  51.58)
          ID 4 :  -9.08,  12.34,  25.25    (dist:  42.81)
          ID 5 :  -6.60,   8.36,  29.40    (dist:  47.06)
          ID 6 :  -2.15,  13.57,  32.08    (dist:  49.81)
          ID 7 :  -8.19,  13.10,  22.26    (dist:  39.79)
          ID 8 :   7.94,   4.87,  24.10    (dist:  44.76)
          ID 9 :   5.26,  12.88,  40.19    (dist:  58.95)

END OF REPORT
