POST-RUN IDBUILDGA SUMMARY
-----------------------
number of generations : 1000
time to completion    : 1m 23s
best distance         : 2.03966

TOP SPECIMEN STATS
------------------
token           : M
hash            : d6bb6935
min distance    : 2.03966
age             : 2

MARKER PLACEMENT
----------------
        marker         x      y      z
--------------------------------------
Focal marker 1 : -26.41,   6.73, -17.50
Focal marker 2 :   8.59,   6.73, -17.50    (dist:  35.00)
---------------------------------------
          ID 0 :   3.94,  12.95,  39.76    (dist:  59.02)
          ID 1 :   8.55,   8.27,  30.83    (dist:  51.41)
          ID 2 :   6.68,  14.32,  41.04    (dist:  61.06)
          ID 3 :   4.25,   4.62,  29.38    (dist:  48.74)
          ID 4 :   9.58,   8.96,  33.03    (dist:  53.86)
          ID 5 :  -9.59,  11.25,  24.44    (dist:  42.18)
          ID 6 :   6.43,  10.44,  36.15    (dist:  55.92)
          ID 7 :  -3.85,  11.60,  26.29    (dist:  44.35)
          ID 8 :  -3.32,   3.64,  22.13    (dist:  40.14)
          ID 9 :   8.74,  14.50,  24.91    (dist:  46.59)

END OF REPORT
