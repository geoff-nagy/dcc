POST-RUN IDBUILDGA SUMMARY
-----------------------
number of generations : 1000
time to completion    : 1m 23s
best distance         : 2.13292

TOP SPECIMEN STATS
------------------
token           : M
hash            : 5eab8ca4
min distance    : 2.13292
age             : 5

MARKER PLACEMENT
----------------
        marker         x      y      z
--------------------------------------
Focal marker 1 :  -9.76,  14.62, -17.50
Focal marker 2 :  25.24,  14.62, -17.50    (dist:  35.00)
---------------------------------------
          ID 0 :   9.13,  14.27,  40.29    (dist:  57.81)
          ID 1 :   5.68,   0.59,  35.61    (dist:  54.97)
          ID 2 :   7.72,  11.29,  22.11    (dist:  39.75)
          ID 3 :   8.80,  10.70,  32.63    (dist:  50.29)
          ID 4 :  -8.69,   4.17,  41.50    (dist:  62.13)
          ID 5 :   7.71,   3.63,  33.77    (dist:  52.44)
          ID 6 :  -2.29,  14.61,  29.17    (dist:  47.74)
          ID 7 :   1.67,   9.74,  24.13    (dist:  42.36)
          ID 8 :  -5.86,   1.57,  22.84    (dist:  44.53)
          ID 9 :   9.12,   1.52,  41.03    (dist:  59.99)

END OF REPORT
