POST-RUN IDBUILDGA SUMMARY
-----------------------
number of generations : 1000
time to completion    : 17m 39s
best distance         : 2.49137

TOP SPECIMEN STATS
------------------
token           : M
hash            : 424b141c
min distance    : 2.49137
age             : 31

MARKER PLACEMENT
----------------
        marker         x      y      z
--------------------------------------
Focal marker 1 :  -9.13,   1.08, -17.50
Focal marker 2 :  25.87,   1.08, -17.50    (dist:  35.00)
---------------------------------------
          ID 0 :   7.08,   4.93,  22.25    (dist:  39.95)
          ID 1 :  -9.98,  13.98,  41.86    (dist:  63.45)
          ID 2 :   5.94,  11.14,  23.69    (dist:  42.47)
          ID 3 :  -5.62,  11.98,  40.68    (dist:  60.82)
          ID 4 :  -8.91,   2.43,  38.17    (dist:  58.31)
          ID 5 :  -0.16,   4.79,  29.52    (dist:  47.93)
          ID 6 :   0.99,  12.07,  33.80    (dist:  52.98)
          ID 7 :  -3.95,   5.85,  36.38    (dist:  55.48)
          ID 8 :  -4.88,  11.32,  24.34    (dist:  45.07)
          ID 9 :   5.77,   8.80,  32.33    (dist:  50.49)

END OF REPORT
