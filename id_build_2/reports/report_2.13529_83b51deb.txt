POST-RUN IDBUILDGA SUMMARY
-----------------------
number of generations : 20
time to completion    : 1m 42s
best distance         : 2.13529

TOP SPECIMEN STATS
------------------
token           : M
hash            : 83b51deb
min distance    : 2.13529
age             : 8

MARKER PLACEMENT
----------------
        marker         x      y      z
--------------------------------------
Focal marker 1 :  -7.88,   0.41, -17.50
Focal marker 2 :  27.12,   0.41, -17.50    (dist:  35.00)
---------------------------------------
          ID 0 :  -9.46,  12.59,  41.29    (dist:  62.99)
          ID 1 :   7.20,   0.23,  22.42    (dist:  40.00)
          ID 2 :  -7.75,   5.82,  35.12    (dist:  55.67)
          ID 3 :  -3.39,   8.97,  31.32    (dist:  51.24)
          ID 4 :  -1.78,   1.61,  26.91    (dist:  45.87)
          ID 5 :   2.98,   0.12,  35.62    (dist:  53.54)
          ID 6 :   0.46,  11.26,  23.63    (dist:  43.51)
          ID 7 :  -9.73,   8.59,  26.09    (dist:  48.39)
          ID 8 :  -4.29,   1.76,  41.67    (dist:  60.80)
          ID 9 :  -7.73,   4.62,  38.13    (dist:  58.42)

END OF REPORT
