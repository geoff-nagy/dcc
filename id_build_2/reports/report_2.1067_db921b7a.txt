POST-RUN IDBUILDGA SUMMARY
-----------------------
number of generations : 20
time to completion    : 1m 25s
best distance         : 2.1067

TOP SPECIMEN STATS
------------------
token           : M
hash            : db921b7a
min distance    : 2.1067
age             : 7

MARKER PLACEMENT
----------------
        marker         x      y      z
--------------------------------------
Focal marker 1 :  -7.59,  12.43, -17.50
Focal marker 2 :  27.41,  12.43, -17.50    (dist:  35.00)
---------------------------------------
          ID 0 :  -9.69,   2.89,  41.77    (dist:  63.15)
          ID 1 :  -0.40,   1.05,  41.54    (dist:  61.00)
          ID 2 :   5.93,  10.55,  25.37    (dist:  43.09)
          ID 3 :   4.35,   9.99,  22.42    (dist:  40.38)
          ID 4 :   4.30,   5.75,  30.30    (dist:  48.59)
          ID 5 :  -4.31,  14.69,  38.97    (dist:  58.28)
          ID 6 :   5.85,   6.23,  35.19    (dist:  53.21)
          ID 7 :  -2.72,   2.17,  30.58    (dist:  50.75)
          ID 8 :  -1.93,   1.99,  35.52    (dist:  55.32)
          ID 9 :  -9.92,   9.63,  24.25    (dist:  46.30)

END OF REPORT
