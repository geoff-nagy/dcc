POST-RUN IDBUILDGA SUMMARY
-----------------------
number of generations : 1000
time to completion    : 8s
best distance         : 1.71038

TOP SPECIMEN STATS
------------------
token           : R
hash            : 51bd022c
min distance    : 1.71038
age             : 1

MARKER PLACEMENT
----------------
        marker         x      y      z
--------------------------------------
Focal marker 1 : -23.12,   1.55, -17.50
Focal marker 2 :  11.88,   1.55, -17.50    (dist:  35.00)
---------------------------------------
          ID 0 :  -1.85,  10.37,  40.53    (dist:  58.82)
          ID 1 :   9.08,  10.40,  40.81    (dist:  60.78)
          ID 2 :  -2.57,   2.56,  22.20    (dist:  39.83)
          ID 3 :   0.64,   2.87,  33.14    (dist:  51.04)
          ID 4 :  -0.03,  10.03,  25.30    (dist:  43.99)
          ID 5 :  -7.34,   6.81,  27.95    (dist:  45.79)
          ID 6 :  -8.15,   7.42,  24.29    (dist:  42.28)
          ID 7 :  -6.71,   4.39,  30.50    (dist:  48.09)
          ID 8 :   6.58,   8.43,  35.53    (dist:  54.84)
          ID 9 :  -5.24,   8.04,  35.15    (dist:  53.05)

END OF REPORT
