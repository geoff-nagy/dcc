POST-RUN IDBUILDGA SUMMARY
-----------------------
number of generations : 20
time to completion    : 1m 44s
best distance         : 2.18634

TOP SPECIMEN STATS
------------------
token           : M
hash            : 952df694
min distance    : 2.18634
age             : 9

MARKER PLACEMENT
----------------
        marker         x      y      z
--------------------------------------
Focal marker 1 : -25.47,  13.60, -17.50
Focal marker 2 :   9.53,  13.60, -17.50    (dist:  35.00)
---------------------------------------
          ID 0 :  -0.74,   0.15,  31.94    (dist:  51.74)
          ID 1 :   4.82,  11.11,  38.77    (dist:  57.76)
          ID 2 :  -0.26,   6.17,  23.34    (dist:  42.22)
          ID 3 :   7.60,   1.92,  41.67    (dist:  62.28)
          ID 4 :   5.14,   6.06,  29.50    (dist:  49.37)
          ID 5 :  -8.10,   3.30,  25.70    (dist:  44.41)
          ID 6 :  -3.73,   6.90,  28.96    (dist:  47.14)
          ID 7 :  -5.35,   8.02,  36.17    (dist:  54.02)
          ID 8 :  -4.36,  12.22,  22.35    (dist:  40.03)
          ID 9 :   0.41,   9.54,  41.74    (dist:  59.96)

END OF REPORT
