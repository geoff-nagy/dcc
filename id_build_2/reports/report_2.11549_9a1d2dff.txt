POST-RUN IDBUILDGA SUMMARY
-----------------------
number of generations : 20
time to completion    : 1m 43s
best distance         : 2.11549

TOP SPECIMEN STATS
------------------
token           : M
hash            : 9a1d2dff
min distance    : 2.11549
age             : 11

MARKER PLACEMENT
----------------
        marker         x      y      z
--------------------------------------
Focal marker 1 : -13.26,  12.70, -17.50
Focal marker 2 :  21.74,  12.70, -17.50    (dist:  35.00)
---------------------------------------
          ID 0 :  -7.55,   2.71,  41.88    (dist:  61.36)
          ID 1 :  -6.72,   0.15,  26.39    (dist:  46.95)
          ID 2 :   4.96,  12.79,  22.43    (dist:  39.94)
          ID 3 :   9.00,   5.78,  35.67    (dist:  53.83)
          ID 4 :   3.81,   9.67,  24.64    (dist:  42.25)
          ID 5 :  -2.22,   0.06,  40.00    (dist:  59.22)
          ID 6 :   6.17,  10.30,  34.04    (dist:  51.64)
          ID 7 :  -4.01,   0.46,  36.81    (dist:  56.28)
          ID 8 :   3.48,   9.87,  26.94    (dist:  44.53)
          ID 9 :  -6.49,   4.97,  29.75    (dist:  49.06)

END OF REPORT
