# run_idbuild.py
# a simple script that runs multiple instances of my IDBuildGA program concurrently
# Geoff Nagy

import sys as sys                                           # for arg reading
import subprocess                                           # for command-line process invocation
import time													# for time elapsed tracking
import glob													# for file pattern matching
import os													# for folder creation
import shutil												# for file moving
#import signal												# for graceful SIGINT handling
import tempfile

from time import sleep
from subprocess import Popen

# global var indicating that the script should terminate
#quitRun = False

# override signal handler
#def signalHandler(sig, frame):
#	global quitRun
#	print("\n   [ctrl+c was received; will wait for all processes to terminate...]")
#	quitRun = True

# prints out nicely-formatted time, given the number of seconds
def formatTimeSeconds(seconds):

	formatted = ""

	secs = int(seconds) % 60
	mins = int(seconds) / 60
	hrs = int(mins) / 60
	days = int(hrs) / 24

	# now apply modulus to get properly-wrapped amounts
	mins %= 60
	hrs %= 24
	days %= 365		# okay, not the most accurate, but whatever; I only need an estimate

	# days, hours, and minutes are optional
	if(days): formatted += str(days) + "d "
	if(hrs):  formatted += str(hrs) + "h "
	if(mins): formatted += str(mins) + "m "

	# always print seconds
	formatted += str(secs) + "s"

	return formatted

# main function
def main():

	# set up interrupt handler so we can exit gracefully
	#global quitRun
	#signal.signal(signal.SIGINT, signalHandler)

	# our program we invoke
	PROGRAM_NAME = "./idbuildga"

	# ensure we got the correct number of args
	if len(sys.argv) < 3:
		print("    usage: " + sys.argv[0] + " <num concurrent> <num generations>")
		print("    e.g., python " + sys.argv[0] + " 12 1000")
		exit(1)

	# grab args
	numConcurrent = int(sys.argv[1])
	numGenerations = int(sys.argv[2])

	# print run statistics to the user
	print("going to run the IDBuildGA program")
	print("-- concurrent processes   : " + str(numConcurrent))
	print("-- number of generations  : " + str(numGenerations))

	# confirm yes/no
	choice = '0'
	while choice != 'y' and choice != 'n':
		sys.stdout.write("-- is this correct (y/n)? : ")
		choice = raw_input().lower()

	# quit if requested
	if choice == 'n':
		print("aborting")
		quit(0)

	# build command name with args
	command = [PROGRAM_NAME, str(numGenerations)]

	# time and track the trials
	startTime = time.time()
	runningProcesses = []
	runsSuccessful = 0
	runsFailed = 0
	runsTotal = 0

	# loop forever
	print("   [starting...]")
	while(True):#len(runningProcesses) > 0):

		# ensure that the number of running processes is equal to our requested amount
		while(len(runningProcesses) < numConcurrent):

			# start the trial, and add the process and its trial file to a list of currently running trials
			p = Popen(command, stdout = tempfile.TemporaryFile(), stderr = tempfile.TemporaryFile())
			runningProcesses.append(p)

		# forget any trials that have completed, so that we'll know to start up more
		for i in range(len(runningProcesses) - 1, -1, -1):

			# has the current process returned or not?
			retcode = runningProcesses[i].poll()
			if retcode is not None:

				# warn user if a trial failed to complete
				runsTotal += 1
				if retcode == 0:
					runsSuccessful += 1
				else:
					runsFailed += 1
					#print("[WARNING]: a run failed to complete with error code " + str(retcode))

				# remove process from currently running list
				del runningProcesses[i]

		# print progress update
		elapsedTime = time.time() - startTime
		sys.stdout.write("\r   completed " + str(runsTotal) + " runs (" + str(runsFailed) + " failed) in " + formatTimeSeconds(elapsedTime) + "    ")
		sys.stdout.flush()

		# wait to avoid tying up a processor
		sleep(1)

	# done
	sys.stdout.write("\n   [done]\n")
	quit(0)

# - - - main program - - - #

main()

# end of file
