# run_experiments
# a simple script that runs my pairing proxy experiments
# Geoff Nagy

import sys as sys                                           # for arg reading
import subprocess                                           # for command-line process invocation
import time													# for time elapsed tracking

from time import sleep
from subprocess import Popen

#from results_plotter import plotResults

# do we want to run the stats, too?
runStats = False
if len(sys.argv) > 1 and sys.argv[1] == '--run-stats':
	runStats = True

# build the experiments we want
PROGRAM_NAME = "./linux-gcc/mqc-vicon"

# list of experiment commands goes here
experimentCommands = []

# --- begin aug 7 experiments ---

for v in range(60, 400, 10):

	# 0% paired
	for i in range(0, 100):
		experimentCommands.append([PROGRAM_NAME, "gui=false", "simulated=true", "num-pairs=0", "num-tracked-if-paired=3", "num-single=44", "num-tracked-if-single=7", "comms-delay-ms=0", "view-range-cm=" + str(v), "trial-name=aug-11/test_0_paired_view_" + str(v) + "_" + str(i), "trial-time=45"])

	# 25% paired
	for i in range(0, 100):
		experimentCommands.append([PROGRAM_NAME, "gui=false", "simulated=true", "num-pairs=6", "num-tracked-if-paired=3", "num-single=32", "num-tracked-if-single=7", "comms-delay-ms=0", "view-range-cm=" + str(v), "trial-name=aug-11/test_25_paired_view_" + str(v) + "_" + str(i), "trial-time=45"])

	# 50% paired
	for i in range(0, 100):
		experimentCommands.append([PROGRAM_NAME, "gui=false", "simulated=true", "num-pairs=11", "num-tracked-if-paired=3", "num-single=22", "num-tracked-if-single=7", "comms-delay-ms=0", "view-range-cm=" + str(v), "trial-name=aug-11/test_50_paired_view_" + str(v) + "_" + str(i), "trial-time=45"])

	# 75% paired
	for i in range(0, 100):
		experimentCommands.append([PROGRAM_NAME, "gui=false", "simulated=true", "num-pairs=16", "num-tracked-if-paired=3", "num-single=12", "num-tracked-if-single=7", "comms-delay-ms=0", "view-range-cm=" + str(v), "trial-name=aug-11/test_75_paired_view_" + str(v) + "_" + str(i), "trial-time=45"])

	# 100% paired
	for i in range(0, 100):
		experimentCommands.append([PROGRAM_NAME, "gui=false", "simulated=true", "num-pairs=22", "num-tracked-if-paired=3", "num-single=0", "num-tracked-if-single=7", "comms-delay-ms=0", "view-range-cm=" + str(v), "trial-name=aug-11/test_100_paired_view_" + str(v) + "_" + str(i), "trial-time=45"])

# --- end aug 3 experiments

numExperiments = len(experimentCommands)

# run the experiments we want
#for i in range(0, numExperiments):
#    print("--------------------------------------------------------------------------");
#    print("running experiment " + str(i + 1) + " of " + str(numExperiments) + "...")
#    print("\tcommand : " + " ".join(experimentCommands[i]))
#    print("--------------------------------------------------------------------------");
#    subprocess.call(experimentCommands[i])

# run the experiments asynchronously
running = []
startTime = time.time()

# while we still have experiments to run
numProcesses = 12
print("running " + str(numExperiments) + " experiments silently (" + str(numProcesses) + " concurrently)...")
while(len(experimentCommands)):

	# ensure that the number of running processes is equal to our core count
	while(len(running) < numProcesses and len(experimentCommands) > 0):
		running.append(Popen(experimentCommands.pop(), stdout=subprocess.PIPE, stderr=subprocess.PIPE))

	# delete any processes that aren't running
	for i in range(len(running) - 1, -1, -1):

		# has the current process returned or not?
		retcode = running[i].poll()
		if retcode is not None:
			del running[i]

	# print update
	sys.stdout.write("\r   progress: [" + str(numExperiments - len(experimentCommands)) + "/" + str(numExperiments) + "]")
	sys.stdout.flush()

	# wait before doing this nonsense again
	sleep(1)

# complete
elapsedTime = time.time() - startTime
print("\ncompleted in " + time.strftime("%Hh %Mm %Ss", time.gmtime(elapsedTime)))

# do we run the stats, too?
#if runStats:

    # build list of experiment names
#    experimentNames = []
#    for i in range(0, numExperiments):
#        experimentNames.append(experimentCommands[i][5])

#    plotResults(experimentNames)

    # invoke the plotter
    #plotResults(experimentNames)
