# run_experiments
# a simple script that runs my pairing proxy experiments
# Geoff Nagy

import sys as sys                                           # for arg reading
import subprocess                                           # for command-line process invocation
import time													# for time elapsed tracking
import glob													# for file pattern matching
import os													# for folder creation
import shutil												# for file moving

from time import sleep
from subprocess import Popen

# our
PROGRAM_NAME = "./dcc"

# ensure we got the correct number of args
if len(sys.argv) < 4:
	print("    usage: " + sys.argv[0] + " <num_concurrent> <world> <base_trial_name>")
	print("    e.g., " + sys.argv[0] + " 12 config/worlds/world.gum config/trials/trial_base")
	exit(1)

# grab args
numConcurrent = int(sys.argv[1])
worldFilename = sys.argv[2]

# build filenames
experimentFiles = glob.glob("./" + sys.argv[3] + "*.txt");#sys.argv[3:]
runningExperiments = []
runningFiles = []

# print run statistics to the user
numExperiments = len(experimentFiles)
if(numExperiments > 0):
	print("found " + str(numExperiments) + " trial files to run")
	print("-- trials will be run silently")
	print("-- the \"results\" and \"finished\" subdirectories will be created if they don't already exist")
	print("-- number of processes: " + str(numConcurrent))
else:
	print("no trial files matching this pattern were found")
	exit(0)

# confirm yes/no
choice = '0'
while choice != 'y' and choice != 'n':
	print("is this correct (y/n)?")
	choice = input().lower()

# quit if requested
if choice == 'n':
	print("aborting")
	quit(0)

# start the timer so we can see how long the trials took
startTime = time.time()

# while we still have experiments to run
while(len(experimentFiles) or len(runningExperiments)):

	# ensure that the number of running processes is equal to our core count
	while(len(runningExperiments) < numConcurrent and len(experimentFiles) > 0):

		# extract the name of the next trial file we want to run
		trialFilename = experimentFiles.pop()
		command = [PROGRAM_NAME, "gui=false", "world=" + worldFilename, "trial=" + trialFilename]

		# start the trial, and add the process and its trial file to a list of currently running trials
		runningExperiments.append(Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE))
		runningFiles.append(trialFilename)

	# forget any trials that have completed, so that we'll know to start up more
	for i in range(len(runningExperiments) - 1, -1, -1):

		# has the current process returned or not?
		retcode = runningExperiments[i].poll()
		if retcode is not None:

			# did the trial complete properly? if so, move the trial file to the "finished" folder
			trialFilename = runningFiles[i]
			if retcode == 0:

				# make sure the "finished" folder is created
				index = trialFilename.rfind('/')
				finishedFolder = trialFilename[:index] + "/finished"
				if not os.path.exists(finishedFolder):
					os.makedirs(finishedFolder)

				# copy to the "finished" folder
				shutil.move(trialFilename, finishedFolder)

				# progress report
				#print("completed " + runningFiles[i] + " | progress: [" + str(numExperiments - len(experimentFiles)) + "/" + str(numExperiments) + "]")

			else:

				# warn user that a trial failed to complete
				print("[WARNING]: trial " + trialFilename + " failed to complete with error code " + str(retcode))

			# print progress update
			sys.stdout.write("\r   progress: [" + str(numExperiments - len(experimentFiles)) + "/" + str(numExperiments) + "]")
			sys.stdout.flush()

			# remove trial file from currently running list
			del runningFiles[i]

			# remove the trial process from currently running experiments
			del runningExperiments[i]

	# wait before doing this nonsense again
	sleep(0.2)

# complete
elapsedTime = time.time() - startTime
print("\ncompleted in " + time.strftime("%Hh %Mm %Ss", time.gmtime(elapsedTime)))
