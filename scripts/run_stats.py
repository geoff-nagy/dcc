# run_stats.py
# Geoff Nagy
# A collection of Python functions to compute the stats related to my pairwise
# flocking experiments

import math
import matplotlib.pyplot as plt                             # for graphing functions
import matplotlib.cm as cm									# for colour-mapping plots
import numpy as np                                          # for statistical functions
import scipy.stats as st									# for statistical functions
import statsmodels.stats.api as sms

import pingouin as pg

from statsmodels.stats.multicomp import pairwise_tukeyhsd

from results_reader import readResults

from mpl_toolkits.mplot3d import Axes3D						# for 3D plots

# these are used for our sanity checks before running stats to make sure we
# have all of the data we require
EXPECTED_NUM_TRIALS = 50
EXPECTED_TRIAL_LENGTH = 60
EXPECTED_STATS_PER_SECOND = 9

# these are the indices of each statistic in each per-second sample of a trial
STAT_TIME_INDEX = 0
STAT_NUM_SUB_FLOCKS = 1
STAT_AVG_SUB_FLOCK_SIZE = 2
STAT_AVG_SUB_FLOCK_DIAMETER = 3
STAT_AVG_SUB_FLOCK_DEGREE = 4
STAT_AVG_NUM_CONNECTIONS = 5					# should be equivalent to STAT_AVG_COMPRESSED_NUM_CONNECTIONS
STAT_AVG_COMPRESSED_SUB_FLOCK_DIAMETER = 6
STAT_AVG_COMPRESSED_SUB_FLOCK_DEGREE = 7
STAT_AVG_COMPRESSED_NUM_CONNECTIONS = 8			# should be equivalent to STAT_AVG_NUM_CONNECTIONS

# perform a check on all trials of the given experiment name to make sure that
# all of the data are present
def completenessCheck(experimentName, data):

	# check to make sure the number of trials is correct
	numTrials = len(data)
	if(numTrials != EXPECTED_NUM_TRIALS):
		raise Exception("experiment \"" + experimentName + "\" was expected to contain " + str(EXPECTED_NUM_TRIALS) + " but contains " + str(numTrials))

	# now check to make sure each trial has the correct length
	for i in range(0, EXPECTED_NUM_TRIALS):
		numSecondsPerTrial = len(data[i])
		if(numSecondsPerTrial != EXPECTED_TRIAL_LENGTH):
			raise Exception("trial " + str(i) + " in experiment \"" + experimentName + "\" was expected to be " + str(EXPECTED_TRIAL_LENGTH) + "s long but is " + str(numSecondsPerTrial) + "s long")

		# now check that each trial has the correct number of stats
		for j in range(0, EXPECTED_TRIAL_LENGTH):
			numStatsPerSecond = len(data[i][j])
			if(numStatsPerSecond != EXPECTED_STATS_PER_SECOND):
				raise Exception("time index " + str(j) + " of trial " + str(i) + " in experiment \"" + experimentName + "\" was expected to have " + str(EXPECTED_STATS_PER_SECOND) + " stats but has " + str(numStatsPerSecond))

	return True

def getAverageStatAtEnd(data, stat):

	avg = 0.0
	for i in range(0, EXPECTED_NUM_TRIALS):
		avg = avg + data[i][EXPECTED_TRIAL_LENGTH - 1][stat]

	return avg / EXPECTED_NUM_TRIALS

def getAverageStatOverTime(data, stat):

	avg = []
	for i in range(0, EXPECTED_NUM_TRIALS):
		avg.append(data[i][EXPECTED_TRIAL_LENGTH - 1][stat])

	return avg

def getStatAtEnd(data, stat):

	stats = []
	for i in range(0, EXPECTED_NUM_TRIALS):
		stats.append(data[i][EXPECTED_TRIAL_LENGTH - 1][stat])

	return stats

def getStatOverAll(data, stat):

	stats = []
	for i in range(0, EXPECTED_NUM_TRIALS):
		for j in range(0, EXPECTED_TRIAL_LENGTH):
			stats.append(data[i][j][stat])

	return stats

def getStatOverallByExperimentName(experimentName, stat):

	data = readResults(experimentName)

	# don't forget to check that all the data is there
	if(completenessCheck(experimentName, data)):
		print("-- sanity check for experiment \"" + experimentName + "\" passed")

	return getStatAtEnd(data, stat)

# what charts do we want?
# - num subflocks over time by paired percentages
# - a 2x2 chart of the above with close pairing and asymmetric tracking enabled
#		- be sure to use confidence intervals where appropriate
# - a 5x5 histogram: mean number of sub-flocks per paired percentage, varying noise and reliability
#		- will need statistical significance calculations

def computeAverageSubFlocksOverAll(experimentName):

	data = readResults(experimentName)

	# don't forget to check that all the data is there
	if(completenessCheck(experimentName, data)):
		print("-- sanity check for experiment \"" + experimentName + "\" passed")

	subFlocksOverAll = getStatOverAll(data, STAT_NUM_SUB_FLOCKS)

	avgNumSubFlocks = np.mean(subFlocksOverAll)
	stdDevNumSubFlocks = np.std(subFlocksOverAll)

	return avgNumSubFlocks, stdDevNumSubFlocks

def computeAverageSubFlocksAtEnd(experimentName):

	data = readResults(experimentName)

	numTrials = len(data)
	numSecondsPerTrial = len(data[0])
	numStatsPerSecond = len(data[0][0])

	# don't forget to check that all the data is there
	if(completenessCheck(experimentName, data)):
		print("-- sanity check for experiment \"" + experimentName + "\" passed")

	subFlocksAtEnd = getStatAtEnd(data, STAT_NUM_SUB_FLOCKS)

	avgNumSubFlocks = np.mean(subFlocksAtEnd);
	stdDevNumSubFlocks = np.std(subFlocksAtEnd)

	return avgNumSubFlocks, stdDevNumSubFlocks

	#avgNumSubFlocks = getAverageStatAtEnd(data, STAT_NUM_SUB_FLOCKS)
	#return avgNumSubFlocks

	#print("average number of sub flocks is " + str(avgNumSubFlocks))

	#subFlocksOverTime = getAverageStatOverTime(data, STAT_NUM_SUB_FLOCKS)
	#print("sub flocks over time: " + str(subFlocksOverTime))

def makeViewRangeGraph():

	pairedPercentages = [0, 25, 50, 75, 100]

	# get the data associated with sub flocks at end of simulation
	numDataPoints = 0
	avgSubFlocks = []
	stdDevSubFlocks = []

	# for each paired percentage we ran experiments for
	for i in range(0, len(pairedPercentages)):

		# add element for this pairing percentage
		avgSubFlocks.append([])
		stdDevSubFlocks.append([])

		# for each view range we ran experiments for
		for v in range(60, 400, 10):
			avg, stddev = computeAverageSubFlocksAtEnd("aug-11/test_" + str(pairedPercentages[i]) + "_paired_view_" + str(v) + "_")

			avgSubFlocks[i].append(avg)
			stdDevSubFlocks[i].append(stddev)

			# count number of data points we have
			if(i == 0):
				numDataPoints = numDataPoints + 1

	# prepare to plot the values
	fig = plt.figure()

	# set reasonable axes
	axes = plt.gca()
	axes.set_ylim([0, 44])
	ax = fig.add_subplot(111);

	# plot each paired percentage on the same figure
	for i in range(0, len(pairedPercentages)):

		# build coordinates for plot
		dataX = np.array(range(60, 400, 10));

		# plot
		ax.errorbar(dataX, avgSubFlocks[i], label=str(pairedPercentages[i]) + " Paired", yerr=stdDevSubFlocks[i])

	# complete the graph labelling and show it
	ax.legend(loc='upper center')
	ax.set_title("Number of Sub Flocks at End")
	ax.set_ylabel("Sub Flocks")
	ax.set_xlabel("View Range")
	plt.show(fig)

def varyingNumberOfPairsStats(folder, viewRange):

	# so, what's happening here is:
	# - one-way ANOVA F-test
	# - Tukey's post-hoc one-way ANOVA to find differences between pairs of means
	# - charting of the results with 95% confidence intervals
	# - (should we also add parametric correlation?)

	# - - - ANOVA and Tukey's post-hoc one-way ANOVA to find differences between pairs of means - - - #

	print("====================================================================")
	print("=========================== VARYING PAIRS ==========================")
	print("====================================================================")

	print("--------------------------------------------------------------------")
	subFlocks4Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_22_0.0_1.0_true_true_" + viewRange + "_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks3Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_17_0.0_1.0_true_true_" + viewRange + "_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks2Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_11_0.0_1.0_true_true_" + viewRange + "_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks1Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_true_true_" + viewRange + "_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks0Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_true_true_" + viewRange + "_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	data = subFlocks0Pairs + subFlocks1Pairs + subFlocks2Pairs + subFlocks3Pairs + subFlocks4Pairs
	groups = ([0] * EXPECTED_NUM_TRIALS) + ([1] * EXPECTED_NUM_TRIALS) + ([2] * EXPECTED_NUM_TRIALS) + ([3] * EXPECTED_NUM_TRIALS) + ([4] * EXPECTED_NUM_TRIALS)

	print("--------------------------------------------------------------------")
	leveneF, leveneP = st.levene(subFlocks0Pairs, subFlocks1Pairs, subFlocks2Pairs, subFlocks3Pairs, subFlocks4Pairs, center = 'trimmed')
	print("Levene's test yields F:" + str(leveneF) + ", p: " + str(leveneP))

	print("--------------------------------------------------------------------")
	f, p = st.f_oneway(subFlocks0Pairs, subFlocks1Pairs, subFlocks2Pairs, subFlocks3Pairs, subFlocks4Pairs)
	print("One-way ANOVA F:" + str(f) + ", p: " + str(p))

	print("--------------------------------------------------------------------")
	print(pairwise_tukeyhsd(data, groups, alpha=0.05))

	print("--------------------------------------------------------------------")

	x = [4, 6, 5, 7, 6]
	y = [2, 2, 3, 1, 2]
	# Run a T-test
	print(pg.ttest(x, y, paired=True))

	# - - - chart - - - #

	# prepare to plot the values
	fig = plt.figure()
	ax = fig.add_subplot(111)

	# compute averages
	subFlockMeans = [np.mean(subFlocks0Pairs), np.mean(subFlocks1Pairs), np.mean(subFlocks2Pairs), np.mean(subFlocks3Pairs), np.mean(subFlocks4Pairs)]

	# compute confidence intervals
	lower0, upper0 = sms.DescrStatsW(subFlocks0Pairs).tconfint_mean()
	lower1, upper1 = sms.DescrStatsW(subFlocks1Pairs).tconfint_mean()
	lower2, upper2 = sms.DescrStatsW(subFlocks2Pairs).tconfint_mean()
	lower3, upper3 = sms.DescrStatsW(subFlocks3Pairs).tconfint_mean()
	lower4, upper4 = sms.DescrStatsW(subFlocks4Pairs).tconfint_mean()
	confIntervals = [[subFlockMeans[0] - lower0, subFlockMeans[1] - lower1, subFlockMeans[2] - lower2, subFlockMeans[3] - lower3, subFlockMeans[4] - lower4],
	                 [upper0 - subFlockMeans[0], upper1 - subFlockMeans[1], upper2 - subFlockMeans[2], upper3 - subFlockMeans[3], upper4 - subFlockMeans[4]]]

	# plot the data
	xCoords = [0, 1, 2, 3, 4]
	xLabels = ['0', '5', '11', '17', '22']
	ax.bar(xCoords, subFlockMeans, yerr = confIntervals, tick_label=xLabels)

	# complete the graph labelling and show it
	ax.set_title("Sub Flocks at Trial End (View Range " + viewRange + " cm)")
	ax.set_ylabel("Number of Sub Flocks")
	ax.set_xlabel("Number of Paired Agents")
	plt.show(fig)

def varyingNumberOfPairsConnections44Agents(folder, viewRange):

		# so, what we want is:
		# - one-way ANOVA F-test
		# - Tukey's post-hoc one-way ANOVA to find differences between pairs of means
		# - can also add parametric correlation?

		# - - - ANOVA for sub-flocks at end, over percentage of paired agents - - - #

		print("====================================================================")
		print("=========================== VARYING PAIRS ==========================")
		print("====================================================================")

		print("--------------------------------------------------------------------")
		subFlocks22Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_22_0.0_1.0_true_true_" + viewRange + "_true_true_*.txt", STAT_AVG_NUM_CONNECTIONS)
		subFlocks17Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_17_0.0_1.0_true_true_" + viewRange + "_true_true_*.txt", STAT_AVG_NUM_CONNECTIONS)
		subFlocks11Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_11_0.0_1.0_true_true_" + viewRange + "_true_true_*.txt", STAT_AVG_NUM_CONNECTIONS)
		subFlocks5Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_true_true_" + viewRange + "_true_true_*.txt", STAT_AVG_NUM_CONNECTIONS)
		subFlocks0Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_true_true_" + viewRange + "_true_true*.txt", STAT_AVG_NUM_CONNECTIONS)

		data = subFlocks0Pairs + subFlocks5Pairs + subFlocks11Pairs + subFlocks17Pairs + subFlocks22Pairs
		groups = ([0] * EXPECTED_NUM_TRIALS) + ([5] * EXPECTED_NUM_TRIALS) + ([11] * EXPECTED_NUM_TRIALS) + ([17] * EXPECTED_NUM_TRIALS) + ([22] * EXPECTED_NUM_TRIALS)

		# we divide the number of connections by 2 to compensate for a minor bug in our output that was fixed in later stats-recording
		# [WARN]: be careful when using this so you don't accidentally divide by 2 when you don't need to!
		subFlocks0Pairs[:] = [x / 2 for x in subFlocks0Pairs]
		subFlocks5Pairs[:] = [x / 2 for x in subFlocks5Pairs]
		subFlocks11Pairs[:] = [x / 2 for x in subFlocks11Pairs]
		subFlocks17Pairs[:] = [x / 2 for x in subFlocks17Pairs]
		subFlocks22Pairs[:] = [x / 2 for x in subFlocks22Pairs]

		print("--------------------------------------------------------------------")
		f, p = st.f_oneway(subFlocks0Pairs, subFlocks5Pairs, subFlocks11Pairs, subFlocks17Pairs, subFlocks22Pairs)
		print("One-way ANOVA F:" + str(f) + ", p: " + str(p))

		print("--------------------------------------------------------------------")
		print(pairwise_tukeyhsd(data, groups, alpha=0.05))

		# - - - graph for sub-flocks at at, over percentage of paired agents - - - #

		# prepare to plot the values
		fig = plt.figure()
		ax = fig.add_subplot(111);
		#ax.set_ylim([0, 5])

		# compute averages
		subFlockMeans = [np.mean(subFlocks0Pairs), np.mean(subFlocks5Pairs), np.mean(subFlocks11Pairs), np.mean(subFlocks17Pairs), np.mean(subFlocks22Pairs)]

		# compute confidence intervals
		lower0, upper0 = sms.DescrStatsW(subFlocks0Pairs).tconfint_mean()
		lower1, upper1 = sms.DescrStatsW(subFlocks5Pairs).tconfint_mean()
		lower2, upper2 = sms.DescrStatsW(subFlocks11Pairs).tconfint_mean()
		lower3, upper3 = sms.DescrStatsW(subFlocks17Pairs).tconfint_mean()
		lower4, upper4 = sms.DescrStatsW(subFlocks22Pairs).tconfint_mean()
		confIntervals = [[subFlockMeans[0] - lower0, subFlockMeans[1] - lower1, subFlockMeans[2] - lower2, subFlockMeans[3] - lower3, subFlockMeans[4] - lower4],
		                 [upper0 - subFlockMeans[0], upper1 - subFlockMeans[1], upper2 - subFlockMeans[2], upper3 - subFlockMeans[3], upper4 - subFlockMeans[4]]]

		# plot the data
		xCoords = [0, 1, 2, 3, 4]
		xLabels = ['0', '5', '11', '17', '22']
		ax.bar(xCoords, subFlockMeans, yerr = confIntervals, tick_label=xLabels)

		# complete the graph labelling and show it
		ax.set_title("Total Number of Connections (View Range " + viewRange + " cm)")
		ax.set_ylabel("Number of Connections")
		ax.set_xlabel("Number of Paired Agents")
		plt.show()

		#--------------------------------------------------------------------
		#One-way ANOVA F:12060.146853015076, p: 5.953591586931314e-280
		#--------------------------------------------------------------------
		# Multiple Comparison of Means - Tukey HSD, FWER=0.05
		#=====================================================
		#group1 group2 meandiff p-adj  lower    upper   reject
		#-----------------------------------------------------
		#     0      5   -65.68 0.001  -69.643  -61.717   True
		#     0     11  -143.28 0.001 -147.243 -139.317   True
		#     0     17  -214.84 0.001 -218.803 -210.877   True
		#     0     22  -279.36 0.001 -283.323 -275.397   True
		#     5     11    -77.6 0.001  -81.563  -73.637   True
		#     5     17  -149.16 0.001 -153.123 -145.197   True
		#     5     22  -213.68 0.001 -217.643 -209.717   True
		#    11     17   -71.56 0.001  -75.523  -67.597   True
		#    11     22  -136.08 0.001 -140.043 -132.117   True
		#    17     22   -64.52 0.001  -68.483  -60.557   True
		#-----------------------------------------------------

def varyingNumberOfPairsDegrees(folder):

		#avg5Pairs, stdDev5Pairs = computeAverageSubFlocksOverAll("scripts/trials/oct-7/results/trial_output_5_0.0_1.0_true_true_*.txt")
		#avg4Pairs, stdDev4Pairs = computeAverageSubFlocksOverAll("scripts/trials/oct-7/results/trial_output_4_0.0_1.0_true_true_*.txt")
		#avg3Pairs, stdDev3Pairs = computeAverageSubFlocksOverAll("scripts/trials/oct-7/results/trial_output_3_0.0_1.0_true_true_*.txt")
		#avg2Pairs, stdDev2Pairs = computeAverageSubFlocksOverAll("scripts/trials/oct-7/results/trial_output_2_0.0_1.0_true_true_*.txt")
		#avg1Pairs, stdDev1Pairs = computeAverageSubFlocksOverAll("scripts/trials/oct-7/results/trial_output_1_0.0_1.0_true_true_*.txt")
		#avg0Pairs, stdDev0Pairs = computeAverageSubFlocksOverAll("scripts/trials/oct-7/results/trial_output_0_0.0_1.0_true_true_*.txt")

		# print results
		#print("5 pairs: " + str(avg5Pairs) + " (" + str(stdDev5Pairs) + ")")
		#print("4 pairs: " + str(avg4Pairs) + " (" + str(stdDev4Pairs) + ")")
		#print("3 pairs: " + str(avg3Pairs) + " (" + str(stdDev3Pairs) + ")")
		#print("2 pairs: " + str(avg2Pairs) + " (" + str(stdDev2Pairs) + ")")
		#print("1 pairs: " + str(avg1Pairs) + " (" + str(stdDev1Pairs) + ")")
		#print("0 pairs: " + str(avg0Pairs) + " (" + str(stdDev0Pairs) + ")")

		# so, what we want is:
		# - one-way ANOVA F-test
		# - Tukey's post-hoc one-way ANOVA to find differences between pairs of means
		# - can also add parametric correlation?

		# - - - ANOVA for sub-flocks at end, over percentage of paired agents - - - #

		print("====================================================================")
		print("=========================== VARYING PAIRS ==========================")
		print("====================================================================")

		print("--------------------------------------------------------------------")
		subFlocks5Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_22_0.0_1.0_true_true_110_*.txt", STAT_AVG_SUB_FLOCK_DEGREE)
		subFlocks4Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_17_0.0_1.0_true_true_110_*.txt", STAT_AVG_SUB_FLOCK_DEGREE)
		subFlocks3Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_11_0.0_1.0_true_true_110_*.txt", STAT_AVG_SUB_FLOCK_DEGREE)
		subFlocks2Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_true_true_110_*.txt", STAT_AVG_SUB_FLOCK_DEGREE)
		subFlocks1Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_true_true_110_*.txt", STAT_AVG_SUB_FLOCK_DEGREE)
		subFlocks0Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_true_true_110_*.txt", STAT_AVG_SUB_FLOCK_DEGREE)

		data = subFlocks0Pairs + subFlocks1Pairs + subFlocks2Pairs + subFlocks3Pairs + subFlocks4Pairs + subFlocks5Pairs
		groups = ([0] * EXPECTED_NUM_TRIALS) + ([1] * EXPECTED_NUM_TRIALS) + ([2] * EXPECTED_NUM_TRIALS) + ([3] * EXPECTED_NUM_TRIALS) + ([4] * EXPECTED_NUM_TRIALS) + ([5] * EXPECTED_NUM_TRIALS)

		print("--------------------------------------------------------------------")
		f, p = st.f_oneway(subFlocks0Pairs, subFlocks1Pairs, subFlocks2Pairs, subFlocks3Pairs, subFlocks4Pairs, subFlocks5Pairs)
		print("One-way ANOVA F:" + str(f) + ", p: " + str(p))

		print("--------------------------------------------------------------------")
		print(pairwise_tukeyhsd(data, groups, alpha=0.05))

		# - - - graph for sub-flocks at at, over percentage of paired agents - - - #

		# prepare to plot the values
		fig = plt.figure()
		ax = fig.add_subplot(111);
		#ax.set_ylim([0, 5])

		# compute averages
		subFlockMeans = [np.mean(subFlocks0Pairs), np.mean(subFlocks1Pairs), np.mean(subFlocks2Pairs), np.mean(subFlocks3Pairs), np.mean(subFlocks4Pairs), np.mean(subFlocks5Pairs)]

		# compute confidence intervals
		lower0, upper0 = sms.DescrStatsW(subFlocks0Pairs).tconfint_mean()
		lower1, upper1 = sms.DescrStatsW(subFlocks1Pairs).tconfint_mean()
		lower2, upper2 = sms.DescrStatsW(subFlocks2Pairs).tconfint_mean()
		lower3, upper3 = sms.DescrStatsW(subFlocks3Pairs).tconfint_mean()
		lower4, upper4 = sms.DescrStatsW(subFlocks4Pairs).tconfint_mean()
		lower5, upper5 = sms.DescrStatsW(subFlocks5Pairs).tconfint_mean()
		confIntervals = [[subFlockMeans[0] - lower0, subFlockMeans[1] - lower1, subFlockMeans[2] - lower2, subFlockMeans[3] - lower3, subFlockMeans[4] - lower4, subFlockMeans[5] - lower5],
		                 [upper0 - subFlockMeans[0], upper1 - subFlockMeans[1], upper2 - subFlockMeans[2], upper3 - subFlockMeans[3], upper4 - subFlockMeans[4], upper5 - subFlockMeans[5]]]

		# plot the data
		xCoords = [0, 1, 2, 3, 4, 5]
		ax.bar(xCoords, subFlockMeans, yerr = confIntervals)

		# complete the graph labelling and show it
		ax.set_title("Average Sub-Flock Degree (Normalized by Sub-Flock Size)")
		ax.set_ylabel("Average Degrees")
		ax.set_xlabel("Number of Paired Agents")
		plt.show(fig)

def varyingNumberOfPairsDiameters44Agents(folder, viewRange):

	# so, what's happening here is:
	# - one-way ANOVA F-test
	# - Tukey's post-hoc one-way ANOVA to find differences between pairs of means
	# - charting of the results with 95% confidence intervals
	# - (should we also add parametric correlation?)

	# - - - ANOVA and Tukey's post-hoc one-way ANOVA to find differences between pairs of means - - - #

	print("====================================================================")
	print("=========================== VARYING PAIRS ==========================")
	print("====================================================================")

	print("--------------------------------------------------------------------")
	subFlocks4Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_22_0.0_1.0_true_true_" + viewRange + "_true_true_*.txt", STAT_AVG_COMPRESSED_SUB_FLOCK_DIAMETER)
	subFlocks3Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_17_0.0_1.0_true_true_" + viewRange + "_true_true_*.txt", STAT_AVG_COMPRESSED_SUB_FLOCK_DIAMETER)
	subFlocks2Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_11_0.0_1.0_true_true_" + viewRange + "_true_true_*.txt", STAT_AVG_COMPRESSED_SUB_FLOCK_DIAMETER)
	subFlocks1Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_true_true_" + viewRange + "_true_true_*.txt", STAT_AVG_COMPRESSED_SUB_FLOCK_DIAMETER)
	subFlocks0Pairs = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_true_true_" + viewRange + "_true_true_*.txt", STAT_AVG_COMPRESSED_SUB_FLOCK_DIAMETER)

	data = subFlocks0Pairs + subFlocks1Pairs + subFlocks2Pairs + subFlocks3Pairs + subFlocks4Pairs
	groups = ([0] * EXPECTED_NUM_TRIALS) + ([1] * EXPECTED_NUM_TRIALS) + ([2] * EXPECTED_NUM_TRIALS) + ([3] * EXPECTED_NUM_TRIALS) + ([4] * EXPECTED_NUM_TRIALS)

	print("--------------------------------------------------------------------")
	f, p = st.f_oneway(subFlocks0Pairs, subFlocks1Pairs, subFlocks2Pairs, subFlocks3Pairs, subFlocks4Pairs)
	print("One-way ANOVA F:" + str(f) + ", p: " + str(p))

	print("--------------------------------------------------------------------")
	print(pairwise_tukeyhsd(data, groups, alpha=0.05))

	# - - - chart - - - #

	# prepare to plot the values
	fig = plt.figure()
	ax = fig.add_subplot(111)

	# compute averages
	subFlockMeans = [np.mean(subFlocks0Pairs), np.mean(subFlocks1Pairs), np.mean(subFlocks2Pairs), np.mean(subFlocks3Pairs), np.mean(subFlocks4Pairs)]

	# compute confidence intervals
	lower0, upper0 = sms.DescrStatsW(subFlocks0Pairs).tconfint_mean()
	lower1, upper1 = sms.DescrStatsW(subFlocks1Pairs).tconfint_mean()
	lower2, upper2 = sms.DescrStatsW(subFlocks2Pairs).tconfint_mean()
	lower3, upper3 = sms.DescrStatsW(subFlocks3Pairs).tconfint_mean()
	lower4, upper4 = sms.DescrStatsW(subFlocks4Pairs).tconfint_mean()
	confIntervals = [[subFlockMeans[0] - lower0, subFlockMeans[1] - lower1, subFlockMeans[2] - lower2, subFlockMeans[3] - lower3, subFlockMeans[4] - lower4],
	                 [upper0 - subFlockMeans[0], upper1 - subFlockMeans[1], upper2 - subFlockMeans[2], upper3 - subFlockMeans[3], upper4 - subFlockMeans[4]]]

	# plot the data
	xCoords = [0, 1, 2, 3, 4]
	xLabels = ['0', '5', '11', '17', '22']
	ax.bar(xCoords, subFlockMeans, yerr = confIntervals, tick_label=xLabels)

	# complete the graph labelling and show it
	ax.set_title("Normalized Compressed Sub-Flock Diameter (View Range " + viewRange + " cm)")
	ax.set_ylabel("Diameter")
	ax.set_xlabel("Number of Paired Agents")
	plt.show(fig)

def sensorNoiseStats(folder):

	print("====================================================================")
	print("============================ SENSOR NOISE ==========================")
	print("====================================================================")

	# we'll do a series of t-tests with Bonferroni correction on noise/no noise, for each level of pairedness
	# this will look like a 5-stacked bar chart (noise level on x, stacked by pairedness [maybe just 0 vs 5])

	print("--------------------------------------------------------------------")
	subFlocks0PairsNoise002 = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.02_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks5PairsNoise002 = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.02_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneNoise002F, leveneNoise002p = st.levene(subFlocks0PairsNoise002, subFlocks5PairsNoise002, center = 'median')
	variance0Pairs = np.var(subFlocks0PairsNoise002)
	variance5Pairs = np.var(subFlocks5PairsNoise002)
	tTest002t, tTest002p = st.ttest_ind(subFlocks0PairsNoise002, subFlocks5PairsNoise002, equal_var = False)

	print("For sensor noise standard deviation of 0.02, Levene's test gives F = " + str(leveneNoise002F) + " at p = " + str(leveneNoise002p))
	print("variance for 0 pairs is " + str(variance0Pairs) + " and 5 pairs is " + str(variance5Pairs))
	print("0 pairs (M = " + str(np.mean(subFlocks0PairsNoise002)) + ", SE = " + str(st.sem(subFlocks0PairsNoise002)) + ") and 5 pairs (M = " + str(np.mean(subFlocks5PairsNoise002)) + ", SE = " + str(st.sem(subFlocks5PairsNoise002)))
	print("independent t-test assuming unequal variance t = " + str(tTest002t) + " at p = " + str(tTest002p))

	print("--------------------------------------------------------------------")
	subFlocks0PairsNoise004 = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.04_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks5PairsNoise004 = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.04_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneNoise004F, leveneNoise004p = st.levene(subFlocks0PairsNoise004, subFlocks5PairsNoise004, center = 'median')
	variance0Pairs = np.var(subFlocks0PairsNoise004)
	variance5Pairs = np.var(subFlocks5PairsNoise004)
	tTest004t, tTest004p = st.ttest_ind(subFlocks0PairsNoise004, subFlocks5PairsNoise004, equal_var = False)

	print("For sensor noise standard deviation of 0.04, Levene's test gives F = " + str(leveneNoise004F) + " at p = " + str(leveneNoise004p))
	print("variance for 0 pairs is " + str(variance0Pairs) + " and 5 pairs is " + str(variance5Pairs))
	print("0 pairs (M = " + str(np.mean(subFlocks0PairsNoise004)) + ", SE = " + str(st.sem(subFlocks0PairsNoise004)) + ") and 5 pairs (M = " + str(np.mean(subFlocks5PairsNoise004)) + ", SE = " + str(st.sem(subFlocks5PairsNoise004)))
	print("independent t-test assuming unequal variance t = " + str(tTest004t) + " at p = " + str(tTest004p))

	print("--------------------------------------------------------------------")
	subFlocks0PairsNoise006 = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.06_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks5PairsNoise006 = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.06_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneNoise006F, leveneNoise006p = st.levene(subFlocks0PairsNoise006, subFlocks5PairsNoise006, center = 'median')
	variance0Pairs = np.var(subFlocks0PairsNoise006)
	variance5Pairs = np.var(subFlocks5PairsNoise006)
	tTest006t, tTest006p = st.ttest_ind(subFlocks0PairsNoise006, subFlocks5PairsNoise006, equal_var = False)

	print("For sensor noise standard deviation of 0.06, Levene's test gives F = " + str(leveneNoise006F) + " at p = " + str(leveneNoise006p))
	print("variance for 0 pairs is " + str(variance0Pairs) + " and 5 pairs is " + str(variance5Pairs))
	print("0 pairs (M = " + str(np.mean(subFlocks0PairsNoise006)) + ", SE = " + str(st.sem(subFlocks0PairsNoise006)) + ") and 5 pairs (M = " + str(np.mean(subFlocks5PairsNoise006)) + ", SE = " + str(st.sem(subFlocks5PairsNoise006)))
	print("independent t-test assuming unequal variance t = " + str(tTest006t) + " at p = " + str(tTest006p))

	print("--------------------------------------------------------------------")
	subFlocks0PairsNoise008 = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.08_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks5PairsNoise008 = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.08_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneNoise008F, leveneNoise008p = st.levene(subFlocks0PairsNoise008, subFlocks5PairsNoise008, center = 'median')
	variance0Pairs = np.var(subFlocks0PairsNoise008)
	variance5Pairs = np.var(subFlocks5PairsNoise008)
	tTest008t, tTest008p = st.ttest_ind(subFlocks0PairsNoise008, subFlocks5PairsNoise008, equal_var = False)

	print("For sensor noise standard deviation of 0.08, Levene's test gives F = " + str(leveneNoise008F) + " at p = " + str(leveneNoise008p))
	print("variance for 0 pairs is " + str(variance0Pairs) + " and 5 pairs is " + str(variance5Pairs))
	print("0 pairs (M = " + str(np.mean(subFlocks0PairsNoise008)) + ", SE = " + str(st.sem(subFlocks0PairsNoise008)) + ") and 5 pairs (M = " + str(np.mean(subFlocks5PairsNoise008)) + ", SE = " + str(st.sem(subFlocks5PairsNoise008)))
	print("independent t-test assuming unequal variance t = " + str(tTest008t) + " at p = " + str(tTest008p))

	# prepare to plot the values
	fig = plt.figure()
	ax1 = fig.add_subplot(111)

	# compute averages
	subFlock0PairsMeans = [np.mean(subFlocks0PairsNoise002), np.mean(subFlocks0PairsNoise004), np.mean(subFlocks0PairsNoise006), np.mean(subFlocks0PairsNoise008)]
	subFlock5PairsMeans = [np.mean(subFlocks5PairsNoise002), np.mean(subFlocks5PairsNoise004), np.mean(subFlocks5PairsNoise006), np.mean(subFlocks5PairsNoise008)]

	# compute confidence intervals
	lower0, upper0 = sms.DescrStatsW(subFlocks0PairsNoise002).tconfint_mean()
	lower1, upper1 = sms.DescrStatsW(subFlocks0PairsNoise004).tconfint_mean()
	lower2, upper2 = sms.DescrStatsW(subFlocks0PairsNoise006).tconfint_mean()
	lower3, upper3 = sms.DescrStatsW(subFlocks0PairsNoise008).tconfint_mean()
	confIntervals0Pairs = [[subFlock0PairsMeans[0] - lower0, subFlock0PairsMeans[1] - lower1, subFlock0PairsMeans[2] - lower2, subFlock0PairsMeans[3] - lower3],
	                       [upper0 - subFlock0PairsMeans[0], upper1 - subFlock0PairsMeans[1], upper2 - subFlock0PairsMeans[2], upper3 - subFlock0PairsMeans[3]]]

	lower0, upper0 = sms.DescrStatsW(subFlocks5PairsNoise002).tconfint_mean()
	lower1, upper1 = sms.DescrStatsW(subFlocks5PairsNoise004).tconfint_mean()
	lower2, upper2 = sms.DescrStatsW(subFlocks5PairsNoise006).tconfint_mean()
	lower3, upper3 = sms.DescrStatsW(subFlocks5PairsNoise008).tconfint_mean()
	confIntervals5Pairs = [[subFlock5PairsMeans[0] - lower0, subFlock5PairsMeans[1] - lower1, subFlock5PairsMeans[2] - lower2, subFlock5PairsMeans[3] - lower3],
	                       [upper0 - subFlock5PairsMeans[0], upper1 - subFlock5PairsMeans[1], upper2 - subFlock5PairsMeans[2], upper3 - subFlock5PairsMeans[3]]]

	# plot each net gain for each day on the same figure
	labels = ["0.02", "0.04", "0.06", "0.08"]
	x = np.arange(len(labels))  # the label locations
	barWidth = 0.35
	ax1.bar(x - barWidth / 2, subFlock0PairsMeans, barWidth, yerr = confIntervals0Pairs, label = "0 Pairs", hatch = "xxxx", color="#aaaaee")#, color="#aaaaaa")
	ax1.bar(x + barWidth / 2, subFlock5PairsMeans, barWidth, yerr = confIntervals5Pairs, label = "5 Pairs", hatch = "////", color="#aaddaa")#, label="5 Pairs")#, color="#111111")

	# complete the graph labelling and show it
	#ax.legend(loc='upper right')
	ax1.set_title("Sub-Flocks By Sensor Noise")
	ax1.set_ylabel("Number of Sub-Flocks")
	ax1.set_xlabel("Sensor Noise Std. Dev. (meters)")
	ax1.set_xticks(x)
	ax1.set_xticklabels(labels)
	ax1.legend()
	plt.show(fig)

def sensorReliabilityStats44Agents(folder, viewRange):

	print("====================================================================")
	print("======================== SENSOR RELIABILITY ========================")
	print("====================================================================")

	print("--------------------------------------------------------------------")
	subFlocks0PairsReliability025 = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_0.25_true_true_" + viewRange + "_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks22PairsReliability025 = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_22_0.0_0.25_true_true_" + viewRange + "_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability025F, leveneReliability025p = st.levene(subFlocks0PairsReliability025, subFlocks22PairsReliability025, center = 'median')
	variance0Pairs = np.var(subFlocks0PairsReliability025)
	variance22Pairs = np.var(subFlocks22PairsReliability025)
	tTest025t, tTest025p = st.ttest_ind(subFlocks0PairsReliability025, subFlocks22PairsReliability025, equal_var = False)

	print("For sensor reliability level of 0.25, Levene's test gives F = " + str(leveneReliability025F) + " at p = " + str(leveneReliability025p))
	print("variance for 0 pairs is " + str(variance0Pairs) + " and 22 pairs is " + str(variance22Pairs))
	print("0 pairs (M = " + str(np.mean(subFlocks0PairsReliability025)) + ", SE = " + str(st.sem(subFlocks0PairsReliability025)) + ") and 22 pairs (M = " + str(np.mean(subFlocks22PairsReliability025)) + ", SE = " + str(st.sem(subFlocks22PairsReliability025)))
	print("independent t-test assuming unequal variance t = " + str(tTest025t) + " at p = " + str(tTest025p))

	print("--------------------------------------------------------------------")
	subFlocks0PairsReliability05 = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_0.5_true_true_" + viewRange + "_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks22PairsReliability05 = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_22_0.0_0.5_true_true_" + viewRange + "_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability05F, leveneReliability05p = st.levene(subFlocks0PairsReliability05, subFlocks22PairsReliability05, center = 'median')
	variance0Pairs = np.var(subFlocks0PairsReliability05)
	variance22Pairs = np.var(subFlocks22PairsReliability05)
	tTest05t, tTest05p = st.ttest_ind(subFlocks0PairsReliability05, subFlocks22PairsReliability05, equal_var = False)

	print("For sensor reliability level of 0.5, Levene's test gives F = " + str(leveneReliability05F) + " at p = " + str(leveneReliability05p))
	print("variance for 0 pairs is " + str(variance0Pairs) + " and 22 pairs is " + str(variance22Pairs))
	print("0 pairs (M = " + str(np.mean(subFlocks0PairsReliability05)) + ", SE = " + str(st.sem(subFlocks0PairsReliability05)) + ") and 22 pairs (M = " + str(np.mean(subFlocks22PairsReliability05)) + ", SE = " + str(st.sem(subFlocks22PairsReliability05)))
	print("independent t-test assuming unequal variance t = " + str(tTest05t) + " at p = " + str(tTest05p))

	print("--------------------------------------------------------------------")
	subFlocks0PairsReliability075 = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_0.75_true_true_" + viewRange + "_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks22PairsReliability075 = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_22_0.0_0.75_true_true_" + viewRange + "_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability075F, leveneReliability075p = st.levene(subFlocks0PairsReliability075, subFlocks22PairsReliability075, center = 'median')
	variance0Pairs = np.var(subFlocks0PairsReliability075)
	variance22Pairs = np.var(subFlocks22PairsReliability075)
	tTest075t, tTest075p = st.ttest_ind(subFlocks0PairsReliability075, subFlocks22PairsReliability075, equal_var = False)

	print("For sensor reliability level of 0.75, Levene's test gives F = " + str(leveneReliability075F) + " at p = " + str(leveneReliability075p))
	print("variance for 0 pairs is " + str(variance0Pairs) + " and 22 pairs is " + str(variance22Pairs))
	print("0 pairs (M = " + str(np.mean(subFlocks0PairsReliability075)) + ", SE = " + str(st.sem(subFlocks0PairsReliability075)) + ") and 22 pairs (M = " + str(np.mean(subFlocks22PairsReliability075)) + ", SE = " + str(st.sem(subFlocks22PairsReliability075)))
	print("independent t-test assuming unequal variance t = " + str(tTest075t) + " at p = " + str(tTest075p))

	print("--------------------------------------------------------------------")
	subFlocks0PairsReliability100 = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_true_true_" + viewRange + "_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks22PairsReliability100 = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_22_0.0_1.0_true_true_" + viewRange + "_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability100F, leveneReliability100p = st.levene(subFlocks0PairsReliability100, subFlocks22PairsReliability100, center = 'median')
	variance0Pairs = np.var(subFlocks0PairsReliability100)
	variance22Pairs = np.var(subFlocks22PairsReliability100)
	tTest100t, tTest100p = st.ttest_ind(subFlocks0PairsReliability100, subFlocks22PairsReliability100, equal_var = False)

	print("For sensor reliability level of 1.0, Levene's test gives F = " + str(leveneReliability100F) + " at p = " + str(leveneReliability100p))
	print("variance for 0 pairs is " + str(variance0Pairs) + " and 22 pairs is " + str(variance22Pairs))
	print("0 pairs (M = " + str(np.mean(subFlocks0PairsReliability100)) + ", SE = " + str(st.sem(subFlocks0PairsReliability100)) + ") and 22 pairs (M = " + str(np.mean(subFlocks22PairsReliability100)) + ", SE = " + str(st.sem(subFlocks22PairsReliability100)))
	print("independent t-test assuming unequal variance t = " + str(tTest100t) + " at p = " + str(tTest100p))

	# prepare to plot the values
	fig = plt.figure()
	ax1 = fig.add_subplot(111)

	# compute averages
	subFlock0PairsMeans =  [np.mean(subFlocks0PairsReliability025), np.mean(subFlocks0PairsReliability05), np.mean(subFlocks0PairsReliability075), np.mean(subFlocks0PairsReliability100)]
	subFlock22PairsMeans = [np.mean(subFlocks22PairsReliability025), np.mean(subFlocks22PairsReliability05), np.mean(subFlocks22PairsReliability075), np.mean(subFlocks22PairsReliability100)]

	# compute confidence intervals
	lower0, upper0 = sms.DescrStatsW(subFlocks0PairsReliability025).tconfint_mean()
	lower1, upper1 = sms.DescrStatsW(subFlocks0PairsReliability05).tconfint_mean()
	lower2, upper2 = sms.DescrStatsW(subFlocks0PairsReliability075).tconfint_mean()
	lower3, upper3 = sms.DescrStatsW(subFlocks0PairsReliability100).tconfint_mean()
	confIntervals0Pairs = [[subFlock0PairsMeans[0] - lower0, subFlock0PairsMeans[1] - lower1, subFlock0PairsMeans[2] - lower2, subFlock0PairsMeans[3] - lower3],
	                       [upper0 - subFlock0PairsMeans[0], upper1 - subFlock0PairsMeans[1], upper2 - subFlock0PairsMeans[2], upper3 - subFlock0PairsMeans[3]]]

	lower0, upper0 = sms.DescrStatsW(subFlocks22PairsReliability025).tconfint_mean()
	lower1, upper1 = sms.DescrStatsW(subFlocks22PairsReliability05).tconfint_mean()
	lower2, upper2 = sms.DescrStatsW(subFlocks22PairsReliability075).tconfint_mean()
	lower3, upper3 = sms.DescrStatsW(subFlocks22PairsReliability100).tconfint_mean()
	confIntervals22Pairs = [[subFlock22PairsMeans[0] - lower0, subFlock22PairsMeans[1] - lower1, subFlock22PairsMeans[2] - lower2, subFlock22PairsMeans[3] - lower3],
	                       [upper0 - subFlock22PairsMeans[0], upper1 - subFlock22PairsMeans[1], upper2 - subFlock22PairsMeans[2], upper3 - subFlock22PairsMeans[3]]]

	# plot each net gain for each day on the same figure
	labels = ["25%", "50%", "75%", "100%"]
	x = np.arange(len(labels))  # the label locations
	barWidth = 0.35
	ax1.bar(x - barWidth / 2, subFlock0PairsMeans, barWidth, yerr = confIntervals0Pairs, label = "0 Pairs")#, color="#aaaaaa")
	ax1.bar(x + barWidth / 2, subFlock22PairsMeans, barWidth, yerr = confIntervals22Pairs, label = "22 Pairs")#, label="5 Pairs")#, color="#111111")

	# complete the graph labelling and show it
	#ax.legend(loc='upper right')
	ax1.set_title("Sub-Flocks By Sensor Reliability (View Range " + viewRange + " cm)")
	ax1.set_ylabel("Number of Sub-Flocks")
	ax1.set_xlabel("Sensor Reliability Percentage")
	ax1.set_xticks(x)
	ax1.set_xticklabels(labels)
	ax1.legend()
	plt.show(fig)

def closePairingStats(folder):

	print("====================================================================")
	print("=========================== CLOSE PAIRING ==========================")
	print("====================================================================")

	print("--------------------------------------------------------------------")
	subFlocks0PairsTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks0PairsFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_false_true_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability0F, leveneReliability0p = st.levene(subFlocks0PairsTrue, subFlocks0PairsFalse, center = 'median')
	variance0PairsTrue = np.var(subFlocks0PairsTrue)
	variance0PairsFalse = np.var(subFlocks0PairsFalse)
	tTest0t, tTest0p = st.ttest_ind(subFlocks0PairsTrue, subFlocks0PairsFalse, equal_var = False)

	print("For 0 pairs with and without close pairing, Levene's test gives F = " + str(leveneReliability0F) + " at p = " + str(leveneReliability0p))
	print("variance for close pairing is " + str(variance0PairsTrue) + " and no close pairing is " + str(variance0PairsFalse))
	print("close pairing (M = " + str(np.mean(subFlocks0PairsTrue)) + ", SE = " + str(st.sem(subFlocks0PairsTrue)) + ") and no close pairing (M = " + str(np.mean(subFlocks0PairsFalse)) + ", SE = " + str(st.sem(subFlocks0PairsFalse)))
	print("independent t-test assuming unequal variance t = " + str(tTest0t) + " at p = " + str(tTest0p))

	print("--------------------------------------------------------------------")
	subFlocks1PairsTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_1_0.0_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks1PairsFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_1_0.0_1.0_false_true_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability1F, leveneReliability1p = st.levene(subFlocks1PairsTrue, subFlocks1PairsFalse, center = 'median')
	variance1PairsTrue = np.var(subFlocks1PairsTrue)
	variance1PairsFalse = np.var(subFlocks1PairsFalse)
	tTest1t, tTest1p = st.ttest_ind(subFlocks1PairsTrue, subFlocks1PairsFalse, equal_var = False)

	print("For 1 pair with and without close pairing, Levene's test gives F = " + str(leveneReliability1F) + " at p = " + str(leveneReliability1p))
	print("variance for close pairing is " + str(variance1PairsTrue) + " and no close pairing is " + str(variance1PairsFalse))
	print("close pairing (M = " + str(np.mean(subFlocks1PairsTrue)) + ", SE = " + str(st.sem(subFlocks1PairsTrue)) + ") and no close pairing (M = " + str(np.mean(subFlocks1PairsFalse)) + ", SE = " + str(st.sem(subFlocks1PairsFalse)))
	print("independent t-test assuming unequal variance t = " + str(tTest1t) + " at p = " + str(tTest1p))

	print("--------------------------------------------------------------------")
	subFlocks2PairsTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_2_0.0_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks2PairsFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_2_0.0_1.0_false_true_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability2F, leveneReliability2p = st.levene(subFlocks2PairsTrue, subFlocks2PairsFalse, center = 'median')
	variance2PairsTrue = np.var(subFlocks2PairsTrue)
	variance2PairsFalse = np.var(subFlocks2PairsFalse)
	tTest2t, tTest2p = st.ttest_ind(subFlocks2PairsTrue, subFlocks2PairsFalse, equal_var = False)

	print("For 2 pairs with and without close pairing, Levene's test gives F = " + str(leveneReliability2F) + " at p = " + str(leveneReliability2p))
	print("variance for close pairing is " + str(variance2PairsTrue) + " and no close pairing is " + str(variance2PairsFalse))
	print("close pairing (M = " + str(np.mean(subFlocks2PairsTrue)) + ", SE = " + str(st.sem(subFlocks2PairsTrue)) + ") and no close pairing (M = " + str(np.mean(subFlocks2PairsFalse)) + ", SE = " + str(st.sem(subFlocks2PairsFalse)))
	print("independent t-test assuming unequal variance t = " + str(tTest2t) + " at p = " + str(tTest2p))

	print("--------------------------------------------------------------------")
	subFlocks3PairsTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_3_0.0_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks3PairsFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_3_0.0_1.0_false_true_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability3F, leveneReliability3p = st.levene(subFlocks3PairsTrue, subFlocks3PairsFalse, center = 'median')
	variance3PairsTrue = np.var(subFlocks3PairsTrue)
	variance3PairsFalse = np.var(subFlocks3PairsFalse)
	tTest3t, tTest3p = st.ttest_ind(subFlocks3PairsTrue, subFlocks3PairsFalse, equal_var = False)

	print("For 3 pairs with and without close pairing, Levene's test gives F = " + str(leveneReliability3F) + " at p = " + str(leveneReliability3p))
	print("variance for close pairing is " + str(variance3PairsTrue) + " and no close pairing is " + str(variance3PairsFalse))
	print("close pairing (M = " + str(np.mean(subFlocks3PairsTrue)) + ", SE = " + str(st.sem(subFlocks3PairsTrue)) + ") and no close pairing (M = " + str(np.mean(subFlocks3PairsFalse)) + ", SE = " + str(st.sem(subFlocks3PairsFalse)))
	print("independent t-test assuming unequal variance t = " + str(tTest3t) + " at p = " + str(tTest3p))

	print("--------------------------------------------------------------------")
	subFlocks4PairsTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_4_0.0_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks4PairsFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_4_0.0_1.0_false_true_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability4F, leveneReliability4p = st.levene(subFlocks4PairsTrue, subFlocks4PairsFalse, center = 'median')
	variance4PairsTrue = np.var(subFlocks4PairsTrue)
	variance4PairsFalse = np.var(subFlocks4PairsFalse)
	tTest4t, tTest4p = st.ttest_ind(subFlocks4PairsTrue, subFlocks4PairsFalse, equal_var = False)

	print("For 4 pairs with and without close pairing, Levene's test gives F = " + str(leveneReliability4F) + " at p = " + str(leveneReliability4p))
	print("variance for close pairing is " + str(variance4PairsTrue) + " and no close pairing is " + str(variance4PairsFalse))
	print("close pairing (M = " + str(np.mean(subFlocks4PairsTrue)) + ", SE = " + str(st.sem(subFlocks4PairsTrue)) + ") and no close pairing (M = " + str(np.mean(subFlocks4PairsFalse)) + ", SE = " + str(st.sem(subFlocks4PairsFalse)))
	print("independent t-test assuming unequal variance t = " + str(tTest4t) + " at p = " + str(tTest4p))

	print("--------------------------------------------------------------------")
	subFlocks5PairsTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks5PairsFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_false_true_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability5F, leveneReliability5p = st.levene(subFlocks5PairsTrue, subFlocks5PairsFalse, center = 'median')
	variance5PairsTrue = np.var(subFlocks5PairsTrue)
	variance5PairsFalse = np.var(subFlocks5PairsFalse)
	tTest5t, tTest5p = st.ttest_ind(subFlocks5PairsTrue, subFlocks5PairsFalse, equal_var = False)

	print("For 5 pairs with and without close pairing, Levene's test gives F = " + str(leveneReliability5F) + " at p = " + str(leveneReliability5p))
	print("variance for close pairing is " + str(variance5PairsTrue) + " and no close pairing is " + str(variance5PairsFalse))
	print("close pairing (M = " + str(np.mean(subFlocks5PairsTrue)) + ", SE = " + str(st.sem(subFlocks5PairsTrue)) + ") and no close pairing (M = " + str(np.mean(subFlocks5PairsFalse)) + ", SE = " + str(st.sem(subFlocks5PairsFalse)))
	print("independent t-test assuming unequal variance t = " + str(tTest5t) + " at p = " + str(tTest5p))

	# prepare to plot the values
	fig = plt.figure()
	ax1 = fig.add_subplot(111)

	# compute averages
	subFlockTruePairsMeans = [np.mean(subFlocks0PairsTrue), np.mean(subFlocks1PairsTrue), np.mean(subFlocks2PairsTrue), np.mean(subFlocks3PairsTrue), np.mean(subFlocks4PairsTrue), np.mean(subFlocks5PairsTrue)]
	subFlockFalsePairsMeans = [np.mean(subFlocks0PairsFalse), np.mean(subFlocks1PairsFalse), np.mean(subFlocks2PairsFalse), np.mean(subFlocks3PairsFalse), np.mean(subFlocks4PairsFalse), np.mean(subFlocks5PairsFalse)]

	# compute confidence intervals
	lower0, upper0 = sms.DescrStatsW(subFlocks0PairsTrue).tconfint_mean()
	lower1, upper1 = sms.DescrStatsW(subFlocks1PairsTrue).tconfint_mean()
	lower2, upper2 = sms.DescrStatsW(subFlocks2PairsTrue).tconfint_mean()
	lower3, upper3 = sms.DescrStatsW(subFlocks3PairsTrue).tconfint_mean()
	lower4, upper4 = sms.DescrStatsW(subFlocks4PairsTrue).tconfint_mean()
	lower5, upper5 = sms.DescrStatsW(subFlocks5PairsTrue).tconfint_mean()
	confIntervalsTruePairs = [[subFlockTruePairsMeans[0] - lower0, subFlockTruePairsMeans[1] - lower1, subFlockTruePairsMeans[2] - lower2, subFlockTruePairsMeans[3] - lower3, subFlockTruePairsMeans[4] - lower4, subFlockTruePairsMeans[5] - lower5],
	                          [upper0 - subFlockTruePairsMeans[0], upper1 - subFlockTruePairsMeans[1], upper2 - subFlockTruePairsMeans[2], upper3 - subFlockTruePairsMeans[3], upper4 - subFlockTruePairsMeans[4], upper5 - subFlockTruePairsMeans[5]]]

	lower0, upper0 = sms.DescrStatsW(subFlocks0PairsFalse).tconfint_mean()
	lower1, upper1 = sms.DescrStatsW(subFlocks1PairsFalse).tconfint_mean()
	lower2, upper2 = sms.DescrStatsW(subFlocks2PairsFalse).tconfint_mean()
	lower3, upper3 = sms.DescrStatsW(subFlocks3PairsFalse).tconfint_mean()
	lower4, upper4 = sms.DescrStatsW(subFlocks4PairsFalse).tconfint_mean()
	lower5, upper5 = sms.DescrStatsW(subFlocks5PairsFalse).tconfint_mean()
	confIntervalsFalsePairs = [[subFlockFalsePairsMeans[0] - lower0, subFlockFalsePairsMeans[1] - lower1, subFlockFalsePairsMeans[2] - lower2, subFlockFalsePairsMeans[3] - lower3, subFlockFalsePairsMeans[4] - lower4, subFlockFalsePairsMeans[5] - lower5],
	                          [upper0 - subFlockFalsePairsMeans[0], upper1 - subFlockFalsePairsMeans[1], upper2 - subFlockFalsePairsMeans[2], upper3 - subFlockFalsePairsMeans[3], upper4 - subFlockFalsePairsMeans[4], upper5 - subFlockFalsePairsMeans[5]]]

	# plot each net gain for each day on the same figure
	labels = ["0", "1", "2", "3", "4", "5"]
	x = np.arange(len(labels))  # the label locations
	barWidth = 0.35
	ax1.bar(x - barWidth / 2, subFlockTruePairsMeans, barWidth, yerr = confIntervalsTruePairs, label = "Close Pairing On", hatch = "xxxx", color="#aaaaee")#, color="#aaaaaa")
	ax1.bar(x + barWidth / 2, subFlockFalsePairsMeans, barWidth, yerr = confIntervalsFalsePairs, label = "Close Pairing Off", hatch = "////", color="#aaddaa")#, label="5 Pairs")#, color="#111111")

	# complete the graph labelling and show it
	#ax.legend(loc='upper right')
	ax1.set_title("Sub-Flocks With and Without Close Pairing")
	ax1.set_ylabel("Number of Sub-Flocks")
	ax1.set_xlabel("Number of Pairs")
	ax1.set_xticks(x)
	ax1.set_xticklabels(labels)
	ax1.legend()
	plt.show(fig)

def asymmetricTrackingStats(folder):

	print("====================================================================")
	print("======================== ASYMMETRIC TRACKING =======================")
	print("====================================================================")

	print("--------------------------------------------------------------------")
	subFlocks0PairsTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks0PairsFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_true_false_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability0F, leveneReliability0p = st.levene(subFlocks0PairsTrue, subFlocks0PairsFalse, center = 'median')
	variance0PairsTrue = np.var(subFlocks0PairsTrue)
	variance0PairsFalse = np.var(subFlocks0PairsFalse)
	tTest0t, tTest0p = st.ttest_ind(subFlocks0PairsTrue, subFlocks0PairsFalse, equal_var = False)

	print("For 0 pairs with and without asymmetric tracking, Levene's test gives F = " + str(leveneReliability0F) + " at p = " + str(leveneReliability0p))
	print("variance for asymmetric tracking is " + str(variance0PairsTrue) + " and no asymmetric tracking is " + str(variance0PairsFalse))
	print("asymmetric tracking (M = " + str(np.mean(subFlocks0PairsTrue)) + ", SE = " + str(st.sem(subFlocks0PairsTrue)) + ") and no asymmetric tracking (M = " + str(np.mean(subFlocks0PairsFalse)) + ", SE = " + str(st.sem(subFlocks0PairsFalse)))
	print("independent t-test assuming unequal variance t = " + str(tTest0t) + " at p = " + str(tTest0p))

	print("--------------------------------------------------------------------")
	subFlocks1PairsTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_1_0.0_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks1PairsFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_1_0.0_1.0_true_false_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability1F, leveneReliability1p = st.levene(subFlocks1PairsTrue, subFlocks1PairsFalse, center = 'median')
	variance1PairsTrue = np.var(subFlocks1PairsTrue)
	variance1PairsFalse = np.var(subFlocks1PairsFalse)
	tTest1t, tTest1p = st.ttest_ind(subFlocks1PairsTrue, subFlocks1PairsFalse, equal_var = False)

	print("For 1 pair with and without asymmetric tracking, Levene's test gives F = " + str(leveneReliability1F) + " at p = " + str(leveneReliability1p))
	print("variance for asymmetric tracking is " + str(variance1PairsTrue) + " and no asymmetric tracking is " + str(variance1PairsFalse))
	print("asymmetric tracking (M = " + str(np.mean(subFlocks1PairsTrue)) + ", SE = " + str(st.sem(subFlocks1PairsTrue)) + ") and no asymmetric tracking (M = " + str(np.mean(subFlocks1PairsFalse)) + ", SE = " + str(st.sem(subFlocks1PairsFalse)))
	print("independent t-test assuming unequal variance t = " + str(tTest1t) + " at p = " + str(tTest1p))

	print("--------------------------------------------------------------------")
	subFlocks2PairsTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_2_0.0_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks2PairsFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_2_0.0_1.0_true_false_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability2F, leveneReliability2p = st.levene(subFlocks2PairsTrue, subFlocks2PairsFalse, center = 'median')
	variance2PairsTrue = np.var(subFlocks2PairsTrue)
	variance2PairsFalse = np.var(subFlocks2PairsFalse)
	tTest2t, tTest2p = st.ttest_ind(subFlocks2PairsTrue, subFlocks2PairsFalse, equal_var = False)

	print("For 2 pairs with and without asymmetric tracking, Levene's test gives F = " + str(leveneReliability2F) + " at p = " + str(leveneReliability2p))
	print("variance for asymmetric tracking is " + str(variance2PairsTrue) + " and no asymmetric tracking is " + str(variance2PairsFalse))
	print("asymmetric tracking (M = " + str(np.mean(subFlocks2PairsTrue)) + ", SE = " + str(st.sem(subFlocks2PairsTrue)) + ") and no asymmetric tracking (M = " + str(np.mean(subFlocks2PairsFalse)) + ", SE = " + str(st.sem(subFlocks2PairsFalse)))
	print("independent t-test assuming unequal variance t = " + str(tTest2t) + " at p = " + str(tTest2p))

	print("--------------------------------------------------------------------")
	subFlocks3PairsTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_3_0.0_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks3PairsFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_3_0.0_1.0_true_false_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability3F, leveneReliability3p = st.levene(subFlocks3PairsTrue, subFlocks3PairsFalse, center = 'median')
	variance3PairsTrue = np.var(subFlocks3PairsTrue)
	variance3PairsFalse = np.var(subFlocks3PairsFalse)
	tTest3t, tTest3p = st.ttest_ind(subFlocks3PairsTrue, subFlocks3PairsFalse, equal_var = False)

	print("For 3 pairs with and without asymmetric tracking, Levene's test gives F = " + str(leveneReliability3F) + " at p = " + str(leveneReliability3p))
	print("variance for asymmetric tracking is " + str(variance3PairsTrue) + " and no asymmetric tracking is " + str(variance3PairsFalse))
	print("asymmetric tracking (M = " + str(np.mean(subFlocks3PairsTrue)) + ", SE = " + str(st.sem(subFlocks3PairsTrue)) + ") and no asymmetric tracking (M = " + str(np.mean(subFlocks3PairsFalse)) + ", SE = " + str(st.sem(subFlocks3PairsFalse)))
	print("independent t-test assuming unequal variance t = " + str(tTest3t) + " at p = " + str(tTest3p))

	print("--------------------------------------------------------------------")
	subFlocks4PairsTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_4_0.0_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks4PairsFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_4_0.0_1.0_true_false_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability4F, leveneReliability4p = st.levene(subFlocks4PairsTrue, subFlocks4PairsFalse, center = 'median')
	variance4PairsTrue = np.var(subFlocks4PairsTrue)
	variance4PairsFalse = np.var(subFlocks4PairsFalse)
	tTest4t, tTest4p = st.ttest_ind(subFlocks4PairsTrue, subFlocks4PairsFalse, equal_var = False)

	print("For 4 pairs with and without asymmetric tracking, Levene's test gives F = " + str(leveneReliability4F) + " at p = " + str(leveneReliability4p))
	print("variance for asymmetric tracking is " + str(variance4PairsTrue) + " and no asymmetric tracking is " + str(variance4PairsFalse))
	print("asymmetric tracking (M = " + str(np.mean(subFlocks4PairsTrue)) + ", SE = " + str(st.sem(subFlocks4PairsTrue)) + ") and no asymmetric tracking (M = " + str(np.mean(subFlocks4PairsFalse)) + ", SE = " + str(st.sem(subFlocks4PairsFalse)))
	print("independent t-test assuming unequal variance t = " + str(tTest4t) + " at p = " + str(tTest4p))

	print("--------------------------------------------------------------------")
	subFlocks5PairsTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks5PairsFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_true_false_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability5F, leveneReliability5p = st.levene(subFlocks5PairsTrue, subFlocks5PairsFalse, center = 'median')
	variance5PairsTrue = np.var(subFlocks5PairsTrue)
	variance5PairsFalse = np.var(subFlocks5PairsFalse)
	tTest5t, tTest5p = st.ttest_ind(subFlocks5PairsTrue, subFlocks5PairsFalse, equal_var = False)

	print("For 5 pairs with and without asymmetric tracking, Levene's test gives F = " + str(leveneReliability5F) + " at p = " + str(leveneReliability5p))
	print("variance for asymmetric tracking is " + str(variance5PairsTrue) + " and no asymmetric tracking is " + str(variance5PairsFalse))
	print("asymmetric tracking (M = " + str(np.mean(subFlocks5PairsTrue)) + ", SE = " + str(st.sem(subFlocks5PairsTrue)) + ") and no asymmetric tracking (M = " + str(np.mean(subFlocks5PairsFalse)) + ", SE = " + str(st.sem(subFlocks5PairsFalse)))
	print("independent t-test assuming unequal variance t = " + str(tTest5t) + " at p = " + str(tTest5p))

	# prepare to plot the values
	fig = plt.figure()
	ax1 = fig.add_subplot(111)

	# compute averages
	subFlockTruePairsMeans = [np.mean(subFlocks0PairsTrue), np.mean(subFlocks1PairsTrue), np.mean(subFlocks2PairsTrue), np.mean(subFlocks3PairsTrue), np.mean(subFlocks4PairsTrue), np.mean(subFlocks5PairsTrue)]
	subFlockFalsePairsMeans = [np.mean(subFlocks0PairsFalse), np.mean(subFlocks1PairsFalse), np.mean(subFlocks2PairsFalse), np.mean(subFlocks3PairsFalse), np.mean(subFlocks4PairsFalse), np.mean(subFlocks5PairsFalse)]

	# compute confidence intervals
	lower0, upper0 = sms.DescrStatsW(subFlocks0PairsTrue).tconfint_mean()
	lower1, upper1 = sms.DescrStatsW(subFlocks1PairsTrue).tconfint_mean()
	lower2, upper2 = sms.DescrStatsW(subFlocks2PairsTrue).tconfint_mean()
	lower3, upper3 = sms.DescrStatsW(subFlocks3PairsTrue).tconfint_mean()
	lower4, upper4 = sms.DescrStatsW(subFlocks4PairsTrue).tconfint_mean()
	lower5, upper5 = sms.DescrStatsW(subFlocks5PairsTrue).tconfint_mean()
	confIntervalsTruePairs = [[subFlockTruePairsMeans[0] - lower0, subFlockTruePairsMeans[1] - lower1, subFlockTruePairsMeans[2] - lower2, subFlockTruePairsMeans[3] - lower3, subFlockTruePairsMeans[4] - lower4, subFlockTruePairsMeans[5] - lower5],
	                          [upper0 - subFlockTruePairsMeans[0], upper1 - subFlockTruePairsMeans[1], upper2 - subFlockTruePairsMeans[2], upper3 - subFlockTruePairsMeans[3], upper4 - subFlockTruePairsMeans[4], upper5 - subFlockTruePairsMeans[5]]]

	lower0, upper0 = sms.DescrStatsW(subFlocks0PairsFalse).tconfint_mean()
	lower1, upper1 = sms.DescrStatsW(subFlocks1PairsFalse).tconfint_mean()
	lower2, upper2 = sms.DescrStatsW(subFlocks2PairsFalse).tconfint_mean()
	lower3, upper3 = sms.DescrStatsW(subFlocks3PairsFalse).tconfint_mean()
	lower4, upper4 = sms.DescrStatsW(subFlocks4PairsFalse).tconfint_mean()
	lower5, upper5 = sms.DescrStatsW(subFlocks5PairsFalse).tconfint_mean()
	confIntervalsFalsePairs = [[subFlockFalsePairsMeans[0] - lower0, subFlockFalsePairsMeans[1] - lower1, subFlockFalsePairsMeans[2] - lower2, subFlockFalsePairsMeans[3] - lower3, subFlockFalsePairsMeans[4] - lower4, subFlockFalsePairsMeans[5] - lower5],
	                          [upper0 - subFlockFalsePairsMeans[0], upper1 - subFlockFalsePairsMeans[1], upper2 - subFlockFalsePairsMeans[2], upper3 - subFlockFalsePairsMeans[3], upper4 - subFlockFalsePairsMeans[4], upper5 - subFlockFalsePairsMeans[5]]]

	# plot each net gain for each day on the same figure
	labels = ["0", "1", "2", "3", "4", "5"]
	x = np.arange(len(labels))  # the label locations
	barWidth = 0.35
	ax1.bar(x - barWidth / 2, subFlockTruePairsMeans, barWidth, yerr = confIntervalsTruePairs, label = "Asymmetric Tracking On", hatch = "xxxx", color="#aaaaee")#, color="#aaaaaa")
	ax1.bar(x + barWidth / 2, subFlockFalsePairsMeans, barWidth, yerr = confIntervalsFalsePairs, label = "Asymmetric Tracking Off", hatch = "////", color="#aaddaa")#, label="5 Pairs")#, color="#111111")

	# complete the graph labelling and show it
	#ax.legend(loc='upper right')
	ax1.set_title("Sub-Flocks With and Without Asymmetric Tracking")
	ax1.set_ylabel("Number of Sub-Flocks")
	ax1.set_xlabel("Number of Pairs")
	ax1.set_xticks(x)
	ax1.set_xticklabels(labels)
	ax1.legend()
	plt.show(fig)

def closePairingAndAsymmetricTrackingStats(folder):

	print("====================================================================")
	print("============== ASYMMETRIC TRACKING AND CLOSE PAIRING =============")
	print("====================================================================")

	print("--------------------------------------------------------------------")
	subFlocks0PairsTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks0PairsFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_false_false_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability0F, leveneReliability0p = st.levene(subFlocks0PairsTrue, subFlocks0PairsFalse, center = 'median')
	variance0PairsTrue = np.var(subFlocks0PairsTrue)
	variance0PairsFalse = np.var(subFlocks0PairsFalse)
	tTest0t, tTest0p = st.ttest_ind(subFlocks0PairsTrue, subFlocks0PairsFalse, equal_var = False)

	print("For 0 pairs with and without ATCP, Levene's test gives F = " + str(leveneReliability0F) + " at p = " + str(leveneReliability0p))
	print("variance for ATCP is " + str(variance0PairsTrue) + " and no ATCP is " + str(variance0PairsFalse))
	print("ATCP (M = " + str(np.mean(subFlocks0PairsTrue)) + ", SE = " + str(st.sem(subFlocks0PairsTrue)) + ") and no ATCP (M = " + str(np.mean(subFlocks0PairsFalse)) + ", SE = " + str(st.sem(subFlocks0PairsFalse)))
	print("independent t-test assuming unequal variance t = " + str(tTest0t) + " at p = " + str(tTest0p))

	print("--------------------------------------------------------------------")
	subFlocks1PairsTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_1_0.0_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks1PairsFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_1_0.0_1.0_false_false_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability1F, leveneReliability1p = st.levene(subFlocks1PairsTrue, subFlocks1PairsFalse, center = 'median')
	variance1PairsTrue = np.var(subFlocks1PairsTrue)
	variance1PairsFalse = np.var(subFlocks1PairsFalse)
	tTest1t, tTest1p = st.ttest_ind(subFlocks1PairsTrue, subFlocks1PairsFalse, equal_var = False)

	print("For 1 pair with and without ATCP, Levene's test gives F = " + str(leveneReliability1F) + " at p = " + str(leveneReliability1p))
	print("variance for ATCP is " + str(variance1PairsTrue) + " and no ATCP is " + str(variance1PairsFalse))
	print("ATCP (M = " + str(np.mean(subFlocks1PairsTrue)) + ", SE = " + str(st.sem(subFlocks1PairsTrue)) + ") and no ATCP (M = " + str(np.mean(subFlocks1PairsFalse)) + ", SE = " + str(st.sem(subFlocks1PairsFalse)))
	print("independent t-test assuming unequal variance t = " + str(tTest1t) + " at p = " + str(tTest1p))

	print("--------------------------------------------------------------------")
	subFlocks2PairsTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_2_0.0_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks2PairsFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_2_0.0_1.0_false_false_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability2F, leveneReliability2p = st.levene(subFlocks2PairsTrue, subFlocks2PairsFalse, center = 'median')
	variance2PairsTrue = np.var(subFlocks2PairsTrue)
	variance2PairsFalse = np.var(subFlocks2PairsFalse)
	tTest2t, tTest2p = st.ttest_ind(subFlocks2PairsTrue, subFlocks2PairsFalse, equal_var = False)

	print("For 2 pairs with and without ATCP, Levene's test gives F = " + str(leveneReliability2F) + " at p = " + str(leveneReliability2p))
	print("variance for ATCP is " + str(variance2PairsTrue) + " and no ATCP is " + str(variance2PairsFalse))
	print("ATCP (M = " + str(np.mean(subFlocks2PairsTrue)) + ", SE = " + str(st.sem(subFlocks2PairsTrue)) + ") and no ATCP (M = " + str(np.mean(subFlocks2PairsFalse)) + ", SE = " + str(st.sem(subFlocks2PairsFalse)))
	print("independent t-test assuming unequal variance t = " + str(tTest2t) + " at p = " + str(tTest2p))

	print("--------------------------------------------------------------------")
	subFlocks3PairsTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_3_0.0_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks3PairsFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_3_0.0_1.0_false_false_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability3F, leveneReliability3p = st.levene(subFlocks3PairsTrue, subFlocks3PairsFalse, center = 'median')
	variance3PairsTrue = np.var(subFlocks3PairsTrue)
	variance3PairsFalse = np.var(subFlocks3PairsFalse)
	tTest3t, tTest3p = st.ttest_ind(subFlocks3PairsTrue, subFlocks3PairsFalse, equal_var = False)

	print("For 3 pairs with and without ATCP, Levene's test gives F = " + str(leveneReliability3F) + " at p = " + str(leveneReliability3p))
	print("variance for ATCP is " + str(variance3PairsTrue) + " and no ATCP is " + str(variance3PairsFalse))
	print("ATCP (M = " + str(np.mean(subFlocks3PairsTrue)) + ", SE = " + str(st.sem(subFlocks3PairsTrue)) + ") and no ATCP (M = " + str(np.mean(subFlocks3PairsFalse)) + ", SE = " + str(st.sem(subFlocks3PairsFalse)))
	print("independent t-test assuming unequal variance t = " + str(tTest3t) + " at p = " + str(tTest3p))

	print("--------------------------------------------------------------------")
	subFlocks4PairsTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_4_0.0_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks4PairsFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_4_0.0_1.0_false_false_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability4F, leveneReliability4p = st.levene(subFlocks4PairsTrue, subFlocks4PairsFalse, center = 'median')
	variance4PairsTrue = np.var(subFlocks4PairsTrue)
	variance4PairsFalse = np.var(subFlocks4PairsFalse)
	tTest4t, tTest4p = st.ttest_ind(subFlocks4PairsTrue, subFlocks4PairsFalse, equal_var = False)

	print("For 4 pairs with and without ATCP, Levene's test gives F = " + str(leveneReliability4F) + " at p = " + str(leveneReliability4p))
	print("variance for ATCP is " + str(variance4PairsTrue) + " and no ATCP is " + str(variance4PairsFalse))
	print("ATCP (M = " + str(np.mean(subFlocks4PairsTrue)) + ", SE = " + str(st.sem(subFlocks4PairsTrue)) + ") and no ATCP (M = " + str(np.mean(subFlocks4PairsFalse)) + ", SE = " + str(st.sem(subFlocks4PairsFalse)))
	print("independent t-test assuming unequal variance t = " + str(tTest4t) + " at p = " + str(tTest4p))

	print("--------------------------------------------------------------------")
	subFlocks5PairsTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks5PairsFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_false_false_*.txt", STAT_NUM_SUB_FLOCKS)

	leveneReliability5F, leveneReliability5p = st.levene(subFlocks5PairsTrue, subFlocks5PairsFalse, center = 'median')
	variance5PairsTrue = np.var(subFlocks5PairsTrue)
	variance5PairsFalse = np.var(subFlocks5PairsFalse)
	tTest5t, tTest5p = st.ttest_ind(subFlocks5PairsTrue, subFlocks5PairsFalse, equal_var = False)

	print("For 5 pairs with and without ATCP, Levene's test gives F = " + str(leveneReliability5F) + " at p = " + str(leveneReliability5p))
	print("variance for ATCP is " + str(variance5PairsTrue) + " and no ATCP is " + str(variance5PairsFalse))
	print("ATCP (M = " + str(np.mean(subFlocks5PairsTrue)) + ", SE = " + str(st.sem(subFlocks5PairsTrue)) + ") and no ATCP (M = " + str(np.mean(subFlocks5PairsFalse)) + ", SE = " + str(st.sem(subFlocks5PairsFalse)))
	print("independent t-test assuming unequal variance t = " + str(tTest5t) + " at p = " + str(tTest5p))

	# prepare to plot the values
	fig = plt.figure()
	ax1 = fig.add_subplot(111)

	# compute averages
	subFlockTruePairsMeans = [np.mean(subFlocks0PairsTrue), np.mean(subFlocks1PairsTrue), np.mean(subFlocks2PairsTrue), np.mean(subFlocks3PairsTrue), np.mean(subFlocks4PairsTrue), np.mean(subFlocks5PairsTrue)]
	subFlockFalsePairsMeans = [np.mean(subFlocks0PairsFalse), np.mean(subFlocks1PairsFalse), np.mean(subFlocks2PairsFalse), np.mean(subFlocks3PairsFalse), np.mean(subFlocks4PairsFalse), np.mean(subFlocks5PairsFalse)]

	# compute confidence intervals
	lower0, upper0 = sms.DescrStatsW(subFlocks0PairsTrue).tconfint_mean()
	lower1, upper1 = sms.DescrStatsW(subFlocks1PairsTrue).tconfint_mean()
	lower2, upper2 = sms.DescrStatsW(subFlocks2PairsTrue).tconfint_mean()
	lower3, upper3 = sms.DescrStatsW(subFlocks3PairsTrue).tconfint_mean()
	lower4, upper4 = sms.DescrStatsW(subFlocks4PairsTrue).tconfint_mean()
	lower5, upper5 = sms.DescrStatsW(subFlocks5PairsTrue).tconfint_mean()
	confIntervalsTruePairs = [[subFlockTruePairsMeans[0] - lower0, subFlockTruePairsMeans[1] - lower1, subFlockTruePairsMeans[2] - lower2, subFlockTruePairsMeans[3] - lower3, subFlockTruePairsMeans[4] - lower4, subFlockTruePairsMeans[5] - lower5],
	                          [upper0 - subFlockTruePairsMeans[0], upper1 - subFlockTruePairsMeans[1], upper2 - subFlockTruePairsMeans[2], upper3 - subFlockTruePairsMeans[3], upper4 - subFlockTruePairsMeans[4], upper5 - subFlockTruePairsMeans[5]]]

	lower0, upper0 = sms.DescrStatsW(subFlocks0PairsFalse).tconfint_mean()
	lower1, upper1 = sms.DescrStatsW(subFlocks1PairsFalse).tconfint_mean()
	lower2, upper2 = sms.DescrStatsW(subFlocks2PairsFalse).tconfint_mean()
	lower3, upper3 = sms.DescrStatsW(subFlocks3PairsFalse).tconfint_mean()
	lower4, upper4 = sms.DescrStatsW(subFlocks4PairsFalse).tconfint_mean()
	lower5, upper5 = sms.DescrStatsW(subFlocks5PairsFalse).tconfint_mean()
	confIntervalsFalsePairs = [[subFlockFalsePairsMeans[0] - lower0, subFlockFalsePairsMeans[1] - lower1, subFlockFalsePairsMeans[2] - lower2, subFlockFalsePairsMeans[3] - lower3, subFlockFalsePairsMeans[4] - lower4, subFlockFalsePairsMeans[5] - lower5],
	                          [upper0 - subFlockFalsePairsMeans[0], upper1 - subFlockFalsePairsMeans[1], upper2 - subFlockFalsePairsMeans[2], upper3 - subFlockFalsePairsMeans[3], upper4 - subFlockFalsePairsMeans[4], upper5 - subFlockFalsePairsMeans[5]]]

	# plot each net gain for each day on the same figure
	labels = ["0", "1", "2", "3", "4", "5"]
	x = np.arange(len(labels))  # the label locations
	barWidth = 0.35
	ax1.bar(x - barWidth / 2, subFlockTruePairsMeans, barWidth, yerr = confIntervalsTruePairs, label = "AT + CP On", hatch = "xxxx", color="#aaaaee")#, color="#aaaaaa")
	ax1.bar(x + barWidth / 2, subFlockFalsePairsMeans, barWidth, yerr = confIntervalsFalsePairs, label = "AT + CP Off", hatch = "////", color="#aaddaa")#, label="5 Pairs")#, color="#111111")

	# complete the graph labelling and show it
	#ax.legend(loc='upper right')
	ax1.set_title("Sub-Flocks With and Without Asymmetric Tracking + Close Pairing")
	ax1.set_ylabel("Number of Sub-Flocks")
	ax1.set_xlabel("Number of Pairs")
	ax1.set_xticks(x)
	ax1.set_xticklabels(labels)
	ax1.legend()
	plt.show(fig)

def fourByFourStats(folder):

	print("====================================================================")
	print("======= FOUR BY FOUR ASYMMETRIC TRACKING AND CLOSE PAIRING =======")
	print("====================================================================")

	print("--------------------------------------------------------------------")
	subFlocks0PairsTrueTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_true_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks0PairsTrueTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_true_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	subFlocks1PairsTrueTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_true_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks1PairsTrueTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_true_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	subFlocks2PairsTrueTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_11_0.0_1.0_true_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks2PairsTrueTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_11_0.0_1.0_true_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	subFlocks3PairsTrueTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_17_0.0_1.0_true_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks3PairsTrueTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_17_0.0_1.0_true_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	subFlocks4PairsTrueTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_22_0.0_1.0_true_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks4PairsTrueTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_22_0.0_1.0_true_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	#subFlocks5PairsTrueTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_true_true_110_*.txt", STAT_NUM_SUB_FLOCKS)
	#subFlocks5PairsTrueTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_true_true_110_*.txt", STAT_NUM_SUB_FLOCKS)

	print("--------------------------------------------------------------------")
	subFlocks0PairsTrueFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_true_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks0PairsTrueFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_true_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	subFlocks1PairsTrueFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_true_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks1PairsTrueFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_true_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	subFlocks2PairsTrueFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_11_0.0_1.0_true_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks2PairsTrueFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_11_0.0_1.0_true_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	subFlocks3PairsTrueFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_17_0.0_1.0_true_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks3PairsTrueFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_17_0.0_1.0_true_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	subFlocks4PairsTrueFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_22_0.0_1.0_true_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks4PairsTrueFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_22_0.0_1.0_true_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	#subFlocks5PairsTrueFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_true_false_110_*.txt", STAT_NUM_SUB_FLOCKS)
	#subFlocks5PairsTrueFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_true_false_110_*.txt", STAT_NUM_SUB_FLOCKS)

	print("--------------------------------------------------------------------")
	subFlocks0PairsFalseTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_false_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks0PairsFalseTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_false_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	subFlocks1PairsFalseTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_false_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks1PairsFalseTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_false_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	subFlocks2PairsFalseTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_11_0.0_1.0_false_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks2PairsFalseTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_11_0.0_1.0_false_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	subFlocks3PairsFalseTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_17_0.0_1.0_false_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks3PairsFalseTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_17_0.0_1.0_false_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	subFlocks4PairsFalseTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_22_0.0_1.0_false_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks4PairsFalseTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_22_0.0_1.0_false_true_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	#subFlocks5PairsFalseTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_false_true_110_*.txt", STAT_NUM_SUB_FLOCKS)
	#subFlocks5PairsFalseTrue = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_false_true_110_*.txt", STAT_NUM_SUB_FLOCKS)

	print("--------------------------------------------------------------------")
	subFlocks0PairsFalseFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_false_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks0PairsFalseFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_0_0.0_1.0_false_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	subFlocks1PairsFalseFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_false_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks1PairsFalseFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_false_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	subFlocks2PairsFalseFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_11_0.0_1.0_false_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks2PairsFalseFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_11_0.0_1.0_false_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	subFlocks3PairsFalseFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_17_0.0_1.0_false_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks3PairsFalseFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_17_0.0_1.0_false_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	subFlocks4PairsFalseFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_22_0.0_1.0_false_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)
	subFlocks4PairsFalseFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_22_0.0_1.0_false_false_250_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	#subFlocks5PairsFalseFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_false_false_110_*.txt", STAT_NUM_SUB_FLOCKS)
	#subFlocks5PairsFalseFalse = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_5_0.0_1.0_false_false_110_*.txt", STAT_NUM_SUB_FLOCKS)

	# compute averages
	subFlockTrueTruePairsMeans = [np.mean(subFlocks0PairsTrueTrue), np.mean(subFlocks1PairsTrueTrue), np.mean(subFlocks2PairsTrueTrue), np.mean(subFlocks3PairsTrueTrue), np.mean(subFlocks4PairsTrueTrue)]#, np.mean(subFlocks5PairsTrueTrue)]
	subFlockTrueFalsePairsMeans = [np.mean(subFlocks0PairsTrueFalse), np.mean(subFlocks1PairsTrueFalse), np.mean(subFlocks2PairsTrueFalse), np.mean(subFlocks3PairsTrueFalse), np.mean(subFlocks4PairsTrueFalse)]#, np.mean(subFlocks5PairsTrueFalse)]
	subFlockFalseTruePairsMeans = [np.mean(subFlocks0PairsFalseTrue), np.mean(subFlocks1PairsFalseTrue), np.mean(subFlocks2PairsFalseTrue), np.mean(subFlocks3PairsFalseTrue), np.mean(subFlocks4PairsFalseTrue)]#, np.mean(subFlocks5PairsFalseTrue)]
	subFlockFalseFalsePairsMeans = [np.mean(subFlocks0PairsFalseFalse), np.mean(subFlocks1PairsFalseFalse), np.mean(subFlocks2PairsFalseFalse), np.mean(subFlocks3PairsFalseFalse), np.mean(subFlocks4PairsFalseFalse)]#, np.mean(subFlocks5PairsFalseFalse)]

	# compute confidence intervals
	#lower0, upper0 = sms.DescrStatsW(subFlocks0PairsTrue).tconfint_mean()
	#lower1, upper1 = sms.DescrStatsW(subFlocks1PairsTrue).tconfint_mean()
	#lower2, upper2 = sms.DescrStatsW(subFlocks2PairsTrue).tconfint_mean()
	#lower3, upper3 = sms.DescrStatsW(subFlocks3PairsTrue).tconfint_mean()
	#lower4, upper4 = sms.DescrStatsW(subFlocks4PairsTrue).tconfint_mean()
	#lower5, upper5 = sms.DescrStatsW(subFlocks5PairsTrue).tconfint_mean()
	#confIntervalsTruePairs = [[subFlockTruePairsMeans[0] - lower0, subFlockTruePairsMeans[1] - lower1, subFlockTruePairsMeans[2] - lower2, subFlockTruePairsMeans[3] - lower3, subFlockTruePairsMeans[4] - lower4, subFlockTruePairsMeans[5] - lower5],
	#                          [upper0 - subFlockTruePairsMeans[0], upper1 - subFlockTruePairsMeans[1], upper2 - subFlockTruePairsMeans[2], upper3 - subFlockTruePairsMeans[3], upper4 - subFlockTruePairsMeans[4], upper5 - subFlockTruePairsMeans[5]]]

	#lower0, upper0 = sms.DescrStatsW(subFlocks0PairsFalse).tconfint_mean()
	#lower1, upper1 = sms.DescrStatsW(subFlocks1PairsFalse).tconfint_mean()
	#lower2, upper2 = sms.DescrStatsW(subFlocks2PairsFalse).tconfint_mean()
	#lower3, upper3 = sms.DescrStatsW(subFlocks3PairsFalse).tconfint_mean()
	#lower4, upper4 = sms.DescrStatsW(subFlocks4PairsFalse).tconfint_mean()
	#lower5, upper5 = sms.DescrStatsW(subFlocks5PairsFalse).tconfint_mean()
	#confIntervalsFalsePairs = [[subFlockFalsePairsMeans[0] - lower0, subFlockFalsePairsMeans[1] - lower1, subFlockFalsePairsMeans[2] - lower2, subFlockFalsePairsMeans[3] - lower3, subFlockFalsePairsMeans[4] - lower4, subFlockFalsePairsMeans[5] - lower5],
	#                          [upper0 - subFlockFalsePairsMeans[0], upper1 - subFlockFalsePairsMeans[1], upper2 - subFlockFalsePairsMeans[2], upper3 - subFlockFalsePairsMeans[3], upper4 - subFlockFalsePairsMeans[4], upper5 - subFlockFalsePairsMeans[5]]]

	# prepare to plot the values
	fig = plt.figure()
	#plt.rcParams.update({'hatch.color': '#aaaaaa'})
	#plt.rcParams.update({'hatch.linewidth': '1'})
	plt.rcParams['hatch.linewidth'] = 1
	ax1 = fig.add_subplot(111)

	# plot each net gain for each day on the same figure
	#[0, 5, 11, 17, 22]
	labels = ["0", "5", "11", "17", "22"]#, "5"]
	x = np.arange(len(labels))  # the label locations
	barWidth = 0.2
	barHalfWidth = barWidth / 2
	ax1.bar(x + barWidth * 1 - (barWidth * 2), subFlockTrueTruePairsMeans, barWidth, label = "CP on, AT on", hatch = "xxxx", color="#aaaaee")#, color="#aaaaaa")
	ax1.bar(x + barWidth * 2 - (barWidth * 2), subFlockTrueFalsePairsMeans, barWidth, label = "CP on, AT off", hatch = "////", color="#aaccaa")#, label="5 Pairs")#, color="#111111")
	ax1.bar(x + barWidth * 3 - (barWidth * 2), subFlockFalseTruePairsMeans, barWidth, label = "CP off, AT on", hatch = "\\\\\\\\", color="#ccaaaa")#, label="5 Pairs")#, color="#111111")
	ax1.bar(x + barWidth * 4 - (barWidth * 2), subFlockFalseFalsePairsMeans, barWidth, label = "CP off, AT off", hatch = "--", color="#ccccaa")#, label="5 Pairs")#, color="#111111")

	# complete the graph labelling and show it
	#ax.legend(loc='upper right')
	ax1.set_title("Sub-Flocks With and Without Asymmetric Tracking + Close Pairing")
	ax1.set_ylabel("Number of Sub-Flocks")
	ax1.set_xlabel("Number of Pairs")
	ax1.set_xticks(x)
	ax1.set_xticklabels(labels)
	ax1.legend()
	plt.show(fig)

def fourByFourStatsHistogram(folder, viewRange, pairs, at, ct):

	print("====================================================================")
	print("======= FOUR BY FOUR ASYMMETRIC TRACKING AND CLOSE PAIRING =========")
	print("====================================================================")

	print("--------------------------------------------------------------------")

	subFlocks = getStatOverallByExperimentName("scripts/trials/" + folder + "/results/trial_output_" + str(pairs) + "_0.0_1.0_" + at + "_" + ct + "_" + viewRange + "_true_true_*.txt", STAT_NUM_SUB_FLOCKS)

	# compute averages
	#subFlockTrueTruePairsMeans = [np.mean(subFlocks0PairsTrueTrue), np.mean(subFlocks1PairsTrueTrue), np.mean(subFlocks2PairsTrueTrue), np.mean(subFlocks3PairsTrueTrue), np.mean(subFlocks4PairsTrueTrue)]#, np.mean(subFlocks5PairsTrueTrue)]
	#subFlockTrueFalsePairsMeans = [np.mean(subFlocks0PairsTrueFalse), np.mean(subFlocks1PairsTrueFalse), np.mean(subFlocks2PairsTrueFalse), np.mean(subFlocks3PairsTrueFalse), np.mean(subFlocks4PairsTrueFalse)]#, np.mean(subFlocks5PairsTrueFalse)]
	#subFlockFalseTruePairsMeans = [np.mean(subFlocks0PairsFalseTrue), np.mean(subFlocks1PairsFalseTrue), np.mean(subFlocks2PairsFalseTrue), np.mean(subFlocks3PairsFalseTrue), np.mean(subFlocks4PairsFalseTrue)]#, np.mean(subFlocks5PairsFalseTrue)]
	#subFlockFalseFalsePairsMeans = [np.mean(subFlocks0PairsFalseFalse), np.mean(subFlocks1PairsFalseFalse), np.mean(subFlocks2PairsFalseFalse), np.mean(subFlocks3PairsFalseFalse), np.mean(subFlocks4PairsFalseFalse)]#, np.mean(subFlocks5PairsFalseFalse)]

	# compute confidence intervals
	#lower0, upper0 = sms.DescrStatsW(subFlocks0PairsTrue).tconfint_mean()
	#lower1, upper1 = sms.DescrStatsW(subFlocks1PairsTrue).tconfint_mean()
	#lower2, upper2 = sms.DescrStatsW(subFlocks2PairsTrue).tconfint_mean()
	#lower3, upper3 = sms.DescrStatsW(subFlocks3PairsTrue).tconfint_mean()
	#lower4, upper4 = sms.DescrStatsW(subFlocks4PairsTrue).tconfint_mean()
	#lower5, upper5 = sms.DescrStatsW(subFlocks5PairsTrue).tconfint_mean()
	#confIntervalsTruePairs = [[subFlockTruePairsMeans[0] - lower0, subFlockTruePairsMeans[1] - lower1, subFlockTruePairsMeans[2] - lower2, subFlockTruePairsMeans[3] - lower3, subFlockTruePairsMeans[4] - lower4, subFlockTruePairsMeans[5] - lower5],
	#                          [upper0 - subFlockTruePairsMeans[0], upper1 - subFlockTruePairsMeans[1], upper2 - subFlockTruePairsMeans[2], upper3 - subFlockTruePairsMeans[3], upper4 - subFlockTruePairsMeans[4], upper5 - subFlockTruePairsMeans[5]]]

	#lower0, upper0 = sms.DescrStatsW(subFlocks0PairsFalse).tconfint_mean()
	#lower1, upper1 = sms.DescrStatsW(subFlocks1PairsFalse).tconfint_mean()
	#lower2, upper2 = sms.DescrStatsW(subFlocks2PairsFalse).tconfint_mean()
	#lower3, upper3 = sms.DescrStatsW(subFlocks3PairsFalse).tconfint_mean()
	#lower4, upper4 = sms.DescrStatsW(subFlocks4PairsFalse).tconfint_mean()
	#lower5, upper5 = sms.DescrStatsW(subFlocks5PairsFalse).tconfint_mean()
	#confIntervalsFalsePairs = [[subFlockFalsePairsMeans[0] - lower0, subFlockFalsePairsMeans[1] - lower1, subFlockFalsePairsMeans[2] - lower2, subFlockFalsePairsMeans[3] - lower3, subFlockFalsePairsMeans[4] - lower4, subFlockFalsePairsMeans[5] - lower5],
	#                          [upper0 - subFlockFalsePairsMeans[0], upper1 - subFlockFalsePairsMeans[1], upper2 - subFlockFalsePairsMeans[2], upper3 - subFlockFalsePairsMeans[3], upper4 - subFlockFalsePairsMeans[4], upper5 - subFlockFalsePairsMeans[5]]]

	# prepare to plot the values
	fig = plt.figure()
	#plt.rcParams.update({'hatch.color': '#aaaaaa'})
	#plt.rcParams.update({'hatch.linewidth': '1'})
	#plt.rcParams['hatch.linewidth'] = 1
	ax1 = fig.add_subplot(111)

	# plot each net gain for each day on the same figure
	#[0, 5, 11, 17, 22]
	#labels = ["0", "5", "11", "17", "22"]#, "5"]
	#x = np.arange(len(labels))  # the label locations
	#barWidth = 0.2
	#barHalfWidth = barWidth / 2
	#ax1.bar(x + barWidth * 1 - (barWidth * 2), subFlockTrueTruePairsMeans, barWidth, label = "CP on, AT on", hatch = "xxxx", color="#aaaaee")#, color="#aaaaaa")
	#ax1.bar(x + barWidth * 2 - (barWidth * 2), subFlockTrueFalsePairsMeans, barWidth, label = "CP on, AT off", hatch = "////", color="#aaccaa")#, label="5 Pairs")#, color="#111111")
	#ax1.bar(x + barWidth * 3 - (barWidth * 2), subFlockFalseTruePairsMeans, barWidth, label = "CP off, AT on", hatch = "\\\\\\\\", color="#ccaaaa")#, label="5 Pairs")#, color="#111111")
	#ax1.bar(x + barWidth * 4 - (barWidth * 2), subFlockFalseFalsePairsMeans, barWidth, label = "CP off, AT off", hatch = "--", color="#ccccaa")#, label="5 Pairs")#, color="#111111")

	ax1.set_ylim([0, 51])
	ax1.set_xlim([0.5, 7.5])
	ax1.tick_params(axis='both', which='major', labelsize=16)
	ax1.hist(subFlocks, bins=np.arange(8) - 0.5, rwidth=0.9)#bins=[1, 2, 3, 4, 5, 6, 7] - 0.5, rwidth=0.5)#, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22])
	#bins=np.arange(6)-0.8, rwidth=0.3)

	# complete the graph labelling and show it
	#ax.legend(loc='upper right')
	ax1.set_title("Sub-Flocks (Pairs=" + str(pairs) + ", AT=" + at + ", CT=" + ct + ")", fontsize=16)
	ax1.set_ylabel("Frequency", fontsize=16)
	ax1.set_xlabel("Number of Sub-Flocks", fontsize=16)
	#ax1.set_xticks(x)
	#ax1.set_xticklabels(labels)
	#ax1.legend()
	plt.show()

# we'll also do varying levels of pairedness doubled by close pairing (pairedness on x, stacked by close pairing)

# and by asymmetric tracking (pairedness on x, stacked by asymmetric tracking)

# and by asymmetric tracking *and* close tracking (pairedness on x, stacked by AT & CT)

# - - - other stats go here - - - #

#makeViewRangeGraph()

#computeAverageSubFlocksAtEnd("test_0paired")

#computeAverageSubFlocksAtEnd("test_100paired")

#varyingNumberOfPairsConnections("oct-23-a")
#varyingNumberOfPairsDegrees("oct-23-a")
#varyingNumberOfPairsDiameters("oct-23-a")

def plotUBeeFlightPath(filename, firstResultLine):

	# path data goes here; use parallel arrays for simplicity
	lineNum = 1
	posX = []
	posY = []
	posZ = []
	vel = []

	# open the file and iterate through each line
	file = open(filename, "r")
	for line in file:

		# ignore empty lines and lines before take-off
		if not line.isspace() and lineNum >= firstResultLine:

			# tokenize the line by commas
			line.replace(" ", "")           # in case we put spaces in the results!
			tokens = line.split(",")        # tokenize time index and value

			# build position data points
			t = tokens[0]
			posX.append(float(tokens[1]) + 0.3)
			posY.append(float(tokens[2]))
			posZ.append(float(tokens[3]))

			# build velocity data points
			velX = float(tokens[4])
			velY = float(tokens[5])
			velZ = float(tokens[6])

			# compute velocity but limit it to something sane in case there's a glitch in the data
			v = math.sqrt((velX * velX) + (velY * velY) + (velZ * velZ))
			vel.append(v)

			#if v > 5:
				#print("line " + str(lineNum) + " has vel " + str(v) + "=" + str(velX) + ", " + str(velY) + ", " + str(velZ) + " at time " + str(t))

		# next line
		lineNum = lineNum + 1

	# now plot that sh---I mean, stuff

	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')

	ax.set_xlim3d(-0.75, 0.75)
	ax.set_ylim3d(-0.75, 0.75)
	ax.set_zlim3d(0, 1.5)
	p = ax.scatter(posX, posY, posZ, c=vel, cmap='plasma', zdir='x', s=1)
	#ax.colorbar()

	fig.colorbar(p)

	#plt.colorbar()
	plt.show(fig)

# - - - uBee box flight test stats - - - #
# self-explanatory; this generates the 3D position/velocity plot for the provided box flight trial

#plotUBeeFlightPath("config/trials/results/box_output_sim_mar_16.txt", 169)

# - - - mar 12 thesis experiment stats - - - #
# these are for the baseline stats that generate results for the MRS-like experiments

#varyingNumberOfPairsDiameters44Agents("mar_3", "400")
#varyingNumberOfPairsStats("mar_3", "400")
varyingNumberOfPairsConnections44Agents("mar_3", "400")
#sensorReliabilityStats44Agents("mar_3", "400")

# AT, CT
#fourByFourStatsHistogram("mar_3", "400", 0, "true", "true")
#fourByFourStatsHistogram("mar_3", "400", 5, "true", "true")
#fourByFourStatsHistogram("mar_3", "400", 11, "true", "true")
#fourByFourStatsHistogram("mar_3", "400", 17, "true", "true")
#fourByFourStatsHistogram("mar_3", "400", 22, "true", "true")

# AT, !CT
#fourByFourStatsHistogram("mar_3", "400", 0, "true", "false")
#fourByFourStatsHistogram("mar_3", "400", 5, "true", "false")
#fourByFourStatsHistogram("mar_3", "400", 11, "true", "false")
#fourByFourStatsHistogram("mar_3", "400", 17, "true", "false")
#fourByFourStatsHistogram("mar_3", "400", 22, "true", "false")

# !AT, CT
#fourByFourStatsHistogram("mar_3", "400", 0, "false", "true")
#fourByFourStatsHistogram("mar_3", "400", 5, "false", "true")
#fourByFourStatsHistogram("mar_3", "400", 11, "false", "true")
#fourByFourStatsHistogram("mar_3", "400", 17, "false", "true")
#fourByFourStatsHistogram("mar_3", "400", 22, "false", "true")

# !AT, !CT
#fourByFourStatsHistogram("mar_3", "400", 0, "false", "false")
#fourByFourStatsHistogram("mar_3", "400", 5, "false", "false")
#fourByFourStatsHistogram("mar_3", "400", 11, "false", "false")
#fourByFourStatsHistogram("mar_3", "400", 17, "false", "false")
#fourByFourStatsHistogram("mar_3", "400", 22, "false", "false")

# - - - begin feb 2 presentation stats - - - #

#varyingNumberOfPairsDiameters44Agents("nov_9")
#varyingNumberOfPairsStats("nov_9")
#varyingNumberOfPairsConnections44Agents("nov_9")
#sensorReliabilityStats44Agents("nov_9")

#sensorNoiseStats()
#sensorReliabilityStats()
#closePairingStats()
#asymmetricTrackingStats()
#closePairingAndAsymmetricTrackingStats()

# it appears that the view range is key; a longer view range makes the pairwise flocking more effective

#fourByFourStats("nov_9")

# - - -end feb 2 presentation stats - - - #

#varyingNumberOfPairsDiameters("nov_9")

#fourByFourStatsHistogram("nov_9", 0, "true", "true")
#fourByFourStatsHistogram("nov_9", 5, "true", "true")
#fourByFourStatsHistogram("nov_9", 11, "true", "true")
#fourByFourStatsHistogram("nov_9", 17, "true", "true")
#fourByFourStatsHistogram("nov_9", 22, "true", "true")
