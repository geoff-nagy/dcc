# results_reader.y
# Geoff Nagy
# Used to grab all the results from the specified experiment base name

import glob         # for file pattern matching

# this function reads all trials of a given experiment base name
def readResults(baseFilename):

	# get a list of all files in the directory from which the script was invoked,
	# and only include text files that contain the experiment name
	filenames = glob.glob(baseFilename)#"./" + baseFilename + "*.txt");

	# make sure we have a non-empty list before we go any further
	numFiles = len(filenames)
	if numFiles < 1:
		print("-- no files matched the base experiment name \"" + baseFilename + "\"")
		exit()

	# report on files found
	#print("-- found " + str(numFiles) + " result files for experiment \"" + baseFilename + "\"")

	# create list of trials---this will be a list where each element is a
	# time stamp, and a time stamp consists of a series of stats (connected
	# components, etc.)
	data = []

	# iterate through files and accumulate statistics
	for filename in filenames:

		# this will contain values from one trial exactly
		trial = []

		# open the file and iterate through each line
		file = open(filename, "r")
		for line in file:

			# create first stat
			stat = []

			# ignore empty lines
			if not line.isspace():

				# tokenize the line by commas
				line.replace(" ", "")           # in case we put spaces in the results!
				tokens = line.split(",")        # tokenize time index and value
				#stat.append(tokens[1])          # append value
				#print("token: " + tokens[1])
				#print(str(len(tokens[0])))
				#print(int(tokens[1]))

				# build the stats data
				stat.append(int(tokens[0]))			# time index
				stat.append(int(tokens[1]))			# number of sub flocks
				stat.append(float(tokens[2]))		# average sub flock size
				stat.append(float(tokens[3]))		# average sub flock diameter
				stat.append(float(tokens[4]))		# average sub flock degree
				stat.append(float(tokens[5]))		# number of connections
				stat.append(float(tokens[6]))		# compressed diameter
				stat.append(float(tokens[7]))		# compressed degree
				stat.append(float(tokens[8]))		# compressed number of connections

	            # add the stat data to the trial
				trial.append(stat)#int(tokens[1]))
				#trial.append(float(tokens[3]))
				#trial.append(float(tokens[5]))

		# we're done reading this trial file; append the results to the total
		data.append(trial)
		trial = []

	return data
