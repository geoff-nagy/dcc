# generate_experiments.py
#------------------------------------------------------------------------------
# a script that generates my uBee simulation experiments for my PhD thesis
# Geoff Nagy
#------------------------------------------------------------------------------

import sys as sys                                           # for arg reading
import time													# for time elapsed tracking
import os													# for folder creation

# this function generates trial configuration files for my thesis experiments;
# each file represents a single trial/instance of my simulation; you can then
# pass these files as arguments to instances of the DCC simulator to run them;
# the actual running of these trial files is handled by run_experiments.py
def generateExperiments(directoryToSave, templateFile):

	# read the template file into one large string
	file = open(templateFile, "r")
	template = file.read()
	file.close()

	# ranges for trial variables---tweak these as required
	pairs = [0, 5, 11, 17, 22]
	sensorNoise = [0.0, 0.04, 0.08, 0.12, 0.16]
	sensorReliability = [0.2, 0.4, 0.6, 0.8, 1.0]
	closePairing = ["true", "false"]
	asymmetricTracking = ["true", "false"]
	viewRange = [50, 80, 110, 140, 170]
	numTrialsPerConfig = 100
	numTrialsTotal = len(pairs) * len(sensorNoise) * len(sensorReliability) * len(closePairing) * len(asymmetricTracking) * len(viewRange) * numTrialsPerConfig
	trialIndex = 0

	# tell the user what we're going to do
	print("-- template file is \"" + templateFile + "\"")
	print("-- going to write " + str(numTrialsTotal) + " trials to the directory \"" + directoryToSave + "\"...")
	if not os.path.exists(directoryToSave):
		print("-- the target directory \"" + directoryToSave + "\" will be created")
	else:
		# directory already exists
		print("-- the target directory \"" + directoryToSave + "\" already exists and does not need to be created")

		# warn the user if there are files there already
		filesPresent = len([name for name in os.listdir(directoryToSave) if os.path.isfile(os.path.join(directoryToSave, name))])
		if(filesPresent):
			print("-- [WARNING] there are " + str(filesPresent) + " files already present in this directory; proceeding may overwrite them")

	# confirm yes/no
	choice = '0'
	while choice != 'y' and choice != 'n':
		print("is this correct (y/n)?")
		choice = raw_input().lower()

	# quit if requested
	if choice == 'n':
		print("aborting")
		quit(0)

	# create the target folder if it doesn't exist
	if not os.path.exists(directoryToSave):
		os.makedirs(directoryToSave)

	# start progress timer
	startTime = time.time()

	# iterate over number of pairs we want
	for p in pairs:

		# iterate over sensor noise std devs
		for n in sensorNoise:

			# iterate over sensor reliability thresholds
			for r in sensorReliability:

				# iterate over close pairing
				for c in closePairing:

					# iterate over asymmetric tracking
					for a in asymmetricTracking:

						# iterate over view ranges
						for v in viewRange:

							# iterate over trials we want per config
							for t in range(0, numTrialsPerConfig):

								# need to figure out how many singles to create since we know the number of pairs we want
								numSingles = 44 - (p * 2)

								# copy the template and do variable replacement on the params we're iterating over
								trialConfig = template.replace("%num_pairs", str(p), 100)
								trialConfig = trialConfig.replace("%num_single", str(numSingles), 100)
								trialConfig = trialConfig.replace("%sensor_noise_std_dev", str(n), 100)
								trialConfig = trialConfig.replace("%sensor_reliability_threshold", str(r), 100)
								trialConfig = trialConfig.replace("%close_pairing", c, 100)
								trialConfig = trialConfig.replace("%asymmetric_tracking", a, 100)
								trialConfig = trialConfig.replace("%view_range", str(v), 100)
								trialConfig = trialConfig.replace("%trial_num", str(t), 100)

								# now save the file to the specified directory
								file = open(directoryToSave + "/" + "trial_" + str(trialIndex) + ".txt", "w")
								file.write(trialConfig)
								file.close()

								# next trial index
								trialIndex += 1

								# print progress
								if trialIndex % 10 == 0:
									sys.stdout.write("\rprogress: [" + str((trialIndex * 100) / numTrialsTotal) + "%]")
									sys.stdout.flush()

	# end of output; tell user how long it took
	elapsedTime = time.time() - startTime
	sys.stdout.write("\rprogress: [100%] -- ")
	print("completed in " + time.strftime("%Hh %Mm %Ss", time.gmtime(elapsedTime)))

# ask the user to supply arguments
if len(sys.argv) == 3:
	generateExperiments(sys.argv[1], sys.argv[2])
else:
	print("usage: " + sys.argv[0] + " <target_dir> <template_file>")
