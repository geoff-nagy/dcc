# run_experiments
# a simple script that runs my pairing proxy experiments
# Geoff Nagy

import sys as sys                                           # for arg reading
import subprocess                                           # for command-line process invocation
import time													# for time elapsed tracking

DIRECTORY_TO_SAVE = "trials/oct-7"

def generateExperiments():

	# read the template file into one large string
	file = open("template.gum", "r")
	template = file.read()
	file.close()

	# ranges for trial variables
	pairs = [0, 1, 2, 3, 4, 5]
	sensorNoise = [0.0, 0.02, 0.04, 0.06, 0.08]
	sensorReliability = [0.2, 0.4, 0.6, 0.8, 1.0]
	closePairing = ["true", "false"]
	asymmetricTracking = ["true", "false"]
	numTrialsPerConfig = 100
	numTrialsTotal = len(pairs) * len(sensorNoise) * len(sensorReliability) * len(closePairing) * len(asymmetricTracking) * numTrialsPerConfig
	trialIndex = 0

	# tell the user what we're going to do
	sys.stdout.write("going to write " + str(numTrialsTotal) + " trials to the directory \"" + DIRECTORY_TO_SAVE + "\"...\n")
	startTime = time.time()

	# iterate over number of pairs we want
	for p in pairs:

		# iterate over sensor noise std devs
		for n in sensorNoise:

			# iterate over sensor reliability thresholds
			for r in sensorReliability:

				# iterate over close pairing
				for c in closePairing:

					# iterate over asymmetric tracking
					for a in asymmetricTracking:

						# iterate over trials we want per config
						for t in range(0, numTrialsPerConfig):

							# need to figure out how many singles to create since we know the number of pairs we want
							numSingles = 10 - (p * 2)

							# copy the template and do variable replacement on the params we're iterating over
							trialConfig = template.replace("%num_pairs", str(p), 100)
							trialConfig = trialConfig.replace("%num_single", str(numSingles), 100)
							trialConfig = trialConfig.replace("%sensor_noise_std_dev", str(n), 100)
							trialConfig = trialConfig.replace("%sensor_reliability_threshold", str(r), 100)
							trialConfig = trialConfig.replace("%close_pairing", c, 100)
							trialConfig = trialConfig.replace("%asymmetric_tracking", a, 100)
							trialConfig = trialConfig.replace("%trial_num", str(t), 100)

							# now save the file to the specified directory
							file = open(DIRECTORY_TO_SAVE + "/" + "trial_" + str(trialIndex) + ".txt", "w")
							file.write(trialConfig)
							file.close()

							# next trial index
							trialIndex += 1

							# print progress
							if trialIndex % 10 == 0:
								sys.stdout.write("\rprogress: [" + str((trialIndex * 100) / numTrialsTotal) + "%]")
								sys.stdout.flush()

	# end of output; tell user how long it took
	elapsedTime = time.time() - startTime
	sys.stdout.write("\rprogress: [100%] -- ")
	sys.stdout.write("completed in " + time.strftime("%Hh %Mm %Ss", time.gmtime(elapsedTime)) + "\n")

generateExperiments()
