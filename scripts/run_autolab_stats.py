# run_autolab_stats.py
# Geoff Nagy
# runs stats for my uBee experiments in the Autolab

import glob													# for file pattern matching
import math
import matplotlib.pyplot as plt                             # for graphing functions
import matplotlib.cm as cm									# for colour-mapping plots
import numpy as np                                          # for statistical functions
import scipy.stats as st									# for statistical functions
import statsmodels.stats.api as sms
import pingouin as pg

from statsmodels.stats.multicomp import pairwise_tukeyhsd
from results_reader import readResults

# - - - globals - - - #

# these are the indices of each statistic in each per-second sample of a trial
STAT_TIME_INDEX = 0
STAT_NUM_SUB_FLOCKS = 1
STAT_AVG_SUB_FLOCK_SIZE = 2
STAT_AVG_SUB_FLOCK_DIAMETER = 3
STAT_AVG_SUB_FLOCK_DEGREE = 4
STAT_AVG_NUM_CONNECTIONS = 5					# should be equivalent to STAT_AVG_COMPRESSED_NUM_CONNECTIONS
STAT_AVG_COMPRESSED_SUB_FLOCK_DIAMETER = 6
STAT_AVG_COMPRESSED_SUB_FLOCK_DEGREE = 7
STAT_AVG_COMPRESSED_NUM_CONNECTIONS = 8			# should be equivalent to STAT_AVG_NUM_CONNECTIONS

# - - - functions - - - #

def printResultsTest():

	results = readResults("../config/trials/results/pfl_4_no_pairing_view_range_150/pfl_output*")
	for str in results:
		print(str)

def numSubFlocksHistogram():

	# read values
	dataNoPairing = readResults("../config/trials/results/pfl_4_no_pairing_view_range_150/pfl_output*")
	dataPairingNoCatchUp = readResults("../config/trials/results/pfl_4_pairing_view_range_150/pfl_output*")
	dataPairingWithCatchUp = readResults("../config/trials/results/pfl_4_pairing_view_range_150_catch_up/pfl_output*")

	subFlocksNoPairing = []
	for trial in dataNoPairing:
		subFlocksNoPairing.append(trial[-1][STAT_NUM_SUB_FLOCKS])

	subFlocksPairingNoCatchUp = []
	for trial in dataPairingNoCatchUp:
		subFlocksPairingNoCatchUp.append(trial[-1][STAT_NUM_SUB_FLOCKS])

	subFlocksPairingWithCatchUp = []
	for trial in dataPairingWithCatchUp:
		subFlocksPairingWithCatchUp.append(trial[-1][STAT_NUM_SUB_FLOCKS])

	# prepare to plot the values
	fig = plt.figure()
	#plt.rcParams.update({'hatch.color': '#aaaaaa'})
	#plt.rcParams.update({'hatch.linewidth': '1'})
	#plt.rcParams['hatch.linewidth'] = 1
	ax1 = fig.add_subplot(111)

	# plot each net gain for each day on the same figure
	#[0, 5, 11, 17, 22]
	#labels = ["0", "5", "11", "17", "22"]#, "5"]
	#x = np.arange(len(labels))  # the label locations
	#barWidth = 0.2
	#barHalfWidth = barWidth / 2
	#ax1.bar(x + barWidth * 1 - (barWidth * 2), subFlockTrueTruePairsMeans, barWidth, label = "CP on, AT on", hatch = "xxxx", color="#aaaaee")#, color="#aaaaaa")
	#ax1.bar(x + barWidth * 2 - (barWidth * 2), subFlockTrueFalsePairsMeans, barWidth, label = "CP on, AT off", hatch = "////", color="#aaccaa")#, label="5 Pairs")#, color="#111111")
	#ax1.bar(x + barWidth * 3 - (barWidth * 2), subFlockFalseTruePairsMeans, barWidth, label = "CP off, AT on", hatch = "\\\\\\\\", color="#ccaaaa")#, label="5 Pairs")#, color="#111111")
	#ax1.bar(x + barWidth * 4 - (barWidth * 2), subFlockFalseFalsePairsMeans, barWidth, label = "CP off, AT off", hatch = "--", color="#ccccaa")#, label="5 Pairs")#, color="#111111")

	ax1.set_ylim([0, 10])
	ax1.set_xlim([0.5, 4.5])
	ax1.set_xticks([1, 2, 3, 4])

	# uncomment for first paired/unpaired
	#ax1.hist(subFlocksNoPairing, bins=np.arange(6)-0.7, rwidth=0.4)
	#ax1.hist(subFlocksPairingNoCatchUp, bins=np.arange(6)-0.3, rwidth=0.4)

	# uncomment for all three
	ax1.hist(subFlocksNoPairing, bins=np.arange(6)-0.8, rwidth=0.3)
	ax1.hist(subFlocksPairingNoCatchUp, bins=np.arange(6)-0.5, rwidth=0.3)
	ax1.hist(subFlocksPairingWithCatchUp, bins=np.arange(6)-0.2, rwidth=0.3)

	# complete the graph labelling and show it
	ax1.legend(["No Pairs", "2 Pairs", "2 Pairs with Catch-up"], loc='upper right')
	ax1.set_title("Number of Sub-Flocks")
	ax1.set_ylabel("Frequency")
	ax1.set_xlabel("Number of Sub-Flocks")

	plt.show()
	#ax1.set_xticks(x)
	#ax1.set_xticklabels(labels)
	#ax1.legend()
	#plt.show(fig)

	# avg and std dev
	meanNoPairing = np.mean(subFlocksNoPairing)
	meanPairingNoCatchUp = np.mean(subFlocksPairingNoCatchUp)
	meanPairingWithCatchUp = np.mean(subFlocksPairingWithCatchUp)

	stdNoPairing = np.std(subFlocksNoPairing)
	stdPairingNoCatchUp = np.std(subFlocksPairingNoCatchUp)
	stdPairingWithCatchUp = np.std(subFlocksPairingWithCatchUp)

	print("mean, no pairing: " + str(meanNoPairing) + " (" + str(stdNoPairing) + ")")
	print("mean, pairing: " + str(meanPairingNoCatchUp) + " (" + str(stdPairingNoCatchUp) + ")")
	print("mean, pairing with catch-up: " + str(meanPairingWithCatchUp) + " (" + str(stdPairingWithCatchUp) + ")")

	# means:
	#mean, no pairing: 2.3333333333333335 (0.9428090415820634)
	#mean, pairing: 2.5833333333333335 (0.759202798262025)
	#mean, pairing with catch-up: 2.0833333333333335 (0.4930066485916347)

	# levene's test
	stat, p = st.levene(subFlocksNoPairing, subFlocksPairingNoCatchUp)
	print("levene's test gives stat = " + str(stat) + " and p = " + str(p))

	# we don't really need to worry about some small deviation from normality,
	# but let's do the test anyways:
	stat, p = st.shapiro(subFlocksNoPairing)
	print("shapiro-wilks test for no pairing gives stat = " + str(stat) + " and p = " + str(p))
	stat, p = st.shapiro(subFlocksPairingNoCatchUp)
	print("shapiro-wilks test for with pairing and catch up gives stat = " + str(stat) + " and p = " + str(p))

	# independent t-test assuming unequal variances
	stat, p = st.ttest_ind(subFlocksNoPairing, subFlocksPairingNoCatchUp, equal_var = True)
	print("independent t-test gives stat = " + str(stat) + " and p = " + str(p))

	# no pairing versus pairing:


	# no pairing versus pairing with catch-up:
	# levene's test gives stat = 9.135593220338984 and p = 0.00625902085834755
	# shapiro-wilks test for no pairing gives stat = 0.8767930865287781 and p = 0.07974652945995331
	# shapiro-wilks test for with pairing and catch up gives stat = 0.6986055374145508 and p = 0.0008169984794221818
	# independent t-test gives stat = 0.7793343077204956 and p = 0.4467537424573014

def numConnectionsStats():

	# compute average number of connections under each condition

	# read values
	dataNoPairing = readResults("../config/trials/results/pfl_4_no_pairing_view_range_150/pfl_output*")
	dataPairingNoCatchUp = readResults("../config/trials/results/pfl_4_pairing_view_range_150/pfl_output*")
	dataPairingWithCatchUp = readResults("../config/trials/results/pfl_4_pairing_view_range_150_catch_up/pfl_output*")

	numConnectionsNoPairing = []
	for trial in dataNoPairing:
		for entry in trial:
			numConnectionsNoPairing.append(entry[STAT_AVG_NUM_CONNECTIONS])

	numConnectionsPairingNoCatchUp = []
	for trial in dataPairingNoCatchUp:
		for entry in trial:
			numConnectionsPairingNoCatchUp.append(entry[STAT_AVG_NUM_CONNECTIONS])

	numConnectionsPairingWithCatchup = []
	for trial in dataPairingWithCatchUp:
		for entry in trial:
			numConnectionsPairingWithCatchup.append(entry[STAT_AVG_NUM_CONNECTIONS])

	meanNoPairing = np.mean(numConnectionsNoPairing)
	meanPairingNoCatchUp = np.mean(numConnectionsPairingNoCatchUp)
	meanPairingWithCatchUp = np.mean(numConnectionsPairingWithCatchup)

	stdNoPairing = np.std(numConnectionsNoPairing)
	stdPairingNoCatchUp = np.std(numConnectionsPairingNoCatchUp)
	stdPairingWithCatchUp = np.std(numConnectionsPairingWithCatchup)

	print("number of connections")
	print("---------------------")
	print("no pairing mean              : " + str(meanNoPairing) + " (" + str(stdNoPairing) + ")")
	print("pairing mean                 : " + str(meanPairingNoCatchUp) + " (" + str(stdPairingNoCatchUp) + ")")
	print("pairing mean (with catch-up) : " + str(meanPairingWithCatchUp) + " (" + str(stdPairingWithCatchUp) + ")")

	#number of connections
	#---------------------
	#no pairing mean              : 6.482142857142857 (3.3003845632450353)
	#pairing mean                 : 4.0181818181818185 (1.3626489931858683)
	#pairing mean (with catch-up) : 4.1002227171492205 (1.0083062356614116)

	# levene's test
	stat, p = st.levene(numConnectionsNoPairing, numConnectionsPairingNoCatchUp)
	print("levene's test gives stat = " + str(stat) + " and p = " + str(p))

	# we don't really need to worry about some small deviation from normality,
	# but let's do the test anyways:
	stat, p = st.shapiro(numConnectionsNoPairing)
	print("shapiro-wilks test for no pairing gives stat = " + str(stat) + " and p = " + str(p))
	stat, p = st.shapiro(numConnectionsPairingNoCatchUp)
	print("shapiro-wilks test for with pairing and catch up gives stat = " + str(stat) + " and p = " + str(p))

	# independent t-test assuming unequal variances
	stat, p = st.ttest_ind(numConnectionsNoPairing, numConnectionsPairingNoCatchUp, equal_var = False)
	print("independent t-test gives stat = " + str(stat) + " and p = " + str(p))

	# levene's test gives stat = 484.3052423676441 and p = 4.3558084804608636e-85
	# shapiro-wilks test for no pairing gives stat = 0.9217506647109985 and p = 2.0069353351501867e-13
	# shapiro-wilks test for with pairing and catch up gives stat = 0.7288520932197571 and p = 2.1725460441379992e-26
	# independent t-test gives stat = 13.722884733219054 and p = 4.187568893793666e-36


def lengthStats():

	# compute average number of connections under each condition

	# read values
	dataNoPairing = readResults("../config/trials/results/pfl_4_no_pairing_view_range_150/pfl_output*")
	dataPairingNoCatchUp = readResults("../config/trials/results/pfl_4_pairing_view_range_150/pfl_output*")
	dataPairingWithCatchUp = readResults("../config/trials/results/pfl_4_pairing_view_range_150_catch_up/pfl_output*")

	lengthNoPairing = []
	for trial in dataNoPairing:
			lengthNoPairing.append(len(trial))

	lengthPairingNoCatchUp = []
	for trial in dataPairingNoCatchUp:
			lengthPairingNoCatchUp.append(len(trial))

	lengthPairingWithCatchUp = []
	for trial in dataPairingWithCatchUp:
			lengthPairingWithCatchUp.append(len(trial))

	meanNoPairing = np.mean(lengthNoPairing)
	meanPairingNoCatchUp = np.mean(lengthPairingNoCatchUp)
	meanPairingWithCatchUp = np.mean(lengthPairingWithCatchUp)

	stdNoPairing = np.std(lengthNoPairing)
	stdPairingNoCatchUp = np.std(lengthPairingNoCatchUp)
	stdPairingWithCatchUp = np.std(lengthPairingWithCatchUp)

	print("trial lengths")
	print("---------------------")
	print("no pairing mean              : " + str(meanNoPairing) + " (" + str(stdNoPairing) + ")")
	print("pairing mean                 : " + str(meanPairingNoCatchUp) + " (" + str(stdPairingNoCatchUp) + ")")
	print("pairing mean (with catch-up) : " + str(meanPairingWithCatchUp) + " (" + str(stdPairingWithCatchUp) + ")")

	#trial lengths
	#---------------------
	#no pairing mean              : 32.666666666666664 (2.494438257849294)
	#pairing mean                 : 32.083333333333336 (3.4268141991586814)
	#pairing mean (with catch-up) : 37.416666666666664 (5.8659800734593555)

	# levene's test
	stat, p = st.levene(lengthNoPairing, lengthPairingWithCatchUp)
	print("levene's test gives stat = " + str(stat) + " and p = " + str(p))

	# we don't really need to worry about some small deviation from normality,
	# but let's do the test anyways:
	stat, p = st.shapiro(lengthNoPairing)
	print("shapiro-wilks test for no pairing gives stat = " + str(stat) + " and p = " + str(p))
	stat, p = st.shapiro(lengthPairingWithCatchUp)
	print("shapiro-wilks test for with pairing and catch up gives stat = " + str(stat) + " and p = " + str(p))

	# independent t-test assuming unequal variances
	stat, p = st.ttest_ind(lengthNoPairing, lengthPairingWithCatchUp, equal_var = False)
	print("independent t-test gives stat = " + str(stat) + " and p = " + str(p))

	# levene's test gives stat = 4.815945330296128 and p = 0.03904760343480669
	# shapiro-wilks test for no pairing gives stat = 0.929723858833313 and p = 0.37719398736953735
	# shapiro-wilks test for with pairing and catch up gives stat = 0.9293585419654846 and p = 0.37336283922195435
	# independent t-test gives stat = -2.471474587116624 and p = 0.02605969529435318

#numSubFlocksHistogram()
numConnectionsStats()
#lengthStats()
