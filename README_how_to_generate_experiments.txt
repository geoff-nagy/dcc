How to Generate DCC Experiments/Simulations
-------------------------------------------

First, change into the scripts directory

	cd dcc/scripts

Now, invoke the Python script that generates the experiments:

	python3 generate_experiments_thesis_mar_3.py trials/mar_3 template_thesis_mar_3.gum

Remember to include the "trials" sub-directory in the destination, otherwise the trial files will be dumped in your current directory, which is probably where you don't want them to be!

You should get a confirmation prompt. If everything looks good, enter 'y' to proceed.

Trials to run will appear in the following directory:

	trials/mar_3

End of documentation
--------------------
