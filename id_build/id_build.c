#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <float.h>

// - - - defines - - - //

#define NUM_IDS 10

#define NUM_MARKER_POSITIONS 36

// - - - types - - - //

typedef struct VEC3
{
	float x;
	float y;
	float z;
} vec3;

// - - - prototypes - - - //

float computeMinDistance(uint8_t *mask);
vec3 getMarkerPos(uint32_t pos);
void printMask(uint8_t *mask);
vec3 vec3Make(float x, float y, float z);
float vec3Dist(vec3 a, vec3 b);

// - - - globals - - - //

vec3 focalMarker1;
vec3 focalMarker2;
vec3 focalCenter;
float focalSpread;

// - - - main functions - - - //

int main(int args, char *argv[])
{
	const uint64_t NUM_COMBOS = pow(2, NUM_MARKER_POSITIONS) - 1;

	//const float MAX_SIMILARITY_TO_ID_DIST = 5.0f;

	uint64_t i;
	int32_t j;
	uint32_t numSet;
	uint8_t *mask;
	float dist;
	float maxDist = 0.0f;

	// set focal marker positions
	focalMarker1 = vec3Make(-25.0f, 0.0f, -17.5f);
	focalMarker2 = vec3Make( 13.0f, 0.0f, -17.5f);
	focalCenter = vec3Make((focalMarker1.x + focalMarker2.x) / 2.0f,
						   (focalMarker1.y + focalMarker2.y) / 2.0f,
						   (focalMarker1.z + focalMarker2.z) / 2.0f);
	focalSpread = vec3Dist(focalMarker1, focalMarker2);

	printf("focal marker 1: %.2f, %.2f, %.2f\n", focalMarker1.x, focalMarker1.y, focalMarker1.z);
	printf("focal marker 2: %.2f, %.2f, %.2f\n", focalMarker2.x, focalMarker2.y, focalMarker2.z);
	printf("focal spread is %f\n", focalSpread);

	// prime the marker position array
	mask = (uint8_t*)malloc(NUM_MARKER_POSITIONS);
	memset(mask, 0, NUM_MARKER_POSITIONS);

	// iterate through all possible values with NUM_MARKER_POSITIONS bits
	for(i = 0; i < NUM_COMBOS; ++ i)
	{
		// show percentage
		if(i % 300000 == 0)
		{
			printf("progress: %.4f%%   \r", (float)(i * 100.0f) / (float)NUM_COMBOS);
		}

		// count the number of set bits; if we have NUM_IDS, then this is a
		// potentially valid set of IDs
		numSet = 0;
		for(j = 0; j < NUM_MARKER_POSITIONS; ++ j)
		{
			if(mask[j]) ++ numSet;
		}

		// is this a valid number of markers set?
		if(numSet == NUM_IDS)
		{
			// make sure that only one marker per spoke is filled in
			/*
			if(mask[0] + mask[1] + mask[2] <= 1 &&
			   mask[3] + mask[4] + mask[5] <= 1 &&
			   mask[6] + mask[7] + mask[8] <= 1 &&
			   mask[9] + mask[10] + mask[11] <= 1 &&
			   mask[12] + mask[13] + mask[14] <= 1 &&
			   mask[15] + mask[16] + mask[17] <= 1 &&
			   mask[18] + mask[19] + mask[20] <= 1 &&
			   mask[21] + mask[22] + mask[23] <= 1 &&
			   mask[24] + mask[25] + mask[26] <= 1 &&
			   mask[27] + mask[28] + mask[29] <= 1 &&
			   mask[30] + mask[31] + mask[32] <= 1 &&
			   mask[33] + mask[34] + mask[35])*/

			{
				// compute minimum distance between ID pairs in this set from the
				// focal center; we want the maximum over all sets of IDs so that
				// they're easily distinguished by our IDing algorithm
				dist = computeMinDistance(mask);
				if(dist > maxDist)
				{
					maxDist = dist;
					printf("------------------------------------------------------------------------\n");
					printf("found a new best with max distance %.2f\n\n", maxDist);
					printMask(mask);
				}
			}
		}

		// simple counter to advance to next set of IDs
		j = 0;
		mask[j] ++;
		while(mask[j] >= 2 && j < NUM_MARKER_POSITIONS)
		{
			mask[j] = 0;
			mask[++j] ++;
		}
	}

	// clean up and exit
	free(mask);
	return 0;
}

float computeMinDistance(uint8_t *mask)
{
	const float MIN_DIFF_FROM_FOCAL_SPREAD = 2.0f;
	const float MIN_DIFF_FROM_FOCAL_POINT = 35.0f;

	uint32_t i, j;
	vec3 pos1, pos2;
	float dist1, dist2;
	float pairDist;
	float minDist = FLT_MAX;

	for(i = 0; i < NUM_MARKER_POSITIONS; ++ i)
	{
		// is there a marker at this position?
		if(mask[i])
		{
			// make sure our distance to the focal center is not too close to the
			// distance between the focal markers, and that all of our markers are
			// at least a certain distance apart
			pos1 = getMarkerPos(i);
			dist1 = vec3Dist(pos1, focalCenter);
			if(fabs(dist1 - focalSpread) >= MIN_DIFF_FROM_FOCAL_SPREAD &&		// distance must be far enough away from focal spread
			   vec3Dist(pos1, focalMarker1) >= MIN_DIFF_FROM_FOCAL_POINT &&		// marker must be far enough away from focal point 1
			   vec3Dist(pos1, focalMarker2) >= MIN_DIFF_FROM_FOCAL_POINT)		// marker must be far enough away from focal point 2
			{
				// now check inter-ID distances
				for(j = i + 1; j < NUM_MARKER_POSITIONS; ++ j)
				{
					// is there a marker at the second position?
					if(mask[j])
					{
						// get the distance to the focal center for the current pair of IDs
						pos2 = getMarkerPos(j);
						dist2 = vec3Dist(pos2, focalCenter);

						// track the lowest difference in distance between all pairs of
						// ID markers from the focal center
						pairDist = fabs(dist1 - dist2);
						if(pairDist < minDist)
						{
							minDist = pairDist;
						}
					}
				}
			}
			else
			{
				return 0.0f;
			}
		}
	}

	return minDist;
}

vec3 getMarkerPos(uint32_t pos)
{
	// these marker positions are listed in clockwise order
	const vec3 MARKERS[] = {vec3Make(-7.75f,  0.0f,  22.0f),		// first row
							vec3Make(-7.75f,  7.0f,  22.0f),
							vec3Make(-7.75f, 15.0f,  22.0f),
							vec3Make( 0.00f,  0.0f,  22.0f),
							vec3Make( 0.00f,  7.0f,  22.0f),
							vec3Make( 0.00f, 15.0f,  22.0f),
							vec3Make( 7.75f,  0.0f,  22.0f),
							vec3Make( 7.75f,  7.0f,  22.0f),
							vec3Make( 7.75f, 15.0f,  22.0f),
							vec3Make(-7.75f,  0.0f,  26.0f),		// second row
							vec3Make(-7.75f,  7.0f,  26.0f),
							vec3Make(-7.75f, 15.0f,  26.0f),
							vec3Make( 0.00f,  0.0f,  26.0f),
							vec3Make( 0.00f,  7.0f,  26.0f),
							vec3Make( 0.00f, 15.0f,  26.0f),
							vec3Make( 7.75f,  0.0f,  26.0f),
							vec3Make( 7.75f,  7.0f,  26.0f),
							vec3Make( 7.75f, 15.0f,  26.0f),
							vec3Make(-7.75f,  0.0f,  32.0f),		// third row
							vec3Make(-7.75f,  7.0f,  32.0f),
							vec3Make(-7.75f, 15.0f,  32.0f),
							vec3Make( 0.00f,  0.0f,  32.0f),
							vec3Make( 0.00f,  7.0f,  32.0f),
							vec3Make( 0.00f, 15.0f,  32.0f),
							vec3Make( 7.75f,  0.0f,  32.0f),
							vec3Make( 7.75f,  7.0f,  32.0f),
							vec3Make( 7.75f, 15.0f,  32.0f),
							vec3Make(-7.75f,  0.0f,  37.0f),		// fourth row
							vec3Make(-7.75f,  7.0f,  37.0f),
							vec3Make(-7.75f, 15.0f,  37.0f),
							vec3Make( 0.00f,  0.0f,  37.0f),
							vec3Make( 0.00f,  7.0f,  37.0f),
							vec3Make( 0.00f, 15.0f,  37.0f),
							vec3Make( 7.75f,  0.0f,  37.0f),
							vec3Make( 7.75f,  7.0f,  37.0f),
							vec3Make( 7.75f, 15.0f,  37.0f)};

	/*
	const vec2 MARKERS[] = {vec2Make(-15.0f, 30.0f),	// top
		 					vec2Make(-15.0f, 35.0f),
							vec2Make(-15.0f, 40.0f),
							vec2Make(-5.0f,  30.0f),
							vec2Make(-5.0f,  35.0f),
							vec2Make(-5.0f,  40.0f),
							vec2Make(5.0f,   30.0f),
							vec2Make(5.0f,   35.0f),
							vec2Make(5.0f,   40.0f),
							vec2Make(15.0f,  30.0f),
							vec2Make(15.0f,  35.0f),
							vec2Make(15.0f,  40.0f),
							vec2Make(30.0f,  15.0f),	// right
							vec2Make(35.0f,  15.0f),
							vec2Make(40.0f,  15.0f),
							vec2Make(30.0f,  5.0f),
							vec2Make(35.0f,  5.0f),
							vec2Make(40.0f,  5.0f),
							vec2Make(30.0f,  -5.0f),
							vec2Make(35.0f,  -5.0f),
							vec2Make(40.0f,  -5.0f),
							vec2Make(30.0f, -15.0f),
							vec2Make(35.0f, -15.0f),
							vec2Make(40.0f, -15.0f),
							vec2Make(15.0f, -30.0f),	// bottom
							vec2Make(15.0f, -35.0f),
							vec2Make(15.0f, -40.0f),
							vec2Make(5.0f, -30.0f),
							vec2Make(5.0f, -35.0f),
							vec2Make(5.0f, -40.0f),
							vec2Make(-5.0f, -30.0f),
							vec2Make(-5.0f, -35.0f),
							vec2Make(-5.0f, -40.0f),
							vec2Make(-15.0f, -30.0f),
							vec2Make(-15.0f, -35.0f),
							vec2Make(-15.0f, -40.0f)};
	*/

	return MARKERS[pos];
}

void printMask(uint8_t *mask)
{
	vec3 pos;
	uint32_t i;

	// print raw mask
	printf("raw mask:\n");
	for(i = 0; i < NUM_MARKER_POSITIONS; ++ i)
	{
		printf("%c ", mask[i] ? '1': '0');
	}
	printf("\n\n");

	// print header
	printf("index    position (mm)              focal dist (mm)\n");
	printf("---------------------------------------------\n");

	// now print a table of the marker indices, positions, and distances from focal center
	for(i = 0; i < NUM_MARKER_POSITIONS; ++ i)
	{
		if(mask[i])
		{
			pos = getMarkerPos(i);
			printf("%2u      %6.2f, %6.2f, %6.2f     %6.2f\n", i, pos.x, pos.y, pos.z, vec3Dist(pos, focalCenter));
		}
	}
}

vec3 vec3Make(float x, float y, float z)
{
	vec3 result;
	result.x = x;
	result.y = y;
	result.z = z;
	return result;
}

float vec3Dist(vec3 a, vec3 b)
{
	vec3 diff;
	diff.x = a.x - b.x;
	diff.y = a.y - b.y;
	diff.z = a.z - b.z;
	return sqrt((diff.x * diff.x) + (diff.y * diff.y) + (diff.z * diff.z));
}
