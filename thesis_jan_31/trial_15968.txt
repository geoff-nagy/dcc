trial
{
	# this determines what kind of TrialController to create
	type = "pairwise_simulated"

	# name of output file
	output = "trial_output_5_0.0_1.0_false_false_300_68"

	# remaining params are specific to the PairwiseFlockingTrialController

	# paired agents track at most this number of other agents + their partner
	num_tracked_if_paired = 3

	# unpaired agents track at most this number of other agents
	num_tracked_if_single = 7

	# view range beyond which agents do not track neighbours
	view_range_cm = 300

	# how long the agents will fly for, for each trial, in seconds
	time_sec = 60

	# controls placement of simulated drones for every trial
	drones
	{
		# need enough cells to accommodate all drones
		origin = {0.0, 0.0}
		cells = {8, 8}
		cell_size = 0.2

		# num of drones is num_single + (num_pairs * 2)
		num_pairs = 5
		num_single = 34

		# drone neighbour sensing error standard deviation, in meters
		sensor_noise_std_dev = 0.0

		# drone neighbour sensing reliability, as a percentage threshold, above which we sense our neighbours
		sensor_reliability_threshold = 1.0

		# close pairing affects whether or not we snuggle up to our partners
		close_pairing = false

		# asymmetric tracking affects whether or not paired agents track other interaction partners on opposite sides of their neighbours
		asymmetric_tracking = false

		# obstacle occlusion, iff enabled, causes obstacles to break agents' lines of sight
		obstacle_occlusion = true

		# partner separation, iff enabled, allows mated pairs to separate
		partner_separation = true
	}

	# set to false if you want to disable obstacles
	obstacles_enabled = true
	obstacle_field
	{
		# creates a uniform field of obstacles centered around a given point
		origin = {0.0, 0.0}
		cells = {10, 10}
		cell_size = 1.0

		# radius of obstacle representing area we can't see through
		hard_radius = 0.10

		# radius of obstacle representing area we try to avoid
		soft_radius = 0.3
	}

	# these denote the edges that the drones must not go past
	bounds
	{
		# boundary dimensions in meters
		size = {200.0, 200.0}

		# distance from edges at which drones experience repulsion away from walls
		edge = 1.0
	}
}
