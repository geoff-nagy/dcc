# Drone Command Center (DCC) Tool
Geoff Nagy
Email: gnagy@sfu.ca
LinkedIn: https://www.linkedin.com/in/geoffnagy/
Twirrter: https://twitter.com/geoff_nagy

## Overview

This is a software tool for performing drone experiments. It supports simulated or real drones, simulated or real sensing systems, and is designed to be extendable. It was originally designed to run my drone experiments for my PhD thesis.

You might notice that this tool, at the software engineering level, has a lot of different layers of abstraction. Although this might make it a little more difficult to build a mental model of what's happening organizationally, this was deemed acceptable since the overall goal was to retain a level of flexibility and extendability beyond just one type of drone, experiment, or sensing model. In a very broad sense, this tool has 3 different sub-systems:

Sensing model
	This was built in such a way that virtually any kind of global sensors (e.g., Vicon) can be integrated into the DCC.

Flight command model

Drone model
